
PaddleC
=======

_PaddleC_ is a Ruby C extension attempting to provide objects and methods to rapidly set up real-time signal processing test benches in Ruby (because Ruby is so cool).

The Ruby code would look like _buffer administration_, 
leaving the heavy processing to _C_ subroutines while keepking the user code concise and elegant, 
without the need to recompile the whole thing each time the slightest change is made to the application.

`FloatBuffer` and `ComplexBuffer` wraps native arrays of single floats. 
Computations are performed in _C_ on single floats, and depending on the host architecture it may be accelerated with AVX, SSE, FMA or NEON SIMD instructions.


How to install
--------------

First, install dependencies:
```bash
sudo apt update
sudo apt install libpulse-dev
sudo apt install libfftw3-dev libfftw3-single3
```

Clone this repository, build the gem and install it:
```bash
git clone https://gitlab.com/theotime_bollengier/paddlec.git
cd paddlec
gem build paddlec.gemspec
gem install paddlec-XXX.gem # XXX is the version of the gem
```

Generate the documentation:
```bash
# Install yard if it is not already installed
gem install yard
# Use the helper script to generate the documentation
./dodoc.sh
# Open the freshly generated documentation
firefox doc/index.html
```


TODO
----

* select, reject
* FFTw, FFTscope, Oscilloscope
* libswresample
* boolean buffers and [] += 
* pulseaudio sink / source
* rtlsdr source
* oscillators, PLLs, costas loops...
* doc rdoc, yard groups
* ...

