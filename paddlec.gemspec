require File.expand_path('../lib/paddlec/version', __FILE__)

Gem::Specification.new do |s|
	s.name = "paddlec"
	s.version = PaddleC::VERSION
	s.date = Time.now.strftime '%Y-%m-%d'
	s.summary = "A Ruby C extension for real-time signal processing."
	s.description = "PaddleC is a Ruby C extension attempting to provide objects and methods to rapidly set up real-time signal processing test benches in Ruby."
	s.license = 'GPL-3.0+'
	s.authors = ["Théotime Bollengier"]
	s.email = 'theotime.bollengier@gmail.com'
	s.homepage = 'https://gitlab.com/theotime_bollengier/paddlec'
	s.add_runtime_dependency 'roctave', '~> 0.0', '>= 0.0.1'
	s.files = [
		'ext/libpaddlec/arithmetic.c',
		'ext/libpaddlec/comparison.c',
		'ext/libpaddlec/complex.c',
		'ext/libpaddlec/delay.c',
		'ext/libpaddlec/fir_filter_avx.c',
		'ext/libpaddlec/fir_filter.c',
		'ext/libpaddlec/fir_filter_neon.c',
		'ext/libpaddlec/fir_filter_sse.c',
		'ext/libpaddlec/libpaddlec.c',
		'ext/libpaddlec/libpaddlec.h',
		'ext/libpaddlec/math.c',
		'ext/libpaddlec/no_fast_math.c',
		'ext/libpaddlec/rounding.c',
		'ext/paddlec/extconf.rb',
		'ext/paddlec/complex_buffer.c',
		'ext/paddlec/complex_buffer.h',
		'ext/paddlec/delay.c',
		'ext/paddlec/delay.h',
		'ext/paddlec/fir_filter.c',
		'ext/paddlec/fir_filter.h',
		'ext/paddlec/float_buffer.c',
		'ext/paddlec/float_buffer.h',
		'ext/paddlec/paddlec.c',
		'ext/paddlec/paddlec.h',
		'ext/paddlec/pulseaudio.c',
		'ext/paddlec/pulseaudio.h',
		'lib/paddlec/version.rb',
		'lib/paddlec.rb',
		'LICENSE',
		'paddlec.gemspec',
		'README.md',
		'samples/fmdemod_chunk_buffer.rb',
		'samples/fmdemod_chunk.rb',
		'samples/fmdemod.rb',
		'samples/stereo_chunk.rb'
	]
	s.extensions = [
		'ext/paddlec/extconf.rb'
	]
end

