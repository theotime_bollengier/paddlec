#!/bin/bash

rm -rv doc .yardoc

yard doc -o doc --verbose --list-undoc \
	lib/paddlec.rb \
	lib/paddlec/version.rb \
	ext/paddlec/paddlec.c \
	ext/paddlec/complex_buffer.c \
	ext/paddlec/float_buffer.c \
	ext/paddlec/fir_filter.c \
	ext/paddlec/pulseaudio.c \
	ext/paddlec/delay.c

echo
echo Documentation done.
echo

