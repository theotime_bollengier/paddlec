#!/usr/bin/env ruby

require 'roctave'
require '../ext/paddlec/paddlec'


show_filters = false

fs = 2.048e6

# b = File.binread(File.expand_path '~/sdrrec/pouet.raw').unpack('C*').each_slice(2).collect{|a| Complex((a.first - 128) / 128.0, (a.last - 128) / 128.0)}.to_complex_buffer
# puts b.length
# File.binwrite('pouet_cf32le.raw', b.pack)
# exit


unless File.file? 'coefficients.f32le' then
	STDOUT.write 'Generating filter coeficients... '; STDOUT.flush
	now = Time.now

	b_bbdecimator = Roctave.fir1(254, 95e3*2/fs)
	Roctave.freqz(b_bbdecimator, :magnitude, nb_points: 2048, fs: fs) if show_filters

	b_differentiator = Roctave.fir_differentiator(32)
	Roctave.freqz(b_differentiator, :magnitude, nb_points: 2048, fs: fs/8) if show_filters

	b_adecimator = Roctave.fir_low_pass(512, 15e3*2/256e3)
	Roctave.freqz(b_adecimator, :magnitude, nb_points: 2048, fs: fs/8) if show_filters

	tau = 50e-6
	ts = 8*8/fs
	b = [1 - Math.exp(-ts/tau)]
	a = [1, -Math.exp(-ts/tau)]
	h, w = Roctave.freqz(b, a, nb_points: 1024)
	h.collect!{|v| v.abs}
	w.collect!{|v| v/w.last}
	n = 2048
	b_deemphasis = Roctave.fir2(n, w, h, :odd_symmetry)
	Roctave.freqz(b_deemphasis, :magnitude, nb_points: 2048, fs: fs/8/8) if show_filters

	puts (Time.now - now).round(3)

	File.open('coefficients.f32le', 'wb') do |ofile|
		ofile.write b_bbdecimator.pack('f*')
		ofile.write b_differentiator.pack('f*')
		ofile.write b_adecimator.pack('f*')
		ofile.write b_deemphasis.pack('f*')
	end
else
	ifile = File.open('coefficients.f32le', 'rb')
	b = ifile.read.unpack('f*')
	ifile.close
	b_bbdecimator = b.shift(255)
	b_differentiator = b.shift(33)
	b_adecimator = b.shift(513)
	b_deemphasis = b.shift(2049)
end


STDOUT.write 'Instantiating filters... '; STDOUT.flush
now = Time.now

bbdecimator = PaddleC::FirDecimator.new b_bbdecimator, 8
differentiator = PaddleC::FirFilter.new b_differentiator
adecimator = PaddleC::FirDecimator.new b_adecimator, 8
deemphasis = PaddleC::FirFilter.new b_deemphasis
puts (Time.now - now).round(3)


start = Time.now
STDOUT.write 'Reading input... '; STDOUT.flush
now = Time.now
#if2MHz_complex = PaddleC::ComplexBuffer.unpack(File.binread(File.expand_path('~/sdrrec/pouet.raw')), :u8)
if2MHz_complex = PaddleC::ComplexBuffer.unpack(File.binread(File.expand_path('~/sdrrec/95.0MHz_2.048Msps_cu8.raw')), :u8)
puts (Time.now - now).round(3)
puts if2MHz_complex.length

STDOUT.write 'Input decimation... '; STDOUT.flush
now = Time.now
baseband256kHz_complex = bbdecimator.decimate(if2MHz_complex)
puts (Time.now - now).round(3)

STDOUT.write 'Differentiation... '; STDOUT.flush
now = Time.now
baseband_diff_complex, baseband_delayed_complex = differentiator.filter(baseband256kHz_complex, delayed: true)
puts (Time.now - now).round(3)

STDOUT.write 'Demodulation... '; STDOUT.flush
now = Time.now
demodulated = (baseband_diff_complex.imag*baseband_delayed_complex.real - baseband_diff_complex.real*baseband_delayed_complex.imag) / baseband_delayed_complex.abs2
puts (Time.now - now).round(3)

STDOUT.write 'Decimation to audio... '; STDOUT.flush
now = Time.now
audioemph = adecimator.decimate(demodulated)
puts (Time.now - now).round(3)

STDOUT.write 'Deemphasis... '; STDOUT.flush
now = Time.now
audio = deemphasis.filter(audioemph) * 0.25
puts (Time.now - now).round(3)

puts "Processing took #{(Time.now - start).round 3} s"

PaddleC::PulseAudio::Simple::Sink.open(sample_spec: 'float32le 1ch 32000Hz') do |sink|
	sink << audio
end

=begin
STDOUT.write 'Writing output to output.f32le... '; STDOUT.flush
now = Time.now
File.binwrite('output.f32le', audio.pack)
puts (Time.now - now).round(3)

puts "Processing took #{(Time.now - start).round 3} s"

system "ffmpeg -v error -ar 32k -ac 1 -f f32le -i output.f32le -y output.wav"

system "mpv output.wav"
=end

