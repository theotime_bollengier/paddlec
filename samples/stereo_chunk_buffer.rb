#!/usr/bin/env ruby

require 'roctave'
require '../lib/paddlec'

def show_bar(f, t = 80.0)
	p = (f.pos * t / f.size).round
	STDOUT.write "\r[#{'='*p}>#{'-'*(t - p)}]  "
	STDOUT.flush
end


show_filters = false

fs = 2.048e6

# b = File.binread(File.expand_path '~/sdrrec/pouet.raw').unpack('C*').each_slice(2).collect{|a| Complex((a.first - 128) / 128.0, (a.last - 128) / 128.0)}.to_complex_buffer
# puts b.length
# File.binwrite('pouet_cf32le.raw', b.pack)
# exit


unless File.file? 'coefficients.f32le' then
	STDOUT.write 'Generating filter coeficients... '; STDOUT.flush
	now = Time.now

	b_bbdecimator = Roctave.fir1(254, 95e3*2/fs)
	Roctave.freqz(b_bbdecimator, :magnitude, nb_points: 2048, fs: fs) if show_filters

	b_differentiator = Roctave.fir_differentiator(32)
	Roctave.freqz(b_differentiator, :magnitude, nb_points: 2048, fs: fs/8) if show_filters

	b_adecimator = Roctave.fir_low_pass(512, 15e3*2/256e3)
	Roctave.freqz(b_adecimator, :magnitude, nb_points: 2048, fs: fs/8) if show_filters

	tau = 50e-6
	ts = 8*8/fs
	b = [1 - Math.exp(-ts/tau)]
	a = [1, -Math.exp(-ts/tau)]
	h, w = Roctave.freqz(b, a, nb_points: 1024)
	h.collect!{|v| v.abs}
	w.collect!{|v| v/w.last}
	n = 2048
	b_deemphasis = Roctave.fir2(n, w, h, :odd_symmetry)
	Roctave.freqz(b_deemphasis, :magnitude, nb_points: 2048, fs: fs/8/8) if show_filters

	b_bppilote = Roctave.fir_band_pass(256, 2*19e3/(fs/8), 2*1e3/(fs/8))
	Roctave.freqz(b_bppilote, :magnitude, nb_points: 2048, fs: fs/8) if show_filters

	b_hilbert = Roctave.fir_hilbert(48, window: :blackman)
	Roctave.freqz(b_hilbert, :magnitude, nb_points: 2048, fs: fs/8) if show_filters

	b_bplmr = Roctave.fir_band_pass(256, 2*38e3/(fs/8), 2*30e3/(fs/8), window: :blackman)
	Roctave.freqz(b_bplmr, :magnitude, nb_points: 2048, fs: fs/8) if show_filters

	puts (Time.now - now).round(3)

	File.open('coefficients.f32le', 'wb') do |ofile|
		ofile.write b_bbdecimator.pack('f*')
		ofile.write b_differentiator.pack('f*')
		ofile.write b_adecimator.pack('f*')
		ofile.write b_deemphasis.pack('f*')
		ofile.write b_bppilote.pack('f*')
		ofile.write b_hilbert.pack('f*')
		ofile.write b_bplmr.pack('f*')
	end
else
	ifile = File.open('coefficients.f32le', 'rb')
	b = ifile.read.unpack('f*')
	ifile.close
	b_bbdecimator = b.shift(255)
	b_differentiator = b.shift(33)
	b_adecimator = b.shift(513)
	b_deemphasis = b.shift(2049)
	b_bppilote = b.shift(257)
	b_hilbert = b.shift(49)
	b_bplmr = b.shift(257)
end



bbdecimator = PaddleC::FirDecimator.new b_bbdecimator, 8
differentiator = PaddleC::FirFilter.new b_differentiator
adecimator_lpr = PaddleC::FirDecimator.new b_adecimator, 8
adecimator_lmr = PaddleC::FirDecimator.new b_adecimator, 8
deemphasis_lpr = PaddleC::FirFilter.new b_deemphasis
deemphasis_lmr = PaddleC::FirFilter.new b_deemphasis
bppilote       = PaddleC::FirFilter.new b_bppilote
bplmr          = PaddleC::FirFilter.new b_bplmr
hilbert        = PaddleC::FirTransformer.new b_hilbert
delay_p = PaddleC::Delay.new 152
delay_h = PaddleC::Delay.new 24

time_chunk = 100e-3
len = (32e3 * time_chunk).round

strbuf = ''
if2MHz_complex = PaddleC::ComplexBuffer.new
baseband256kHz_complex = PaddleC::ComplexBuffer.new
baseband_diff_complex = PaddleC::ComplexBuffer.new
baseband_delayed_complex = PaddleC::ComplexBuffer.new
baseband_diff_i = PaddleC::FloatBuffer.new
baseband_diff_r = PaddleC::FloatBuffer.new
baseband_del_i = PaddleC::FloatBuffer.new
baseband_del_r = PaddleC::FloatBuffer.new
baseband_del_abs2 = PaddleC::FloatBuffer.new
demodulated = PaddleC::FloatBuffer.new
pilote19 = PaddleC::FloatBuffer.new
pilote19c = PaddleC::ComplexBuffer.new
pilote38 = PaddleC::FloatBuffer.new
lmrh = PaddleC::FloatBuffer.new
lmrhd = PaddleC::FloatBuffer.new
lmremph = PaddleC::FloatBuffer.new
lmr = PaddleC::FloatBuffer.new

			lpremphdelayed = PaddleC::FloatBuffer.new
			lpremph = PaddleC::FloatBuffer.new
			lpr = PaddleC::FloatBuffer.new

			left = PaddleC::FloatBuffer.new
			right = PaddleC::FloatBuffer.new




decoding = 0.0
waiting = 0.0
fname = '~/sdrrec/102.4MHz_2.048Msps_cu8.raw'
fname = '~/sdrrec/102.4MHz_2.048Msps_cu8_1.raw'
fname = '~/sdrrec/102.4MHz_2.048Msps_cu8_2.raw'
fname = '~/sdrrec/102.4MHz_2.048Msps_cu8_3.raw'
fname = '~/sdrrec/pouet.raw'
fname = '~/sdrrec/95.0MHz_2.048Msps_cu8.raw'


PaddleC::PulseAudio::Simple::Sink.open(stream_name: fname, sample_spec: 'float32 2ch 32000Hz') do |sink|
	File.open(File.expand_path(fname), 'rb') do |f|
		loop do
			show_bar(f)

			now = Time.now

			strbuf = f.read(8*8*len*2)
			if2MHz_complex.unpack(strbuf, :u8)

			bbdecimator.decimate(if2MHz_complex, buffer: baseband256kHz_complex)

			differentiator.filter(baseband256kHz_complex, buffer: baseband_diff_complex, delayed: baseband_delayed_complex)

			baseband_diff_complex.imag(baseband_diff_i)
			baseband_diff_complex.real(baseband_diff_r)
			baseband_delayed_complex.imag(baseband_del_i)
			baseband_delayed_complex.real(baseband_del_r)
			baseband_delayed_complex.abs2(baseband_del_abs2)

			baseband_diff_i.mult!(baseband_del_r)
			baseband_diff_r.mult!(baseband_del_i)
			baseband_diff_i.sub!(baseband_diff_r)

			baseband_diff_i.div(baseband_del_abs2, demodulated)


			bppilote.filter(demodulated, buffer: pilote19)
			hilbert.transform(pilote19, buffer: pilote19c)
			pilote19c.phase(pilote38).mult!(2).sin!

			bplmr.filter(demodulated, buffer: lmrh)
			delay_h.delay(lmrh, lmrhd)
			lmrhd.mult! pilote38
			adecimator_lmr.decimate(lmrhd, buffer: lmremph)
			deemphasis_lmr.filter(lmremph, buffer: lmr).mult!(0.25)

			delay_p.delay(demodulated, lpremphdelayed)
			adecimator_lpr.decimate(lpremphdelayed, buffer: lpremph)
			deemphasis_lpr.filter(lpremph, buffer: lpr).mult!(0.25)

			lpr.add(lmr, left).div!(2)
			lpr.sub(lmr, right).div!(2)

			stereo = PaddleC::ComplexBuffer.new(left, right)

			decoding += Time.now - now

			now = Time.now

			sink << stereo

			waiting += Time.now - now

			break if f.eof?
		end
		show_bar(f)
		puts
	end
end


puts "decoding took #{decoding.round(3)}s"
puts "waiting took #{waiting.round(3)}s"




