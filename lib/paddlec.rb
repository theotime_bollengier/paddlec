## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of PaddleC
## 
## PaddleC is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## PaddleC is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with PaddleC. If not, see <https://www.gnu.org/licenses/>.


module PaddleC
end

require 'roctave'
require_relative 'paddlec/version.rb'
#require_relative 'paddlec/paddlec'
require_relative '../ext/paddlec/paddlec'

