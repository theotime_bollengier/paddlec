/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <ruby.h>
#include "paddlec_defs.h"
#include "libpaddlec.h"
#include "paddlec.h"
#include "float_buffer.h"
#include "complex_buffer.h"
#include "fir_filter.h"
#include "delay.h"
#ifdef HAVE_PULSEAUDIO_L
#include "pulseaudio.h"
#endif


/* Document-module: PaddleC
 *
 * PaddleC is an attempt to provide objects and methods to perform real-time processing in Ruby.
 */


ID id_each;
ID id_collectI;
ID id_to_enum;
ID id_length;
ID id_real;
ID id_imag;
ID id_buffer;
ID id_delayed;
ID id_zip;
ID id_join;
ID id_to_s;
ID id_to_gplot;
ID id_acos;
ID id_acosh;
ID id_asin;
ID id_asinh;
ID id_atan;
ID id_atanh;
ID id_cbrt;
ID id_cos;
ID id_cosh;
ID id_erf;
ID id_erfc;
ID id_exp;
ID id_log;
ID id_log10;
ID id_log2;
ID id_sin;
ID id_sinh;
ID id_sqrt;
ID id_tan;
ID id_tanh;
ID id_u8;
ID id_s8;
ID id_u16;
ID id_s16;
ID id_u32;
ID id_s32;
ID id_f32;


VALUE m_PaddleC;

VALUE c_FFI_Pointer = Qundef;
VALUE o_FFI_Type_FLOAT = Qnil;



/* Allow the {https://rubygems.org/gems/gnuplot gnuplot} gem to draw {PaddleC::FloatBuffer}.
 * @return [String]
 */
static VALUE paddlec_array_to_gplot(VALUE self)
{
	int len, i;
	VALUE a, b, res, space, bn;
	VALUE *splat;

	if (rb_obj_is_kind_of(rb_ary_entry(self, 0), rb_cArray) || rb_obj_is_kind_of(rb_ary_entry(self, 0), c_FloatBuffer)) {
		len = rb_array_len(self) - 1;
		if (len < 0)
			len = 0;
		splat = malloc(len*sizeof(VALUE*));
		for (i = 0; i < len; i++)
			splat[i] = rb_ary_entry(self, i+1);
		a = rb_funcallv(rb_ary_entry(self, 0), id_zip, len, splat);
		free(splat);
		len = rb_array_len(a);
		b = rb_ary_new_capa(len + 1);
		space = rb_str_new_cstr(" ");
		for (i = 0; i < len; i++)
			rb_ary_store(b, i, rb_funcallv(rb_ary_entry(a, i), id_join, 1, &space));
		rb_ary_store(b, len, rb_str_new_cstr("e"));
		bn = rb_str_new_cstr("\n");
		res = rb_funcallv(b, id_join, 1, &bn);
	}
	else if (rb_obj_is_kind_of(rb_ary_entry(self, 0), rb_cNumeric)) {
		len = rb_array_len(self);
		res = rb_str_new_cstr("");
		for (i = 0; i < len; i++) {
			a = rb_funcallv(rb_ary_entry(self, i), id_to_s, 0, NULL);
			a = rb_str_cat_cstr(a, "\n");
			res = rb_str_append(res, a);
		}
	}
	else {
		len = rb_array_len(self) - 1;
		if (len < 0)
			len = 0;
		splat = malloc(len*sizeof(VALUE*));
		for (i = 0; i < len; i++)
			splat[i] = rb_ary_entry(self, i+1);
		a = rb_funcallv(rb_ary_entry(self, 0), id_zip, len, splat);
		free(splat);
		res = rb_funcallv(a, id_to_gplot, 0, NULL);
	}

	return res;
}


static void paddlec_check_if_FFI_is_defined()
{
	VALUE mFFI;
	VALUE cType;

	if (rb_funcall(rb_cObject, rb_intern("const_defined?"), 1, ID2SYM(rb_intern("FFI"))) == Qtrue) {
		mFFI = rb_const_get(rb_cObject, rb_intern("FFI"));
		c_FFI_Pointer = rb_const_get(mFFI, rb_intern("Pointer"));
		cType = rb_const_get(mFFI, rb_intern("Type"));
		o_FFI_Type_FLOAT = rb_const_get(cType, rb_intern("FLOAT"));
	}
}



/* @return [Symbol, nil]
 */
static VALUE paddlec_paddlec_accelerator(VALUE self)
{
	const char *acc = pdlc_accelerator();
	if (acc[0] == '\0')
		return Qnil;
	return ID2SYM(rb_intern(acc));
}


/* @!group  Mathematic functions. These are methods from the +Math+ module. They can operate on {PaddleC::FloatBuffer} and Float. 
 */


/* Computes the arc cosine of x. Returns 0..PI.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload acos(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload acos(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_acos(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_acos, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)acosf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the inverse hyperbolic cosine of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload acosh(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload acosh(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_acosh(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_acosh, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)acoshf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the arc sine of x. Returns -PI/2..PI/2.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload asin(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload asin(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_asin(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_asin, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)asinf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the inverse hyperbolic sine of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload asinh(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload asinh(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_asinh(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_asinh, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)asinhf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the arc tangent of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload atan(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload atan(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_atan(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_atan, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)atanf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the inverse hyperbolic tangent of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload atanh(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload atanh(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_atanh(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_atanh, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)atanhf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Returns the cube root of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload cbrt(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload cbrt(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_cbrt(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_cbrt, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)cbrtf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the cosine of x (expressed in radians). Returns a Float in the range -1.0..1.0.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload cos(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload cos(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_cos(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_cos, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)cosf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the hyperbolic cosine of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload cosh(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload cosh(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_cosh(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_cosh, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)coshf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Calculates the error function of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload erf(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload erf(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_erf(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_erf, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)erff(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Calculates the complementary error function of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload erfc(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload erfc(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_erfc(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_erfc, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)erfcf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Calculates the value of e (the base of natural logarithms) raised to the power of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload exp(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload exp(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_exp(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_exp, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)expf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Calculates the natural logarithm of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload log(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload log(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_log(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_log, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)logf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Calculates the base 10 logarithm of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload log10(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload log10(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_log10(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_log10, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)log10f(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Calculate the base 2 logarithm of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload log2(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload log2(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_log2(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_log2, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)log2f(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the sine of x (expressed in radians). Returns a Float in the range -1.0..1.0.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload sin(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload sin(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_sin(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_sin, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)sinf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the hyperbolic sine of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload sinh(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload sinh(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_sinh(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_sinh, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)sinhf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the non-negative square root of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload sqrt(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload sqrt(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_sqrt(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_sqrt, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)sqrtf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the tangent of x (expressed in radians).
 * @return [PaddleC::FloatBuffer, Float]
 * @overload tan(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload tan(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_tan(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_tan, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)tanf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}


/* Computes the hyperbolic tangent of x.
 * @return [PaddleC::FloatBuffer, Float]
 * @overload tanh(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload tanh(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_tanh(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_tanh, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)tanhf(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}

/* @!endgroup
 */

void Init_paddlec()
{
	id_each = rb_intern("each");
	id_collectI = rb_intern("collect!");
	id_to_enum = rb_intern("to_enum");
	id_length = rb_intern("length");
	id_real = rb_intern("real");
	id_imag = rb_intern("imag");
	id_buffer = rb_intern("buffer");
	id_delayed = rb_intern("delayed");
	id_zip = rb_intern("zip");
	id_join = rb_intern("join");
	id_to_s = rb_intern("to_s");
	id_to_gplot = rb_intern("to_gplot");
	id_acos = rb_intern("acos");
	id_acosh = rb_intern("acosh");
	id_asin = rb_intern("asin");
	id_asinh = rb_intern("asinh");
	id_atan = rb_intern("atan");
	id_atanh = rb_intern("atanh");
	id_cbrt = rb_intern("cbrt");
	id_cos = rb_intern("cos");
	id_cosh = rb_intern("cosh");
	id_erf = rb_intern("erf");
	id_erfc = rb_intern("erfc");
	id_exp = rb_intern("exp");
	id_log = rb_intern("log");
	id_log10 = rb_intern("log10");
	id_log2 = rb_intern("log2");
	id_sin = rb_intern("sin");
	id_sinh = rb_intern("sinh");
	id_sqrt = rb_intern("sqrt");
	id_tan = rb_intern("tan");
	id_tanh = rb_intern("tanh");
	id_u8  = rb_intern("u8");
	id_s8  = rb_intern("s8");
	id_u16 = rb_intern("u16");
	id_s16 = rb_intern("s16");
	id_u32 = rb_intern("u32");
	id_s32 = rb_intern("s32");
	id_f32 = rb_intern("f32");

	paddlec_check_if_FFI_is_defined();

	m_PaddleC = rb_define_module("PaddleC");
	rb_define_module_function(m_PaddleC, "accelerator", paddlec_paddlec_accelerator, 0);
	rb_define_module_function(m_PaddleC, "acos",        paddlec_math_acos,           1);
	rb_define_module_function(m_PaddleC, "acosh",       paddlec_math_acosh,          1);
	rb_define_module_function(m_PaddleC, "asin",        paddlec_math_asin,           1);
	rb_define_module_function(m_PaddleC, "asinh",       paddlec_math_asinh,          1);
	rb_define_module_function(m_PaddleC, "atan",        paddlec_math_atan,           1);
	rb_define_module_function(m_PaddleC, "atanh",       paddlec_math_atanh,          1);
	rb_define_module_function(m_PaddleC, "cbrt",        paddlec_math_cbrt,           1);
	rb_define_module_function(m_PaddleC, "cos",         paddlec_math_cos,            1);
	rb_define_module_function(m_PaddleC, "cosh",        paddlec_math_cosh,           1);
	rb_define_module_function(m_PaddleC, "erf",         paddlec_math_erf,            1);
	rb_define_module_function(m_PaddleC, "erfc",        paddlec_math_erfc,           1);
	rb_define_module_function(m_PaddleC, "exp",         paddlec_math_exp,            1);
	rb_define_module_function(m_PaddleC, "log",         paddlec_math_log,            1);
	rb_define_module_function(m_PaddleC, "log10",       paddlec_math_log10,          1);
	rb_define_module_function(m_PaddleC, "log2",        paddlec_math_log2,           1);
	rb_define_module_function(m_PaddleC, "sin",         paddlec_math_sin,            1);
	rb_define_module_function(m_PaddleC, "sinh",        paddlec_math_sinh,           1);
	rb_define_module_function(m_PaddleC, "sqrt",        paddlec_math_sqrt,           1);
	rb_define_module_function(m_PaddleC, "tan",         paddlec_math_tan,            1);
	rb_define_module_function(m_PaddleC, "tanh",        paddlec_math_tanh,           1);

	Init_paddlec_float_buffer();
	Init_paddlec_complex_buffer();
	Init_paddlec_fir_filter();
	Init_paddlec_delay();

#ifdef HAVE_PULSEAUDIO_L
	Init_paddlec_pulseaudio();
#endif

	rb_define_method(rb_cArray, "to_gplot", paddlec_array_to_gplot, 0);
}

