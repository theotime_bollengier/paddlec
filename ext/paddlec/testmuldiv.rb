#!/usr/bin/env ruby

require './paddlec'
require 'colorize'

class Complex
	def ceil
		Complex(self.real.ceil, self.imag.ceil)
	end
	def floor
		Complex(self.real.floor, self.imag.floor)
	end
	def truncate
		Complex(self.real.truncate, self.imag.truncate)
	end
	def round
		Complex(self.real.round, self.imag.round)
	end
	def %(other)
		if other.kind_of?(Complex) then
			Complex(self.real % other.real, self.imag % other.imag)
		else
			Complex(self.real % other.real, self.imag % other.real)
		end
	end
end
class Float
	alias :oldmod :%
	def %(other)
		if other.kind_of?(Complex)
			Complex(self.oldmod(other.real), self.oldmod(other.imag))
		else
			self.oldmod(other)
		end
	end
end

tests = 0
passed = 0
errors = 0

nbel = 1000
m = 4
ca = nbel.times.collect{Complex((rand*2-1)*m, (rand*2-1)*m)}.to_complex_buffer
cb = nbel.times.collect{Complex((rand*2-1)*m, (rand*2-1)*m)}.to_complex_buffer
fa = nbel.times.collect{(rand*2-1)*m}.to_float_buffer
fb = nbel.times.collect{(rand*2-1)*m}.to_float_buffer
cs = Complex(rand*2-1, Math.sqrt(2))
fs = Math::PI

p = [
	[fa, fb],
	[fa, cb],
	[ca, fb],
	[ca, cb],
	[fa, fs],
	[fa, cs],
	[ca, fs],
	[ca, cs]
]

[:+, :-, :*, :/, :**, :%].each do |method|
	p.length.times.each do |k|
		d = p[k]
		a = d.first
		b = d.last
		atype = a.class.name.sub(/PaddleC::/, '').center(13)
		btype = b.class.name.sub(/PaddleC::/, '').center(13)
		if b.kind_of?(Numeric) then
			g = nbel.times.collect{|i| a[i].send(method, b)}
		else
			g = nbel.times.collect{|i| a[i].send(method, b[i])}
		end
		begin
			r = a.send(method, b).to_a
			err = nbel.times.collect{|i| r[i].finite? ? (g[i] - r[i]).abs2 : 0.0}.max
			s = "#{atype}.#{method.to_s.bold}(#{btype}) => #{err}"
			tests += 1
			if err < 1e-6 then
				puts s.light_green
				passed += 1
			else
				puts s.light_red
				errors += 1
			end
		rescue Exception => e
			puts "#{atype}.#{method.to_s.bold}(#{btype}) => #{e.message}".light_yellow
		end
	end
	puts
end


[
	[:add!, :+],
	[:sub!, :-],
	[:mult!, :*],
	[:div!, :/],
	[:pow!, :**],
	[:modulo!, :%],
].each do |inp_m|
	inplace_method = inp_m.first
	ref_method = inp_m.last
	p.length.times.each do |k|
		d = p[k]
		a = d.first
		b = d.last
		r = a.clone
		atype = a.class.name.sub(/PaddleC::/, '').center(13)
		btype = b.class.name.sub(/PaddleC::/, '').center(13)
		if b.kind_of?(Numeric) then
			g = nbel.times.collect{|i| a[i].send(ref_method, b)}
		else
			g = nbel.times.collect{|i| a[i].send(ref_method, b[i])}
		end
		begin
			r.send(inplace_method, b)
			r = r.to_a
			err = nbel.times.collect{|i| r[i].finite? ? (g[i] - r[i]).abs2 : 0.0}.max
			s = "#{atype}.#{inplace_method.to_s.bold}(#{btype}) => #{err}"
			tests += 1
			if err < 1e-6 then
				puts s.light_green
				passed += 1
			else
				puts s.light_red
				errors += 1
			end
		rescue Exception => e
			puts "#{atype}.#{inplace_method.to_s.bold}(#{btype}) => #{e.message}".light_yellow
		end
	end
	puts
end

[
	[:ceil, :ceil!, :ceil],
	[:floor, :floor!, :floor],
	[:truncate, :truncate!, :truncate],
	[:round, :round!, :round],
].each do |methods|
	pmethod = methods[0]
	imethod = methods[1]
	rmethod = methods[2]
	[fa, ca].each do |recv|
		type = recv.class.name.sub(/PaddleC::/, '').center(13)
		g = nbel.times.collect{|i| recv[i].send(rmethod)}

		r = recv.send(pmethod).to_a
		err = nbel.times.collect{|i| r[i].finite? ? (g[i] - r[i]).abs2 : 0.0}.max
		s = "#{type}.#{pmethod.to_s.bold} => #{err}"
		tests += 1
		if err < 1e-6 then
			puts s.light_green
			passed += 1
		else
			puts s.light_red
			errors += 1
		end

		next if imethod.nil?
		r = recv.clone
		r.send(imethod)
		err = nbel.times.collect{|i| r[i].finite? ? (g[i] - r[i]).abs2 : 0.0}.max
		s = "#{type}.#{imethod.to_s.bold} => #{err}"
		tests += 1
		if err < 1e-6 then
			puts s.light_green
			passed += 1
		else
			puts s.light_red
			errors += 1
		end
	end
end
puts


s = "#{passed} / #{tests} passed, #{errors} / #{tests} failed"
if errors == 0 then
	puts s.light_green
else
	puts s.light_red
end

