#!/usr/bin/env ruby

require 'colorize'
require 'roctave'
require '../../lib/paddlec.rb'

include PaddleC::PulseAudio

Connection.open do |c|
	#pp c.server_info
	#puts
	#pp c.mem_stats
	#puts
	#pp c.sink
	#puts
	#pp c.sinks
	#puts
	#pp c.sources
	c.modules.each{|mi| puts "#{mi.index.to_s.light_green.bold} #{mi.name.light_cyan}#{mi.argument ? " #{mi.argument}".light_magenta : ''}#{mi.proplist['module.description'] ? " #{mi.proplist['module.description'].light_green}" : ''}"}
	#puts
	#c.unload_module 'module-filter-heuristics'
	#c.modules.each{|mi| puts "#{mi.index.to_s.light_green.bold} #{mi.name.light_cyan}#{mi.argument ? " #{mi.argument}".light_magenta : ''}#{mi.proplist['module.description'] ? " #{mi.proplist['module.description'].light_green}" : ''}"}
	#puts
	#pp c.load_module 'module-filter-heuristics'
	#puts 
	#c.modules.each{|mi| puts "#{mi.index.to_s.light_green.bold} #{mi.name.light_cyan}#{mi.argument ? " #{mi.argument}".light_magenta : ''}#{mi.proplist['module.description'] ? " #{mi.proplist['module.description'].light_green}" : ''}"}
	pp c.module 'module-filter-heuristics'
	pp c.module 28
end


exit

ss = SampleSpec.new 'float32 2ch 44100Hz'

ba = BufferAttributes.new
ba.tlength = 8192*ss.frame_size

Connection.open do |c|
	puts "c.tile_size(ss): #{c.tile_size(ss)}"
	length = c.tile_length(ss)
	puts "length: #{length} (#{(length.to_f / ss.rate).round(3)}s)"
	b = PaddleC::ComplexBuffer.new(ba.tlength / ss.frame_size)
	b.collect!{Complex((rand*2-1)/2, (rand*2-1)/2)}

	p = c.new_playback

	#p.on_state_change do |s, state|
	#	puts '-'*30
	#	puts s.in_callback?
	#	puts state.to_s.light_cyan
	#	puts s.state.to_s.light_cyan
	#	puts '-'*30
	#end

#	p.on_latency_update do |playback| 
#		puts (playback.latency / 1000000.0).to_s.light_blue
#	end
#
#	p.on_underflow do |playback| 
#		puts "<< UNDERFLOW".to_s.light_red
#	end
#
#	p.on_overflow do |playback| 
#		puts "<< OVERFLOW".to_s.light_red
#	end
#
#	p.on_started do |playback|
#		puts "Started".light_magenta
#	end
#
#	p.on_moved do |playback|
#		puts "Moved to #{playback.device_name}".light_magenta
#	end
#
#	p.on_suspended do |playback|
#		puts "Suspended: #{playback.suspended}".light_magenta.bold
#	end
#
#	p.on_buffer_attr_change do |playback|
#		puts "New buffer attributes: #{playback.buffer_attributes}".light_magenta.bold
#	end
#
#	p.on_event do |playback, event, proplist|
#		puts ('-'*30).light_cyan
#		puts "Event: '#{event}".light_cyan
#		puts proplist.inspect.light_cyan
#		puts ('-'*30).light_cyan
#	end

	p.on_write_request do |s, l, e, o| 
		puts "I'm ready to get #{l} samples, #{e} elements, #{o} bytes".light_green
		s << b
	end

	p.connect(sample_spec: ss, buffer_attributes: ba)

	# puts "index: #{p.index}"
	# puts "connected?: #{p.connected?}"
	# puts "state: #{p.state}"
	# puts "device_index: #{p.device_index}"
	# puts "device_name: '#{p.device_name}'"
	# puts "suspended?: #{p.suspended?}"
	# puts "corked?: #{p.corked?}"
	# puts "connected?: #{p.connected?}"
	# puts "disconnected?: #{p.disconnected?}"
	# puts "state: #{p.state}"
	# puts "sample_spec:"
	# pp p.sample_spec
	# puts "channel_map:"
	# pp p.channel_map
	# puts "format:"
	# pp p.format
	# puts "buffer_attributes:"
	# pp p.buffer_attributes
	# puts
	# puts "writable_bytes: #{p.writable_bytes}"
	# puts "writable_samples: #{p.writable_samples}"


	puts "corked?: #{p.corked?}"
	#p << b
	puts 'hello there'
	#p.trigger

	#sleep 0.5
	#p << b

	sleep 1
	p.name = 'coucou les amis'
	p.pause
	sleep 0.95
	p.resume
	p.on_latency_update
	sleep 3
	puts "will disconnect".light_yellow
	p.disconnect
	puts "disconnected".light_yellow
end

exit

=begin
ss = SampleSpec.new 'float32 2ch 44100Hz'

ba = BufferAttributes.new
ba.tlength = 8192*ss.frame_size

Connection.open do |c|
	puts "c.tile_size(ss): #{c.tile_size(ss)}"
	length = c.tile_length(ss)
	puts "length: #{length} (#{(length.to_f / ss.rate).round(3)}s)"
	b = PaddleC::ComplexBuffer.new(ba.tlength / ss.frame_size)
	b.collect!{Complex((rand*2-1)/2, (rand*2-1)/2)}
	c.new_playback_connect(sample_spec: ss, buffer_attributes: ba) do |p|
		# puts "index: #{p.index}"
		# puts "connected?: #{p.connected?}"
		# puts "state: #{p.state}"
		# puts "device_index: #{p.device_index}"
		# puts "device_name: '#{p.device_name}'"
		# puts "suspended?: #{p.suspended?}"
		# puts "corked?: #{p.corked?}"
		# puts "connected?: #{p.connected?}"
		# puts "disconnected?: #{p.disconnected?}"
		# puts "state: #{p.state}"
		# puts "sample_spec:"
		# pp p.sample_spec
		# puts "channel_map:"
		# pp p.channel_map
		# puts "format:"
		# pp p.format
		# puts "buffer_attributes:"
		# pp p.buffer_attributes
		# puts
		# puts "writable_bytes: #{p.writable_bytes}"
		# puts "writable_samples: #{p.writable_samples}"

		p.on_latency_update{|playback| puts (playback.latency / 1000000.0).to_s.light_blue}
		p.name = 'Hello'

		p.on_write_request do |s, l, e, o| 
			puts "I'm ready to get #{l} samples, #{e} elements, #{o} bytes".light_green
			s << b
		end

		puts "corked?: #{p.corked?}"
		p << b
		#p.trigger

		#sleep 0.5
		#p << b

		sleep 1
		p.on_latency_update
		sleep 3
	end
end



exit
=end



Connection.open do |c|
	pp c.server_info
	puts
	pp c.mem_stats
	puts
	pp c.sink
	puts
	#pp c.sinks
end

pp PaddleC::PulseAudio::FormatInfo.encodings

#pp SinkInfo.new

exit

ss = 'float32 1ch 44100Hz'
b = PaddleC::FloatBuffer.new 4096
Simple::Sink.open(sample_spec: ss) do |sink|
	#Simple::Source.open(sample_spec: ss, device: 'alsa_input.pci-0000_00_1b.0.analog-stereo') do |source|
	Simple::Source.open(sample_spec: ss) do |source|
		20.times.each do
			if false then
				source >> b
				sink << b
			else
				#puts source.latency*1e-6
				#b = source.read
				source >> b
				#puts b.bytesize / 2
				sink << b
			end
		end
	end
end

puts b.length
t = Roctave.linspace(0, b.length/44100.0, b.length)
Roctave.plot(t, b)
fft = Roctave.dft(b.to_a, window: :hann, normalize: true)
fft.collect!{|v| 20*Math.log10(v.abs)}
fft = fft[0...fft.length/2]
f = Roctave.linspace((0 ... 44100/2), fft.length)
Roctave.plot(f, fft)

=begin
fs = 44100.0
ss = SampleSpec.new(format: :float32le, rate: fs, channels: 1)

Simple::Sink.open(stream_name: 'titi', sample_spec: ss) do |sink|
	Simple::Source.open(sample_spec: ss) do |source|
		100.times.each do
			sink.write(source.read)
		end
	end
end
=end


=begin
c = PaddleC::PulseAudio::Connection.new
puts "c.closed? #{c.closed?}"
puts "c.state #{c.state}"
c.open name: 'Henri', server: 'unix:/run/user/1000/pulse/native'
puts "c.protocol_version #{c.protocol_version}"
puts "c.server_protocol_version #{c.server_protocol_version}"
puts "c.index #{c.index}"
puts '-'*80
puts `pacmd list-clients`.match(/index:\s+#{c.index}.+index:/m)
puts '-'*80
c.name = "Prout!"
puts '-'*80
puts `pacmd list-clients`.match(/index:\s+#{c.index}.+index:/m)
puts '-'*80
puts "c.drain #{c.drain}"
puts "c.closed? #{c.closed?}"
puts "c.state #{c.state}"
puts "c.path #{c.path}"
puts "local? #{c.local?}"
puts "pending? #{c.pending?}"
pp Thread.list
ss = PaddleC::PulseAudio::Sample::Spec.new(format: :float32le, rate: 44100, channels: 2)
puts "c.tile_size(#{ss}) #{c.tile_size(ss)}"
puts "c.tile_length(#{ss}) #{c.tile_length(ss)}"
puts "c.tile_size(nil) #{c.tile_size(nil)}"
#Thread.list.each{|th| puts th.name}
#puts "c.default_sink = 'alsa_output.pci-0000_00_1b.0.analog-stereo' #{c.default_sink = 'alsa_output.pci-0000_00_1b.0.analog-stereo'}"
#puts "c.default_sink = 'alsa_output.pci-0000_03_00.1.hdmi-stereo-extra2' #{c.default_sink = 'alsa_output.pci-0000_03_00.1.hdmi-stereo-extra2'}"
#puts 'c.exit_daemon'; c.exit_daemon
c.close
puts "c.closed? #{c.closed?}"
puts "c.state #{c.state}"
=end

