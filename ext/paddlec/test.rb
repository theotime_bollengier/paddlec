#!/usr/bin/env ruby

require './paddlec'
require 'roctave'

def max_diff (gold_array, float_buffer)
	diff = gold_array.collect.with_index{|v, i| (v - float_buffer[i]).abs}
	h = {
		max: diff.max,
		average: diff.inject(&:+) / gold_array.length
	}
	h
end

def complex_max_diff (gold_array, float_buffer)
	goldr = gold_array.collect{|v| v.real}
	br = float_buffer.real
	hr = max_diff(goldr, br)
	goldi = gold_array.collect{|v| v.imag}
	bi = float_buffer.imag
	hi = max_diff(goldi, bi)
	[hr, hi]
end

s = File.binread 'output.f32le'

b = PaddleC::FloatBuffer.new s
puts "#{b.length} samples, accelerator: #{PaddleC.accelerator}"
a = b.to_a

n = 1024 + 512
coefs = Roctave.fir1(n, 880*2/32e3)
#coefs = PaddleC::FloatBuffer.new(12, Math::PI).to_a ## 9 10 11
rflt = Roctave::FirFilter.new coefs
pflt = PaddleC::FirFilter.new coefs


start = Time.now
rres = rflt.filter a
t_roctave = Time.now - start
puts "Roctave          took #{(t_roctave.round 3).to_s.center 6} s to process      x1      #{((a.length*(n+1)) / t_roctave / 1e6).round} MMAC/s (#{(a.length / t_roctave).round} sample/s)"

start = Time.now
pres = pflt.filter b
t_paddlec = Time.now - start
puts "PaddleC          took #{(t_paddlec.round 3).to_s.center 6} s to process      x#{(t_roctave / t_paddlec).round 2}   #{((a.length*(n+1)) / t_paddlec / 1e6).round} MMAC/s (#{(a.length / t_paddlec).round} sample/s)"

puts "Roctave: #{rres.length}, PaddleC: #{pres.length}" if pres.length != rres.length

puts
puts "PaddleC error:"
pp max_diff(rres, pres)
puts

puts '-'*80
puts 


b = PaddleC::ComplexBuffer.new s
a = b.to_a

n = 1024 + 512
coefs = Roctave.fir1(n, 880*2/32e3)
#coefs = PaddleC::FloatBuffer.new(12, Math::PI).to_a ## 9 10 11
rflt = Roctave::FirFilter.new coefs
pflt = PaddleC::FirFilter.new coefs

start = Time.now
rres = rflt.filter a
t_roctave = Time.now - start
puts "Roctave          took #{(t_roctave.round 3).to_s.center 6} s to process      x1      #{(2*(a.length*(n+1)) / t_roctave / 1e6).round} MMAC/s (#{(a.length / t_roctave).round} complex sample/s)"

start = Time.now
pres = pflt.filter b
t_paddlec = Time.now - start
puts "PaddleC          took #{(t_paddlec.round 3).to_s.center 6} s to process      x#{(t_roctave / t_paddlec).round 2}   #{(2*(a.length*(n+1)) / t_paddlec / 1e6).round} MMAC/s (#{(a.length / t_paddlec).round} complex sample/s)"

puts "Roctave: #{rres.length}, PaddleC: #{pres.length}" if pres.length != rres.length

puts
pp complex_max_diff(rres, pres)
puts




=begin
def max_diff (gold_array, float_buffer)
	diff = gold_array.collect.with_index{|v, i| (v - float_buffer[i]).abs}
	{
		max: diff.max,
		average: diff.inject(&:+) / gold_array.length
	}
end

s = File.binread 'output.f32le'

b = PaddleC::FloatBuffer.new s
a = b.to_a

n = 1024 + 512
coefs = Roctave.fir1(n, 880*2/32e3)
#coefs = PaddleC::FloatBuffer.new(12, Math::PI).to_a ## 9 10 11
rflt  = Roctave::FirFilter.new coefs
pflt1 = PaddleC::FirFilter.new coefs
pflt2 = PaddleC::FirFilter.new coefs
pflt3 = PaddleC::FirFilter.new coefs
pflt4 = PaddleC::FirFilter.new coefs
pflt5 = PaddleC::FirFilter.new coefs
pflt6 = PaddleC::FirFilter.new coefs
pflt7 = PaddleC::FirFilter.new coefs
pflt8 = PaddleC::FirFilter.new coefs

start = Time.now
rres = rflt.filter a
t_roctave = Time.now - start
puts "Roctave          took #{(t_roctave.round 3).to_s.center 6} s to process      x1"

start = Time.now
pres1 = pflt1.filter b
t_paddlec = Time.now - start
ref = t_roctave
ref = t_paddlec
puts "PaddleC          took #{(t_paddlec.round 3).to_s.center 6} s to process      x#{(ref / t_paddlec).round 2}"

start = Time.now
pres3 = pflt3.filter_blk b
t_paddlec_blk = Time.now - start
puts "PaddleC block    took #{(t_paddlec_blk.round 3).to_s.center 6} s to process      x#{(ref / t_paddlec_blk).round 2}"

start = Time.now
pres2 = pflt2.filter_sse b
t_paddlecsse = Time.now - start
puts "PaddleC with SSE took #{(t_paddlecsse.round 3).to_s.center 6} s to process      x#{(ref / t_paddlecsse).round 2}"

start = Time.now
pres4 = pflt4.filter_blk_sse b
t_paddlecblksse = Time.now - start
puts "PaddleC bloc SSE took #{(t_paddlecblksse.round 3).to_s.center 6} s to process      x#{(ref / t_paddlecblksse).round 2}"

start = Time.now
pres5 = pflt5.filter_blk_psse b
t_paddlecblkpsse = Time.now - start
puts "PaddleC bloc PSE took #{(t_paddlecblkpsse.round 3).to_s.center 6} s to process      x#{(ref / t_paddlecblkpsse).round 2}"

start = Time.now
pres6 = pflt6.filter_blk_transp2 b
t_paddlecblktrs = Time.now - start
puts "PaddleC bloc trs took #{(t_paddlecblktrs.round 3).to_s.center 6} s to process      x#{(ref / t_paddlecblktrs).round 2}"

start = Time.now
pres7 = pflt7.filter_blk_transp2_sse b
t_paddlecblktrse = Time.now - start
puts "PaddleC blocetrs took #{(t_paddlecblktrse.round 3).to_s.center 6} s to process      x#{(ref / t_paddlecblktrse).round 2}"

start = Time.now
pres8 = pflt8.filter_blk_pavx b
t_paddlecblkpavx = Time.now - start
puts "PaddleC bloc AVX took #{(t_paddlecblkpavx.round 3).to_s.center 6} s to process      x#{(ref / t_paddlecblkpavx).round 2}"

puts "Roctave: #{rres.length}, PaddleC: #{pres1.length}" if pres1.length != rres.length
puts "Roctave: #{rres.length}, PaddleC block: #{pres3.length}" if pres3.length != rres.length
puts "Roctave: #{rres.length}, PaddleC with SSE: #{pres2.length}" if pres2.length != rres.length
puts "Roctave: #{rres.length}, PaddleC bloc SSE: #{pres4.length}" if pres4.length != rres.length
puts "Roctave: #{rres.length}, PaddleC bloc PSE: #{pres5.length}" if pres5.length != rres.length
puts "Roctave: #{rres.length}, PaddleC bloc trs: #{pres6.length}" if pres6.length != rres.length
puts "Roctave: #{rres.length}, PaddleC blocetrs: #{pres7.length}" if pres7.length != rres.length
puts "Roctave: #{rres.length}, PaddleC bloc AVX: #{pres8.length}" if pres8.length != rres.length

puts
puts "PaddleC error:"
pp max_diff(rres, pres1)
puts
puts "PaddleC block error:"
pp max_diff(rres, pres3)
puts
puts "PaddleC with SSE error:"
pp max_diff(rres, pres2)
puts
puts "PaddleC bloc SSE error:"
pp max_diff(rres, pres4)
puts
puts "PaddleC bloc PSE error:"
pp max_diff(rres, pres5)
puts
puts "PaddleC bloc AVX error:"
pp max_diff(rres, pres8)
puts
puts "PaddleC bloc trs error:"
pp max_diff(rres, pres6)
puts
puts "PaddleC blocetrs error:"
pp max_diff(rres, pres7)
puts


exit

File.binwrite('roctave.f32le', rres.pack('f*'))
File.binwrite('paddlec.f32le', pres1.pack)
File.binwrite('paddlec_SSE.f32le', pres2.pack)

system "ffmpeg -v error -ar 32k -ac 1 -f f32le -i roctave.f32le -y roctave.wav"
system "ffmpeg -v error -ar 32k -ac 1 -f f32le -i paddlec.f32le -y paddlec.wav"
system "ffmpeg -v error -ar 32k -ac 1 -f f32le -i paddlec_SSE.f32le -y paddlec_SSE.wav"
system "mpv roctave.wav"
system "mpv paddlec.wav"
system "mpv paddlec_SSE.wav"
=end

=begin
a = [Complex(1, -3/0.0), Complex(2, 3/0.0), 3-5i, 3+5i, Complex(5, -0.0/0.0), 0.000123456789 + 0.0000123456789i]
pp a
c = PaddleC::ComplexBuffer.new a
pp c
c.resize 8
pp c
d = PaddleC::ComplexBuffer.new [1, 2, 3].to_float_buffer
pp d
pp d.pointer
e = d.clone
pp e
pp e.pointer
s = e.pack
pp s
f = PaddleC::ComplexBuffer.new s
pp f
pp PaddleC::FloatBuffer.new s
f.each{|v| pp v}
pp f.to_a
f.collect!.with_index{|v, i| Complex(i, -i)}
pp f
=end

=begin
require 'ffi'
require './paddlec'

b = PaddleC::FloatBuffer.new [1, 2, 3]
pp b
puts b.length
puts b.pointer.to_s(16)
b.resize 6
pp b
puts b.length
puts b.pointer.to_s(16)
ptr = b.to_ptr
pp ptr
pp ptr.type_size
puts ptr.size
puts ptr.read(FFI::Type::FLOAT)
puts (ptr+4).read(FFI::Type::FLOAT)
=end

