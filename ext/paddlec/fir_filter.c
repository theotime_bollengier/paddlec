/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <ruby.h>
#include "libpaddlec.h"
#include "paddlec.h"
#include "fir_filter.h"
#include "float_buffer.h"
#include "complex_buffer.h"


VALUE c_FirFilter;
VALUE c_FirTransformer;
VALUE c_FirDecimator;
VALUE c_FirInterpolator;


static void paddlec_fir_filter_free(void *p)
{
	pdlc_fir_filter_t *fir = (pdlc_fir_filter_t*)p;
	pdlc_fir_filter_free(fir);
}


static size_t paddlec_fir_filter_size(const void* data)
{
	pdlc_fir_filter_t *fir = (pdlc_fir_filter_t*)data;
	return pdlc_fir_filter_size(fir);
}


static const rb_data_type_t paddlec_fir_filter_type = {
	.wrap_struct_name = "paddlec_fir_filter_struct",
	.function = {
		.dmark = NULL,
		.dfree = paddlec_fir_filter_free,
		.dsize = paddlec_fir_filter_size,
	},
	.data = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE paddlec_fir_filter_alloc(VALUE klass)
{
	VALUE obj;
	pdlc_fir_filter_t *fir;

	fir = pdlc_fir_filter_new(-1);
	obj = TypedData_Wrap_Struct(klass, &paddlec_fir_filter_type, fir);

	return obj;
}


static inline pdlc_fir_filter_t* paddlec_fir_filter_get_struct(VALUE obj)
{
	pdlc_fir_filter_t *fir;
	TypedData_Get_Struct(obj, pdlc_fir_filter_t, &paddlec_fir_filter_type, fir);
	return fir;
}


/* @return [PaddleC::FirFilter]
 * @overload initialize(n)
 *  @param n [Integer] the filter order
 *  @return [Paddlec::FirFilter]
 * @overload initialize(b)
 *  @param b [Array<Float>, PaddleC::FloatBuffer] the coefficients, as an array or a {FloatBuffer}
 *  @return [Paddlec::FirFilter]
 */
static VALUE paddlec_fir_filter_initialize(VALUE self, VALUE b_or_n)
{
	pdlc_fir_filter_t *fir;
	const pdlc_buffer_t *fbuf;
	int order, i;

	fir = paddlec_fir_filter_get_struct(self);

	if (rb_class_of(b_or_n) == c_FloatBuffer) {
		fbuf = paddlec_float_buffer_get_struct(b_or_n);
		order = fbuf->length;
		if (order < 1)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_initialize(fir, order - 1);
		for (i = 0; i < order; i++)
			pdlc_fir_filter_set_coef_at(fir, i, fbuf->data[i]);
	}
	else if (rb_class_of(b_or_n) == rb_cArray) {
		order = rb_array_len(b_or_n);
		if (order < 1)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_initialize(fir, order - 1);
		for (i = 0; i < order; i++)
			pdlc_fir_filter_set_coef_at(fir, i, (float)NUM2DBL(rb_ary_entry(b_or_n, i)));
	}
	else {
		order = NUM2INT(b_or_n);
		if (order < 0)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_initialize(fir, order);
	}

	return self;
}


/* Reset the filter state.
 * @return [self]
 */
static VALUE paddlec_fir_filter_reset(VALUE self)
{
	pdlc_fir_filter_t *fir;

	fir = paddlec_fir_filter_get_struct(self);
	pdlc_fir_filter_reset(fir);

	return self;
}


/* @return [Integer] the filter order
 */
static VALUE paddlec_fir_filter_order(VALUE self)
{
	const pdlc_fir_filter_t *fir;

	fir = paddlec_fir_filter_get_struct(self);

	return INT2NUM((int)fir->nb_coefs - 1);
}


/* @return [Integer] number of coefficients (order + 1)
 */
static VALUE paddlec_fir_filter_nb_coefficients(VALUE self)
{
	const pdlc_fir_filter_t *fir;

	fir = paddlec_fir_filter_get_struct(self);

	return INT2NUM((int)fir->nb_coefs);
}


/* Return a copy of the filter's numerator coefficients.
 * @return [PaddleC::FloatBuffer] the filter coefficients
 */
static VALUE paddlec_fir_filter_coefficients(VALUE self)
{
	const pdlc_fir_filter_t *fir;
	pdlc_buffer_t *fbuf;
	VALUE res;
	unsigned int i;

	fir = paddlec_fir_filter_get_struct(self);

	res = UINT2NUM(fir->nb_coefs);
	res = rb_class_new_instance(1, &res, c_FloatBuffer);
	fbuf = paddlec_float_buffer_get_struct(res);

	for (i = 0; i < fir->nb_coefs; i++)
		pdlc_fir_filter_get_coef_at(fir, (int)i, fbuf->data + i);

	return res;
}


/* Return a copy of the filter's denominator coefficients, allways [1].
 * @return [PaddleC::FloatBuffer] the filter denominator coefficients
 */
static VALUE paddlec_fir_filter_denominator(VALUE self)
{
	pdlc_buffer_t *fbuf;
	VALUE res;

	res = UINT2NUM(1);
	res = rb_class_new_instance(1, &res, c_FloatBuffer);
	fbuf = paddlec_float_buffer_get_struct(res);

	fbuf->data[0] = 1.0;

	return res;
}


/* @param index [Integer] the coefficient index
 * @return [Float] the coefficient at +index+
 */
static VALUE paddlec_fir_filter_get_coefficient_at(VALUE self, VALUE index)
{
	const pdlc_fir_filter_t *fir;
	int ind;
	float val;
	VALUE res;

	fir = paddlec_fir_filter_get_struct(self);

	ind = NUM2INT(index);
	if (pdlc_fir_filter_get_coef_at(fir, ind, &val) == 0)
		res = DBL2NUM((double)val);
	else
		res = DBL2NUM(0.0);

	return res;
}


/* @param index [Integer] the coefficient index
 * @param coef  [Float] the coefficient value
 * @return [Float, nil] the coefficient if the index is valid, or +nil+ otherwise
 */
static VALUE paddlec_fir_filter_set_coefficient_at(VALUE self, VALUE index, VALUE coef)
{
	pdlc_fir_filter_t *fir;
	int ind;
	float val;
	VALUE res = Qnil;

	fir = paddlec_fir_filter_get_struct(self);
	val = (float)NUM2DBL(coef);

	ind = NUM2INT(index);
	if (pdlc_fir_filter_set_coef_at(fir, ind, val) == 0)
		res = DBL2NUM(val);

	return res;
}


/* This method returns 1.0 if +index+ is 0, 0.0 otherwise. It is there to be compatible with {IirFilter}
 * @param index [Integer] the denominator coefficient index
 * @return [Float] the denominator coefficient at index
 */
static VALUE paddlec_fir_filter_get_denominator_at(VALUE self, VALUE index)
{
	int ind;
	VALUE res;

	ind = NUM2INT(index);
	if (ind == 0)
		res = DBL2NUM(1.0);
	else
		res = DBL2NUM(0.0);

	return res;
}


/* This method does nothing, it is there to be compatible with {IirFilter}
 * @param index [Integer] the coefficient index
 * @param coef  [Float] the denominator coefficient value
 * @return [Float, nil] the denominator coefficient if the index is valid, or nil otherwise
 */
static VALUE paddlec_fir_filter_set_denominator_at(VALUE self, VALUE index, VALUE coef)
{
	(void)self;
	(void)index;
	(void)coef;

	return Qnil;
}


/* Filter a single sample, complex or real, or a {FloatBuffer} or {ComplexBuffer}.
 * The returned element is of the same type as the input element.
 *
 * If complex samples are fed to the filter, float samples must not be fed before a {FirFilter#reset}.
 *
 * The keyword argument +buffer:+ can be used to provide the filter with an already allocated buffer for the output.
 *
 * The keyword argument +delayed:+ can be used to provide the filter with an already allocated buffer filled 
 * with the input signal delayed by +order / 2+.
 * If +delayed:+ is not +nil+, then the output is an array containing the filtered signal and the delayed signal.
 *
 * @overload filter(r_sample) 
 *  @param r_sample [Float] a real input sample
 *  @return [Float] a real processed sample
 *
 * @overload filter(c_sample) 
 *  @param c_sample [Complex] a complex input sample
 *  @return [Complex] a complex processed sample
 *
 * @overload filter(r_sample, delayed: true) 
 *  @param r_sample [Float] a real input sample
 *  @param delayed [Boolean, nil] whether to output the delayed sample or not
 *  @return [Array<Float>] a two element array containing the processed sample and the delayed sample
 *
 * @overload filter(c_sample, delayed: true) 
 *  @param c_sample [Complex] a complex input sample
 *  @param delayed [Boolean, nil] whether to output the delayed sample or not
 *  @return [Array<Complex>] a two element array containing the processed sample and the delayed sample
 *
 * @overload filter(float_buffer, buffer: obuf) 
 *  @param float_buffer [PaddleC::FloatBuffer] a buffer of real input samples
 *  @param buffer [PaddleC::FloatBuffer, nil] if provided, the buffer is resized if needed and filled with output samples, then returned
 *  @return [PaddleC::FloatBuffer] a buffer of real processed samples
 *
 * @overload filter(complex_buffer, buffer: obuf) 
 *  @param complex_buffer [PaddleC::ComplexBuffer] a buffer of complex input samples
 *  @param buffer [PaddleC::ComplexBuffer, nil] if provided, the buffer is resized if needed and filled with output samples, then returned
 *  @return [PaddleC::ComplexBuffer] a buffer of complex processed samples
 *
 * @overload filter(float_buffer, buffer: obuf, delayed: true) 
 *  @param float_buffer [PaddleC::FloatBuffer] a buffer of real input samples
 *  @param buffer [PaddleC::FloatBuffer, nil] if provided, the buffer is resized if needed and filled with output samples, then returned
 *  @param delayed [PaddleC::FloatBuffer, Boolean, nil] a boolean or an already allocated buffer
 *  @return [Array<PaddleC::FloatBuffer>] a two element array containing the processed samples and the delayed samples
 *
 * @overload filter(complex_buffer, buffer: obuf, delayed: true) 
 *  @param complex_buffer [PaddleC::ComplexBuffer] a buffer of complex input samples
 *  @param buffer [PaddleC::ComplexBuffer, nil] if provided, the buffer is resized if needed and filled with output samples, then returned
 *  @param delayed [PaddleC::ComplexBuffer, Boolean, nil] a boolean or an already allocated buffer
 *  @return [Array<PaddleC::ComplexBuffer>] a two element array containing the processed samples and the delayed samples
 */
static VALUE paddlec_fir_filter_filter(int argc, VALUE *argv, VALUE self)
{
	pdlc_fir_filter_t *fir;
	VALUE rbSample, rbOptHash;
	VALUE buffer_and_delayed[2] = {Qundef, Qundef};
	const ID kwkeys[2] = {id_buffer, id_delayed};
	VALUE obuf = Qnil, delayed = Qnil;
	VALUE res = Qnil;
	float delayed_float;
	pdlc_complex_t delayed_complex;
	float output_float;
	pdlc_complex_t output_complex;
	const pdlc_buffer_t *ifbuf;
	const pdlc_complex_buffer_t *icbuf;
	pdlc_buffer_t *ofbuf, *dfbuf;
	pdlc_complex_buffer_t *ocbuf, *dcbuf;

	rb_scan_args(argc, argv, "1:", &rbSample, &rbOptHash);

	if (!NIL_P(rbOptHash))
		rb_get_kwargs(rbOptHash, kwkeys, 0, 2, buffer_and_delayed);
	if (buffer_and_delayed[0] != Qundef)
		obuf = buffer_and_delayed[0];
	if (buffer_and_delayed[1] != Qundef)
		delayed = buffer_and_delayed[1];

	fir = paddlec_fir_filter_get_struct(self);

	if (rb_class_of(rbSample) == c_FloatBuffer) {
		if (!NIL_P(obuf) && rb_class_of(obuf) != c_FloatBuffer)
			rb_raise(rb_eArgError, "only a FloatBuffer is valid for buffer when a FloatBuffer is provided to the filter");
		if (delayed != Qtrue && delayed != Qnil && delayed != Qfalse && rb_class_of(obuf) != c_FloatBuffer)
			rb_raise(rb_eArgError, "only true, false, nil and a FloatBuffer are valid values for delayed when a FloatBuffer is provided to the filter");
		ifbuf = paddlec_float_buffer_get_struct(rbSample);

		if (rb_class_of(obuf) != c_FloatBuffer)
			obuf = rb_class_new_instance(0, NULL, c_FloatBuffer);
		ofbuf = paddlec_float_buffer_get_struct(obuf);

		if (delayed == Qnil || delayed == Qfalse) {
			pdlc_fir_filter_filter_float_buffer(fir, ifbuf, ofbuf, NULL);
			res = obuf;
		}
		else {
			if (rb_class_of(delayed) != c_FloatBuffer)
				delayed = rb_class_new_instance(0, NULL, c_FloatBuffer);
			dfbuf = paddlec_float_buffer_get_struct(delayed);
			pdlc_fir_filter_filter_float_buffer(fir, ifbuf, ofbuf, dfbuf);
			res = rb_ary_new_capa(2);
			rb_ary_store(res, 0, obuf);
			rb_ary_store(res, 1, delayed);
		}
	}
	else if (rb_class_of(rbSample) == c_ComplexBuffer) {
		if (!NIL_P(obuf) && rb_class_of(obuf) != c_ComplexBuffer)
			rb_raise(rb_eArgError, "only a ComplexBuffer is valid for buffer when a ComplexBuffer is provided to the filter");
		if (delayed != Qtrue && delayed != Qnil && delayed != Qfalse && rb_class_of(obuf) != c_ComplexBuffer)
			rb_raise(rb_eArgError, "only true, false, nil and a ComplexBuffer are valid values for delayed when a ComplexBuffer is provided to the filter");
		icbuf = paddlec_complex_buffer_get_struct(rbSample);

		if (rb_class_of(obuf) != c_ComplexBuffer)
			obuf = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		ocbuf = paddlec_complex_buffer_get_struct(obuf);

		if (delayed == Qnil || delayed == Qfalse) {
			pdlc_fir_filter_filter_complex_buffer(fir, icbuf, ocbuf, NULL);
			res = obuf;
		}
		else {
			if (rb_class_of(delayed) != c_ComplexBuffer)
				delayed = rb_class_new_instance(0, NULL, c_ComplexBuffer);
			dcbuf = paddlec_complex_buffer_get_struct(delayed);
			pdlc_fir_filter_filter_complex_buffer(fir, icbuf, ocbuf, dcbuf);
			res = rb_ary_new_capa(2);
			rb_ary_store(res, 0, obuf);
			rb_ary_store(res, 1, delayed);
		}
	}
	else {
		if (!NIL_P(obuf))
			rb_raise(rb_eArgError, "output buffer must not be provided when a single sample is given to the filter");
		if (delayed != Qtrue && delayed != Qnil && delayed != Qfalse)
			rb_raise(rb_eArgError, "only true, false and nil are valid values for delayed when a single sample is provided to the filter");
		if (rb_class_of(rbSample) == rb_cComplex) {
			output_complex.real = (float)NUM2DBL(rb_funcallv(rbSample, id_real, 0, NULL));
			output_complex.imag = (float)NUM2DBL(rb_funcallv(rbSample, id_real, 0, NULL));
			if (delayed == Qtrue) {
				output_complex = pdlc_fir_filter_filter_complex(fir, output_complex, &delayed_complex);
				res = rb_ary_new_capa(2);
				rb_ary_store(res, 0, rb_complex_raw(DBL2NUM((double)output_complex.real), DBL2NUM((double)output_complex.imag)));
				rb_ary_store(res, 1, rb_complex_raw(DBL2NUM((double)delayed_complex.real), DBL2NUM((double)delayed_complex.imag)));
			}
			else {
				output_complex = pdlc_fir_filter_filter_complex(fir, output_complex, NULL);
				res = rb_complex_raw(DBL2NUM((double)output_complex.real), DBL2NUM((double)output_complex.imag));
			}
		}
		else {
			if (delayed == Qtrue) {
				output_float = pdlc_fir_filter_filter_float(fir, (float)NUM2DBL(rbSample), &delayed_float);
				res = rb_ary_new_capa(2);
				rb_ary_store(res, 0, DBL2NUM((double)output_float));
				rb_ary_store(res, 1, DBL2NUM((double)delayed_float));
			}
			else {
				output_float = pdlc_fir_filter_filter_float(fir, (float)NUM2DBL(rbSample), NULL);
				res = DBL2NUM((double)output_float);
			}
		}
	}

	return res;
}


/* @return [PaddleC::FirInterpolator]
 * @overload initialize(n, interpolation_factor)
 *  @param n [Integer] the filter order
 *  @param interpolation_factor [Integer] the interpolation factor (>= 1)
 *  @return [PaddleC::FirInterpolator]
 * @overload initialize(b, interpolation_factor)
 *  @param interpolation_factor [Integer] the interpolation factor (>= 1)
 *  @param b [Array<Float>, PaddleC::FloatBuffer] the coefficients, as an array or a {FloatBuffer}
 *  @return [PaddleC::FirInterpolator]
 */
static VALUE paddlec_fir_filter_interpolator_initialize(VALUE self, VALUE b_or_n, VALUE factor)
{
	pdlc_fir_filter_t *fir;
	const pdlc_buffer_t *fbuf;
	int order, i, ifactor;

	ifactor = NUM2INT(factor);
	if (ifactor < 1)
		rb_raise(rb_eRangeError, "Interpolation factor cannot be less than one");
	if (ifactor > 32)
		rb_raise(rb_eRangeError, "Interpolation factor greater than 32 is too much");

	fir = paddlec_fir_filter_get_struct(self);

	if (rb_class_of(b_or_n) == c_FloatBuffer) {
		fbuf = paddlec_float_buffer_get_struct(b_or_n);
		order = fbuf->length;
		if (order < 1)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_interpolator_initialize(fir, order - 1, ifactor);
		for (i = 0; i < order; i++)
			pdlc_fir_filter_set_coef_at(fir, i, fbuf->data[i]);
	}
	else if (rb_class_of(b_or_n) == rb_cArray) {
		order = rb_array_len(b_or_n);
		if (order < 1)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_interpolator_initialize(fir, order - 1, ifactor);
		for (i = 0; i < order; i++)
			pdlc_fir_filter_set_coef_at(fir, i, (float)NUM2DBL(rb_ary_entry(b_or_n, i)));
	}
	else {
		order = NUM2INT(b_or_n);
		if (order < 0)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_interpolator_initialize(fir, order, ifactor);
	}

	return self;
}


/* @return [Integer] the interpolation factor
 */
static VALUE paddlec_fir_filter_interpolator_interpolation_factor(VALUE self)
{
	const pdlc_fir_filter_t *fir;

	fir = paddlec_fir_filter_get_struct(self);

	return INT2NUM(pdlc_fir_filter_get_factor(fir));
}


/* Interpolate a {FloatBuffer} or {ComplexBuffer}.
 * The returned element is of the same type as the input element.
 *
 * If complex samples are fed to the interpolator, float samples must not be fed before a {FirFilter#reset}.
 *
 * The keyword argument +buffer:+ can be used to provide the interpolator with an already allocated buffer for the output.
 *
 * @overload interpolate(float_buffer, buffer: obuf) 
 *  @param float_buffer [PaddleC::FloatBuffer] a buffer of real input samples
 *  @param buffer [PaddleC::FloatBuffer, nil] if provided, the buffer is resized if needed and filled with output samples, then returned
 *  @return [PaddleC::FloatBuffer] a buffer of real processed samples of size +float_buffer.length * #interpolation_factor+
 *
 * @overload interpolate(complex_buffer, buffer: obuf) 
 *  @param complex_buffer [PaddleC::ComplexBuffer] a buffer of complex input samples
 *  @param buffer [PaddleC::ComplexBuffer, nil] if provided, the buffer is resized if needed and filled with output samples, then returned
 *  @return [PaddleC::ComplexBuffer] a buffer of complex processed samples of size +complex_buffer.length * #interpolation_factor+
 */
static VALUE paddlec_fir_filter_interpolator_interpolate(int argc, VALUE *argv, VALUE self)
{
	pdlc_fir_filter_t *fir;
	VALUE rbSample, rbOptHash;
	VALUE buffer_hash[2] = {Qundef};
	const ID kwkeys[2] = {id_buffer};
	VALUE obuf = Qnil;
	VALUE res = Qnil;
	const pdlc_buffer_t *ifbuf;
	const pdlc_complex_buffer_t *icbuf;
	pdlc_buffer_t *ofbuf;
	pdlc_complex_buffer_t *ocbuf;

	rb_scan_args(argc, argv, "1:", &rbSample, &rbOptHash);

	if (!NIL_P(rbOptHash))
		rb_get_kwargs(rbOptHash, kwkeys, 0, 1, buffer_hash);
	if (buffer_hash[0] != Qundef)
		obuf = buffer_hash[0];

	fir = paddlec_fir_filter_get_struct(self);

	if (rb_class_of(rbSample) == c_FloatBuffer) {
		if (!NIL_P(obuf) && rb_class_of(obuf) != c_FloatBuffer)
			rb_raise(rb_eArgError, "only a %"PRIsVALUE" is valid for buffer when a %"PRIsVALUE" is provided to the Interpolator", rb_class_name(c_FloatBuffer), rb_class_name(c_FloatBuffer));
		ifbuf = paddlec_float_buffer_get_struct(rbSample);

		if (rb_class_of(obuf) != c_FloatBuffer)
			obuf = rb_class_new_instance(0, NULL, c_FloatBuffer);
		ofbuf = paddlec_float_buffer_get_struct(obuf);

		pdlc_fir_filter_interpolate_float_buffer(fir, ifbuf, ofbuf);
		res = obuf;
	}
	else if (rb_class_of(rbSample) == c_ComplexBuffer) {
		if (!NIL_P(obuf) && rb_class_of(obuf) != c_ComplexBuffer)
			rb_raise(rb_eArgError, "only a %"PRIsVALUE" is valid for buffer when a %"PRIsVALUE" is provided to the Interpolator", rb_class_name(c_ComplexBuffer), rb_class_name(c_ComplexBuffer));
		icbuf = paddlec_complex_buffer_get_struct(rbSample);

		if (rb_class_of(obuf) != c_ComplexBuffer)
			obuf = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		ocbuf = paddlec_complex_buffer_get_struct(obuf);

		pdlc_fir_filter_interpolate_complex_buffer(fir, icbuf, ocbuf);
		res = obuf;
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(c_ComplexBuffer));

	return res;
}


/* @return [PaddleC::FirDecimator]
 * @overload initialize(n, decimation_factor)
 *  @param n [Integer] the filter order
 *  @param decimation_factor [Integer] the decimation factor (>= 1)
 *  @return [PaddleC::FirDecimator]
 * @overload initialize(b, decimation_factor)
 *  @param b [Array<Float>, PaddleC::FloatBuffer] the coefficients, as an array or a {FloatBuffer}
 *  @param decimation_factor [Integer] the decimation factor (>= 1)
 *  @return [PaddleC::FirDecimator]
 */
static VALUE paddlec_fir_filter_decimator_initialize(VALUE self, VALUE b_or_n, VALUE factor)
{
	pdlc_fir_filter_t *fir;
	const pdlc_buffer_t *fbuf;
	int order, i, ifactor;

	ifactor = NUM2INT(factor);
	if (ifactor < 1)
		rb_raise(rb_eRangeError, "Decimation factor cannot be less than one");
	if (ifactor > 32)
		rb_raise(rb_eRangeError, "Decimation factor greater than 32 is too much");

	fir = paddlec_fir_filter_get_struct(self);

	if (rb_class_of(b_or_n) == c_FloatBuffer) {
		fbuf = paddlec_float_buffer_get_struct(b_or_n);
		order = fbuf->length;
		if (order < 1)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_decimator_initialize(fir, order - 1, ifactor);
		for (i = 0; i < order; i++)
			pdlc_fir_filter_set_coef_at(fir, i, fbuf->data[i]);
	}
	else if (rb_class_of(b_or_n) == rb_cArray) {
		order = rb_array_len(b_or_n);
		if (order < 1)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_decimator_initialize(fir, order - 1, ifactor);
		for (i = 0; i < order; i++)
			pdlc_fir_filter_set_coef_at(fir, i, (float)NUM2DBL(rb_ary_entry(b_or_n, i)));
	}
	else {
		order = NUM2INT(b_or_n);
		if (order < 0)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_decimator_initialize(fir, order, ifactor);
	}

	return self;
}


/* @return [Integer] the decimation factor
 */
static VALUE paddlec_fir_filter_decimator_decimation_factor(VALUE self)
{
	const pdlc_fir_filter_t *fir;

	fir = paddlec_fir_filter_get_struct(self);

	return INT2NUM(pdlc_fir_filter_get_factor(fir));
}


/* Decimate a {FloatBuffer} or {ComplexBuffer}.
 * The returned element is of the same type as the input element.
 *
 * If complex samples are fed to the decimator, float samples must not be fed before a {FirFilter#reset}.
 *
 * The keyword argument +buffer:+ can be used to provide the decimator with an already allocated buffer for the output.
 *
 * @overload decimate(float_buffer, buffer: obuf) 
 *  @param float_buffer [PaddleC::FloatBuffer] a buffer of real input samples
 *  @param buffer [PaddleC::FloatBuffer, nil] if provided, the buffer is resized if needed and filled with output samples, then returned
 *  @return [PaddleC::FloatBuffer] a buffer of real processed samples of size ≈ +float_buffer.length / #decimation_factor+
 *
 * @overload decimate(complex_buffer, buffer: obuf) 
 *  @param complex_buffer [PaddleC::ComplexBuffer] a buffer of complex input samples
 *  @param buffer [PaddleC::ComplexBuffer, nil] if provided, the buffer is resized if needed and filled with output samples, then returned
 *  @return [PaddleC::ComplexBuffer] a buffer of complex processed samples of size ≈ +complex_buffer.length / #decimation_factor+
 */
static VALUE paddlec_fir_filter_decimator_decimate(int argc, VALUE *argv, VALUE self)
{
	pdlc_fir_filter_t *fir;
	VALUE rbSample, rbOptHash;
	VALUE buffer_hash[2] = {Qundef};
	const ID kwkeys[2] = {id_buffer};
	VALUE obuf = Qnil;
	VALUE res = Qnil;
	const pdlc_buffer_t *ifbuf;
	const pdlc_complex_buffer_t *icbuf;
	pdlc_buffer_t *ofbuf;
	pdlc_complex_buffer_t *ocbuf;

	rb_scan_args(argc, argv, "1:", &rbSample, &rbOptHash);

	if (!NIL_P(rbOptHash))
		rb_get_kwargs(rbOptHash, kwkeys, 0, 1, buffer_hash);
	if (buffer_hash[0] != Qundef)
		obuf = buffer_hash[0];

	fir = paddlec_fir_filter_get_struct(self);

	if (rb_class_of(rbSample) == c_FloatBuffer) {
		if (!NIL_P(obuf) && rb_class_of(obuf) != c_FloatBuffer)
			rb_raise(rb_eArgError, "only a %"PRIsVALUE" is valid for buffer when a %"PRIsVALUE" is provided to the decimator", rb_class_name(c_FloatBuffer), rb_class_name(c_FloatBuffer));
		ifbuf = paddlec_float_buffer_get_struct(rbSample);

		if (rb_class_of(obuf) != c_FloatBuffer)
			obuf = rb_class_new_instance(0, NULL, c_FloatBuffer);
		ofbuf = paddlec_float_buffer_get_struct(obuf);

		pdlc_fir_filter_decimate_float_buffer(fir, ifbuf, ofbuf);
		res = obuf;
	}
	else if (rb_class_of(rbSample) == c_ComplexBuffer) {
		if (!NIL_P(obuf) && rb_class_of(obuf) != c_ComplexBuffer)
			rb_raise(rb_eArgError, "only a %"PRIsVALUE" is valid for buffer when a %"PRIsVALUE" is provided to the decimator", rb_class_name(c_ComplexBuffer), rb_class_name(c_ComplexBuffer));
		icbuf = paddlec_complex_buffer_get_struct(rbSample);

		if (rb_class_of(obuf) != c_ComplexBuffer)
			obuf = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		ocbuf = paddlec_complex_buffer_get_struct(obuf);

		pdlc_fir_filter_decimate_complex_buffer(fir, icbuf, ocbuf);
		res = obuf;
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(c_ComplexBuffer));

	return res;
}


/* @return [PaddleC::FirTransformer]
 * @overload initialize(n)
 *  @param n [Integer] the filter order
 *  @return [Paddlec::FirTransformer]
 * @overload initialize(b)
 *  @param b [Array<Float>, PaddleC::FloatBuffer] the coefficients, as an array or a {FloatBuffer}
 *  @return [Paddlec::FirTransformer]
 */
static VALUE paddlec_fir_filter_transformer_initialize(VALUE self, VALUE b_or_n)
{
	pdlc_fir_filter_t *fir;
	const pdlc_buffer_t *fbuf;
	int order, i;

	fir = paddlec_fir_filter_get_struct(self);

	if (rb_class_of(b_or_n) == c_FloatBuffer) {
		fbuf = paddlec_float_buffer_get_struct(b_or_n);
		order = fbuf->length;
		if (order < 1)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_initialize(fir, order - 1);
		for (i = 0; i < order; i++)
			pdlc_fir_filter_set_coef_at(fir, i, fbuf->data[i]);
	}
	else if (rb_class_of(b_or_n) == rb_cArray) {
		order = rb_array_len(b_or_n);
		if (order < 1)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_initialize(fir, order - 1);
		for (i = 0; i < order; i++)
			pdlc_fir_filter_set_coef_at(fir, i, (float)NUM2DBL(rb_ary_entry(b_or_n, i)));
	}
	else {
		order = NUM2INT(b_or_n);
		if (order < 0)
			rb_raise(rb_eArgError, "Negative order");
		pdlc_fir_filter_initialize(fir, order);
	}

	return self;
}


/* Transform a {FloatBuffer} into a {ComplexBuffer}.
 * The real part of the output is the input signal delayed, 
 * and the imaginary part of the output is the input signal filtered.
 *
 * @overload transform(float_buffer, buffer: obuf) 
 * @param float_buffer [PaddleC::FloatBuffer] a buffer of real input samples
 * @param buffer [PaddleC::ComplexBuffer, nil] if provided, the buffer is resized if needed and filled with output samples, then returned
 * @return [PaddleC::ComplexBuffer] a complex buffer whose real part is the original signal delayed, and the imaginary part the signal filtered
 */
static VALUE paddlec_fir_filter_transformer_transform(int argc, VALUE *argv, VALUE self)
{
	pdlc_fir_filter_t *fir;
	VALUE rbSample, rbOptHash;
	VALUE buffer_hash = Qundef;
	const ID kwkeys = id_buffer;
	VALUE obuf = Qnil;
	VALUE res = Qnil;
	const pdlc_buffer_t *ifbuf;
	pdlc_complex_buffer_t *ocbuf;

	rb_scan_args(argc, argv, "1:", &rbSample, &rbOptHash);

	if (!NIL_P(rbOptHash))
		rb_get_kwargs(rbOptHash, &kwkeys, 0, 1, &buffer_hash);
	if (buffer_hash != Qundef)
		obuf = buffer_hash;

	fir = paddlec_fir_filter_get_struct(self);

	if (rb_class_of(rbSample) == c_FloatBuffer) {
		if (!NIL_P(obuf) && rb_class_of(obuf) != c_ComplexBuffer)
			rb_raise(rb_eArgError, "only a %"PRIsVALUE" is valid for buffer", rb_class_name(c_ComplexBuffer));
		ifbuf = paddlec_float_buffer_get_struct(rbSample);

		if (rb_class_of(obuf) != c_ComplexBuffer)
			obuf = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		ocbuf = paddlec_complex_buffer_get_struct(obuf);

		pdlc_fir_filter_transform(fir, ifbuf, ocbuf);
		res = obuf;
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE, rb_class_name(c_FloatBuffer));

	return res;
}



/* Document-class: PaddleC::FirTransformer
 *
 * PaddleC::FirTransformer is a FIR filter with the +#transform+ method. It is mostly implemented for Hilbert transformers.
 *
 * It can process {PaddleC::FloatBuffer} into {PaddleC::ComplexBuffer}.
 */


static void Init_paddlec_fir_transformer()
{
	c_FirTransformer = rb_define_class_under(m_PaddleC, "FirTransformer", c_FirFilter);
	rb_define_method(c_FirTransformer, "initialize", paddlec_fir_filter_transformer_initialize,  1);
	rb_define_method(c_FirTransformer, "transform",  paddlec_fir_filter_transformer_transform,  -1);
}


/* Document-class: PaddleC::FirDecimator
 *
 * A finite impulse response (FIR) decimator (filter + downsample), relying on {PaddleC::FirFilter}.
 *
 * It can process {PaddleC::FloatBuffer} and {PaddleC::ComplexBuffer}.
 */


static void Init_paddlec_fir_decimator()
{
	c_FirDecimator = rb_define_class_under(m_PaddleC, "FirDecimator", c_FirFilter);
	rb_define_method(c_FirDecimator, "initialize",        paddlec_fir_filter_decimator_initialize,        2);
	rb_define_method(c_FirDecimator, "decimation_factor", paddlec_fir_filter_decimator_decimation_factor, 0);
	rb_define_method(c_FirDecimator, "decimate",          paddlec_fir_filter_decimator_decimate,         -1);
}


/* Document-class: PaddleC::FirInterpolator
 *
 * A finite impulse response (FIR) interpolator (upsample + filter), relying on {PaddleC::FirFilter}.
 *
 * It can process {PaddleC::FloatBuffer} and {PaddleC::ComplexBuffer}.
 */


static void Init_paddlec_fir_interpolator()
{
	c_FirInterpolator = rb_define_class_under(m_PaddleC, "FirInterpolator", c_FirFilter);
	rb_define_method(c_FirInterpolator, "initialize",           paddlec_fir_filter_interpolator_initialize,           2);
	rb_define_method(c_FirInterpolator, "interpolation_factor", paddlec_fir_filter_interpolator_interpolation_factor, 0);
	rb_define_method(c_FirInterpolator, "interpolate",          paddlec_fir_filter_interpolator_interpolate,         -1);
}


/* Document-class: PaddleC::FirFilter
 *
 * A finite impulse response (FIR) filter.
 *
 * It can process +Float+, +Complex+, {PaddleC::FloatBuffer} and {PaddleC::ComplexBuffer}.
 *
 * Internal coefficients and state are hold using native arrays of single floats.
 *
 * Deppending on the host architecture, computation may be accelerated using +AVX+, +SSE+ or +NEON+ SIMD instructions.
 */


void Init_paddlec_fir_filter()
{
	c_FirFilter = rb_define_class_under(m_PaddleC, "FirFilter", rb_cObject);

	rb_define_alloc_func(c_FirFilter, paddlec_fir_filter_alloc);
	rb_define_method(c_FirFilter, "initialize",         paddlec_fir_filter_initialize,         1);
	rb_define_method(c_FirFilter, "filter",             paddlec_fir_filter_filter,            -1);
	rb_define_method(c_FirFilter, "order",              paddlec_fir_filter_order,              0);
	rb_define_method(c_FirFilter, "nb_coefficients",    paddlec_fir_filter_nb_coefficients,    0);
	rb_define_method(c_FirFilter, "reset",              paddlec_fir_filter_reset,              0);
	rb_define_method(c_FirFilter, "coefficients",       paddlec_fir_filter_coefficients,       0);
	rb_define_alias(c_FirFilter,  "numerator", "coefficients");
	rb_define_method(c_FirFilter, "denominator",        paddlec_fir_filter_denominator,        0);
	rb_define_method(c_FirFilter, "get_numerator_at",   paddlec_fir_filter_get_coefficient_at, 1);
	rb_define_method(c_FirFilter, "set_numerator_at",   paddlec_fir_filter_set_coefficient_at, 2);
	rb_define_method(c_FirFilter, "get_denominator_at", paddlec_fir_filter_get_denominator_at, 1);
	rb_define_method(c_FirFilter, "set_denominator_at", paddlec_fir_filter_set_denominator_at, 2);

	Init_paddlec_fir_transformer();
	Init_paddlec_fir_decimator();
	Init_paddlec_fir_interpolator();
}


