/* Copyright (C) 2020 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <ruby.h>
#include "libpaddlec.h"
#include "paddlec.h"
#include "float_buffer.h"
#include "complex_buffer.h"

/* Document-class: PaddleC::Delay
 *
 * A delay line. It can be used to balance delays brought by filters.
 *
 * To prevent incoherent data, 
 * a given {PaddleC::Delay} object must be used exclusively with {PaddleC::FloatBuffer} and Float,
 * or {PaddleC::ComplexBuffer} and Complex.
 */


VALUE c_Delay;


static void paddlec_delay_free(void *p)
{
	pdlc_delay_t *del = (pdlc_delay_t*)p;
	pdlc_delay_free(del);
}


static size_t paddlec_delay_size(const void* data)
{
	const pdlc_delay_t *del = (pdlc_delay_t*)data;
	return pdlc_delay_size(del);
}


static const rb_data_type_t paddlec_delay_type = {
	.wrap_struct_name = "paddlec_delay_struct",
	.function = {
		.dmark = NULL,
		.dfree = paddlec_delay_free,
		.dsize = paddlec_delay_size,
	},
	.data = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE paddlec_delay_alloc(VALUE klass)
{
	VALUE obj;
	pdlc_delay_t *del;

	del = pdlc_delay_new(0);
	obj = TypedData_Wrap_Struct(klass, &paddlec_delay_type, del);

	return obj;
}


pdlc_delay_t* paddlec_delay_get_struct(VALUE obj)
{
	pdlc_delay_t *del;
	TypedData_Get_Struct(obj, pdlc_delay_t, &paddlec_delay_type, del);
	return del;
}


/* Return a new {PaddleC::Delay}.
 * @param len [Integer] the number of samples the input will be delayed, must be strictly positive
 * @return [PaddleC::Delay]
 */
static VALUE paddlec_delay_initialize(VALUE self, VALUE len)
{
	int dl;
	pdlc_delay_t *del;

	if (rb_class_of(len) != rb_cInteger)
		rb_raise(rb_eTypeError, "expecting an integer, not a %"PRIsVALUE, rb_class_name(rb_class_of(len)));
	dl = NUM2INT(len);
	if (dl <= 0)
		rb_raise(rb_eRangeError, "delay length must be strictly positive");

	del = paddlec_delay_get_struct(self);
	pdlc_delay_initialize(del, dl);

	return self;
}


/* Reset the state of the delay line.
 * @return [self]
 */
static VALUE paddlec_delay_reset(VALUE self)
{
	pdlc_delay_t *del = paddlec_delay_get_struct(self);
	pdlc_delay_reset(del);
	return self;
}


/* Return the length of the delay line.
 * @return [Integer]
 */
static VALUE paddlec_delay_length(VALUE self)
{
	pdlc_delay_t *del = paddlec_delay_get_struct(self);
	return UINT2NUM(del->delay);
}


/* Push new samples, pull delayed samples.
 * If +outbuf+ is provided, it will receive the delayed data.
 * @overload delay(float)
 *  @param float [Float]
 *  @return [Float]
 * @overload delay(complex)
 *  @param complex [Complex]
 *  @return [Complex]
 * @overload delay(fbuf, outbuf = nil)
 *  @param fbuf [PaddleC::FloatBuffer]
 *  @param outbuf [PaddleC::FloatBuffer, nil]
 *  @return [PaddleC::FloatBuffer]
 * @overload delay(cbuf, outbuf = nil)
 *  @param cbuf [PaddleC::ComplexBuffer]
 *  @param outbuf [PaddleC::ComplexBuffer, nil]
 *  @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_delay_delay(int argc, VALUE *argv, VALUE self)
{
	VALUE in, out;
	pdlc_delay_t *del;
	float flt;
	pdlc_complex_t cmp;
	const pdlc_buffer_t *ifbuf;
	pdlc_buffer_t *ofbuf;
	const pdlc_complex_buffer_t *icbuf;
	pdlc_complex_buffer_t *ocbuf;

	rb_scan_args(argc, argv, "11", &in, &out);

	del = paddlec_delay_get_struct(self);

	if (rb_class_of(in) == rb_cComplex) {
		if (out != Qnil)
			rb_raise(rb_eArgError, "outbuf argument is not used when a scalar is delayed");
		cmp.real = (float)NUM2DBL(rb_funcallv(in, id_real, 0, NULL));
		cmp.imag = (float)NUM2DBL(rb_funcallv(in, id_imag, 0, NULL));
		cmp = pdlc_delay_delay_complex(del, cmp);
		out = rb_complex_new(DBL2NUM((double)cmp.real), DBL2NUM((double)cmp.imag));
	}
	else if (rb_obj_is_kind_of(in, rb_cNumeric)) {
		if (out != Qnil)
			rb_raise(rb_eArgError, "outbuf argument is not used when a scalar is delayed");
		flt = (float)NUM2DBL(in);
		flt = pdlc_delay_delay_float(del, flt);
		out = DBL2NUM((double)flt);
	}
	else if (rb_class_of(in) == c_FloatBuffer) {
		if (out != Qnil) {
			if (rb_class_of(out) != c_FloatBuffer)
				rb_raise(rb_eTypeError, "outbuf must be the same type of buffer than the input");
		}
		else
			out = rb_class_new_instance(0, NULL, c_FloatBuffer);
		ifbuf = paddlec_float_buffer_get_struct(in);
		ofbuf = paddlec_float_buffer_get_struct(out);
		pdlc_delay_delay_float_buffer(del, ifbuf, ofbuf);
	}
	else if (rb_class_of(in) == c_ComplexBuffer) {
		if (out != Qnil) {
			if (rb_class_of(out) != c_ComplexBuffer)
				rb_raise(rb_eTypeError, "outbuf must be the same type of buffer than the input");
		}
		else
			out = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		icbuf = paddlec_complex_buffer_get_struct(in);
		ocbuf = paddlec_complex_buffer_get_struct(out);
		pdlc_delay_delay_complex_buffer(del, icbuf, ocbuf);
	}
	else
		rb_raise(rb_eTypeError, "expecting a float, a complex, a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE,
				rb_class_name(c_FloatBuffer), rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(in)));

	return out;
}


void Init_paddlec_delay()
{
	c_Delay = rb_define_class_under(m_PaddleC, "Delay", rb_cObject);
	rb_define_alloc_func(c_Delay, paddlec_delay_alloc);
	rb_define_method(c_Delay, "initialize",        paddlec_delay_initialize,         1);
	rb_define_method(c_Delay, "lenght",            paddlec_delay_length,             0);
	rb_define_method(c_Delay, "reset",             paddlec_delay_reset,              0);
	rb_define_method(c_Delay, "delay",             paddlec_delay_delay,             -1);
}


