/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PADDLEC_H
#define PADDLEC_H

#include <ruby.h>

extern ID id_each;
extern ID id_collectI;
extern ID id_to_enum;
extern ID id_length;
extern ID id_real;
extern ID id_imag;
extern ID id_buffer;
extern ID id_delayed;
extern ID id_zip;
extern ID id_join;
extern ID id_to_s;
extern ID id_to_gplot;
extern ID id_acos;
extern ID id_acosh;
extern ID id_asin;
extern ID id_asinh;
extern ID id_atan;
extern ID id_atanh;
extern ID id_cbrt;
extern ID id_cos;
extern ID id_cosh;
extern ID id_erf;
extern ID id_erfc;
extern ID id_exp;
extern ID id_log;
extern ID id_log10;
extern ID id_log2;
extern ID id_sin;
extern ID id_sinh;
extern ID id_sqrt;
extern ID id_tan;
extern ID id_tanh;
extern ID id_u8;
extern ID id_s8;
extern ID id_u16;
extern ID id_s16;
extern ID id_u32;
extern ID id_s32;
extern ID id_f32;

extern VALUE m_PaddleC;

extern VALUE c_FFI_Pointer;
extern VALUE o_FFI_Type_FLOAT;

extern VALUE c_FloatBuffer;
extern VALUE c_ComplexBuffer;
extern VALUE c_FirFilter;
extern VALUE c_FirTransformer;
extern VALUE c_FirDecimator;
extern VALUE c_FirInterpolator;

#endif /* PADDLEC_H */
