#!/usr/bin/env ruby

require './paddlec'
require 'roctave'

def max_diff (gold_array, float_buffer)
	diff = gold_array.collect.with_index{|v, i| (v - float_buffer[i]).abs}
	h = {
		max: diff.max,
		average: diff.inject(&:+) / gold_array.length
	}
	h
end

def complex_max_diff (gold_array, float_buffer)
	goldr = gold_array.collect{|v| v.real}
	br = float_buffer.real
	hr = max_diff(goldr, br)
	goldi = gold_array.collect{|v| v.imag}
	bi = float_buffer.imag
	hi = max_diff(goldi, bi)
	[hr, hi]
end

s = File.binread 'output.f32le'

b = PaddleC::FloatBuffer.new s
#b = b[45000...50000]
puts "#{b.length} samples, accelerator: #{PaddleC.accelerator}"
a = b.to_a

n = 1024 + 512
coefs = Roctave.fir1(n, 880*2/32e3)
#coefs = PaddleC::FloatBuffer.new(12, Math::PI).to_a ## 9 10 11
rflt = Roctave::FirFilter.new coefs
pflt = PaddleC::FirFilter.new coefs


start = Time.now
rres = a.collect{|v| rflt.filter(v)}
t_roctave = Time.now - start
puts "Roctave          took #{(t_roctave.round 3).to_s.center 6} s to process      x1      #{((a.length*(n+1)) / t_roctave / 1e6).round} MMAC/s (#{(a.length / t_roctave).round} sample/s)"

start = Time.now
rresd = a.collect{|v| f, d = rflt.filter_with_delay(v); f}
t_roctaved = Time.now - start
puts "Roctave D        took #{(t_roctaved.round 3).to_s.center 6} s to process      x#{(t_roctave / t_roctaved).round 2}   #{((a.length*(n+1)) / t_roctaved / 1e6).round} MMAC/s (#{(a.length / t_roctaved).round} sample/s)"

start = Time.now
pres = b.collect{|v| pflt.filter(v)}
t_paddlec = Time.now - start
puts "PaddleC          took #{(t_paddlec.round 3).to_s.center 6} s to process      x#{(t_roctave / t_paddlec).round 2}   #{((a.length*(n+1)) / t_paddlec / 1e6).round} MMAC/s (#{(a.length / t_paddlec).round} sample/s)"

start = Time.now
presd = b.collect{|v| f, d = pflt.filter(v, delayed: true); f}
t_paddlecd = Time.now - start
puts "PaddleC D        took #{(t_paddlecd.round 3).to_s.center 6} s to process      x#{(t_roctave / t_paddlecd).round 2}   #{((a.length*(n+1)) / t_paddlecd / 1e6).round} MMAC/s (#{(a.length / t_paddlecd).round} sample/s)"

puts "Roctave: #{rres.length}, PaddleC: #{pres.length}" if pres.length != rres.length
puts "Roctaved: #{rresd.length}, PaddleCd: #{presd.length}" if presd.length != rresd.length

#cerr = rres.length.times.collect{|i| pres[i] - rres[i]}
#derr = rresd.length.times.collect{|i| presd[i] - rresd[i]}
#t = Roctave.linspace((45000...50000), b.length)
#Roctave.plot(t, cerr, ';err;', t, derr, ';D err;');

puts
puts "PaddleC error:"
puts pres.count{|v| not(v.finite?)}
pp max_diff(rres, pres)
puts
puts "PaddleCd error:"
puts presd.count{|v| not(v.finite?)}
pp max_diff(rresd, presd)
puts

puts '-'*80
puts 

exit

b = PaddleC::ComplexBuffer.new s
a = b.to_a

n = 1024 + 512
coefs = Roctave.fir1(n, 880*2/32e3)
#coefs = PaddleC::FloatBuffer.new(12, Math::PI).to_a ## 9 10 11
rflt = Roctave::FirFilter.new coefs
pflt = PaddleC::FirFilter.new coefs

start = Time.now
rres = rflt.filter a
t_roctave = Time.now - start
puts "Roctave          took #{(t_roctave.round 3).to_s.center 6} s to process      x1      #{(2*(a.length*(n+1)) / t_roctave / 1e6).round} MMAC/s (#{(a.length / t_roctave).round} complex sample/s)"

start = Time.now
pres = pflt.filter b
t_paddlec = Time.now - start
puts "PaddleC          took #{(t_paddlec.round 3).to_s.center 6} s to process      x#{(t_roctave / t_paddlec).round 2}   #{(2*(a.length*(n+1)) / t_paddlec / 1e6).round} MMAC/s (#{(a.length / t_paddlec).round} complex sample/s)"

puts "Roctave: #{rres.length}, PaddleC: #{pres.length}" if pres.length != rres.length

puts
pp complex_max_diff(rres, pres)
puts




