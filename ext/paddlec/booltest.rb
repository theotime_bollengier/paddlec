#!/usr/bin/env ruby

require './paddlec'

a = [-2, -1, 0, 1, 2].to_float_buffer
b = [-2, -1, 0, 1, 2].to_complex_buffer

puts 'a'
pp a
puts 'b'
pp b
puts '!a'
pp !a
puts '!!a'
pp !!a
puts 'a == 1'
pp a == 1
puts 'a != 1'
pp a != 1
puts 'a == a'
pp a == a
puts 'a == -a'
pp a == -a
puts 'a != -a'
pp a != -a
puts 'a == b'
pp a == b
puts 'a != (1+1i)'
pp a != (1+1i)
