/* Copyright (C) 2019-2020 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <ruby.h>
#include "libpaddlec.h"
#include "paddlec.h"
#include "complex_buffer.h"
#include "float_buffer.h"


/* Document-class: PaddleC::ComplexBuffer
 *
 * A a Ruby object that wraps a native array of complex numbers, as pairs of single floats.
 */


VALUE c_ComplexBuffer;


static void paddlec_complex_buffer_free(void *p)
{
	pdlc_complex_buffer_t *cbuf = (pdlc_complex_buffer_t*)p;
	pdlc_complex_buffer_free(cbuf);
}


static size_t paddlec_complex_buffer_size(const void* data)
{
	pdlc_complex_buffer_t *cbuf = (pdlc_complex_buffer_t*)data;
	return sizeof(pdlc_complex_buffer_t) + cbuf->capacity*sizeof(pdlc_complex_t);
}


static const rb_data_type_t paddlec_complex_buffer_type = {
	.wrap_struct_name = "paddlec_complex_buffer_struct",
	.function = {
		.dmark = NULL,
		.dfree = paddlec_complex_buffer_free,
		.dsize = paddlec_complex_buffer_size,
	},
	.data = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE paddlec_complex_buffer_alloc(VALUE klass)
{
	VALUE obj;
	pdlc_complex_buffer_t *cbuf;

	cbuf = pdlc_complex_buffer_new(0);
	obj = TypedData_Wrap_Struct(klass, &paddlec_complex_buffer_type, cbuf);

	return obj;
}


pdlc_complex_buffer_t* paddlec_complex_buffer_get_struct(VALUE obj)
{
	pdlc_complex_buffer_t *cbuf;
	TypedData_Get_Struct(obj, pdlc_complex_buffer_t, &paddlec_complex_buffer_type, cbuf);
	return cbuf;
}


static inline VALUE paddlec2COMPLEX(pdlc_complex_t c)
{
	return rb_complex_new(DBL2NUM((double)c.real), DBL2NUM((double)c.imag));
}


static inline pdlc_complex_t COMPLEX2paddlec(VALUE c)
{
	pdlc_complex_t res;
	res.real = (float)NUM2DBL(rb_funcallv(c, id_real, 0, NULL));
	res.imag = (float)NUM2DBL(rb_funcallv(c, id_imag, 0, NULL));
	return res;
}


/* @return [PaddleC::ComplexBuffer]
 * @overload initialize(length = 0, value = 0.0)
 *  @param length [Integer] the length of the buffer
 *  @param value  [Numeric] the value which is set to all elements of the buffer
 *  @return [PaddleC::ComplexBuffer]
 * @overload initialize(a)
 *  @param a [Array<Numeric>, PaddleC::ComplexBuffer, PaddleC::FloatBuffer] an array of complex numbers, a ComplexBuffer or a FloatBuffer
 *  @return [PaddleC::ComplexBuffer]
 * @overload initialize(fbreal, fbimag)
 *  @param fbreal [PaddleC::FloatBuffer] a FloatBuffer for the real part
 *  @param fbimag [PaddleC::FloatBuffer] a FloatBuffer for the imaginary part
 *  @return [PaddleC::ComplexBuffer]
 * @overload initialize(str)
 *  @param str [String] a binary string
 *  @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_initialize(int argc, VALUE *argv, VALUE self)
{
	pdlc_complex_buffer_t *cbuf;
	VALUE rblength, rbvalue;
	VALUE elem;
	long length = 0, length2;
	pdlc_complex_t value = {0.0f, 0.0f};
	size_t i;
	const pdlc_complex_buffer_t *ocbuf;
	const pdlc_buffer_t *ofbuf, *ofbuf2;

	cbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &rblength, &rbvalue);

	if (rb_class_of(rblength) == rb_cArray) {
		if (rbvalue != Qnil)
			rb_raise(rb_eArgError, "Not expecting another argument along with the array");
		length = rb_array_len(rblength);
		pdlc_complex_buffer_resize(cbuf, (size_t)length, 0);
		for (i = 0; i < (size_t)length; i++) {
			elem = rb_ary_entry(rblength, i);
			value.real = (float)NUM2DBL(rb_funcallv(elem, id_real, 0, NULL));
			value.imag = (float)NUM2DBL(rb_funcallv(elem, id_imag, 0, NULL));
			cbuf->data[i] = value;
		}
	}
	else if (rb_class_of(rblength) == c_ComplexBuffer) {
		if (rbvalue != Qnil)
			rb_raise(rb_eArgError, "Not expecting another argument along with the ComplexBuffer");
		ocbuf = paddlec_complex_buffer_get_struct(rblength);
		length = (long)ocbuf->length;
		pdlc_complex_buffer_resize(cbuf, (size_t)length, 0);
		memcpy(cbuf->data, ocbuf->data, length*sizeof(pdlc_complex_t));
	}
	else if (rb_class_of(rblength) == c_FloatBuffer) {
		ofbuf = paddlec_float_buffer_get_struct(rblength);
		length = (long)ofbuf->length;
		if (rb_class_of(rbvalue) == c_FloatBuffer) {
			ofbuf2 = paddlec_float_buffer_get_struct(rbvalue);
			length2 = (long)ofbuf->length;
			if (length2 == length) {
				pdlc_complex_buffer_resize(cbuf, (size_t)length, 0);
				for (i = 0; i < (size_t)length; i++) {
					cbuf->data[i].real = ofbuf->data[i];
					cbuf->data[i].imag = ofbuf2->data[i];
				}
			} 
			else {
				pdlc_complex_buffer_resize(cbuf, (size_t)((length > length2) ? length : length2), 1);
				for (i = 0; i < (size_t)length; i++)
					cbuf->data[i].real = ofbuf->data[i];
				for (i = 0; i < (size_t)length2; i++)
					cbuf->data[i].imag = ofbuf2->data[i];
			}
		}
		else if (rbvalue != Qnil) {
			rb_raise(rb_eArgError, "Not expecting another argument along with the FloatBuffer");
		}
		else {
			pdlc_complex_buffer_resize(cbuf, (size_t)length, 0);
			for (i = 0; i < (size_t)length; i++) {
				cbuf->data[i].real = ofbuf->data[i];
				cbuf->data[i].imag = 0.0f;
			}
		}
	}
	else if (rb_class_of(rblength) == rb_cString) {
		length = (long)RSTRING_LEN(rblength) / sizeof(pdlc_complex_t);
		pdlc_complex_buffer_resize(cbuf, (size_t)length, 0);
		memcpy(cbuf->data, StringValuePtr(rblength), length*sizeof(pdlc_complex_t));
	}
	else {
		if (rblength != Qnil)
			length = NUM2LONG(rblength);
		if (rbvalue != Qnil) {
			value.real = (float)NUM2DBL(rb_funcallv(rbvalue, id_real, 0, NULL));
			value.imag = (float)NUM2DBL(rb_funcallv(rbvalue, id_imag, 0, NULL));
		}
		if (length < 0)
			rb_raise(rb_eArgError, "negative buffer size");

		if (length > 0) {
			if (value.real != 0.0f || value.imag != 0.0f) {
				pdlc_complex_buffer_resize(cbuf, (size_t)length, 0);
				pdlc_complex_buffer_set(cbuf, value);
			}
			else
				pdlc_complex_buffer_resize(cbuf, (size_t)length, 1);
		}
	}

	return self;
}


/* Change the length of the buffer
 * @param new_length [Integer] the new length of the buffer, as a positive integer
 * @return [self] +self+
 */
static VALUE paddlec_complex_buffer_resize(VALUE self, VALUE new_length)
{
	pdlc_complex_buffer_t *cbuf;
	long length;

	length = NUM2LONG(new_length);
	if (length < 0)
		rb_raise(rb_eArgError, "New length cannot be less than 0");

	cbuf = paddlec_complex_buffer_get_struct(self);

	pdlc_complex_buffer_resize(cbuf, (size_t)length, 1);

	return self;
}


/* @return [Array<Float>]
 */
static VALUE paddlec_complex_buffer_to_a(VALUE self)
{
	size_t i;
	pdlc_complex_buffer_t *cbuf;
	VALUE ar;

	cbuf = paddlec_complex_buffer_get_struct(self);
	ar = rb_ary_new_capa(cbuf->length);
	for (i = 0; i < cbuf->length; i++)
		rb_ary_store(ar, i, rb_complex_raw(DBL2NUM((double)cbuf->data[i].real), DBL2NUM((double)cbuf->data[i].imag)));

	return ar;
}


/* @return [Integer] the length of the buffer
 */
static VALUE paddlec_complex_buffer_length(VALUE self)
{
	pdlc_complex_buffer_t *cbuf = paddlec_complex_buffer_get_struct(self);
	return ULONG2NUM(cbuf->length);
}


/* @return [Boolean] true if length < 1
 */
static VALUE paddlec_complex_buffer_isempty(VALUE self)
{
	pdlc_complex_buffer_t *cbuf = paddlec_complex_buffer_get_struct(self);
	return ((cbuf->length < 1) ? Qtrue : Qfalse);
}


static VALUE paddlec_complex_buffer_to_enum_get_size_block(VALUE block_arg, VALUE data, int argc, VALUE* argv)
{
	/* block_arg will be the first yielded value */
	/* data will be the last argument you passed to rb_block_call */
	/* if multiple values are yielded, use argc/argv to access them */
	(void)block_arg;
	(void)argc;
	(void)argv;
	return rb_funcallv(data, id_length, 0, NULL);
}


/* Calls the given block once for each element in +self+, 
 * passing that element as a parameter. 
 * Returns the array itself, or, if no block is given, an Enumerator is returned.
 * @return [Object, Enumerator]
 * @overload each{|item| block}
 *  @return [Array<Float>] self
 * @overload each
 *  @return [Enumerator]
 */
static VALUE paddlec_complex_buffer_each(VALUE self)
{
	size_t i;
	pdlc_complex_buffer_t *cbuf;
	VALUE res;
	VALUE each_sym;

	cbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_block_given_p()) {
		for (i = 0; i < cbuf->length; i++)
			rb_yield(rb_complex_raw(DBL2NUM((double)cbuf->data[i].real), DBL2NUM((double)cbuf->data[i].imag)));
		res = self;
	}
	else {
		each_sym = ID2SYM(id_each);
		res = rb_block_call(self, id_to_enum, 1, &each_sym, paddlec_complex_buffer_to_enum_get_size_block, self);
	}

	return res;
}


/* Invokes the given block once for each element of self, 
 * replacing the element with the value returned by the block.
 * If no block is given, an Enumerator is returned instead.
 * @overload collect!{|value| block}
 * @return [self]
 * @overload collect!
 * @return [Enumerator]
 */
static VALUE paddlec_complex_buffer_collect_inp(VALUE self)
{
	pdlc_complex_buffer_t *cbuf;
	size_t i;
	VALUE res, elem;
	VALUE collectI_sym;

	cbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_block_given_p()) {
		for (i = 0; i < cbuf->length; i++) {
			elem = rb_yield(rb_complex_raw(DBL2NUM((double)cbuf->data[i].real), DBL2NUM((double)cbuf->data[i].imag)));
			cbuf->data[i].real = (float)NUM2DBL(rb_funcallv(elem, id_real, 0, NULL));
			cbuf->data[i].imag = (float)NUM2DBL(rb_funcallv(elem, id_imag, 0, NULL));
		}
		res = self;
	}
	else {
		collectI_sym = ID2SYM(id_collectI);
		res = rb_block_call(self, id_to_enum, 1, &collectI_sym, paddlec_complex_buffer_to_enum_get_size_block, self);
	}

	return res;
}


/* Returns a new ComplexBuffer with the same content as +self+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_clone(VALUE self)
{
	const pdlc_complex_buffer_t *cbuf;
	pdlc_complex_buffer_t *newcbuf;
	VALUE newComplexBuffer;
	VALUE len;

	cbuf = paddlec_complex_buffer_get_struct(self);
	len = rb_funcallv(self, id_length, 0, NULL);

	newComplexBuffer = rb_class_new_instance(1, &len, c_ComplexBuffer);
	newcbuf = paddlec_complex_buffer_get_struct(newComplexBuffer);
	memcpy(newcbuf->data, cbuf->data, cbuf->length*sizeof(pdlc_complex_t));

	return newComplexBuffer;
}


/* Element Reference -- Returns the element at index, 
 * or returns a sub-buffer starting at the start index and continuing for length elements, 
 * or returns a sub-buffer specified by range of indices.
 *
 * Negative indices count backward from the end of the buffer (-1 is the last element). 
 * Returns nil if the index (or starting index) are out of range.
 *
 * @overload [](index)
 *  @param index [Integer] the index of the element
 *  @return [Complex, nil] the element at +index+, or +nil+ if index out of bound
 * @overload [](start, length)
 *  @param start [Integer] the start index of the sub-buffer
 *  @param length [Integer] the length of the sub-buffer
 *  @return [PaddleC::ComplexBuffer, nil] a new ComplexBuffer, or +nil+ if indexes are out of bound
 * @overload [](range)
 *  @param range [Range] the range of indexes of the sub-buffer
 *  @return [PaddleC::ComplexBuffer, nil] a new ComplexBuffer, or +nil+ if indexes are out of bound
 */
static VALUE paddlec_complex_buffer_get(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *cbuf;
	pdlc_complex_buffer_t *ncbuf;
	VALUE rbind_start_range;
	VALUE rblength;
	long ind_start;
	long ind_end;
	long length;
	VALUE res = Qnil;
	VALUE beg, end; 
	int exc;

	cbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "11", &rbind_start_range, &rblength);

	if (rblength == Qnil && rb_range_values(rbind_start_range, &beg, &end, &exc)) {
		ind_start = NUM2LONG(beg);
		if (ind_start < 0)
			ind_start = (long)cbuf->length + ind_start;
		if (ind_start < 0 || ind_start >= (long)cbuf->length)
			return Qnil;
		ind_end = NUM2LONG(end);
		if (ind_end < 0)
			ind_end = (long)cbuf->length + ind_end;
		if (exc)
			ind_end -= 1;
		length = ind_end + 1 - ind_start;
		if (length < 0)
			length = 0;
	}
	else {
		ind_start = NUM2LONG(rbind_start_range);
		if (ind_start < 0)
			ind_start = (long)cbuf->length + ind_start;
		if (ind_start < 0 || ind_start >= (long)cbuf->length)
			return Qnil;
		if (rblength == Qnil)
			return rb_complex_raw(DBL2NUM((double)cbuf->data[ind_start].real), DBL2NUM((double)cbuf->data[ind_start].imag));
		length = NUM2LONG(rblength);
		if (length < 0)
			return Qnil;
	}

	if ((ind_start + length) > (long)cbuf->length)
		length = (long)cbuf->length - ind_start;
	rblength = LONG2NUM(length);

	res = rb_class_new_instance(1, &rblength, c_ComplexBuffer);

	ncbuf = paddlec_complex_buffer_get_struct(res);
	memcpy(ncbuf->data, cbuf->data + ind_start, length*sizeof(pdlc_complex_t));

	return res;
}


/* Element Assignment -- Sets the element at index, 
 * or replaces a subarray from the start index for length elements, 
 * or replaces a subarray specified by the range of indices.
 *
 * Negative indices count backward from the end of the buffer (-1 is the last element). 
 * An exception is raised if indexes are out of bounds. 
 *
 * @overload []=(index, value)
 *  @param index [Integer] the index of the element
 *  @param value [Numeric] the element to set at +index+
 * @overload []=(start, length, vaule)
 *  @param start [Integer] the start index of the sub-buffer
 *  @param length [Integer] the length of the sub-buffer
 *  @param value [Numeric, Array<Numeric>, ComplexBuffer, FloatBuffer] the element to set at +index+
 * @overload []=(range, value)
 *  @param range [Range] the range of indexes of the sub-buffer
 *  @param value [Numeric, Array<Numeric>, ComplexBuffer, FloatBuffer] the element to set at +index+
 */
static VALUE paddlec_complex_buffer_set(int argc, VALUE *argv, VALUE self)
{
	pdlc_complex_buffer_t *cbuf;
	const pdlc_complex_buffer_t *ocbuf;
	const pdlc_buffer_t *ofbuf;
	VALUE rbind_start_range;
	VALUE rblength;
	VALUE rbvalue;
	VALUE elem;
	long ind_start, length, i, j, ind_end;
	VALUE beg, end; 
	int exc;
	pdlc_complex_t value;

	cbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "111", &rbind_start_range, &rblength, &rbvalue);

	if (rblength == Qnil && rb_range_values(rbind_start_range, &beg, &end, &exc)) {
		ind_start = NUM2LONG(beg);
		if (ind_start < 0)
			ind_start = (long)cbuf->length + ind_start;
		if (ind_start < 0 || ind_start >= (long)cbuf->length)
			rb_raise(rb_eIndexError, "index out of bounds");
		ind_end = NUM2LONG(end);
		if (ind_end < 0)
			ind_end = (long)cbuf->length + ind_end;
		if (exc)
			ind_end -= 1;
		length = ind_end + 1 - ind_start;
	}
	else {
		ind_start = NUM2LONG(rbind_start_range);
		if (ind_start < 0)
			ind_start = (long)cbuf->length + ind_start;
		if (ind_start < 0 || ind_start >= (long)cbuf->length)
			rb_raise(rb_eIndexError, "index out of bounds");
		if (rblength == Qnil)
			length = 1;
		else
			length = NUM2LONG(rblength);
	}
	if (length < 1)
		rb_raise(rb_eIndexError, "index range maps to %ld..%ld which does not have a strictly positive length", ind_start, ind_start + length - 1);
	if ((ind_start + length) > (long)cbuf->length)
		rb_raise(rb_eIndexError, "index range maps to %ld..%ld which exceeds the buffer length [0..%lu]", ind_start, ind_start + length - 1, cbuf->length - 1);

	if (rb_class_of(rbvalue) == rb_cArray) {
		if (length != rb_array_len(rbvalue))
			rb_raise(rb_eArgError, "trying to assign a %ld long array to a %ld long sub-buffer", rb_array_len(rbvalue), length);
		for (j = 0, i = ind_start; j < length; i++, j++) {
			elem = rb_ary_entry(rbvalue, j);
			cbuf->data[i].real = (float)NUM2DBL(rb_funcallv(elem, id_real, 0, NULL));
			cbuf->data[i].imag = (float)NUM2DBL(rb_funcallv(elem, id_imag, 0, NULL));
		}
	}
	else if (rb_class_of(rbvalue) == c_ComplexBuffer) {
		ocbuf = paddlec_complex_buffer_get_struct(rbvalue);
		if (length != (long)ocbuf->length)
			rb_raise(rb_eArgError, "trying to assign a %lu long %"PRIsVALUE" to a %ld long sub-buffer", ocbuf->length, rb_class_name(rb_class_of(rbvalue)), length);
		for (j = 0, i = ind_start; j < length; i++, j++)
			cbuf->data[i] = ocbuf->data[j];
	}
	else if (rb_class_of(rbvalue) == c_FloatBuffer) {
		ofbuf = paddlec_float_buffer_get_struct(rbvalue);
		if (length != (long)ofbuf->length)
			rb_raise(rb_eArgError, "trying to assign a %lu long %"PRIsVALUE" to a %ld long sub-buffer", ofbuf->length, rb_class_name(rb_class_of(rbvalue)), length);
		for (j = 0, i = ind_start; j < length; i++, j++) {
			cbuf->data[i].real = ofbuf->data[j];
			cbuf->data[i].imag = 0.0f;
		}
	}
	else {
		value.real = (float)NUM2DBL(rb_funcallv(rbvalue, id_real, 0, NULL));
		value.imag = (float)NUM2DBL(rb_funcallv(rbvalue, id_imag, 0, NULL));
		ind_end = ind_start + length;
		for (i = ind_start; i < ind_end; i++)
			cbuf->data[i] = value;
	}

	return rbvalue;
}


#define PADDLEC_COMPLEX_BUFFER_MAX_TO_S_ELEMS 512
/* @return [String]
 */
static VALUE paddlec_complex_buffer_to_s(VALUE self)
{
	const pdlc_complex_buffer_t *cbuf;
	size_t nb_elm_to_print;
	char *cstr, *p;
	size_t i;
	size_t avail;
	VALUE str;

	cbuf = paddlec_complex_buffer_get_struct(self);

	nb_elm_to_print = cbuf->length;
	if (nb_elm_to_print > PADDLEC_COMPLEX_BUFFER_MAX_TO_S_ELEMS)
		nb_elm_to_print = PADDLEC_COMPLEX_BUFFER_MAX_TO_S_ELEMS;

	avail = 32*nb_elm_to_print;
	cstr = malloc(avail+64);
	p = cstr;
	sprintf(p, "["); p += strlen(p);
	if (nb_elm_to_print > 0) {
		switch (fpclassify(cbuf->data[0].real)) {
			case FP_NORMAL:
			case FP_SUBNORMAL:
			case FP_ZERO:
				sprintf(p, "(%.7g", cbuf->data[0].real); 
				break;
			case FP_INFINITE:
				sprintf(p, "(%sInfinity", (cbuf->data[0].real < 0.0f) ? "-" : ""); 
				break;
			default:
				sprintf(p, "(NaN"); 
		}
		p += strlen(p);
		switch (fpclassify(cbuf->data[0].imag)) {
			case FP_NORMAL:
			case FP_SUBNORMAL:
			case FP_ZERO:
				sprintf(p, "%+.7gi)", cbuf->data[0].imag); 
				break;
			case FP_INFINITE:
				sprintf(p, "%sInfinity*i)", (cbuf->data[0].imag < 0.0f) ? "-" : "+"); 
				break;
			default:
				sprintf(p, "+NaN*i)"); 
		}
		p += strlen(p);
	}
	i = 1;
	while ((size_t)(p - cstr) < avail && i < cbuf->length) {
		switch (fpclassify(cbuf->data[i].real)) {
			case FP_NORMAL:
			case FP_SUBNORMAL:
			case FP_ZERO:
				sprintf(p, ", (%.7g", cbuf->data[i].real); 
				break;
			case FP_INFINITE:
				sprintf(p, ", (%sInfinity", (cbuf->data[i].real < 0.0f) ? "-" : ""); 
				break;
			default:
				sprintf(p, ", (NaN"); 
		}
		p += strlen(p);
		switch (fpclassify(cbuf->data[i].imag)) {
			case FP_NORMAL:
			case FP_SUBNORMAL:
			case FP_ZERO:
				sprintf(p, "%+.7gi)", cbuf->data[i].imag); 
				break;
			case FP_INFINITE:
				sprintf(p, "%sInfinity*i)", (cbuf->data[i].imag < 0.0f) ? "-" : "+"); 
				break;
			default:
				sprintf(p, "+NaN*i)");
		}
		p += strlen(p);
		i++;
	}
	if (i < cbuf->length) {
		sprintf(p, ", ...(+%lu)", cbuf->length - i); 
		p += strlen(p);
	}
	sprintf(p, "]");
	p += 1;

	str = rb_str_new(cstr, (long)(p - cstr));

	free(cstr);

	return str;
}


/* Packs the contents of the buffer into a binary string, according to +format+.
 * If the optional +outbuf+ is present, it must reference a String, which will receive the data.
 * In this case, +outbuf will be resized accordingly.
 * @return [String] the content of the buffer as a binary string
 * @overload pack(format, outbuf = nil)
 * @param format [Symbol] either +:u8+, +:s8+, +:u16+, +:s16+, +:u32+, +:s328+ or +:f32+
 * @param outbuf [String, nil]
 */
static VALUE paddlec_complex_buffer_pack(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *cbuf;
	VALUE str, format;
	size_t i, len, slen;
	ID fmt;
	uint8_t  *u8;
	int8_t   *s8;
	uint16_t *u16;
	int16_t  *s16;
	uint32_t *u32;
	int32_t  *s32;
	float    *f32;

	rb_scan_args(argc, argv, "11", &format, &str);

	cbuf = paddlec_complex_buffer_get_struct(self);
	len = cbuf->length;

	if (rb_class_of(format) != rb_cSymbol)
		rb_raise(rb_eTypeError, "format must be a symbol, not a %"PRIsVALUE, rb_class_name(rb_class_of(format)));
	fmt = SYM2ID(format);
	if (fmt == id_u8 || fmt == id_s8)
		slen = len * 2;
	else if (fmt == id_u16 || fmt == id_s16)
		slen = len * 4;
	else if (fmt == id_u32 || fmt == id_s32 || fmt == id_f32)
		slen = len * 8;
	else
		rb_raise(rb_eTypeError, "format must be either :u8, :s8, :u16, :s16, :u32, :s32 or :f32, not %"PRIsVALUE, format);

	if (str == Qnil)
		str = rb_usascii_str_new_cstr("");
	else if (!rb_obj_is_kind_of(str, rb_cString))
		rb_raise(rb_eTypeError, "expecting a string, not a %"PRIsVALUE, rb_class_name(rb_class_of(str)));
	str = rb_str_resize(str, slen);

	if (fmt == id_u8) {
		u8 = (uint8_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++) {
			u8[i*2+0] = (uint8_t)fminf(255.0f, fmaxf(0.0f, (cbuf->data[i].real * 128.0f) + 128.0f));
			u8[i*2+1] = (uint8_t)fminf(255.0f, fmaxf(0.0f, (cbuf->data[i].imag * 128.0f) + 128.0f));
		}
	}
	else if (fmt == id_s8) {
		s8 = (int8_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++) {
			s8[i*2+0] = (int8_t)fminf(127.0f, fmaxf(-128.0f, cbuf->data[i].real * 128.0f));
			s8[i*2+1] = (int8_t)fminf(127.0f, fmaxf(-128.0f, cbuf->data[i].imag * 128.0f));
		}
	}
	else if (fmt == id_u16) {
		u16 = (uint16_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++) {
			u16[i*2+0] = (uint16_t)fminf(65535.0f, fmaxf(0.0f, (cbuf->data[i].real * 32768.0f) + 32768.0f));
			u16[i*2+1] = (uint16_t)fminf(65535.0f, fmaxf(0.0f, (cbuf->data[i].imag * 32768.0f) + 32768.0f));
		}
	}
	else if (fmt == id_s16) {
		s16 = (int16_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++) {
			s16[i*2+0] = (uint16_t)fminf(32767.0f, fmaxf(-32768.0f, cbuf->data[i].real * 32768.0f));
			s16[i*2+1] = (uint16_t)fminf(32767.0f, fmaxf(-32768.0f, cbuf->data[i].imag * 32768.0f));
		}
	}
	else if (fmt == id_u32) {
		u32 = (uint32_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++) {
			u32[i*2+0] = (uint32_t)fminf(4294967295.0f, fmaxf(0.0f, (cbuf->data[i].real * 2147483648.0f) + 2147483648.0f));
			u32[i*2+1] = (uint32_t)fminf(4294967295.0f, fmaxf(0.0f, (cbuf->data[i].imag * 2147483648.0f) + 2147483648.0f));
		}
	}
	else if (fmt == id_s32) {
		s32 = (int32_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++) {
			s32[i*2+0] = (int32_t)fminf(2147483647.0f, fmaxf(-2147483648.0f, cbuf->data[i].real * 2147483648.0f));
			s32[i*2+1] = (int32_t)fminf(2147483647.0f, fmaxf(-2147483648.0f, cbuf->data[i].imag * 2147483648.0f));
		}
	}
	else if (fmt == id_f32) {
		f32 = (float*)rb_string_value_ptr(&str);
		memcpy(f32, cbuf->data, len*8);
	}

	return str;
}


/* Unpack the string +str+ into +self+ according to +format+.
 * The complex buffer is resized accordingly.
 * Real and imaginary parts of elements of +self+ will be in the range [-1, 1[;
 * @param str [String]
 * @param format [Symbol] either +:u8+, +:s8+, +:u16+, +:s16+, +:u32+, +:s328+ or +:f32+
 * @return [self]
 */
static VALUE paddlec_complex_buffer_unpack(VALUE self, VALUE str, VALUE format)
{
	pdlc_complex_buffer_t *cbuf;
	ID fmt;
	const void *ptr;
	const uint8_t  *u8;
	const  int8_t  *s8;
	const uint16_t *u16;
	const  int16_t *s16;
	const uint32_t *u32;
	const  int32_t *s32;
	const    float *f32;
	size_t i, len;

	if (!rb_obj_is_kind_of(str, rb_cString))
		rb_raise(rb_eTypeError, "expecting a string, not a %"PRIsVALUE, rb_class_name(rb_class_of(str)));
	if (rb_class_of(format) != rb_cSymbol)
		rb_raise(rb_eTypeError, "format must be a symbol, not a %"PRIsVALUE, rb_class_name(rb_class_of(format)));
	fmt = SYM2ID(format);
	ptr = rb_string_value_ptr(&str);
	len = (size_t)RSTRING_LEN(str);

	cbuf = paddlec_complex_buffer_get_struct(self);

	if (fmt == id_u8) {
		u8 = ptr;
		len /= 2;
		pdlc_complex_buffer_resize(cbuf, len, 0);
		for (i = 0; i < len; i++) {
			cbuf->data[i].real = ((float)u8[i*2+0] - 128.0f) / 128.0f;
			cbuf->data[i].imag = ((float)u8[i*2+1] - 128.0f) / 128.0f;
		}
	}
	else if (fmt == id_s8) {
		s8 = ptr;
		len /= 2;
		pdlc_complex_buffer_resize(cbuf, len, 0);
		for (i = 0; i < len; i++) {
			cbuf->data[i].real = (float)s8[i*2+0] / 128.0f;
			cbuf->data[i].imag = (float)s8[i*2+1] / 128.0f;
		}
	}
	else if (fmt == id_u16) {
		u16 = ptr;
		len /= 4;
		pdlc_complex_buffer_resize(cbuf, len, 0);
		for (i = 0; i < len; i++) {
			cbuf->data[i].real = ((float)u16[i*2+0] - 32768.0f) / 32768.0f;
			cbuf->data[i].imag = ((float)u16[i*2+1] - 32768.0f) / 32768.0f;
		}
	}
	else if (fmt == id_s16) {
		s16 = ptr;
		len /= 4;
		pdlc_complex_buffer_resize(cbuf, len, 0);
		for (i = 0; i < len; i++) {
			cbuf->data[i].real = (float)s16[i*2+0] / 32768.0f;
			cbuf->data[i].imag = (float)s16[i*2+1] / 32768.0f;
		}
	}
	else if (fmt == id_u32) {
		u32 = ptr;
		len /= 8;
		pdlc_complex_buffer_resize(cbuf, len, 0);
		for (i = 0; i < len; i++) {
			cbuf->data[i].real = ((float)u32[i*2+0] - 2147483648.0f) / 2147483648.0f;
			cbuf->data[i].imag = ((float)u32[i*2+1] - 2147483648.0f) / 2147483648.0f;
		}
	}
	else if (fmt == id_s32) {
		s32 = ptr;
		len /= 8;
		pdlc_complex_buffer_resize(cbuf, len, 0);
		for (i = 0; i < len; i++) {
			cbuf->data[i].real = (float)s32[i*2+0] / 2147483648.0f;
			cbuf->data[i].imag = (float)s32[i*2+1] / 2147483648.0f;
		}
	}
	else if (fmt == id_f32) {
		f32 = ptr;
		len /= 8;
		pdlc_complex_buffer_resize(cbuf, len, 0);
		memcpy(cbuf->data, f32, len*8);
	}
	else
		rb_raise(rb_eTypeError, "format must be either :u8, :s8, :u16, :s16, :u32, :s32 or :f32, not %"PRIsVALUE, format);

	return self;
}


/* Unpack the string +str+ into a new {PaddleC::ComplexBuffer} according to +format+.
 * The float buffer is resized accordingly.
 * Elements of +self+ will be in the range [-1, 1[;
 * @param str [String]
 * @param format [Symbol] either +:u8+, +:s8+, +:u16+, +:s16+, +:u32+, +:s328+ or +:f32+
 * @return [PaddleC::ComplexBuffer] a new {PaddleC::ComplexBuffer} filled with values from +str+
 */
static VALUE paddlec_complex_buffer_classunpack(VALUE self, VALUE str, VALUE format)
{
	VALUE res = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	paddlec_complex_buffer_unpack(res, str, format);
	return res;
}


/* Returns the native pointer pointing to the array of floats as an integer
 * @return [Integer] native pointer to the data
 */
static VALUE paddlec_complex_buffer_native_ptr(VALUE self)
{
	const pdlc_complex_buffer_t *cbuf;
	void *ptr;
	VALUE res;

	cbuf = paddlec_complex_buffer_get_struct(self);

	ptr = cbuf->data;
	res = ULL2NUM( (sizeof(ptr) == 4) ? (unsigned long long int)(unsigned long int)ptr : (unsigned long long int)ptr );

	return res;
}


/* Returns a FFI::Pointer pointing to the native array of floats.
 * +FFI+ must be requiered +paddlec+ for this method to be defined.
 * @return [FFI::Pointer] FFI::Pointer pointing to the native array of floats
 */
static VALUE paddlec_complex_buffer_ffi_pointer(VALUE self)
{
	const pdlc_complex_buffer_t *cbuf;
	VALUE type_and_addr[2];
	VALUE res;

	cbuf = paddlec_complex_buffer_get_struct(self);

	type_and_addr[0] = o_FFI_Type_FLOAT;
	type_and_addr[1] = paddlec_complex_buffer_native_ptr(self);
	res = rb_class_new_instance(2, type_and_addr, c_FFI_Pointer);
	res = rb_funcall(res, rb_intern("slice"), 2, LONG2NUM(0), ULONG2NUM(cbuf->length*sizeof(pdlc_complex_t)));

	return res;
}


/* @return [self]
 */
static VALUE paddlec_complex_buffer_to_complex_buffer(VALUE self)
{
	return self;
}


/* @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_array_to_complex_buffer(VALUE self)
{
	return rb_class_new_instance(1, &self, c_ComplexBuffer);
}


// /* Returns the real part.
//  * @return [PaddleC::FloatBuffer] the real part of the buffer
//  * @overload real(buffer = nil)
//  *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
//  */
// static VALUE paddlec_complex_buffer_real(int argc, VALUE *argv, VALUE self)
// {
// 	const pdlc_complex_buffer_t *cbuf;
// 	pdlc_buffer_t *ofbuf;
// 	VALUE buffer;
// 
// 	rb_scan_args(argc, argv, "01", &buffer);
// 
// 	if (buffer == Qnil)
// 		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
// 	else if (rb_class_of(buffer) != c_FloatBuffer)
// 		rb_raise(rb_eTypeError, "wrong argument type %"PRIsVALUE" (expected nil or %"PRIsVALUE")", rb_class_name(rb_class_of(buffer)), rb_class_name(c_FloatBuffer));
// 	ofbuf = paddlec_float_buffer_get_struct(buffer);
// 	cbuf = paddlec_complex_buffer_get_struct(self);
// 
// 	pdlc_complex_buffer_real(cbuf, ofbuf);
// 
// 	return buffer;
// }
// 
// 
// /* Returns the imaginary part.
//  * @return [PaddleC::FloatBuffer] the real imaginary of the buffer
//  * @overload imaginary(buffer = nil)
//  * @overload imag(buffer = nil)
//  *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
//  */
// static VALUE paddlec_complex_buffer_imag(int argc, VALUE *argv, VALUE self)
// {
// 	const pdlc_complex_buffer_t *cbuf;
// 	pdlc_buffer_t *ofbuf;
// 	VALUE buffer;
// 
// 	rb_scan_args(argc, argv, "01", &buffer);
// 
// 	if (buffer == Qnil)
// 		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
// 	else if (rb_class_of(buffer) != c_FloatBuffer)
// 		rb_raise(rb_eTypeError, "wrong argument type %"PRIsVALUE" (expected nil or %"PRIsVALUE")", rb_class_name(rb_class_of(buffer)), rb_class_name(c_FloatBuffer));
// 	ofbuf = paddlec_float_buffer_get_struct(buffer);
// 	cbuf = paddlec_complex_buffer_get_struct(self);
// 
// 	pdlc_complex_buffer_imag(cbuf, ofbuf);
// 
// 	return buffer;
// }
// 
// 
// // /* Returns the absolute part of its polar form.
// //  * @return [PaddleC::FloatBuffer] the absolute value
// //  * @overload magnitude(buffer = nil)
// //  * @overload abs(buffer = nil)
// //  *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
// //  */
// // static VALUE paddlec_complex_buffer_abs(int argc, VALUE *argv, VALUE self)
// // {
// // 	const pdlc_complex_buffer_t *cbuf;
// // 	pdlc_buffer_t *ofbuf;
// // 	VALUE buffer;
// // 
// // 	rb_scan_args(argc, argv, "01", &buffer);
// // 
// // 	if (buffer == Qnil)
// // 		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
// // 	else if (rb_class_of(buffer) != c_FloatBuffer)
// // 		rb_raise(rb_eTypeError, "wrong argument type %"PRIsVALUE" (expected nil or %"PRIsVALUE")", rb_class_name(rb_class_of(buffer)), rb_class_name(c_FloatBuffer));
// // 	ofbuf = paddlec_float_buffer_get_struct(buffer);
// // 	cbuf = paddlec_complex_buffer_get_struct(self);
// // 
// // 	pdlc_complex_buffer_abs(cbuf, ofbuf);
// // 
// // 	return buffer;
// // }
// // 
// // 
// // /* Returns the square of the absolute value.
// //  * @return [PaddleC::FloatBuffer] the square of the absolute value
// //  * @overload abs2(buffer = nil)
// //  *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
// //  */
// // static VALUE paddlec_complex_buffer_abs2(int argc, VALUE *argv, VALUE self)
// // {
// // 	const pdlc_complex_buffer_t *cbuf;
// // 	pdlc_buffer_t *ofbuf;
// // 	VALUE buffer;
// // 
// // 	rb_scan_args(argc, argv, "01", &buffer);
// // 
// // 	if (buffer == Qnil)
// // 		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
// // 	else if (rb_class_of(buffer) != c_FloatBuffer)
// // 		rb_raise(rb_eTypeError, "wrong argument type %"PRIsVALUE" (expected nil or %"PRIsVALUE")", rb_class_name(rb_class_of(buffer)), rb_class_name(c_FloatBuffer));
// // 	ofbuf = paddlec_float_buffer_get_struct(buffer);
// // 	cbuf = paddlec_complex_buffer_get_struct(self);
// // 
// // 	pdlc_complex_buffer_abs2(cbuf, ofbuf);
// // 
// // 	return buffer;
// // }
// 
// 
// /* Returns the angle part of its polar form.
//  * @return [PaddleC::FloatBuffer] the angle in radians
//  * @overload arg(buffer = nil)
//  * @overload angle(buffer = nil)
//  * @overload phase(buffer = nil)
//  *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
//  */
// static VALUE paddlec_complex_buffer_angle(int argc, VALUE *argv, VALUE self)
// {
// 	const pdlc_complex_buffer_t *cbuf;
// 	pdlc_buffer_t *ofbuf;
// 	VALUE buffer;
// 
// 	rb_scan_args(argc, argv, "01", &buffer);
// 
// 	if (buffer == Qnil)
// 		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
// 	else if (rb_class_of(buffer) != c_FloatBuffer)
// 		rb_raise(rb_eTypeError, "wrong argument type %"PRIsVALUE" (expected nil or %"PRIsVALUE")", rb_class_name(rb_class_of(buffer)), rb_class_name(c_FloatBuffer));
// 	ofbuf = paddlec_float_buffer_get_struct(buffer);
// 	cbuf = paddlec_complex_buffer_get_struct(self);
// 
// 	pdlc_complex_buffer_angle(cbuf, ofbuf);
// 
// 	return buffer;
// }
// 
// 
// /* Returns the complex conjugate.
//  * @return [PaddleC::FloatBuffer] the complex conjugate
//  * @overload conjugate(buffer = nil)
//  * @overload conj(buffer = nil)
//  *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
//  */
// static VALUE paddlec_complex_buffer_conj(int argc, VALUE *argv, VALUE self)
// {
// 	const pdlc_complex_buffer_t *cbuf;
// 	pdlc_complex_buffer_t *ocbuf;
// 	VALUE buffer;
// 
// 	rb_scan_args(argc, argv, "01", &buffer);
// 
// 	if (buffer == Qnil)
// 		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
// 	else if (rb_class_of(buffer) != c_ComplexBuffer)
// 		rb_raise(rb_eTypeError, "wrong argument type %"PRIsVALUE" (expected nil or %"PRIsVALUE")", rb_class_name(rb_class_of(buffer)), rb_class_name(c_ComplexBuffer));
// 	ocbuf = paddlec_complex_buffer_get_struct(buffer);
// 	cbuf = paddlec_complex_buffer_get_struct(self);
// 
// 	pdlc_complex_buffer_conj(cbuf, ocbuf);
// 
// 	return buffer;
// }


/* @see Array#zip
 * @return [Array]
 */
static VALUE paddlec_complex_buffer_zip(int argc, VALUE *argv, VALUE self)
{
	VALUE a, res;

	a = paddlec_complex_buffer_to_a(self);
	res = rb_funcallv(a, id_zip, argc, argv);

	return res;
}


/* If +numeric+ is a +Complex+, returns an array +[PaddleC::ComplexBuffer, self]+.
 * Otherwise, returns an array +[PaddleC::FloatBuffer, self]+.
 *
 * The coercion mechanism is used by Ruby to handle mixed-type numeric operations: 
 * it is intended to find a compatible common type between the two operands of the operator.
 *
 * @param numeric [Numeric]
 * @return [Array<PaddleC::FloatBuffer, PaddleC::ComplexBuffer>]
 */
static VALUE paddlec_complex_buffer_coerce(VALUE self, VALUE numeric)
{
	VALUE other_coerced;
	VALUE len_val[2];

	len_val[0] = rb_funcallv(self, id_length, 0, NULL);
	len_val[1] = numeric;
	if (rb_class_of(numeric) == rb_cComplex)
		other_coerced = rb_class_new_instance(2, len_val, c_ComplexBuffer);
	else
		other_coerced = rb_class_new_instance(2, len_val, c_FloatBuffer);

	return rb_assoc_new(other_coerced, self);
}


/* Unary plus, returns the receiver.
 * @return [self]
 */
static VALUE paddlec_complex_buffer_unaryplus(VALUE self)
{
	return self;
}


static pdlc_buffer_t* pdlc_complex_buffer_finite(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = ((isfinite(cbuf->data[i].real) && isfinite(cbuf->data[i].imag)) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_complex_buffer_infinite(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = ((isinf(cbuf->data[i].real) || isinf(cbuf->data[i].imag)) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_complex_buffer_nan(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = ((isnan(cbuf->data[i].real) || isnan(cbuf->data[i].imag)) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_complex_buffer_integer(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = ((isfinite(cbuf->data[i].real) && isfinite(cbuf->data[i].imag) && (cbuf->data[i].real == truncf(cbuf->data[i].real)) && (cbuf->data[i].imag == truncf(cbuf->data[i].imag))) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_complex_buffer_nonzero(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = ((cbuf->data[i].real == 0.0f && cbuf->data[i].imag == 0.0f) ? 0.0f : 1.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_complex_buffer_zero(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = ((cbuf->data[i].real == 0.0f && cbuf->data[i].imag == 0.0f) ? 1.0f : 0.0f);

	return ofbuf;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for values which are valid IEEE floating point number, i.e. which are not infinite or NaN, 0.0 otherwize.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_finite(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_complex_buffer_finite(mcbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for values which are positive or negative infinity, 0.0 otherwize.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_infinite(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_complex_buffer_infinite(mcbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for values which are NaN, 0.0 otherwize.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_nan(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_complex_buffer_nan(mcbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 if +value == value.truncate+, 0.0 otherwise.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_integer(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_complex_buffer_integer(mcbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 if +value != 0+, 0.0 otherwise.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_nonzero(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_complex_buffer_nonzero(mcbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 if +value == 0+, 0.0 otherwise.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_zero(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_complex_buffer_zero(mcbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_equ(VALUE self, VALUE other)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_equ(mcbuf, ocbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_equ(mcbuf, oc, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_equ(mcbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_equ(mcbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_different(VALUE self, VALUE other)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_different(mcbuf, ocbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_different(mcbuf, oc, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_different(mcbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_different(mcbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Returns the smallest numbers greater than or equal to values of +self+, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload ceil(ndigits = 0, buffer = nil)
 * @param ndigits [Integer] precision
 * @param buffer [nil, PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_ceil(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	VALUE digits, buffer, tmp;
	int d;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &digits, &buffer);
	if (NIL_P(buffer) && rb_obj_is_kind_of(digits, c_ComplexBuffer)) {
		buffer = digits;
		digits = Qnil;
	}
	else if (rb_obj_is_kind_of(buffer, rb_cNumeric) && rb_obj_is_kind_of(digits, c_ComplexBuffer)) {
		tmp = buffer;
		buffer = digits;
		digits = tmp;
	}

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	if (digits == Qnil)
		pdlc_complex_buffer_ceil(mcbuf, rcbuf);
	else {
		d = NUM2INT(digits);
		pdlc_complex_buffer_ceil_digits(mcbuf, d, rcbuf);
	}

	return buffer;
}


/* Returns the smallest integral values that are not less than values of +self+, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload ceil!(ndigits = 0)
 * @param ndigits [Integer] precision
 * @return [self]
 */
static VALUE paddlec_complex_buffer_ceil_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_complex_buffer_t *mcbuf;
	VALUE digits;
	int d;

	rb_scan_args(argc, argv, "01", &digits);
	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (digits == Qnil) 
		pdlc_complex_buffer_ceil_inplace(mcbuf);
	else {
		d = NUM2INT(digits);
		pdlc_complex_buffer_ceil_digits_inplace(mcbuf, d);
	}

	return self;
}


/* Returns the largest integral values that are not greater than values of +self+, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload floor(ndigits = 0, buffer = nil)
 * @param ndigits [Integer] precision
 * @param buffer [nil, PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_floor(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	VALUE digits, buffer, tmp;
	int d;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &digits, &buffer);
	if (NIL_P(buffer) && rb_obj_is_kind_of(digits, c_ComplexBuffer)) {
		buffer = digits;
		digits = Qnil;
	}
	else if (rb_obj_is_kind_of(buffer, rb_cNumeric) && rb_obj_is_kind_of(digits, c_ComplexBuffer)) {
		tmp = buffer;
		buffer = digits;
		digits = tmp;
	}

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	if (digits == Qnil)
		pdlc_complex_buffer_floor(mcbuf, rcbuf);
	else {
		d = NUM2INT(digits);
		pdlc_complex_buffer_floor_digits(mcbuf, d, rcbuf);
	}

	return buffer;
}


/* Returns the largest integral values that are not greater than values of +self+, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload floor!(ndigits = 0)
 * @param ndigits [Integer] precision
 * @return [self]
 */
static VALUE paddlec_complex_buffer_floor_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_complex_buffer_t *mcbuf;
	VALUE digits;
	int d;

	rb_scan_args(argc, argv, "01", &digits);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (digits == Qnil) 
		pdlc_complex_buffer_floor_inplace(mcbuf);
	else {
		d = NUM2INT(digits);
		pdlc_complex_buffer_floor_digits_inplace(mcbuf, d);
	}

	return self;
}


/* Truncates values of +self+ to the nearest integers not larger in absolute value, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload truncate(ndigits = 0, buffer = nil)
 * @param ndigits [Integer] precision
 * @param buffer [nil, PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_truncate(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	VALUE digits, buffer, tmp;
	int d;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &digits, &buffer);
	if (NIL_P(buffer) && rb_obj_is_kind_of(digits, c_ComplexBuffer)) {
		buffer = digits;
		digits = Qnil;
	}
	else if (rb_obj_is_kind_of(buffer, rb_cNumeric) && rb_obj_is_kind_of(digits, c_ComplexBuffer)) {
		tmp = buffer;
		buffer = digits;
		digits = tmp;
	}

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	if (digits == Qnil)
		pdlc_complex_buffer_truncate(mcbuf, rcbuf);
	else {
		d = NUM2INT(digits);
		pdlc_complex_buffer_truncate_digits(mcbuf, d, rcbuf);
	}

	return buffer;
}


/* Truncates values of +self+ to the nearest integers not larger in absolute value, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload truncate!(ndigits = 0)
 * @param ndigits [Integer] precision
 * @return [self]
 */
static VALUE paddlec_complex_buffer_truncate_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_complex_buffer_t *mcbuf;
	VALUE digits;
	int d;

	rb_scan_args(argc, argv, "01", &digits);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (digits == Qnil) 
		pdlc_complex_buffer_truncate_inplace(mcbuf);
	else {
		d = NUM2INT(digits);
		pdlc_complex_buffer_truncate_digits_inplace(mcbuf, d);
	}

	return self;
}


/* Rounds values of +self+ to the nearest integers, but round halfway cases away from zero, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload round(ndigits = 0, buffer = nil)
 * @param ndigits [Integer] precision
 * @param buffer [nil, PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_round(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	VALUE digits, buffer, tmp;
	int d;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &digits, &buffer);
	if (NIL_P(buffer) && rb_obj_is_kind_of(digits, c_ComplexBuffer)) {
		buffer = digits;
		digits = Qnil;
	}
	else if (rb_obj_is_kind_of(buffer, rb_cNumeric) && rb_obj_is_kind_of(digits, c_ComplexBuffer)) {
		tmp = buffer;
		buffer = digits;
		digits = tmp;
	}

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	if (digits == Qnil)
		pdlc_complex_buffer_round(mcbuf, rcbuf);
	else {
		d = NUM2INT(digits);
		pdlc_complex_buffer_round_digits(mcbuf, d, rcbuf);
	}

	return buffer;
}


/* Rounds values of +self+ to the nearest integers, but round halfway cases away from zero, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload round!(ndigits = 0)
 * @param ndigits [Integer] precision
 * @return [self]
 */
static VALUE paddlec_complex_buffer_round_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_complex_buffer_t *mcbuf;
	VALUE digits;
	int d;

	rb_scan_args(argc, argv, "01", &digits);
	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (digits == Qnil) 
		pdlc_complex_buffer_round_inplace(mcbuf);
	else {
		d = NUM2INT(digits);
		pdlc_complex_buffer_round_digits_inplace(mcbuf, d);
	}

	return self;
}


/* Returns the absolute value / the absolute part of its polar form.
 * @overload abs(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_abs(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_complex_buffer_abs(mcbuf, rfbuf);

	return buffer;
}


/* Returns the square of the absolute value / the square of the absolute part of its polar form.
 * @overload abs2(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_abs2(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_complex_buffer_abs2(mcbuf, rfbuf);

	return buffer;
}


/* Returns the angle part of its polar form.
 * @overload arg(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_arg(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_complex_buffer_arg(mcbuf, rfbuf);

	return buffer;
}


/* Returns the complex conjugate.
 * @overload conjugate(buffer = nil)
 * @param buffer [nil, PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_conjugate(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	pdlc_complex_buffer_conjugate(mcbuf, rcbuf);

	return buffer;
}


/* Returns the complex conjugate.
 * @return [self]
 */
static VALUE paddlec_complex_buffer_conjugate_inplace(VALUE self)
{
	pdlc_complex_buffer_t *mcbuf;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	pdlc_complex_buffer_conjugate_inplace(mcbuf);

	return self;
}


/* Returns the imaginary part.
 * @overload imaginary(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_imag(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_complex_buffer_imag(mcbuf, rfbuf);

	return buffer;
}


/* Returns the real part.
 * @overload real(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_complex_buffer_real(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_complex_buffer_real(mcbuf, rfbuf);

	return buffer;
}


/* Swap the real and imaginary parts.
 * @overload swapIQ(buffer = nil)
 * @param buffer [nil, PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_swapIQ(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	pdlc_complex_buffer_swapIQ(mcbuf, rcbuf);

	return buffer;
}


/* Swap the real and imaginary parts.
 * @return [self]
 */
static VALUE paddlec_complex_buffer_swapIQ_inplace(VALUE self)
{
	pdlc_complex_buffer_t *mcbuf;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	pdlc_complex_buffer_swapIQ_inplace(mcbuf);

	return self;
}


/* Unary minus, returns the receiver, negated.
 * @overload -@(buffer = nil)
 * @param buffer [nil, PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_unaryminus(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	VALUE buffer;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	pdlc_complex_buffer_unaryminus(mcbuf, rcbuf);

	return buffer;
}


/* Unary minus, returns the receiver, negated.
 * @return [self]
 */
static VALUE paddlec_complex_buffer_unaryminus_inplace(VALUE self)
{
	pdlc_complex_buffer_t *mcbuf;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	pdlc_complex_buffer_unaryminus_inplace(mcbuf);

	return self;
}


/* @return [PaddleC::ComplexBuffer]
 * @overload -(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_sub(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_sub(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_sub(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_sub(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_sub(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_sub_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_sub_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_sub_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_sub_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_sub_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element soustraction.
 * @return [PaddleC::ComplexBuffer]
 * @overload esub(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_esub(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_esub(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_esub(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_esub(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_esub(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element soustraction.
 * @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_esub_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_esub_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_esub_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_esub_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_esub_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* @return [PaddleC::ComplexBuffer]
 * @overload +(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_add(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_add(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_add(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_add(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_add(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_add_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_add_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_add_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_add_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_add_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element addition.
 * @return [PaddleC::ComplexBuffer]
 * @overload eadd(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_eadd(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_eadd(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_eadd(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_eadd(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_eadd(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element addition.
 * @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_eadd_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_eadd_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_eadd_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_eadd_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_eadd_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Performs multiplication.
 * @return [PaddleC::ComplexBuffer]
 * @overload *(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_mult(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_mult(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_mult(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_mult(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_mult(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Performs multiplication.
 * @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_mult_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_mult_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_mult_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_mult_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_mult_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element multiplication.
 * @return [PaddleC::ComplexBuffer]
 * @overload emult(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_emult(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_emult(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_emult(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_emult(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_emult(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element multiplication.
 * @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_emult_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_emult_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_emult_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_emult_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_emult_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Performs division.
 * @return [PaddleC::ComplexBuffer]
 * @overload /(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_div(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_div(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_div(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_div(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_div(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Performs division.
 * @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_div_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_div_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_div_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_div_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_div_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element division.
 * @return [PaddleC::ComplexBuffer]
 * @overload ediv(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_ediv(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_ediv(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_ediv(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_ediv(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_ediv(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element division.
 * @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_ediv_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_ediv_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_ediv_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_ediv_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_ediv_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Performs exponentiation. Raises +self+ to the power of +rhs+.
 * @return [PaddleC::ComplexBuffer]
 * @overload **(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_pow(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_pow(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_pow(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Performs exponentiation. Raises +self+ to the power of +rhs+.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_pow_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_pow_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_pow_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element modulo.
 * @return [PaddleC::ComplexBuffer]
 * @overload emod(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_emod(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_emod(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_emod(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_emod(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_emod(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element modulo.
 * @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_emod_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_emod_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_emod_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_emod_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_emod_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element exponentiation.
 * @return [PaddleC::ComplexBuffer]
 * @overload epow(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_epow(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_epow(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_epow(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_epow(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_epow(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element exponentiation.
 * @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_epow_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_epow_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_epow_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_epow_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_epow_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Return the lesser and greater values of +self+.
 * Returns nil if empty.
 * @return [Array<Complex>, nil] a two element array containint the lesser and greater values from +self+.
 */
static VALUE paddlec_complex_buffer_minmax(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_t mi, ma;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (mcbuf->length < 1)
		return Qnil;

	pdlc_complex_buffer_minmax_of_self(mcbuf, &mi, &ma);

	return rb_assoc_new(paddlec2COMPLEX(mi), paddlec2COMPLEX(ma));
}


/* @return [nil, Complex, PaddleC::ComplexBuffer]
 * Without any parameter, returns the lesser value stored in +self+ (or +nil+ if empty).
 * If a {PaddleC::FloatBuffer}, a {PaddleC::FloatBuffer], a Float or a Complex is provided as +other+, 
 * returns a new buffer for which each element is the lesser value of the corresponding element of +self+ and +other+.
 * @overload min()
 * 	@return [Complex, nil]
 * @overload min(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_min(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "02", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (other == Qnil) {
		if (mcbuf->length < 1)
			buffer = Qnil;
		else {
			oc = pdlc_complex_buffer_min_of_self(mcbuf);
			buffer = paddlec2COMPLEX(oc);
		}
	}
	else if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_min(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_min(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_min(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_min(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_min_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_min_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_min_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_min_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_min_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* @return [nil, Complex, PaddleC::ComplexBuffer]
 * Without any parameter, returns the greater value stored in +self+ (or +nil+ if empty).
 * If a {PaddleC::FloatBuffer}, a {PaddleC::FloatBuffer], a Float or a Complex is provided as +other+, 
 * returns a new buffer for which each element is the lesser value of the corresponding element of +self+ and +other+.
 * @overload max()
 * 	@return [Complex, nil]
 * @overload max(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_max(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "02", &other, &buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (other == Qnil) {
		if (mcbuf->length < 1)
			buffer = Qnil;
		else {
			oc = pdlc_complex_buffer_max_of_self(mcbuf);
			buffer = paddlec2COMPLEX(oc);
		}
	}
	else if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_cb_cb_max(mcbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_cb_cs_max(mcbuf, oc, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_cb_fb_max(mcbuf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_cb_fs_max(mcbuf, of, rcbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* @param other [c_ComplexBuffer, rb_cComplex, c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_complex_buffer_max_inplace(VALUE self, VALUE other)
{
	pdlc_complex_buffer_t *mcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;

	mcbuf = paddlec_complex_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_cb_cb_max_inplace(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_cb_cs_max_inplace(mcbuf, oc);
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_cb_fb_max_inplace(mcbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_cb_fs_max_inplace(mcbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Clipp the real and imaginary parts of the signal so that the output has no values whose real and imaginary part are greater than +max+ nor lesser than +min+.
 * @overload clipp(max = 1.0, min = -max, buffer = nil)
 * @param max [Float] the maximum value allowed
 * @param min [Float] the minimum value allowed
 * @param buffer [nil, PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_clipp(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	float mi, ma;
	VALUE buffer, rbmin, rbmax;

	rb_scan_args(argc, argv, "3", &rbmax, &rbmin, &buffer);

	if (rbmax == Qnil)
		ma = 1.0f;
	else
		ma = (float)NUM2DBL(rbmax);

	if (rbmin == Qnil)
		mi = -ma;
	else
		mi = (float)NUM2DBL(rbmin);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}

	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	pdlc_complex_buffer_clipp(mcbuf, mi, ma, rcbuf);

	return buffer;
}


/* Clipp the real and imaginary parts of the signal so that the output has no values whose real and imaginary part are greater than +max+ nor lesser than +min+.
 * @overload clipp!(max = 1.0, min = -max)
 * @param max [Float] the maximum value allowed
 * @param min [Float] the minimum value allowed
 * @return [self]
 */
static VALUE paddlec_complex_buffer_clipp_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_complex_buffer_t *mcbuf;
	float mi, ma;
	VALUE rbmin, rbmax;

	rb_scan_args(argc, argv, "2", &rbmax, &rbmin);

	if (rbmax == Qnil)
		ma = 1.0f;
	else
		ma = (float)NUM2DBL(rbmax);

	if (rbmin == Qnil)
		mi = -ma;
	else
		mi = (float)NUM2DBL(rbmin);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	pdlc_complex_buffer_clipp_inplace(mcbuf, mi, ma);

	return self;
}


/* Returns a new {PaddleC::ComplexBuffer} containing +self+'s elements in reverse order.
 * @overload reverse(buffer = nil)
 * @param buffer [PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_reverse(int argc, VALUE *argv, VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_buffer_t *rcbuf;
	VALUE buffer;

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}

	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	mcbuf = paddlec_complex_buffer_get_struct(self);

	pdlc_complex_buffer_reverse(mcbuf, rcbuf);

	return buffer;
}


/* Reverse +self+ in place.
 * @return [self]
 */
static VALUE paddlec_complex_buffer_reverse_inplace(VALUE self)
{
	pdlc_complex_buffer_t *mcbuf;
	mcbuf = paddlec_complex_buffer_get_struct(self);
	pdlc_complex_buffer_reverse_inplace(mcbuf);
	return self;
}


/* Returns the sum of all values in +self+, or +nil+ if empty.
 * @return [Complex, nil]
 */
static VALUE paddlec_complex_buffer_sum(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_t res;
	mcbuf = paddlec_complex_buffer_get_struct(self);
	if (mcbuf->length < 1)
		return Qnil;
	res = pdlc_complex_buffer_sum(mcbuf);
	return paddlec2COMPLEX(res);
}


/* Returns the mean of all values in +self+, or +nil+ if empty.
 * @return [Complex, nil]
 */
static VALUE paddlec_complex_buffer_mean(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_t res;
	mcbuf = paddlec_complex_buffer_get_struct(self);
	if (mcbuf->length < 1)
		return Qnil;
	res = pdlc_complex_buffer_mean(mcbuf);
	return paddlec2COMPLEX(res);
}


/* Returns the product of all values in +self+, or +nil+ if empty.
 * @return [Complex, nil]
 */
static VALUE paddlec_complex_buffer_prod(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_t res;
	mcbuf = paddlec_complex_buffer_get_struct(self);
	if (mcbuf->length < 1)
		return Qnil;
	res = pdlc_complex_buffer_prod(mcbuf);
	return paddlec2COMPLEX(res);
}


/* Returns the product of all values in +self+, processing separately the real part and the imaginary part. 
 * Returns +nil+ if empty.
 * @return [Complex, nil]
 */
static VALUE paddlec_complex_buffer_eprod(VALUE self)
{
	const pdlc_complex_buffer_t *mcbuf;
	pdlc_complex_t res;
	mcbuf = paddlec_complex_buffer_get_struct(self);
	if (mcbuf->length < 1)
		return Qnil;
	res = pdlc_complex_buffer_eprod(mcbuf);
	return paddlec2COMPLEX(res);
}


/* Returns the sum of absolute differences of values of +self+ with +other+, or +nil+ if empty.
 * @return [Complex, nil]
 * @param other [Complex, PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_sad(VALUE self, VALUE other)
{
	const pdlc_complex_buffer_t *mcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc, rc;

	mcbuf = paddlec_complex_buffer_get_struct(self);
	if (mcbuf->length < 1)
		return Qnil;

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		if (ocbuf->length < 1)
			return Qnil;
		rc = pdlc_complex_buffer_cb_cb_sad(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		oc = COMPLEX2paddlec(other);
		rc = pdlc_complex_buffer_cb_cs_sad(mcbuf, oc);
	}
	else
		rb_raise(rb_eArgError, "expecting a Complex or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(other)));

	return paddlec2COMPLEX(rc);
}


/* Returns the sum of square differences of values of +self+ with +other+, or +nil+ if empty.
 * @return [Complex, nil]
 * @param other [Complex, PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_ssd(VALUE self, VALUE other)
{
	const pdlc_complex_buffer_t *mcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc, rc;

	mcbuf = paddlec_complex_buffer_get_struct(self);
	if (mcbuf->length < 1)
		return Qnil;

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		if (ocbuf->length < 1)
			return Qnil;
		rc = pdlc_complex_buffer_cb_cb_ssd(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		oc = COMPLEX2paddlec(other);
		rc = pdlc_complex_buffer_cb_cs_ssd(mcbuf, oc);
	}
	else
		rb_raise(rb_eArgError, "expecting a Complex or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(other)));

	return paddlec2COMPLEX(rc);
}


/* Returns the sum of square differences of values of +self+ with +other+, processing the real part and the imaginary part independantly.
 * Returns +nil+ if empty.
 * @return [Complex, nil]
 * @param other [Complex, PaddleC::ComplexBuffer]
 */
static VALUE paddlec_complex_buffer_essd(VALUE self, VALUE other)
{
	const pdlc_complex_buffer_t *mcbuf;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc, rc;

	mcbuf = paddlec_complex_buffer_get_struct(self);
	if (mcbuf->length < 1)
		return Qnil;

	if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		if (ocbuf->length < 1)
			return Qnil;
		rc = pdlc_complex_buffer_cb_cb_essd(mcbuf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		oc = COMPLEX2paddlec(other);
		rc = pdlc_complex_buffer_cb_cs_essd(mcbuf, oc);
	}
	else
		rb_raise(rb_eArgError, "expecting a Complex or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(other)));

	return paddlec2COMPLEX(rc);
}



void Init_paddlec_complex_buffer()
{
	c_ComplexBuffer = rb_define_class_under(m_PaddleC, "ComplexBuffer", rb_cObject);
	rb_define_module_function(c_ComplexBuffer, "unpack",    paddlec_complex_buffer_classunpack,       2);

	rb_define_alloc_func(c_ComplexBuffer, paddlec_complex_buffer_alloc);
	rb_include_module(c_ComplexBuffer, rb_mEnumerable);
	rb_define_method(c_ComplexBuffer, "initialize",         paddlec_complex_buffer_initialize,       -1);
	rb_define_method(c_ComplexBuffer, "to_complex_buffer",  paddlec_complex_buffer_to_complex_buffer, 0);
	rb_define_method(c_ComplexBuffer, "each",               paddlec_complex_buffer_each,              0);
	rb_define_method(c_ComplexBuffer, "collect!",           paddlec_complex_buffer_collect_inp,       0);
	rb_define_method(c_ComplexBuffer, "length",             paddlec_complex_buffer_length,            0);
	rb_define_method(c_ComplexBuffer, "empty?",             paddlec_complex_buffer_isempty,           0);
	rb_define_method(c_ComplexBuffer, "to_a",               paddlec_complex_buffer_to_a,              0);
	rb_define_method(c_ComplexBuffer, "to_s",               paddlec_complex_buffer_to_s,              0);
	rb_define_method(c_ComplexBuffer, "inspect",            paddlec_complex_buffer_to_s,              0);
	rb_define_method(c_ComplexBuffer, "zip",                paddlec_complex_buffer_zip,              -1);
	rb_define_method(c_ComplexBuffer, "clone",              paddlec_complex_buffer_clone,             0);
	rb_define_method(c_ComplexBuffer, "[]",                 paddlec_complex_buffer_get,              -1);
	rb_define_method(c_ComplexBuffer, "slice",              paddlec_complex_buffer_get,              -1);
	rb_define_method(c_ComplexBuffer, "[]=",                paddlec_complex_buffer_set,              -1);
	rb_define_method(c_ComplexBuffer, "pack",               paddlec_complex_buffer_pack,             -1);
	rb_define_method(c_ComplexBuffer, "unpack",             paddlec_complex_buffer_unpack,            2);
	rb_define_method(c_ComplexBuffer, "resize",             paddlec_complex_buffer_resize,            1);
	rb_define_method(c_ComplexBuffer, "pointer",            paddlec_complex_buffer_native_ptr,        0);
	rb_define_method(c_ComplexBuffer, "coerce",             paddlec_complex_buffer_coerce,            1);
	if (c_FFI_Pointer != Qundef)
		rb_define_method(c_ComplexBuffer, "to_ptr",         paddlec_complex_buffer_ffi_pointer,       0);

	rb_define_method(rb_cArray, "to_complex_buffer",        paddlec_array_to_complex_buffer,          0);

	rb_define_method(c_ComplexBuffer, "finite?",            paddlec_complex_buffer_finite,            0);
	rb_define_method(c_ComplexBuffer, "infinite?",          paddlec_complex_buffer_infinite,          0);
	rb_define_method(c_ComplexBuffer, "nan?",               paddlec_complex_buffer_nan,               0);
	rb_define_method(c_ComplexBuffer, "integer?",           paddlec_complex_buffer_integer,           0);
	rb_define_method(c_ComplexBuffer, "nonzero?",           paddlec_complex_buffer_nonzero,           0);
	rb_define_method(c_ComplexBuffer, "zero?",              paddlec_complex_buffer_zero,              0);
	rb_define_method(c_ComplexBuffer, "==",                 paddlec_complex_buffer_equ,               1);
	rb_define_method(c_ComplexBuffer, "!=",                 paddlec_complex_buffer_different,         1);
	rb_define_method(c_ComplexBuffer, "ceil",               paddlec_complex_buffer_ceil,             -1);
	rb_define_method(c_ComplexBuffer, "ceil!",              paddlec_complex_buffer_ceil_inplace,     -1);
	rb_define_method(c_ComplexBuffer, "floor",              paddlec_complex_buffer_floor,            -1);
	rb_define_method(c_ComplexBuffer, "floor!",             paddlec_complex_buffer_floor_inplace,    -1);
	rb_define_method(c_ComplexBuffer, "truncate",           paddlec_complex_buffer_truncate,         -1);
	rb_define_method(c_ComplexBuffer, "truncate!",          paddlec_complex_buffer_truncate_inplace, -1);
	rb_define_method(c_ComplexBuffer, "round",              paddlec_complex_buffer_round,            -1);
	rb_define_method(c_ComplexBuffer, "round!",             paddlec_complex_buffer_round_inplace,    -1);
	rb_define_method(c_ComplexBuffer, "abs",                paddlec_complex_buffer_abs,              -1);
	rb_define_alias(c_ComplexBuffer,  "magnitude", "abs");
	rb_define_method(c_ComplexBuffer, "abs2",               paddlec_complex_buffer_abs2,             -1);
	rb_define_method(c_ComplexBuffer, "arg",                paddlec_complex_buffer_arg,              -1);
	rb_define_alias(c_ComplexBuffer,  "angle", "arg");
	rb_define_alias(c_ComplexBuffer,  "phase", "arg");
	rb_define_method(c_ComplexBuffer, "conjugate",          paddlec_complex_buffer_conjugate,        -1);
	rb_define_alias(c_ComplexBuffer,  "conj", "conjugate");
	rb_define_method(c_ComplexBuffer, "conjugate!",         paddlec_complex_buffer_conjugate_inplace, 0);
	rb_define_method(c_ComplexBuffer, "imaginary",          paddlec_complex_buffer_imag,             -1);
	rb_define_alias(c_ComplexBuffer,  "imag", "imaginary");
	rb_define_method(c_ComplexBuffer, "real",               paddlec_complex_buffer_real,             -1);
	rb_define_method(c_ComplexBuffer, "swapIQ",             paddlec_complex_buffer_swapIQ,           -1);
	rb_define_alias(c_ComplexBuffer,  "swapRI", "swapIQ");
	rb_define_method(c_ComplexBuffer, "swapIQ!",            paddlec_complex_buffer_swapIQ_inplace,    0);
	rb_define_alias(c_ComplexBuffer,  "swapRI!", "swapIQ!");
	rb_define_method(c_ComplexBuffer, "+@",                 paddlec_complex_buffer_unaryplus,         0);
	rb_define_method(c_ComplexBuffer, "-@",                 paddlec_complex_buffer_unaryminus,       -1);
	rb_define_method(c_ComplexBuffer, "negate!",            paddlec_complex_buffer_unaryminus_inplace,0);
	rb_define_method(c_ComplexBuffer, "-",                  paddlec_complex_buffer_sub,              -1);
	rb_define_alias(c_ComplexBuffer,  "sub", "-");
	rb_define_method(c_ComplexBuffer, "sub!",               paddlec_complex_buffer_sub_inplace,       1);
	rb_define_method(c_ComplexBuffer, "esub",               paddlec_complex_buffer_esub,             -1);
	rb_define_method(c_ComplexBuffer, "esub!",              paddlec_complex_buffer_esub_inplace,      1);
	rb_define_method(c_ComplexBuffer, "+",                  paddlec_complex_buffer_add,              -1);
	rb_define_alias(c_ComplexBuffer,  "add", "+");
	rb_define_method(c_ComplexBuffer, "add!",               paddlec_complex_buffer_add_inplace,       1);
	rb_define_method(c_ComplexBuffer, "eadd",               paddlec_complex_buffer_eadd,             -1);
	rb_define_method(c_ComplexBuffer, "eadd!",              paddlec_complex_buffer_eadd_inplace,      1);
	rb_define_method(c_ComplexBuffer, "*",                  paddlec_complex_buffer_mult,             -1);
	rb_define_alias(c_ComplexBuffer,  "mult", "*");
	rb_define_method(c_ComplexBuffer, "mult!",              paddlec_complex_buffer_mult_inplace,      1);
	rb_define_method(c_ComplexBuffer, "emult",              paddlec_complex_buffer_emult,            -1);
	rb_define_method(c_ComplexBuffer, "emul!",              paddlec_complex_buffer_emult_inplace,     1);
	rb_define_method(c_ComplexBuffer, "/",                  paddlec_complex_buffer_div,              -1);
	rb_define_alias(c_ComplexBuffer,  "div", "/");
	rb_define_method(c_ComplexBuffer, "div!",               paddlec_complex_buffer_div_inplace,       1);
	rb_define_method(c_ComplexBuffer, "ediv",               paddlec_complex_buffer_ediv,             -1);
	rb_define_method(c_ComplexBuffer, "ediv!",              paddlec_complex_buffer_ediv_inplace,      1);
	rb_define_method(c_ComplexBuffer, "**",                 paddlec_complex_buffer_pow,              -1);
	rb_define_alias(c_ComplexBuffer,  "pow", "**");
	rb_define_method(c_ComplexBuffer, "pow!",               paddlec_complex_buffer_pow_inplace,       1);
	rb_define_method(c_ComplexBuffer, "emod",               paddlec_complex_buffer_emod,             -1);
	rb_define_method(c_ComplexBuffer, "emod!",              paddlec_complex_buffer_emod_inplace,      1);
	rb_define_method(c_ComplexBuffer, "epow",               paddlec_complex_buffer_epow,             -1);
	rb_define_method(c_ComplexBuffer, "epow!",              paddlec_complex_buffer_epow_inplace,      1);
	rb_define_method(c_ComplexBuffer, "min",                paddlec_complex_buffer_min,              -1);
	rb_define_method(c_ComplexBuffer, "min!",               paddlec_complex_buffer_min_inplace,       1);
	rb_define_method(c_ComplexBuffer, "max",                paddlec_complex_buffer_max,              -1);
	rb_define_method(c_ComplexBuffer, "max!",               paddlec_complex_buffer_max_inplace,       1);
	rb_define_method(c_ComplexBuffer, "minmax",             paddlec_complex_buffer_minmax,            0);
	rb_define_method(c_ComplexBuffer, "clipp",              paddlec_complex_buffer_clipp,            -1);
	rb_define_method(c_ComplexBuffer, "clipp!",             paddlec_complex_buffer_clipp_inplace,    -1);
	rb_define_method(c_ComplexBuffer, "reverse",            paddlec_complex_buffer_reverse,          -1);
	rb_define_method(c_ComplexBuffer, "reverse!",           paddlec_complex_buffer_reverse_inplace,   0);
	rb_define_method(c_ComplexBuffer, "sum",                paddlec_complex_buffer_sum,               0);
	rb_define_method(c_ComplexBuffer, "mean",               paddlec_complex_buffer_mean,              0);
	rb_define_method(c_ComplexBuffer, "prod",               paddlec_complex_buffer_prod,              0);
	rb_define_method(c_ComplexBuffer, "eprod",              paddlec_complex_buffer_eprod,             0);
	rb_define_method(c_ComplexBuffer, "sad",                paddlec_complex_buffer_sad,               1);
	rb_define_method(c_ComplexBuffer, "ssd",                paddlec_complex_buffer_ssd,               1);
	rb_define_method(c_ComplexBuffer, "essd",               paddlec_complex_buffer_essd,              1);
}

