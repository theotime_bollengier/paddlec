#!/usr/bin/env ruby

require 'colorize'
require 'roctave'
require './paddlec'

def max_diff (gold_array, float_buffer)
	diff = gold_array.collect.with_index{|v, i| (v - float_buffer[i]).abs}
	h = {
		max: diff.max,
		average: diff.inject(&:+) / gold_array.length
	}
	h
end

def complex_max_diff (gold_array, float_buffer)
	goldr = gold_array.collect{|v| v.real}
	br = float_buffer.real
	hr = max_diff(goldr, br)
	goldi = gold_array.collect{|v| v.imag}
	bi = float_buffer.imag
	hi = max_diff(goldi, bi)
	[hr, hi]
end

s = File.binread 'output.f32le'

b = PaddleC::FloatBuffer.new s
puts "#{b.length} samples, accelerator: #{PaddleC.accelerator}"
a = b.to_a

n = 1024 + 512
#n = 128
decimation_factor = 2
coefs = Roctave.fir1(n, 0.5)
#coefs = PaddleC::FloatBuffer.new(n, 0.2).to_a ## 9 10 11
rflt = Roctave::FirFilter.new coefs
rflt.decimation_factor = decimation_factor
pflt = PaddleC::FirDecimator.new coefs, decimation_factor


start = Time.now
rres = rflt.filter a
t_roctave = Time.now - start
puts "Roctave          took #{(t_roctave.round 3).to_s.center 6} s to process      x1      #{((a.length*(n+1)) / t_roctave / decimation_factor / 1e6).round} MMAC/s (#{(a.length / t_roctave).round} sample/s)"

start = Time.now
pres = pflt.decimate b
t_paddlec = Time.now - start
puts "PaddleC          took #{(t_paddlec.round 3).to_s.center 6} s to process      x#{(t_roctave / t_paddlec).round 2}   #{((a.length*(n+1)) / t_paddlec / decimation_factor / 1e6).round} MMAC/s (#{(a.length / t_paddlec).round} sample/s)"

puts "Roctave: #{rres.length}, PaddleC: #{pres.length}" if pres.length != rres.length

#puts pres
puts
puts "PaddleC error:"
md = max_diff(rres, pres)
if md[:max] > 0.0001 then
	puts md.inspect.light_red
else
	puts md.inspect
end
puts


puts '-'*80
puts 


b = PaddleC::ComplexBuffer.new s
a = b.to_a

rflt = Roctave::FirFilter.new coefs
rflt.decimation_factor = decimation_factor
pflt = PaddleC::FirDecimator.new coefs, decimation_factor

start = Time.now
rres = rflt.filter a
t_roctave = Time.now - start
puts "Roctave          took #{(t_roctave.round 3).to_s.center 6} s to process      x1      #{(2*a.length*(n+1) / t_roctave / decimation_factor / 1e6).round} MMAC/s (#{(a.length / t_roctave).round} complex sample/s)"

start = Time.now
pres = pflt.decimate b
t_paddlec = Time.now - start
puts "PaddleC          took #{(t_paddlec.round 3).to_s.center 6} s to process      x#{(t_roctave / t_paddlec).round 2}   #{(2*a.length*(n+1) / t_paddlec / decimation_factor / 1e6).round} MMAC/s (#{(a.length / t_paddlec).round} complex sample/s)"

puts "Roctave: #{rres.length}, PaddleC: #{pres.length}" if pres.length != rres.length

puts
pp complex_max_diff(rres, pres)
puts



