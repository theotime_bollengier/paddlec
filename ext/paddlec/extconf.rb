require 'mkmf'

if have_library('pulse') and have_library('pulse-simple') then
	$defs << '-DHAVE_PULSEAUDIO_L'
else
	warn "Cannot find pulseaudio library, you should do 'sudo apt install libpulse-dev' and install this gem again'"
end

if have_library('fftw3f') then
	$defs << '-DHAVE_FFTW3F_L'
else
	warn "Cannot find fftw3f library, you should do 'sudo apt install libfftw3-dev libfftw3-single3' and install this gem again'"
end

if RUBY_PLATFORM =~ /linux/ then
	cpuinfo = File.read '/proc/cpuinfo'
	avx   = not(not(cpuinfo =~ /\savx/i))
	sse   = not(not(cpuinfo =~ /\ssse/i))
	neon  = not(not(cpuinfo =~ /\sneon/i))
	vfpv4 = not(not(cpuinfo =~ /\svfpv4/i))
else
	avx   = false
	sse   = false
	neon  = false
	vfpv4 = false
end

vflgs = ''
if avx then
	vflgs += ' -mavx'
elsif sse then
	vflgs += ' -msse'
elsif neon then
	vflgs += ' -mfpu=neon'
	vflgs += '-vfpv4' if vfpv4
elsif vfpv4 then
	vflgs += ' -mfpu=vfpv4'
end

File.open(File.expand_path(File.join('..', '..', 'libpaddlec', 'Makefile'), __FILE__), 'w') do |m|
	m.puts <<EOF

CC = gcc
CFLAGS += -W -Wall
CFLAGS += -fPIC
CFLAGS += -O3 -march=native -ffast-math#{vflgs}

#SRC = $(wildcard *.c)
SRC += libpaddlec.c
SRC += fir_filter.c
SRC += delay.c
SRC += arithmetic.c
SRC += math.c
SRC += complex.c
SRC += comparison.c
SRC += rounding.c
SRC += no_fast_math.c
OBJS = $(SRC:.c=.o)

all: libpaddlec.a libpaddlec.so

libpaddlec.a: $(OBJS)
	ar rcs $@ $^

libpaddlec.so: $(OBJS)
	$(CC) $(CFLAGS) -shared -o $@ $^ -lm

fir_filter.o: fir_filter.c fir_filter_avx.c fir_filter_sse.c fir_filter_neon.c Makefile
	$(CC) $(CFLAGS) -c -o $@ $<

no_fast_math.o: no_fast_math.c Makefile
	$(CC) $(CFLAGS) -fno-fast-math -c -o $@ $<

%.o: %.c Makefile
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	@rm -vf *.o
	@rm -vf libpaddlec.a
	@rm -vf libpaddlec.so

.PHONY: clean

EOF
end

libpaddlec_dir =  File.expand_path(File.join('..', '..', 'libpaddlec'), __FILE__)
$CFLAGS += " -I #{libpaddlec_dir}"
#$CFLAGS += ' -O3 -mtune=native -ffast-math'
#$CFLAGS += " -O0 -g "

libpaddlec_static = false
if libpaddlec_static then
	$LDFLAGS += " -L #{libpaddlec_dir}"
	#$LOCAL_LIBS << ' -Wl,--whole-archive -lpaddlec -Wl,--no-whole-archive '
	$LOCAL_LIBS << '-lpaddlec'
	abort "Cannot make libpaddlec.a" unless system "cd #{libpaddlec_dir} && make clean && make libpaddlec.a"
else
	$LOCAL_LIBS += " -Wl,-rpath,#{libpaddlec_dir} -L#{libpaddlec_dir} -lpaddlec"
	abort "Cannot make libpaddlec.so" unless system "cd #{libpaddlec_dir} && make clean && make libpaddlec.so"
end

create_header 'paddlec_defs.h'
create_makefile 'paddlec/paddlec'


