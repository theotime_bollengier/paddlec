#!/usr/bin/env ruby

require 'roctave'
require './paddlec'

b = [1, 2, 3, 4]

t = (0...10).to_a
x = PaddleC::FloatBuffer.new t.length
x[2] = 1
x[4] = 1
x = x.to_a

rr, rd = Roctave::FirFilter.new(b).filter_with_delay(x)

flt = PaddleC::FirFilter.new b

y = []
d = []
x.each do |v|
	o, e = flt.filter(v, delayed: true)
	y << o
	d << e
end

Roctave.plot(t, x, '-x;Input;', t, rd, '.;R delayed;', t, rr, '.;R filtered;', t, d, '-+;Delayed;', t, y, '-o;Filtered;')

