#!/usr/bin/env ruby

require 'roctave'
require './paddlec'

n = 128
b = Roctave.fir_hilbert(n, window: :hamming)
#Roctave.freqz(b, :magnitude, nb_points: 2048, fs: 1000)

t = Roctave.linspace(0, 1, 1000)
x = t.collect{|v| Math.sin(2*Math::PI*15*v)}

#Roctave.plot(t, x, ylim: [-1.2, 1.2])

xb = x.to_float_buffer
ht = PaddleC::FirTransformer.new b
yb = ht.transform xb
real = yb.real
imag = yb.imag

phase = yb.phase.collect!{|v| v*2}

#Roctave.plot(t, phase, title: 'Phase');
Roctave.plot(t, real, ';real;', t, imag, ';imag;', t, phase.cos, ';reconstructed;', ylim: [-1.2, 1.2])
Roctave.plot(t, phase.cos, ';reconstructed;', ylim: [-1.2, 1.2])


