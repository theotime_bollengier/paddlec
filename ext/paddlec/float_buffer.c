/* Copyright (C) 2019-2020 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <ruby.h>
#include "libpaddlec.h"
#include "paddlec.h"
#include "float_buffer.h"
#include "complex_buffer.h"


/* Document-class: PaddleC::FloatBuffer
 *
 * A a Ruby object that wraps a native array of single floats.
 */


VALUE c_FloatBuffer;


static void paddlec_float_buffer_free(void *p)
{
	pdlc_buffer_t *fbuf = (pdlc_buffer_t*)p;
	pdlc_buffer_free(fbuf);
}


static size_t paddlec_float_buffer_size(const void* data)
{
	pdlc_buffer_t *fbuf = (pdlc_buffer_t*)data;
	return sizeof(pdlc_buffer_t) + fbuf->capacity*sizeof(float);
}


static const rb_data_type_t paddlec_float_buffer_type = {
	.wrap_struct_name = "paddlec_float_buffer_struct",
	.function = {
		.dmark = NULL,
		.dfree = paddlec_float_buffer_free,
		.dsize = paddlec_float_buffer_size,
	},
	.data = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE paddlec_float_buffer_alloc(VALUE klass)
{
	VALUE obj;
	pdlc_buffer_t *fbuf;

	fbuf = pdlc_buffer_new(0);
	obj = TypedData_Wrap_Struct(klass, &paddlec_float_buffer_type, fbuf);

	return obj;
}


pdlc_buffer_t* paddlec_float_buffer_get_struct(VALUE obj)
{
	pdlc_buffer_t *fbuf;
	TypedData_Get_Struct(obj, pdlc_buffer_t, &paddlec_float_buffer_type, fbuf);
	return fbuf;
}


/* @return [PaddleC::FloatBuffer]
 * @overload initialize(length = 0, value = 0.0)
 *  @param length [Integer] the length of the buffer
 *  @param value  [Float] the value which is set to all elements of the buffer
 *  @return [PaddleC::FloatBuffer]
 * @overload initialize(a)
 *  @param a [Array<Float>, PaddleC::FloatBuffer] an array of float, or a FloatBuffer
 *  @return [PaddleC::FloatBuffer]
 * @overload initialize(str)
 *  @param str [String] a binary string
 *  @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_initialize(int argc, VALUE *argv, VALUE self)
{
	pdlc_buffer_t *fbuf;
	VALUE rblength, rbvalue;
	long length = 0;
	float value = 0.0f;
	size_t i;
	const pdlc_buffer_t *ofbuf;

	fbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &rblength, &rbvalue);

	if (rb_class_of(rblength) == rb_cArray) {
		if (rbvalue != Qnil)
			rb_raise(rb_eArgError, "Not expecting another argument along with the array");
		length = rb_array_len(rblength);
		pdlc_buffer_resize(fbuf, (size_t)length, 0);
		for (i = 0; i < (size_t)length; i++)
			fbuf->data[i] = (float)NUM2DBL(rb_ary_entry(rblength, i));
	}
	else if (rb_class_of(rblength) == c_FloatBuffer) {
		if (rbvalue != Qnil)
			rb_raise(rb_eArgError, "Not expecting another argument along with the FloatBuffer");
		ofbuf = paddlec_float_buffer_get_struct(rblength);
		length = (long)ofbuf->length;
		pdlc_buffer_resize(fbuf, (size_t)length, 0);
		memcpy(fbuf->data, ofbuf->data, length*sizeof(float));
	}
	else if (rb_class_of(rblength) == rb_cString) {
		length = (long)RSTRING_LEN(rblength) / sizeof(float);
		pdlc_buffer_resize(fbuf, (size_t)length, 0);
		memcpy(fbuf->data, StringValuePtr(rblength), length*sizeof(float));
	}
	else {
		if (rblength != Qnil)
			length = NUM2LONG(rblength);
		if (rbvalue != Qnil)
			value = (float)NUM2DBL(rbvalue);
		if (length < 0)
			rb_raise(rb_eArgError, "negative buffer size");

		if (length > 0) {
			if (value != 0.0) {
				pdlc_buffer_resize(fbuf, (size_t)length, 0);
				pdlc_buffer_set(fbuf, value);
			}
			else
				pdlc_buffer_resize(fbuf, (size_t)length, 1);
		}
	}

	return self;
}


/* @!group Array
 */

/* Change the length of the buffer
 * @param new_length [Integer] the new length of the buffer, as a positive integer
 * @return [self] +self+
 */
static VALUE paddlec_float_buffer_resize(VALUE self, VALUE new_length)
{
	pdlc_buffer_t *fbuf;
	long length;

	length = NUM2LONG(new_length);
	if (length < 0)
		rb_raise(rb_eArgError, "New length cannot be less than 0");

	fbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_resize(fbuf, (size_t)length, 1);

	return self;
}


/* @return [Array<Float>]
 */
static VALUE paddlec_float_buffer_to_a(VALUE self)
{
	size_t i;
	pdlc_buffer_t *fbuf;
	VALUE ar;

	fbuf = paddlec_float_buffer_get_struct(self);
	ar = rb_ary_new_capa(fbuf->length);
	for (i = 0; i < fbuf->length; i++)
		rb_ary_store(ar, i, DBL2NUM((double)fbuf->data[i]));

	return ar;
}


/* @return [Integer] the length of the buffer
 */
static VALUE paddlec_float_buffer_length(VALUE self)
{
	pdlc_buffer_t *fbuf = paddlec_float_buffer_get_struct(self);
	return ULONG2NUM(fbuf->length);
}


/* @return [Boolean] true if length < 1
 */
static VALUE paddlec_float_buffer_isempty(VALUE self)
{
	pdlc_buffer_t *fbuf = paddlec_float_buffer_get_struct(self);
	return ((fbuf->length < 1) ? Qtrue : Qfalse);
}


/* Element Reference -- Returns the element at index, 
 * or returns a sub-buffer starting at the start index and continuing for length elements, 
 * or returns a sub-buffer specified by range of indices.
 *
 * Negative indices count backward from the end of the buffer (-1 is the last element). 
 * Returns nil if the index (or starting index) are out of range.
 *
 * @overload [](index)
 *  @param index [Integer] the index of the element
 *  @return [Float, nil] the element at +index+, or +nil+ if index out of bound
 * @overload [](start, length)
 *  @param start [Integer] the start index of the sub-buffer
 *  @param length [Integer] the length of the sub-buffer
 *  @return [PaddleC::FloatBuffer, nil] a new FloatBuffer, or +nil+ if indexes are out of bound
 * @overload [](range)
 *  @param range [Range] the range of indexes of the sub-buffer
 *  @return [PaddleC::FloatBuffer, nil] a new FloatBuffer, or +nil+ if indexes are out of bound
 */
static VALUE paddlec_float_buffer_get(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *fbuf;
	pdlc_buffer_t *nfbuf;
	VALUE rbind_start_range;
	VALUE rblength;
	long ind_start;
	long ind_end;
	long length;
	VALUE res = Qnil;
	VALUE beg, end; 
	int exc;

	fbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "11", &rbind_start_range, &rblength);

	if (rblength == Qnil && rb_range_values(rbind_start_range, &beg, &end, &exc)) {
		ind_start = NUM2LONG(beg);
		if (ind_start < 0)
			ind_start = (long)fbuf->length + ind_start;
		if (ind_start < 0 || ind_start >= (long)fbuf->length)
			return Qnil;
		ind_end = NUM2LONG(end);
		if (ind_end < 0)
			ind_end = (long)fbuf->length + ind_end;
		if (exc)
			ind_end -= 1;
		length = ind_end + 1 - ind_start;
		if (length < 0)
			length = 0;
	}
	else {
		ind_start = NUM2LONG(rbind_start_range);
		if (ind_start < 0)
			ind_start = (long)fbuf->length + ind_start;
		if (ind_start < 0 || ind_start >= (long)fbuf->length)
			return Qnil;
		if (rblength == Qnil)
			return DBL2NUM((double)fbuf->data[ind_start]);
		length = NUM2LONG(rblength);
		if (length < 0)
			return Qnil;
	}

	if ((ind_start + length) > (long)fbuf->length)
		length = (long)fbuf->length - ind_start;
	rblength = LONG2NUM(length);

	res = rb_class_new_instance(1, &rblength, c_FloatBuffer);

	nfbuf = paddlec_float_buffer_get_struct(res);
	memcpy(nfbuf->data, fbuf->data + ind_start, length*sizeof(float));

	return res;
}


/* Element Assignment -- Sets the element at index, 
 * or replaces a subarray from the start index for length elements, 
 * or replaces a subarray specified by the range of indices.
 *
 * Negative indices count backward from the end of the buffer (-1 is the last element). 
 * An exception is raised if indexes are out of bounds. 
 *
 * @overload []=(index, value)
 *  @param index [Integer] the index of the element
 *  @param value [Float] the element to set at +index+
 * @overload []=(start, length, vaule)
 *  @param start [Integer] the start index of the sub-buffer
 *  @param length [Integer] the length of the sub-buffer
 *  @param value [Float, Array<Float>, FloatBuffer] the element to set at +index+
 * @overload []=(range, value)
 *  @param range [Range] the range of indexes of the sub-buffer
 *  @param value [Float, Array<Float>, FloatBuffer] the element to set at +index+
 */
static VALUE paddlec_float_buffer_set(int argc, VALUE *argv, VALUE self)
{
	pdlc_buffer_t *fbuf;
	const pdlc_buffer_t *ofbuf;
	VALUE rbind_start_range;
	VALUE rblength;
	VALUE rbvalue;
	long ind_start, length, i, j, ind_end;
	VALUE beg, end; 
	int exc;
	float value;

	fbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "111", &rbind_start_range, &rblength, &rbvalue);

	if (rblength == Qnil && rb_range_values(rbind_start_range, &beg, &end, &exc)) {
		ind_start = NUM2LONG(beg);
		if (ind_start < 0)
			ind_start = (long)fbuf->length + ind_start;
		if (ind_start < 0 || ind_start >= (long)fbuf->length)
			rb_raise(rb_eIndexError, "index out of bounds");
		ind_end = NUM2LONG(end);
		if (ind_end < 0)
			ind_end = (long)fbuf->length + ind_end;
		if (exc)
			ind_end -= 1;
		length = ind_end + 1 - ind_start;
	}
	else {
		ind_start = NUM2LONG(rbind_start_range);
		if (ind_start < 0)
			ind_start = (long)fbuf->length + ind_start;
		if (ind_start < 0 || ind_start >= (long)fbuf->length)
			rb_raise(rb_eIndexError, "index out of bounds");
		if (rblength == Qnil)
			length = 1;
		else
			length = NUM2LONG(rblength);
	}
	if (length < 1)
		rb_raise(rb_eIndexError, "index range maps to %ld..%ld which does not have a strictly positive length", ind_start, ind_start + length - 1);
	if ((ind_start + length) > (long)fbuf->length)
		rb_raise(rb_eIndexError, "index range maps to %ld..%ld which exceeds the buffer length [0..%lu]", ind_start, ind_start + length - 1, fbuf->length - 1);

	if (rb_class_of(rbvalue) == rb_cArray) {
		if (length != rb_array_len(rbvalue))
			rb_raise(rb_eArgError, "trying to assign a %ld long array to a %ld long sub-buffer", rb_array_len(rbvalue), length);
		for (j = 0, i = ind_start; j < length; i++, j++)
			fbuf->data[i] = (float)NUM2DBL(rb_ary_entry(rbvalue, j));
	}
	else if (rb_class_of(rbvalue) == c_FloatBuffer) {
		ofbuf = paddlec_float_buffer_get_struct(rbvalue);
		if (length != (long)ofbuf->length)
			rb_raise(rb_eArgError, "trying to assign a %lu long %"PRIsVALUE" to a %ld long sub-buffer", ofbuf->length, rb_class_name(rb_class_of(rbvalue)), length);
		for (j = 0, i = ind_start; j < length; i++, j++)
			fbuf->data[i] = ofbuf->data[j];
	}
	else {
		value = (float)NUM2DBL(rbvalue);
		ind_end = ind_start + length;
		for (i = ind_start; i < ind_end; i++)
			fbuf->data[i] = value;
	}

	return rbvalue;
}


/* @see Array#zip
 * @return [Array]
 */
static VALUE paddlec_float_buffer_zip(int argc, VALUE *argv, VALUE self)
{
	VALUE a, res;

	a = paddlec_float_buffer_to_a(self);
	res = rb_funcallv(a, id_zip, argc, argv);

	return res;
}


/* @!endgroup
 */


/* @!group Enumerable
 */


static VALUE paddlec_float_buffer_to_enum_get_size_block(VALUE block_arg, VALUE data, int argc, VALUE* argv)
{
	(void)block_arg;
	(void)argc;
	(void)argv;
	return rb_funcallv(data, id_length, 0, NULL);
}


/* Calls the given block once for each element in +self+, 
 * passing that element as a parameter. 
 * Returns the array itself, or, if no block is given, an Enumerator is returned.
 * @return [Object, Enumerator]
 * @overload each{|item| block}
 *  @return [Array<Float>] self
 * @overload each
 *  @return [Enumerator]
 */
static VALUE paddlec_float_buffer_each(VALUE self)
{
	size_t i;
	pdlc_buffer_t *fbuf;
	VALUE res;
	VALUE each_sym;

	fbuf = paddlec_float_buffer_get_struct(self);

	if (rb_block_given_p()) {
		for (i = 0; i < fbuf->length; i++)
			rb_yield(DBL2NUM(fbuf->data[i]));
		res = self;
	}
	else {
		each_sym = ID2SYM(id_each);
		res = rb_block_call(self, id_to_enum, 1, &each_sym, paddlec_float_buffer_to_enum_get_size_block, self);
	}

	return res;
}


/* Invokes the given block once for each element of self, 
 * replacing the element with the value returned by the block.
 * If no block is given, an Enumerator is returned instead.
 * @overload collect!{|value| block}
 * @return [self]
 * @overload collect!
 * @return [Enumerator]
 */
static VALUE paddlec_float_buffer_collect_inp(VALUE self)
{
	pdlc_buffer_t *fbuf;
	size_t i;
	VALUE res;
	VALUE collectI_sym;

	fbuf = paddlec_float_buffer_get_struct(self);

	if (rb_block_given_p()) {
		for (i = 0; i < fbuf->length; i++)
			fbuf->data[i] = (float)NUM2DBL(rb_yield(DBL2NUM(fbuf->data[i])));
		res = self;
	}
	else {
		collectI_sym = ID2SYM(id_collectI);
		res = rb_block_call(self, id_to_enum, 1, &collectI_sym, paddlec_float_buffer_to_enum_get_size_block, self);
	}

	return res;
}


/* @!endgroup
 */


/* @!group Object
 */


/* Returns a new FloatBuffer with the same content as +self+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_clone(VALUE self)
{
	const pdlc_buffer_t *fbuf;
	pdlc_buffer_t *newfbuf;
	VALUE newFloatBuffer;
	VALUE len;

	fbuf = paddlec_float_buffer_get_struct(self);
	len = rb_funcallv(self, id_length, 0, NULL);

	newFloatBuffer = rb_class_new_instance(1, &len, c_FloatBuffer);
	newfbuf = paddlec_float_buffer_get_struct(newFloatBuffer);
	memcpy(newfbuf->data, fbuf->data, fbuf->length*sizeof(float));

	return newFloatBuffer;
}


/* @!endgroup
 */


/* @!group String
 */


#define PADDLEC_FLOAT_BUFFER_MAX_TO_S_ELEMS 1024
/* @return [String]
 */
static VALUE paddlec_float_buffer_to_s(VALUE self)
{
	const pdlc_buffer_t *fbuf;
	size_t nb_elm_to_print;
	char *cstr, *p;
	size_t i;
	size_t avail;
	VALUE str;

	fbuf = paddlec_float_buffer_get_struct(self);

	nb_elm_to_print = fbuf->length;
	if (nb_elm_to_print > PADDLEC_FLOAT_BUFFER_MAX_TO_S_ELEMS)
		nb_elm_to_print = PADDLEC_FLOAT_BUFFER_MAX_TO_S_ELEMS;

	avail = 16*nb_elm_to_print;
	cstr = malloc(avail+64);
	p = cstr;
	sprintf(p, "["); p += strlen(p);
	if (nb_elm_to_print > 0) {
		switch (fpclassify(fbuf->data[0])) {
			case FP_NORMAL:
			case FP_SUBNORMAL:
			case FP_ZERO:
				sprintf(p, "%.7g", fbuf->data[0]); 
				break;
			case FP_INFINITE:
				sprintf(p, "%sInfinity", (fbuf->data[0] < 0.0f) ? "-" : ""); 
				break;
			default:
				sprintf(p, "NaN"); 
		}
		p += strlen(p);
	}
	i = 1;
	while ((size_t)(p - cstr) < avail && i < fbuf->length) {
		switch (fpclassify(fbuf->data[i])) {
			case FP_NORMAL:
			case FP_SUBNORMAL:
			case FP_ZERO:
				sprintf(p, ", %.7g", fbuf->data[i]); 
				break;
			case FP_INFINITE:
				sprintf(p, ", %sInfinity", (fbuf->data[i] < 0.0f) ? "-" : ""); 
				break;
			default:
				sprintf(p, ", NaN"); 
		}
		p += strlen(p);
		i++;
	}
	if (i < fbuf->length) {
		sprintf(p, ", ...(+%lu)", fbuf->length - i); 
		p += strlen(p);
	}
	sprintf(p, "]");
	p += 1;

	str = rb_str_new(cstr, (long)(p - cstr));

	free(cstr);

	return str;
}


/* @return [String]
 */
static VALUE paddlec_float_buffer_to_gplot(VALUE self)
{
	const pdlc_buffer_t *fbuf;
	size_t nb_elm_to_print;
	char *cstr, *p;
	size_t i;
	size_t avail;
	VALUE str;

	fbuf = paddlec_float_buffer_get_struct(self);

	nb_elm_to_print = fbuf->length;

	avail = 16*nb_elm_to_print;
	cstr = malloc(avail+64);
	p = cstr;
	i = 0;
	while (i < fbuf->length && (size_t)(p - cstr) < avail) {
		switch (fpclassify(fbuf->data[i])) {
			case FP_NORMAL:
			case FP_SUBNORMAL:
			case FP_ZERO:
				sprintf(p, "%.7g\n", fbuf->data[i]); 
				break;
			case FP_INFINITE:
				sprintf(p, "%sInfinity\n", (fbuf->data[i] < 0.0f) ? "-" : ""); 
				break;
			default:
				sprintf(p, "NaN\n"); 
		}
		p += strlen(p);
		i++;
	}

	str = rb_str_new(cstr, (long)(p - cstr));

	free(cstr);

	return str;
}


/* Packs the contents of the buffer into a binary string, according to +format+.
 * If the optional +outbuf+ is present, it must reference a String, which will receive the data.
 * In this case, +outbuf will be resized accordingly.
 * @return [String] the content of the buffer as a binary string
 * @overload pack(format, outbuf = nil)
 * @param format [Symbol] either +:u8+, +:s8+, +:u16+, +:s16+, +:u32+, +:s328+ or +:f32+
 * @param outbuf [String, nil]
 */
static VALUE paddlec_float_buffer_pack(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *fbuf;
	VALUE str, format;
	size_t i, len, slen;
	ID fmt;
	uint8_t  *u8;
	int8_t   *s8;
	uint16_t *u16;
	int16_t  *s16;
	uint32_t *u32;
	int32_t  *s32;
	float    *f32;

	rb_scan_args(argc, argv, "11", &format, &str);

	fbuf = paddlec_float_buffer_get_struct(self);
	len = fbuf->length;

	if (rb_class_of(format) != rb_cSymbol)
		rb_raise(rb_eTypeError, "format must be a symbol, not a %"PRIsVALUE, rb_class_name(rb_class_of(format)));
	fmt = SYM2ID(format);
	if (fmt == id_u8 || fmt == id_s8)
		slen = len;
	else if (fmt == id_u16 || fmt == id_s16)
		slen = len * 2;
	else if (fmt == id_u32 || fmt == id_s32 || fmt == id_f32)
		slen = len * 4;
	else
		rb_raise(rb_eTypeError, "format must be either :u8, :s8, :u16, :s16, :u32, :s32 or :f32, not %"PRIsVALUE, format);

	if (str == Qnil)
		str = rb_usascii_str_new_cstr("");
	else if (!rb_obj_is_kind_of(str, rb_cString))
		rb_raise(rb_eTypeError, "expecting a string, not a %"PRIsVALUE, rb_class_name(rb_class_of(str)));
	str = rb_str_resize(str, slen);

	if (fmt == id_u8) {
		u8 = (uint8_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++)
			u8[i] = (uint8_t)fminf(255.0f, fmaxf(0.0f, (fbuf->data[i] * 128.0f) + 128.0f));
	}
	else if (fmt == id_s8) {
		s8 = (int8_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++)
			s8[i] = (int8_t)fminf(127.0f, fmaxf(-128.0f, fbuf->data[i] * 128.0f));
	}
	else if (fmt == id_u16) {
		u16 = (uint16_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++)
			u16[i] = (uint16_t)fminf(65535.0f, fmaxf(0.0f, (fbuf->data[i] * 32768.0f) + 32768.0f));
	}
	else if (fmt == id_s16) {
		s16 = (int16_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++)
			s16[i] = (uint16_t)fminf(32767.0f, fmaxf(-32768.0f, fbuf->data[i] * 32768.0f));
	}
	else if (fmt == id_u32) {
		u32 = (uint32_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++)
			u32[i] = (uint32_t)fminf(4294967295.0f, fmaxf(0.0f, (fbuf->data[i] * 2147483648.0f) + 2147483648.0f));
	}
	else if (fmt == id_s32) {
		s32 = (int32_t*)rb_string_value_ptr(&str);
		for (i = 0; i < len; i++)
			s32[i] = (int32_t)fminf(2147483647.0f, fmaxf(-2147483648.0f, fbuf->data[i] * 2147483648.0f));
	}
	else if (fmt == id_f32) {
		f32 = (float*)rb_string_value_ptr(&str);
		memcpy(f32, fbuf->data, len*4);
	}

	return str;
}


/* Unpack the string +str+ into +self+ according to +format+.
 * The float buffer is resized accordingly.
 * Elements of +self+ will be in the range [-1, 1[;
 * @param str [String]
 * @param format [Symbol] either +:u8+, +:s8+, +:u16+, +:s16+, +:u32+, +:s328+ or +:f32+
 * @return [self]
 */
static VALUE paddlec_float_buffer_unpack(VALUE self, VALUE str, VALUE format)
{
	pdlc_buffer_t *fbuf;
	ID fmt;
	const void *ptr;
	const uint8_t  *u8;
	const  int8_t  *s8;
	const uint16_t *u16;
	const  int16_t *s16;
	const uint32_t *u32;
	const  int32_t *s32;
	const    float *f32;
	size_t i, len;

	if (!rb_obj_is_kind_of(str, rb_cString))
		rb_raise(rb_eTypeError, "expecting a string, not a %"PRIsVALUE, rb_class_name(rb_class_of(str)));
	if (rb_class_of(format) != rb_cSymbol)
		rb_raise(rb_eTypeError, "format must be a symbol, not a %"PRIsVALUE, rb_class_name(rb_class_of(format)));
	fmt = SYM2ID(format);
	ptr = rb_string_value_ptr(&str);
	len = (size_t)RSTRING_LEN(str);

	fbuf = paddlec_float_buffer_get_struct(self);

	if (fmt == id_u8) {
		u8 = ptr;
		pdlc_buffer_resize(fbuf, len, 0);
		for (i = 0; i < len; i++)
			fbuf->data[i] = ((float)u8[i] - 128.0f) / 128.0f;
	}
	else if (fmt == id_s8) {
		s8 = ptr;
		pdlc_buffer_resize(fbuf, len, 0);
		for (i = 0; i < len; i++)
			fbuf->data[i] = (float)s8[i] / 128.0f;
	}
	else if (fmt == id_u16) {
		u16 = ptr;
		len /= 2;
		pdlc_buffer_resize(fbuf, len, 0);
		for (i = 0; i < len; i++)
			fbuf->data[i] = ((float)u16[i] - 32768.0f) / 32768.0f;
	}
	else if (fmt == id_s16) {
		s16 = ptr;
		len /= 2;
		pdlc_buffer_resize(fbuf, len, 0);
		for (i = 0; i < len; i++)
			fbuf->data[i] = (float)s16[i] / 32768.0f;
	}
	else if (fmt == id_u32) {
		u32 = ptr;
		len /= 4;
		pdlc_buffer_resize(fbuf, len, 0);
		for (i = 0; i < len; i++)
			fbuf->data[i] = ((float)u32[i] - 2147483648.0f) / 2147483648.0f;
	}
	else if (fmt == id_s32) {
		s32 = ptr;
		len /= 4;
		pdlc_buffer_resize(fbuf, len, 0);
		for (i = 0; i < len; i++)
			fbuf->data[i] = (float)s32[i] / 2147483648.0f;
	}
	else if (fmt == id_f32) {
		f32 = ptr;
		len /= 4;
		pdlc_buffer_resize(fbuf, len, 0);
		memcpy(fbuf->data, f32, len*4);
	}
	else
		rb_raise(rb_eTypeError, "format must be either :u8, :s8, :u16, :s16, :u32, :s32 or :f32, not %"PRIsVALUE, format);

	return self;
}


/* Unpack the string +str+ into a new {PaddleC::FloatBuffer} according to +format+.
 * The float buffer is resized accordingly.
 * Elements of +self+ will be in the range [-1, 1[;
 * @param str [String]
 * @param format [Symbol] either +:u8+, +:s8+, +:u16+, +:s16+, +:u32+, +:s328+ or +:f32+
 * @return [PaddleC::FloatBuffer] a new {PaddleC::FloatBuffer} filled with values from +str+
 */
static VALUE paddlec_float_buffer_classunpack(VALUE self, VALUE str, VALUE format)
{
	VALUE res = rb_class_new_instance(0, NULL, c_FloatBuffer);
	paddlec_float_buffer_unpack(res, str, format);
	return res;
}


/* @!endgroup
 */


/* @!group FFI
 */


/* Returns the native pointer pointing to the array of floats as an integer
 * @return [Integer] native pointer to the data
 */
static VALUE paddlec_float_buffer_native_ptr(VALUE self)
{
	const pdlc_buffer_t *fbuf;
	void *ptr;
	VALUE res;

	fbuf = paddlec_float_buffer_get_struct(self);

	ptr = fbuf->data;
	res = ULL2NUM( (sizeof(ptr) == 4) ? (unsigned long long int)(unsigned long int)ptr : (unsigned long long int)ptr );

	return res;
}


/* Returns a +FFI::Pointer+ pointing to the native array of floats.
 * +FFI+ must be requiered before +paddlec+ for this method to be defined.
 * @return [FFI::Pointer] FFI::Pointer pointing to the native array of floats
 */
static VALUE paddlec_float_buffer_ffi_pointer(VALUE self)
{
	const pdlc_buffer_t *fbuf;
	VALUE type_and_addr[2];
	VALUE res;

	fbuf = paddlec_float_buffer_get_struct(self);

	type_and_addr[0] = o_FFI_Type_FLOAT;
	type_and_addr[1] = paddlec_float_buffer_native_ptr(self);
	res = rb_class_new_instance(2, type_and_addr, c_FFI_Pointer);
	res = rb_funcall(res, rb_intern("slice"), 2, LONG2NUM(0), ULONG2NUM(fbuf->length*sizeof(float)));

	return res;
}


/* @!endgroup
 */


/* @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_array_to_float_buffer(VALUE self)
{
	return rb_class_new_instance(1, &self, c_FloatBuffer);
}


/* @return [self]
 */
static VALUE paddlec_float_buffer_to_float_buffer(VALUE self)
{
	return self;
}


/* @return [PaddleC::ComplexBuffer] a new {PaddleC::ComplexBuffer} whose real part is +slef+ and imaginary part is 0
 */
static VALUE paddlec_float_buffer_to_complex_buffer(VALUE self)
{
	return rb_class_new_instance(1, &self, c_ComplexBuffer);
}


/* If +numeric+ is a +Complex+, returns an array +[PaddleC::ComplexBuffer, self]+.
 * Otherwise, returns an array +[PaddleC::FloatBuffer, self]+.
 *
 * The coercion mechanism is used by Ruby to handle mixed-type numeric operations: 
 * it is intended to find a compatible common type between the two operands of the operator.
 *
 * @param numeric [Numeric]
 * @return [Array<PaddleC::FloatBuffer, PaddleC::ComplexBuffer>]
 */
static VALUE paddlec_float_buffer_coerce(VALUE self, VALUE numeric)
{
	VALUE other_coerced;
	VALUE len_val[2];

	len_val[0] = rb_funcallv(self, id_length, 0, NULL);
	len_val[1] = numeric;
	if (rb_class_of(numeric) == rb_cComplex)
		other_coerced = rb_class_new_instance(2, len_val, c_ComplexBuffer);
	else
		other_coerced = rb_class_new_instance(2, len_val, c_FloatBuffer);

	return rb_assoc_new(other_coerced, self);
}


static pdlc_buffer_t* pdlc_buffer_finite(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = (isfinite(fbuf->data[i]) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_buffer_infinite(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = ((isinf(fbuf->data[i])) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_buffer_nan(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = ((isnan(fbuf->data[i])) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_buffer_integer(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = ((isfinite(fbuf->data[i]) && (fbuf->data[i] == truncf(fbuf->data[i]))) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_buffer_negative(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = ((fbuf->data[i] < 0.0f) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_buffer_positive(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = ((fbuf->data[i] > 0.0f) ? 1.0f : 0.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_buffer_nonzero(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = ((fbuf->data[i] == 0.0f) ? 0.0f : 1.0f);

	return ofbuf;
}


static pdlc_buffer_t* pdlc_buffer_zero(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = ((fbuf->data[i] == 0.0f) ? 1.0f : 0.0f);

	return ofbuf;
}


static inline int pdlc_f2b(float x)
{
	return ((x == 0.0f || isnan(x)) ? 0 : 1);
}


static pdlc_buffer_t* pdlc_buffer_and(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (float)(pdlc_f2b(lhs->data[i]) & pdlc_f2b(rhs->data[i]));

	return res;
}


static pdlc_buffer_t* pdlc_buffer_or(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (float)(pdlc_f2b(lhs->data[i]) | pdlc_f2b(rhs->data[i]));

	return res;
}


static pdlc_buffer_t* pdlc_buffer_xor(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (float)(pdlc_f2b(lhs->data[i]) ^ pdlc_f2b(rhs->data[i]));

	return res;
}


static pdlc_buffer_t* pdlc_buffer_not(const pdlc_buffer_t *lhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (pdlc_f2b(lhs->data[i]) ? 0.0f : 1.0f );

	return res;
}


/* @!group Float
 */


/* Returns a {PaddleC::FloatBuffer}. 1.0 for values which are valid IEEE floating point number, i.e. which are not infinite or NaN, 0.0 otherwize.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_finite(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_buffer_finite(mfbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for values which are positive or negative infinity, 0.0 otherwize.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_infinite(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_buffer_infinite(mfbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for values which are NaN, 0.0 otherwize.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_nan(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_buffer_nan(mfbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 if +value == value.truncate+, 0.0 otherwise.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_integer(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_buffer_integer(mfbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 if +value < 0+, 0.0 otherwise.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_negative(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_buffer_negative(mfbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 if +value > 0+, 0.0 otherwise.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_positive(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_buffer_positive(mfbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 if +value != 0+, 0.0 otherwise.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_nonzero(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_buffer_nonzero(mfbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 if +value == 0+, 0.0 otherwise.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_zero(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_buffer_zero(mfbuf, rfbuf);

	return buffer;
}


/* @!endgroup
 */


/* @!group Logic operators
 */


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::FloatBuffer]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_and(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	ofbuf = paddlec_float_buffer_get_struct(other);

	if (!rb_obj_is_kind_of(other, c_FloatBuffer))
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(other)));

	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_and(mfbuf, ofbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::FloatBuffer]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_or(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	ofbuf = paddlec_float_buffer_get_struct(other);

	if (!rb_obj_is_kind_of(other, c_FloatBuffer))
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(other)));

	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_or(mfbuf, ofbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::FloatBuffer]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_xor(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	ofbuf = paddlec_float_buffer_get_struct(other);

	if (!rb_obj_is_kind_of(other, c_FloatBuffer))
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(other)));

	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_xor(mfbuf, ofbuf, rfbuf);

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_not(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);
	buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);
	pdlc_buffer_not(mfbuf, rfbuf);

	return buffer;
}


/* @!endgroup
 */


/* @!group Comparison
 */


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::FloatBuffer, Numeric]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_cmpless(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_cmpless(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_cmpless(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::FloatBuffer, Numeric]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_cmplessequ(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_cmplessequ(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_cmplessequ(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::FloatBuffer, Numeric]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_cmpgrt(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_cmpgrt(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_cmpgrt(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::FloatBuffer, Numeric]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_cmpgrtequ(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_cmpgrtequ(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_cmpgrtequ(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_equ(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_equ(mfbuf, ocbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_equ(mfbuf, oc, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_equ(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_equ(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.
 * @param other [PaddleC::ComplexBuffer, Complex, PaddleC::FloatBuffer, Numeric]
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_different(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex) || rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_different(mfbuf, ocbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_different(mfbuf, oc, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_different(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_different(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* @!endgroup
 */


/* @!group Rounding
 */


/* Returns the smallest numbers greater than or equal to values of +self+, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload ceil(ndigits = 0, buffer = nil)
 * @param ndigits [Integer] precision
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_ceil(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE digits, buffer, tmp;
	int d;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &digits, &buffer);
	if (NIL_P(buffer) && rb_obj_is_kind_of(digits, c_FloatBuffer)) {
		buffer = digits;
		digits = Qnil;
	}
	else if (rb_obj_is_kind_of(buffer, rb_cNumeric) && rb_obj_is_kind_of(digits, c_FloatBuffer)) {
		tmp = buffer;
		buffer = digits;
		digits = tmp;
	}

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	if (digits == Qnil)
		pdlc_buffer_ceil(mfbuf, rfbuf);
	else {
		d = NUM2INT(digits);
		pdlc_buffer_ceil_digits(mfbuf, d, rfbuf);
	}

	return buffer;
}


/* Returns the smallest integral values that are not less than values of +self+, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload ceil!(ndigits = 0)
 * @param ndigits [Integer] precision
 * @return [self]
 */
static VALUE paddlec_float_buffer_ceil_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_buffer_t *mfbuf;
	VALUE digits;
	int d;

	rb_scan_args(argc, argv, "01", &digits);
	mfbuf = paddlec_float_buffer_get_struct(self);

	if (digits == Qnil) 
		pdlc_buffer_ceil_inplace(mfbuf);
	else {
		d = NUM2INT(digits);
		pdlc_buffer_ceil_digits_inplace(mfbuf, d);
	}

	return self;
}


/* Returns the largest integral values that are not greater than values of +self+, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload floor(ndigits = 0, buffer = nil)
 * @param ndigits [Integer] precision
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_floor(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE digits, buffer, tmp;
	int d;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &digits, &buffer);
	if (NIL_P(buffer) && rb_obj_is_kind_of(digits, c_FloatBuffer)) {
		buffer = digits;
		digits = Qnil;
	}
	else if (rb_obj_is_kind_of(buffer, rb_cNumeric) && rb_obj_is_kind_of(digits, c_FloatBuffer)) {
		tmp = buffer;
		buffer = digits;
		digits = tmp;
	}

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	if (digits == Qnil)
		pdlc_buffer_floor(mfbuf, rfbuf);
	else {
		d = NUM2INT(digits);
		pdlc_buffer_floor_digits(mfbuf, d, rfbuf);
	}

	return buffer;
}


/* Returns the largest integral values that are not greater than values of +self+, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload floor!(ndigits = 0)
 * @param ndigits [Integer] precision
 * @return [self]
 */
static VALUE paddlec_float_buffer_floor_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_buffer_t *mfbuf;
	VALUE digits;
	int d;

	rb_scan_args(argc, argv, "01", &digits);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (digits == Qnil) 
		pdlc_buffer_floor_inplace(mfbuf);
	else {
		d = NUM2INT(digits);
		pdlc_buffer_floor_digits_inplace(mfbuf, d);
	}

	return self;
}


/* Truncates values of +self+ to the nearest integers not larger in absolute value, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload truncate(ndigits = 0, buffer = nil)
 * @param ndigits [Integer] precision
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_truncate(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE digits, buffer, tmp;
	int d;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &digits, &buffer);
	if (NIL_P(buffer) && rb_obj_is_kind_of(digits, c_FloatBuffer)) {
		buffer = digits;
		digits = Qnil;
	}
	else if (rb_obj_is_kind_of(buffer, rb_cNumeric) && rb_obj_is_kind_of(digits, c_FloatBuffer)) {
		tmp = buffer;
		buffer = digits;
		digits = tmp;
	}

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	if (digits == Qnil)
		pdlc_buffer_truncate(mfbuf, rfbuf);
	else {
		d = NUM2INT(digits);
		pdlc_buffer_truncate_digits(mfbuf, d, rfbuf);
	}

	return buffer;
}


/* Truncates values of +self+ to the nearest integers not larger in absolute value, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload truncate!(ndigits = 0)
 * @param ndigits [Integer] precision
 * @return [self]
 */
static VALUE paddlec_float_buffer_truncate_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_buffer_t *mfbuf;
	VALUE digits;
	int d;

	rb_scan_args(argc, argv, "01", &digits);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (digits == Qnil) 
		pdlc_buffer_truncate_inplace(mfbuf);
	else {
		d = NUM2INT(digits);
		pdlc_buffer_truncate_digits_inplace(mfbuf, d);
	}

	return self;
}


/* Rounds values of +self+ to the nearest integers, but round halfway cases away from zero, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload round(ndigits = 0, buffer = nil)
 * @param ndigits [Integer] precision
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_round(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE digits, buffer, tmp;
	int d;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "02", &digits, &buffer);
	if (NIL_P(buffer) && rb_obj_is_kind_of(digits, c_FloatBuffer)) {
		buffer = digits;
		digits = Qnil;
	}
	else if (rb_obj_is_kind_of(buffer, rb_cNumeric) && rb_obj_is_kind_of(digits, c_FloatBuffer)) {
		tmp = buffer;
		buffer = digits;
		digits = tmp;
	}

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	if (digits == Qnil)
		pdlc_buffer_round(mfbuf, rfbuf);
	else {
		d = NUM2INT(digits);
		pdlc_buffer_round_digits(mfbuf, d, rfbuf);
	}

	return buffer;
}


/* Rounds values of +self+ to the nearest integers, but round halfway cases away from zero, with a precision of +ndigits+ decimal digits (default: 0).
 * @overload round!(ndigits = 0)
 * @param ndigits [Integer] precision
 * @return [self]
 */
static VALUE paddlec_float_buffer_round_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_buffer_t *mfbuf;
	VALUE digits;
	int d;

	rb_scan_args(argc, argv, "01", &digits);
	mfbuf = paddlec_float_buffer_get_struct(self);

	if (digits == Qnil) 
		pdlc_buffer_round_inplace(mfbuf);
	else {
		d = NUM2INT(digits);
		pdlc_buffer_round_digits_inplace(mfbuf, d);
	}

	return self;
}


/* @!endgroup
 */


/* @!group Complex
 */


/* Returns the absolute value / the absolute part of its polar form.
 * @overload abs(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_abs(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_abs(mfbuf, rfbuf);

	return buffer;
}


/* Returns the absolute value / the absolute part of its polar form.
 * @return [self]
 */
static VALUE paddlec_float_buffer_abs_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_abs_inplace(mfbuf);

	return self;
}


/* Returns the square of the absolute value / the square of the absolute part of its polar form.
 * @overload abs2(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_abs2(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_abs2(mfbuf, rfbuf);

	return buffer;
}


/* Returns the square of the absolute value / the square of the absolute part of its polar form.
 * @return [self]
 */
static VALUE paddlec_float_buffer_abs2_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_abs2_inplace(mfbuf);

	return self;
}


/* Returns the angle part of its polar form.
 * @overload arg(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_arg(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_arg(mfbuf, rfbuf);

	return buffer;
}


/* Returns the complex conjugate.
 * @overload conjugate(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_conjugate(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_conjugate(mfbuf, rfbuf);

	return buffer;
}


/* Returns the complex conjugate.
 * @return [self]
 */
static VALUE paddlec_float_buffer_conjugate_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_conjugate_inplace(mfbuf);

	return self;
}


/* Returns the imaginary part.
 * @overload imaginary(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_imag(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_imag(mfbuf, rfbuf);

	return buffer;
}


/* Returns the real part.
 * @overload real(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_real(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_real(mfbuf, rfbuf);

	return buffer;
}


/* Swap the real and imaginary parts.
 * @overload swapIQ(buffer = nil)
 * @param buffer [nil, PaddleC::ComplexBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_swapIQ(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_complex_buffer_t *rcbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
	rcbuf = paddlec_complex_buffer_get_struct(buffer);

	pdlc_buffer_swapIQ(mfbuf, rcbuf);

	return buffer;
}


/* @!endgroup
 */


/* @!group Arithmetic
 */


/* Unary plus, returns the receiver.
 * @return [self]
 */
static VALUE paddlec_float_buffer_unaryplus(VALUE self)
{
	return self;
}


/* Unary minus, returns the receiver, negated.
 * @overload -@(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_unaryminus(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_unaryminus(mfbuf, rfbuf);

	return buffer;
}


/* Unary minus, returns the receiver, negated.
 * @return [self]
 */
static VALUE paddlec_float_buffer_unaryminus_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_unaryminus_inplace(mfbuf);

	return self;
}


/* @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload -(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload -(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_sub(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_sub(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_sub(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_sub(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_sub(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_sub_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_sub_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_sub_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element soustraction.
 * @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload esub(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload esub(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_esub(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_esub(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_esub(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_esub(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_esub(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element soustraction.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_esub_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_esub_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_esub_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload +(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload +(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_add(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_add(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_add(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_add(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_add(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_add_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_add_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_add_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element addition.
 * @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload eadd(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload eadd(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_eadd(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_eadd(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_eadd(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_eadd(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_eadd(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element addition.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_eadd_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_eadd_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_eadd_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Performs multiplication.
 * @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload *(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload *(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_mult(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_mult(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_mult(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_mult(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_mult(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Performs multiplication.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_mult_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_mult_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_mult_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element multiplication.
 * @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload emult(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload emult(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_emult(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_emult(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_emult(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_emult(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_emult(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element multiplication.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_emult_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_emult_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_emult_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Performs division.
 * @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload /(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload /(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_div(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_div(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_div(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_div(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_div(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Performs division.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_div_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_div_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_div_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element division.
 * @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload ediv(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload ediv(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_ediv(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_ediv(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_ediv(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_ediv(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_ediv(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element division.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_ediv_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_ediv_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_ediv_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Performs exponentiation. Raises +self+ to the power of +rhs+.
 * @return [PaddleC::FloatBuffer]
 * @overload **(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_pow(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_pow(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_pow(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Performs exponentiation. Raises +self+ to the power of +rhs+.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_pow_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_pow_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_pow_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Returns the modulo after division of values of +lhs+ by values of +rhs+.
 * @return [PaddleC::FloatBuffer]
 * @overload %(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_modulo(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_modulo(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_modulo(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Returns the modulo after division of values of +lhs+ by values of +rhs+.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_modulo_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_modulo_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_modulo_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element modulo.
 * @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload emod(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload emod(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_emod(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_emod(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_emod(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_emod(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_emod(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element modulo.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_emod_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_emod_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_emod_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Element by element exponentiation.
 * @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer]
 * @overload epow(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 * @overload epow(other, buffer = nil)
 *  @param other [PaddleC::ComplexBuffer, Complex]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
 */
static VALUE paddlec_float_buffer_epow(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	pdlc_complex_buffer_t *rcbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	const pdlc_complex_buffer_t *ocbuf;
	pdlc_complex_t oc;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_ComplexBuffer) || rb_obj_is_kind_of(other, rb_cComplex)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_fb_cb_epow(mfbuf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_fb_cs_epow(mfbuf, oc, rcbuf);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_epow(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_epow(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_cComplex), rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* Element by element exponentiation.
 * @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_epow_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_epow_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_epow_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* @!endgroup
 */


/* @!group Math
 */


/* Computes the arc cosine of values of +self+. Returns 0..PI.
 * @overload acos(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_acos(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_acos(mfbuf, rfbuf);

	return buffer;
}


/* Computes the arc cosine of values of +self+. Returns 0..PI.
 * @return [self]
 */
static VALUE paddlec_float_buffer_acos_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_acos_inplace(mfbuf);

	return self;
}


/* Computes the inverse hyperbolic cosine of values of +self+.
 * @overload acosh(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_acosh(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_acosh(mfbuf, rfbuf);

	return buffer;
}


/* Computes the inverse hyperbolic cosine of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_acosh_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_acosh_inplace(mfbuf);

	return self;
}


/* Computes the arc sine of values of +self+. Returns -PI/2..PI/2.
 * @overload asin(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_asin(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_asin(mfbuf, rfbuf);

	return buffer;
}


/* Computes the arc sine of values of +self+. Returns -PI/2..PI/2.
 * @return [self]
 */
static VALUE paddlec_float_buffer_asin_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_asin_inplace(mfbuf);

	return self;
}


/* Computes the inverse hyperbolic sine of values of +self+.
 * @overload asinh(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_asinh(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_asinh(mfbuf, rfbuf);

	return buffer;
}


/* Computes the inverse hyperbolic sine of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_asinh_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_asinh_inplace(mfbuf);

	return self;
}


/* Computes the arc tangent of values of +self+.
 * @overload atan(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_atan(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_atan(mfbuf, rfbuf);

	return buffer;
}


/* Computes the arc tangent of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_atan_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_atan_inplace(mfbuf);

	return self;
}


/* Computes the inverse hyperbolic tangent of values of +self+.
 * @overload atanh(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_atanh(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_atanh(mfbuf, rfbuf);

	return buffer;
}


/* Computes the inverse hyperbolic tangent of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_atanh_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_atanh_inplace(mfbuf);

	return self;
}


/* Returns the cube root of values of +self+.
 * @overload cbrt(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_cbrt(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_cbrt(mfbuf, rfbuf);

	return buffer;
}


/* Returns the cube root of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_cbrt_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_cbrt_inplace(mfbuf);

	return self;
}


/* Computes the cosine of values of +self+ (expressed in radians). Returns a Float in the range -1.0..1.0.
 * @overload cos(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_cos(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_cos(mfbuf, rfbuf);

	return buffer;
}


/* Computes the cosine of values of +self+ (expressed in radians). Returns a Float in the range -1.0..1.0.
 * @return [self]
 */
static VALUE paddlec_float_buffer_cos_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_cos_inplace(mfbuf);

	return self;
}


/* Computes the hyperbolic cosine of values of +self+.
 * @overload cosh(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_cosh(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_cosh(mfbuf, rfbuf);

	return buffer;
}


/* Computes the hyperbolic cosine of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_cosh_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_cosh_inplace(mfbuf);

	return self;
}


/* Calculates the error function of values of +self+.
 * @overload erf(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_erf(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_erf(mfbuf, rfbuf);

	return buffer;
}


/* Calculates the error function of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_erf_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_erf_inplace(mfbuf);

	return self;
}


/* Calculates the complementary error function of values of +self+.
 * @overload erfc(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_erfc(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_erfc(mfbuf, rfbuf);

	return buffer;
}


/* Calculates the complementary error function of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_erfc_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_erfc_inplace(mfbuf);

	return self;
}


/* Calculates the value of e (the base of natural logarithms) raised to the power of values of +self+.
 * @overload exp(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_exp(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_exp(mfbuf, rfbuf);

	return buffer;
}


/* Calculates the value of e (the base of natural logarithms) raised to the power of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_exp_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_exp_inplace(mfbuf);

	return self;
}


/* Calculates the natural logarithm of values of +self+.
 * @overload log(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_log(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_log(mfbuf, rfbuf);

	return buffer;
}


/* Calculates the natural logarithm of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_log_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_log_inplace(mfbuf);

	return self;
}


/* Calculates the base 10 logarithm of values of +self+.
 * @overload log10(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_log10(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_log10(mfbuf, rfbuf);

	return buffer;
}


/* Calculates the base 10 logarithm of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_log10_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_log10_inplace(mfbuf);

	return self;
}


/* Calculate the base 2 logarithm of values of +self+.
 * @overload log2(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_log2(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_log2(mfbuf, rfbuf);

	return buffer;
}


/* Calculate the base 2 logarithm of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_log2_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_log2_inplace(mfbuf);

	return self;
}


/* Computes the sine of values of +self+ (expressed in radians). Returns a Float in the range -1.0..1.0.
 * @overload sin(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_sin(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_sin(mfbuf, rfbuf);

	return buffer;
}


/* Computes the sine of values of +self+ (expressed in radians). Returns a Float in the range -1.0..1.0.
 * @return [self]
 */
static VALUE paddlec_float_buffer_sin_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_sin_inplace(mfbuf);

	return self;
}


/* Computes the hyperbolic sine of values of +self+.
 * @overload sinh(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_sinh(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_sinh(mfbuf, rfbuf);

	return buffer;
}


/* Computes the hyperbolic sine of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_sinh_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_sinh_inplace(mfbuf);

	return self;
}


/* Computes the non-negative square root of values of +self+.
 * @overload sqrt(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_sqrt(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_sqrt(mfbuf, rfbuf);

	return buffer;
}


/* Computes the non-negative square root of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_sqrt_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_sqrt_inplace(mfbuf);

	return self;
}


/* Computes the tangent of values of +self+ (expressed in radians).
 * @overload tan(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_tan(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_tan(mfbuf, rfbuf);

	return buffer;
}


/* Computes the tangent of values of +self+ (expressed in radians).
 * @return [self]
 */
static VALUE paddlec_float_buffer_tan_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_tan_inplace(mfbuf);

	return self;
}


/* Computes the hyperbolic tangent of values of +self+.
 * @overload tanh(buffer = nil)
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_tanh(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	mfbuf = paddlec_float_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	pdlc_buffer_tanh(mfbuf, rfbuf);

	return buffer;
}


/* Computes the hyperbolic tangent of values of +self+.
 * @return [self]
 */
static VALUE paddlec_float_buffer_tanh_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_tanh_inplace(mfbuf);

	return self;
}


/* @!endgroup
 */


/* Return the lesser and greater values of +self+.
 * Returns nil if empty.
 * @return [Array<Float>, nil] a two element array containint the lesser and greater values from +self+.
 */
static VALUE paddlec_float_buffer_minmax(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	float mi, ma;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (mfbuf->length < 1)
		return Qnil;

	pdlc_buffer_minmax_of_self(mfbuf, &mi, &ma);

	return rb_assoc_new(DBL2NUM((double)mi), DBL2NUM((double)ma));
}


/* @return [nil, Float, PaddleC::FloatBuffer]
 * Without any parameter, returns the lesser value stored in +self+ (or +nil+ if empty).
 * If a {PaddleC::FloatBuffer} or a Float is provided as +other+, 
 * returns a new buffer for which each element is the lesser value of the corresponding element of +self+ and +other+.
 * @overload min()
 * 	@return [Float, nil]
 * @overload min(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_min(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "02", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (other == Qnil) {
		if (mfbuf->length < 1)
			buffer = Qnil;
		else {
			of = pdlc_buffer_min_of_self(mfbuf);
			buffer = DBL2NUM((double)of);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_min(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_min(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_min_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_min_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_min_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* @return [nil, Float, PaddleC::FloatBuffer]
 * Without any parameter, returns the greater value stored in +self+ (or +nil+ is empty).
 * If a {PaddleC::FloatBuffer} or a Float is provided as +other+, 
 * returns a new buffer for which each element is the greater value of the corresponding element of +self+ and +other+.
 * @overload max()
 * 	@return [Float, nil]
 * @overload max(other, buffer = nil)
 *  @param other [PaddleC::FloatBuffer, Numeric]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_max(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;
	VALUE other, buffer;

	rb_scan_args(argc, argv, "02", &other, &buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (other == Qnil) {
		if (mfbuf->length < 1)
			buffer = Qnil;
		else {
			of = pdlc_buffer_max_of_self(mfbuf);
			buffer = DBL2NUM((double)of);
		}
	}
	else if (rb_obj_is_kind_of(other, c_FloatBuffer) || rb_obj_is_kind_of(other, rb_cNumeric)) {
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
		if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_fb_fb_max(mfbuf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_fb_fs_max(mfbuf, of, rfbuf);
		}
	}
	else
		rb_raise(rb_eTypeError, "First argument is expected to be a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return buffer;
}


/* @param other [c_FloatBuffer, rb_cNumeric]
 * @return [self]
 */
static VALUE paddlec_float_buffer_max_inplace(VALUE self, VALUE other)
{
	pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of;

	mfbuf = paddlec_float_buffer_get_struct(self);

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_fb_fb_max_inplace(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_fb_fs_max_inplace(mfbuf, of);
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_cNumeric), rb_class_name(rb_class_of(other)));

	return self;
}


/* Clipp the signal so that the output has no values greater than +max+ nor lesser than +min+.
 * @overload clipp(max = 1.0, min = -max, buffer = nil)
 * @param max [Float] the maximum value allowed
 * @param min [Float] the minimum value allowed
 * @param buffer [nil, PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_clipp(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	float mi, ma;
	VALUE buffer, rbmin, rbmax;

	rb_scan_args(argc, argv, "3", &rbmax, &rbmin, &buffer);

	if (rbmax == Qnil)
		ma = 1.0f;
	else
		ma = (float)NUM2DBL(rbmax);

	if (rbmin == Qnil)
		mi = -ma;
	else
		mi = (float)NUM2DBL(rbmin);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}

	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_clipp(mfbuf, mi, ma, rfbuf);

	return buffer;
}


/* Clipp the signal so that the output has no values greater than +max+ nor lesser than +min+.
 * @overload clipp!(max = 1.0, min = -max)
 * @param max [Float] the maximum value allowed
 * @param min [Float] the minimum value allowed
 * @return [self]
 */
static VALUE paddlec_float_buffer_clipp_inplace(int argc, VALUE *argv, VALUE self)
{
	pdlc_buffer_t *mfbuf;
	float mi, ma;
	VALUE rbmin, rbmax;

	rb_scan_args(argc, argv, "2", &rbmax, &rbmin);

	if (rbmax == Qnil)
		ma = 1.0f;
	else
		ma = (float)NUM2DBL(rbmax);

	if (rbmin == Qnil)
		mi = -ma;
	else
		mi = (float)NUM2DBL(rbmin);

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_clipp_inplace(mfbuf, mi, ma);

	return self;
}


/* Returns a new {PaddleC::FloatBuffer} containing +self+'s elements in reverse order.
 * @overload reverse(buffer = nil)
 * @param buffer [PaddleC::FloatBuffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_reverse(int argc, VALUE *argv, VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	pdlc_buffer_t *rfbuf;
	VALUE buffer;

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
		rb_warn("argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}

	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
	rfbuf = paddlec_float_buffer_get_struct(buffer);

	mfbuf = paddlec_float_buffer_get_struct(self);

	pdlc_buffer_reverse(mfbuf, rfbuf);

	return buffer;
}


/* Reverse +self+ in place.
 * @return [self]
 */
static VALUE paddlec_float_buffer_reverse_inplace(VALUE self)
{
	pdlc_buffer_t *mfbuf;
	mfbuf = paddlec_float_buffer_get_struct(self);
	pdlc_buffer_reverse_inplace(mfbuf);
	return self;
}


/* Returns the sum of all values in +self+, or +nil+ if empty.
 * @return [Float, nil]
 */
static VALUE paddlec_float_buffer_sum(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	float res;
	mfbuf = paddlec_float_buffer_get_struct(self);
	if (mfbuf->length < 1)
		return Qnil;
	res = pdlc_buffer_sum(mfbuf);
	return DBL2NUM((double)res);
}


/* Returns the mean of all values in +self+, or +nil+ if empty.
 * @return [Float, nil]
 */
static VALUE paddlec_float_buffer_mean(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	float res;
	mfbuf = paddlec_float_buffer_get_struct(self);
	if (mfbuf->length < 1)
		return Qnil;
	res = pdlc_buffer_mean(mfbuf);
	return DBL2NUM((double)res);
}


/* Returns the variance of all values in +self+, or +nil+ if empty.
 * 𝛔² = V(X) = E[(X - E(X))²] = E(X²) - (E(X))²
 * The variance is the square of the standard deviation.
 * @see PaddleC::FloatBuffer#standard_deviation
 * @return [Float, nil]
 */
static VALUE paddlec_float_buffer_variance(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	float res;
	mfbuf = paddlec_float_buffer_get_struct(self);
	if (mfbuf->length < 1)
		return Qnil;
	res = pdlc_buffer_variance(mfbuf);
	return DBL2NUM((double)res);
}


/* Returns the standard deviation 𝛔 of all values in +self+, or +nil+ if empty.
 * 𝛔² = V(X) = E[(X - E(X))²] = E(X²) - (E(X))²
 * The standard deviation is the square root of the variance.
 * @see PaddleC::FloatBuffer#variance
 * @return [Float, nil]
 */
static VALUE paddlec_float_buffer_standard_deviation(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	float res;
	mfbuf = paddlec_float_buffer_get_struct(self);
	if (mfbuf->length < 1)
		return Qnil;
	res = pdlc_buffer_standard_deviation(mfbuf);
	return DBL2NUM((double)res);
}


/* Returns the product of all values of +self+, or +nil+ if empty.
 * @return [Float, nil]
 */
static VALUE paddlec_float_buffer_prod(VALUE self)
{
	const pdlc_buffer_t *mfbuf;
	float res;
	mfbuf = paddlec_float_buffer_get_struct(self);
	if (mfbuf->length < 1)
		return Qnil;
	res = pdlc_buffer_prod(mfbuf);
	return DBL2NUM((double)res);
}


/* Returns the sum of absolute differences of values of +self+ with +other+, or +nil+ if empty.
 * @return [Float, nil]
 * @param other [Float, PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_sad(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of, rf;

	mfbuf = paddlec_float_buffer_get_struct(self);
	if (mfbuf->length < 1)
		return Qnil;

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		if (ofbuf->length < 1)
			return Qnil;
		rf = pdlc_buffer_fb_fb_sad(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric) && !rb_obj_is_kind_of(other, rb_cComplex)) {
		of = (float)NUM2DBL(other);
		rf = pdlc_buffer_fb_fs_sad(mfbuf, of);
	}
	else
		rb_raise(rb_eArgError, "expecting a Float or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(other)));

	return DBL2NUM((double)rf);
}


/* Returns the sum of square differences of values of +self+ with +other+, or +nil+ if empty.
 * @return [Float, nil]
 * @param other [Float, PaddleC::FloatBuffer]
 */
static VALUE paddlec_float_buffer_ssd(VALUE self, VALUE other)
{
	const pdlc_buffer_t *mfbuf;
	const pdlc_buffer_t *ofbuf;
	float of, rf;

	mfbuf = paddlec_float_buffer_get_struct(self);
	if (mfbuf->length < 1)
		return Qnil;

	if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		if (ofbuf->length < 1)
			return Qnil;
		rf = pdlc_buffer_fb_fb_ssd(mfbuf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric) && !rb_obj_is_kind_of(other, rb_cComplex)) {
		of = (float)NUM2DBL(other);
		rf = pdlc_buffer_fb_fs_ssd(mfbuf, of);
	}
	else
		rb_raise(rb_eArgError, "expecting a Float or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(other)));

	return DBL2NUM((double)rf);
}


void Init_paddlec_float_buffer()
{
	c_FloatBuffer = rb_define_class_under(m_PaddleC, "FloatBuffer", rb_cObject);
	rb_define_module_function(c_FloatBuffer, "unpack",  paddlec_float_buffer_classunpack,          2);

	rb_define_alloc_func(c_FloatBuffer, paddlec_float_buffer_alloc);
	rb_include_module(c_FloatBuffer, rb_mEnumerable);
	rb_define_method(c_FloatBuffer, "initialize",        paddlec_float_buffer_initialize,         -1);
	rb_define_method(c_FloatBuffer, "to_float_buffer",   paddlec_float_buffer_to_float_buffer,     0);
	rb_define_method(c_FloatBuffer, "to_complex_buffer", paddlec_float_buffer_to_complex_buffer,   0);
	rb_define_method(c_FloatBuffer, "each",              paddlec_float_buffer_each,                0);
	rb_define_method(c_FloatBuffer, "collect!",          paddlec_float_buffer_collect_inp,         0);
	rb_define_method(c_FloatBuffer, "length",            paddlec_float_buffer_length,              0);
	rb_define_method(c_FloatBuffer, "empty?",            paddlec_float_buffer_isempty,             0);
	rb_define_method(c_FloatBuffer, "to_a",              paddlec_float_buffer_to_a,                0);
	rb_define_method(c_FloatBuffer, "to_s",              paddlec_float_buffer_to_s,                0);
	rb_define_method(c_FloatBuffer, "to_gplot",          paddlec_float_buffer_to_gplot,            0);
	rb_define_method(c_FloatBuffer, "zip",               paddlec_float_buffer_zip,                -1);
	rb_define_method(c_FloatBuffer, "inspect",           paddlec_float_buffer_to_s,                0);
	rb_define_method(c_FloatBuffer, "clone",             paddlec_float_buffer_clone,               0);
	rb_define_method(c_FloatBuffer, "[]",                paddlec_float_buffer_get,                -1);
	rb_define_method(c_FloatBuffer, "slice",             paddlec_float_buffer_get,                -1);
	rb_define_method(c_FloatBuffer, "[]=",               paddlec_float_buffer_set,                -1);
	rb_define_method(c_FloatBuffer, "pack",              paddlec_float_buffer_pack,               -1);
	rb_define_method(c_FloatBuffer, "unpack",            paddlec_float_buffer_unpack,              2);
	rb_define_method(c_FloatBuffer, "resize",            paddlec_float_buffer_resize,              1);
	rb_define_method(c_FloatBuffer, "pointer",           paddlec_float_buffer_native_ptr,          0);
	rb_define_method(c_FloatBuffer, "coerce",            paddlec_float_buffer_coerce,              1);
	if (c_FFI_Pointer != Qundef)
		rb_define_method(c_FloatBuffer, "to_ptr",        paddlec_float_buffer_ffi_pointer,         0);

	rb_define_method(rb_cArray, "to_float_buffer",       paddlec_array_to_float_buffer,            0);

	rb_define_method(c_FloatBuffer, "finite?",           paddlec_float_buffer_finite,              0);
	rb_define_method(c_FloatBuffer, "infinite?",         paddlec_float_buffer_infinite,            0);
	rb_define_method(c_FloatBuffer, "nan?",              paddlec_float_buffer_nan,                 0);
	rb_define_method(c_FloatBuffer, "integer?",          paddlec_float_buffer_integer,             0);
	rb_define_method(c_FloatBuffer, "negative?",         paddlec_float_buffer_negative,            0);
	rb_define_method(c_FloatBuffer, "positive?",         paddlec_float_buffer_positive,            0);
	rb_define_method(c_FloatBuffer, "nonzero?",          paddlec_float_buffer_nonzero,             0);
	rb_define_method(c_FloatBuffer, "zero?",             paddlec_float_buffer_zero,                0);
	rb_define_method(c_FloatBuffer, "&",                 paddlec_float_buffer_and,                 1);
	rb_define_method(c_FloatBuffer, "|",                 paddlec_float_buffer_or,                  1);
	rb_define_method(c_FloatBuffer, "^",                 paddlec_float_buffer_xor,                 1);
	rb_define_method(c_FloatBuffer, "!",                 paddlec_float_buffer_not,                 0);
	rb_define_method(c_FloatBuffer, "<",                 paddlec_float_buffer_cmpless,             1);
	rb_define_method(c_FloatBuffer, "<=",                paddlec_float_buffer_cmplessequ,          1);
	rb_define_method(c_FloatBuffer, ">",                 paddlec_float_buffer_cmpgrt,              1);
	rb_define_method(c_FloatBuffer, ">=",                paddlec_float_buffer_cmpgrtequ,           1);
	rb_define_method(c_FloatBuffer, "==",                paddlec_float_buffer_equ,                 1);
	rb_define_method(c_FloatBuffer, "!=",                paddlec_float_buffer_different,           1);
	rb_define_method(c_FloatBuffer, "ceil",              paddlec_float_buffer_ceil,               -1);
	rb_define_method(c_FloatBuffer, "ceil!",             paddlec_float_buffer_ceil_inplace,       -1);
	rb_define_method(c_FloatBuffer, "floor",             paddlec_float_buffer_floor,              -1);
	rb_define_method(c_FloatBuffer, "floor!",            paddlec_float_buffer_floor_inplace,      -1);
	rb_define_method(c_FloatBuffer, "truncate",          paddlec_float_buffer_truncate,           -1);
	rb_define_method(c_FloatBuffer, "truncate!",         paddlec_float_buffer_truncate_inplace,   -1);
	rb_define_method(c_FloatBuffer, "round",             paddlec_float_buffer_round,              -1);
	rb_define_method(c_FloatBuffer, "round!",            paddlec_float_buffer_round_inplace,      -1);
	rb_define_method(c_FloatBuffer, "abs",               paddlec_float_buffer_abs,                -1);
	rb_define_alias(c_FloatBuffer,  "magnitude", "abs");
	rb_define_method(c_FloatBuffer, "abs!",              paddlec_float_buffer_abs_inplace,         0);
	rb_define_method(c_FloatBuffer, "abs2",              paddlec_float_buffer_abs2,               -1);
	rb_define_method(c_FloatBuffer, "square!",           paddlec_float_buffer_abs2_inplace,        0);
	rb_define_method(c_FloatBuffer, "arg",               paddlec_float_buffer_arg,                -1);
	rb_define_alias(c_FloatBuffer,  "angle", "arg");
	rb_define_alias(c_FloatBuffer,  "phase", "arg");
	rb_define_method(c_FloatBuffer, "conjugate",         paddlec_float_buffer_conjugate,          -1);
	rb_define_alias(c_FloatBuffer,  "conj", "conjugate");
	rb_define_method(c_FloatBuffer, "conjugate!",        paddlec_float_buffer_conjugate_inplace,   0);
	rb_define_method(c_FloatBuffer, "imaginary",         paddlec_float_buffer_imag,               -1);
	rb_define_alias(c_FloatBuffer,  "imag", "imaginary");
	rb_define_method(c_FloatBuffer, "real",              paddlec_float_buffer_real,               -1);
	rb_define_method(c_FloatBuffer, "swapIQ",            paddlec_float_buffer_swapIQ,             -1);
	rb_define_alias(c_FloatBuffer,  "swapRI", "swapIQ");
	rb_define_method(c_FloatBuffer, "+@",                paddlec_float_buffer_unaryplus,           0);
	rb_define_method(c_FloatBuffer, "-@",                paddlec_float_buffer_unaryminus,         -1);
	rb_define_method(c_FloatBuffer, "negate!",           paddlec_float_buffer_unaryminus_inplace,  0);
	rb_define_method(c_FloatBuffer, "-",                 paddlec_float_buffer_sub,                -1);
	rb_define_alias(c_FloatBuffer,  "sub", "-");
	rb_define_method(c_FloatBuffer, "sub!",              paddlec_float_buffer_sub_inplace,         1);
	rb_define_method(c_FloatBuffer, "esub",              paddlec_float_buffer_esub,               -1);
	rb_define_method(c_FloatBuffer, "esub!",             paddlec_float_buffer_esub_inplace,        1);
	rb_define_method(c_FloatBuffer, "+",                 paddlec_float_buffer_add,                -1);
	rb_define_alias(c_FloatBuffer,  "add", "+");
	rb_define_method(c_FloatBuffer, "add!",              paddlec_float_buffer_add_inplace,         1);
	rb_define_method(c_FloatBuffer, "eadd",              paddlec_float_buffer_eadd,               -1);
	rb_define_method(c_FloatBuffer, "eadd!",             paddlec_float_buffer_eadd_inplace,        1);
	rb_define_method(c_FloatBuffer, "*",                 paddlec_float_buffer_mult,               -1);
	rb_define_alias(c_FloatBuffer,  "mult", "*");
	rb_define_method(c_FloatBuffer, "mult!",             paddlec_float_buffer_mult_inplace,        1);
	rb_define_method(c_FloatBuffer, "emult",             paddlec_float_buffer_emult,              -1);
	rb_define_method(c_FloatBuffer, "emul!",             paddlec_float_buffer_emult_inplace,       1);
	rb_define_method(c_FloatBuffer, "/",                 paddlec_float_buffer_div,                -1);
	rb_define_alias(c_FloatBuffer,  "div", "/");
	rb_define_method(c_FloatBuffer, "div!",              paddlec_float_buffer_div_inplace,         1);
	rb_define_method(c_FloatBuffer, "ediv",              paddlec_float_buffer_ediv,               -1);
	rb_define_method(c_FloatBuffer, "ediv!",             paddlec_float_buffer_ediv_inplace,        1);
	rb_define_method(c_FloatBuffer, "**",                paddlec_float_buffer_pow,                -1);
	rb_define_alias(c_FloatBuffer,  "pow", "**");
	rb_define_method(c_FloatBuffer, "pow!",              paddlec_float_buffer_pow_inplace,         1);
	rb_define_method(c_FloatBuffer, "%",                 paddlec_float_buffer_modulo,             -1);
	rb_define_alias(c_FloatBuffer,  "modulo", "%");
	rb_define_method(c_FloatBuffer, "modulo!",           paddlec_float_buffer_modulo_inplace,      1);
	rb_define_method(c_FloatBuffer, "emod",              paddlec_float_buffer_emod,               -1);
	rb_define_method(c_FloatBuffer, "emod!",             paddlec_float_buffer_emod_inplace,        1);
	rb_define_method(c_FloatBuffer, "epow",              paddlec_float_buffer_epow,               -1);
	rb_define_method(c_FloatBuffer, "epow!",             paddlec_float_buffer_epow_inplace,        1);
	rb_define_method(c_FloatBuffer, "acos",              paddlec_float_buffer_acos,               -1);
	rb_define_method(c_FloatBuffer, "acos!",             paddlec_float_buffer_acos_inplace,        0);
	rb_define_method(c_FloatBuffer, "acosh",             paddlec_float_buffer_acosh,              -1);
	rb_define_method(c_FloatBuffer, "acosh!",            paddlec_float_buffer_acosh_inplace,       0);
	rb_define_method(c_FloatBuffer, "asin",              paddlec_float_buffer_asin,               -1);
	rb_define_method(c_FloatBuffer, "asin!",             paddlec_float_buffer_asin_inplace,        0);
	rb_define_method(c_FloatBuffer, "asinh",             paddlec_float_buffer_asinh,              -1);
	rb_define_method(c_FloatBuffer, "asinh!",            paddlec_float_buffer_asinh_inplace,       0);
	rb_define_method(c_FloatBuffer, "atan",              paddlec_float_buffer_atan,               -1);
	rb_define_method(c_FloatBuffer, "atan!",             paddlec_float_buffer_atan_inplace,        0);
	rb_define_method(c_FloatBuffer, "atanh",             paddlec_float_buffer_atanh,              -1);
	rb_define_method(c_FloatBuffer, "atanh!",            paddlec_float_buffer_atanh_inplace,       0);
	rb_define_method(c_FloatBuffer, "cbrt",              paddlec_float_buffer_cbrt,               -1);
	rb_define_method(c_FloatBuffer, "cbrt!",             paddlec_float_buffer_cbrt_inplace,        0);
	rb_define_method(c_FloatBuffer, "cos",               paddlec_float_buffer_cos,                -1);
	rb_define_method(c_FloatBuffer, "cos!",              paddlec_float_buffer_cos_inplace,         0);
	rb_define_method(c_FloatBuffer, "cosh",              paddlec_float_buffer_cosh,               -1);
	rb_define_method(c_FloatBuffer, "cosh!",             paddlec_float_buffer_cosh_inplace,        0);
	rb_define_method(c_FloatBuffer, "erf",               paddlec_float_buffer_erf,                -1);
	rb_define_method(c_FloatBuffer, "erf!",              paddlec_float_buffer_erf_inplace,         0);
	rb_define_method(c_FloatBuffer, "erfc",              paddlec_float_buffer_erfc,               -1);
	rb_define_method(c_FloatBuffer, "erfc!",             paddlec_float_buffer_erfc_inplace,        0);
	rb_define_method(c_FloatBuffer, "exp",               paddlec_float_buffer_exp,                -1);
	rb_define_method(c_FloatBuffer, "exp!",              paddlec_float_buffer_exp_inplace,         0);
	rb_define_method(c_FloatBuffer, "log",               paddlec_float_buffer_log,                -1);
	rb_define_method(c_FloatBuffer, "log!",              paddlec_float_buffer_log_inplace,         0);
	rb_define_method(c_FloatBuffer, "log10",             paddlec_float_buffer_log10,              -1);
	rb_define_method(c_FloatBuffer, "log10!",            paddlec_float_buffer_log10_inplace,       0);
	rb_define_method(c_FloatBuffer, "log2",              paddlec_float_buffer_log2,               -1);
	rb_define_method(c_FloatBuffer, "log2!",             paddlec_float_buffer_log2_inplace,        0);
	rb_define_method(c_FloatBuffer, "sin",               paddlec_float_buffer_sin,                -1);
	rb_define_method(c_FloatBuffer, "sin!",              paddlec_float_buffer_sin_inplace,         0);
	rb_define_method(c_FloatBuffer, "sinh",              paddlec_float_buffer_sinh,               -1);
	rb_define_method(c_FloatBuffer, "sinh!",             paddlec_float_buffer_sinh_inplace,        0);
	rb_define_method(c_FloatBuffer, "sqrt",              paddlec_float_buffer_sqrt,               -1);
	rb_define_method(c_FloatBuffer, "sqrt!",             paddlec_float_buffer_sqrt_inplace,        0);
	rb_define_method(c_FloatBuffer, "tan",               paddlec_float_buffer_tan,                -1);
	rb_define_method(c_FloatBuffer, "tan!",              paddlec_float_buffer_tan_inplace,         0);
	rb_define_method(c_FloatBuffer, "tanh",              paddlec_float_buffer_tanh,               -1);
	rb_define_method(c_FloatBuffer, "tanh!",             paddlec_float_buffer_tanh_inplace,        0);
	rb_define_method(c_FloatBuffer, "min",               paddlec_float_buffer_min,                -1);
	rb_define_method(c_FloatBuffer, "min!",              paddlec_float_buffer_min_inplace,         1);
	rb_define_method(c_FloatBuffer, "max",               paddlec_float_buffer_max,                -1);
	rb_define_method(c_FloatBuffer, "max!",              paddlec_float_buffer_max_inplace,         1);
	rb_define_method(c_FloatBuffer, "minmax",            paddlec_float_buffer_minmax,              0);
	rb_define_method(c_FloatBuffer, "clipp",             paddlec_float_buffer_clipp,              -1);
	rb_define_method(c_FloatBuffer, "clipp!",            paddlec_float_buffer_clipp_inplace,      -1);
	rb_define_method(c_FloatBuffer, "reverse",           paddlec_float_buffer_reverse,            -1);
	rb_define_method(c_FloatBuffer, "reverse!",          paddlec_float_buffer_reverse_inplace,     0);
	rb_define_method(c_FloatBuffer, "sum",               paddlec_float_buffer_sum,                 0);
	rb_define_method(c_FloatBuffer, "mean",              paddlec_float_buffer_mean,                0);
	rb_define_method(c_FloatBuffer, "variance",          paddlec_float_buffer_variance,            0);
	rb_define_method(c_FloatBuffer, "standard_deviation",paddlec_float_buffer_standard_deviation,  0);
	rb_define_method(c_FloatBuffer, "prod",              paddlec_float_buffer_prod,                0);
	rb_define_method(c_FloatBuffer, "sad",               paddlec_float_buffer_sad,                 1);
	rb_define_method(c_FloatBuffer, "ssd",               paddlec_float_buffer_ssd,                 1);
}


