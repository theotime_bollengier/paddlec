/* Copyright (C) 2019-2020 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include "paddlec_defs.h"
#ifdef HAVE_PULSEAUDIO_L
#include <string.h>
#include <poll.h>
#include <pulse/pulseaudio.h>
#include <pulse/simple.h>
#include <ruby.h>
#include "paddlec_defs.h"
#include "libpaddlec.h"
#include "paddlec.h"
#include "float_buffer.h"
#include "complex_buffer.h"
#include "pulseaudio.h"
#include <ruby/thread.h>


VALUE m_PulseAudio;
VALUE c_PAConnection;
static VALUE c_PASampleFormat;
static VALUE c_PASampleSpec;
static VALUE c_PAChannelMap;
static VALUE c_PABufferAttributes;
static VALUE m_PASimple;
static VALUE c_PASimpleSink;
static VALUE c_PASimpleSource;
static VALUE c_PATimingInfo;
static VALUE c_PAServerInfo;
static VALUE c_PAStatInfo;
static VALUE c_PAFormatInfo;
static VALUE c_PASinkInfo;
static VALUE c_PASinkPortInfo;
static VALUE c_PASourceInfo;
static VALUE c_PASourcePortInfo;
static VALUE c_PAModuleInfo;
static VALUE m_PAStream;
static VALUE c_PAPlayback;

static VALUE rb_cConditionVariable;


static void Init_paddlec_pulseaudio_connection();
static void Init_paddlec_pulseaudio_channelmap();
static void Init_paddlec_pulseaudio_bufferattr();
static void Init_paddlec_pulseaudio_sample_format();
static void Init_paddlec_pulseaudio_sample_spec();
static void Init_paddlec_pulseaudio_simple();
static void Init_paddlec_pulseaudio_stream();
static void Init_paddlec_pulseaudio_timinginfo();
static void Init_paddlec_pulseaudio_serverinfo();
static void Init_paddlec_pulseaudio_statinfo();
static void Init_paddlec_pulseaudio_formatinfo();
static void Init_paddlec_pulseaudio_sinkportinfo();
static void Init_paddlec_pulseaudio_sinkinfo();
static void Init_paddlec_pulseaudio_sourceportinfo();
static void Init_paddlec_pulseaudio_sourceinfo();
static void Init_paddlec_pulseaudio_moduleinfo();


static int paddlec_pulseaudio_interrupted = 0;


typedef struct {
	VALUE        thread;
	VALUE        mutex;
	VALUE        cond;
	VALUE        accept_cond;
    volatile int n_waiting;
    volatile int n_waiting_for_accept;
	pa_mainloop *mainloop;
	pa_context  *context;
	int         *success;
	VALUE        rbval;
} paddlec_pulseaudio_connection_t;


static inline int  paddlec_pulseaudio_connection_in_worker_thread(paddlec_pulseaudio_connection_t *pac);
static inline void paddlec_pulseaudio_connection_lock(paddlec_pulseaudio_connection_t *pac);
static inline void paddlec_pulseaudio_connection_unlock(paddlec_pulseaudio_connection_t *pac);
static inline void paddlec_pulseaudio_connection_clock(paddlec_pulseaudio_connection_t *pac);
static inline void paddlec_pulseaudio_connection_cunlock(paddlec_pulseaudio_connection_t *pac);
static inline int  paddlec_pulseaudio_connection_is_locked(paddlec_pulseaudio_connection_t *pac);
static inline void paddlec_pulseaudio_connection_signal(paddlec_pulseaudio_connection_t *pac, int wait_for_accept);
static inline void paddlec_pulseaudio_connection_wait(paddlec_pulseaudio_connection_t *pac);
static inline void paddlec_pulseaudio_connection_accept(paddlec_pulseaudio_connection_t *pac);
static paddlec_pulseaudio_connection_t* paddlec_pulseaudio_connection_get_struct(VALUE obj);

/* PaddleC::PulseAudio */


/* Document-module: PaddleC::PulseAudio
 *
 * This module includes wrappers to the {https://freedesktop.org/software/pulseaudio/doxygen PulseAudio client API}.
 */
void Init_paddlec_pulseaudio()
{
	m_PulseAudio = rb_define_module_under(m_PaddleC, "PulseAudio");

	Init_paddlec_pulseaudio_sample_format();
	Init_paddlec_pulseaudio_sample_spec();
	Init_paddlec_pulseaudio_channelmap();
	Init_paddlec_pulseaudio_bufferattr();
	Init_paddlec_pulseaudio_timinginfo();
	Init_paddlec_pulseaudio_serverinfo();
	Init_paddlec_pulseaudio_statinfo();
	Init_paddlec_pulseaudio_connection();
	Init_paddlec_pulseaudio_simple();
	Init_paddlec_pulseaudio_stream();
	Init_paddlec_pulseaudio_sinkportinfo();
	Init_paddlec_pulseaudio_sinkinfo();
	Init_paddlec_pulseaudio_sourceportinfo();
	Init_paddlec_pulseaudio_sourceinfo();
	Init_paddlec_pulseaudio_moduleinfo();
	Init_paddlec_pulseaudio_formatinfo();
}


/* PaddleC::PulseAudio::SampleFormat */


static void paddlec_pulseaudio_sample_format_free(void *p)
{
	ruby_xfree(p);
}


static size_t paddlec_pulseaudio_sample_format_rsize(const void* data)
{
	(void)data;
	return sizeof(pa_sample_format_t);
}


static const rb_data_type_t paddlec_pulseaudio_sample_format_type = {
	.wrap_struct_name = "paddlec_pulseaudio_sample_format_struct",
	.function = {
		.dmark = NULL,
		.dfree = paddlec_pulseaudio_sample_format_free,
		.dsize = paddlec_pulseaudio_sample_format_rsize,
	},
	.data  = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE paddlec_pulseaudio_sample_format_alloc(VALUE klass)
{
	VALUE obj;
	pa_sample_format_t *format;

	format = ruby_xmalloc(sizeof(pa_sample_format_t));
	*format = PA_SAMPLE_INVALID;

	obj = TypedData_Wrap_Struct(klass, &paddlec_pulseaudio_sample_format_type, format);

	return obj;
}


static pa_sample_format_t* paddlec_pulseaudio_sample_format_get_struct(VALUE obj)
{
	pa_sample_format_t *format;
	TypedData_Get_Struct(obj, pa_sample_format_t, &paddlec_pulseaudio_sample_format_type, format);
	return format;
}


static pa_sample_format_t paddlec_pulseaudio_sample_format_get_val(VALUE obj)
{
	pa_sample_format_t *format;
	TypedData_Get_Struct(obj, pa_sample_format_t, &paddlec_pulseaudio_sample_format_type, format);
	return *format;
}


/* @param val [String, Symbol, PaddleC::PulseAudio::SampleFormat, Integer]
 * @return [PaddleC::PulseAudio::SampleFormat]
 */
static VALUE paddlec_pulseaudio_sample_format_initialize(VALUE self, VALUE val)
{
	pa_sample_format_t *format;

	format = paddlec_pulseaudio_sample_format_get_struct(self);

	if (rb_obj_is_kind_of(val, rb_cSymbol))
		val = rb_funcallv(val, rb_intern("to_s"), 0, NULL);

	if (rb_obj_is_kind_of(val, rb_cString))
		*format = pa_parse_sample_format(StringValueCStr(val));
	else if (rb_obj_is_kind_of(val, rb_cInteger))
		*format = (pa_sample_format_t)NUM2INT(val);
	else if (rb_obj_is_kind_of(val, c_PASampleFormat))
		*format = paddlec_pulseaudio_sample_format_get_val(val);
	else
		rb_raise(rb_eTypeError, "expecting a String, a Symbol, an Integer or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PASampleFormat), rb_class_name(rb_class_of(val)));

	if (*format < 0 || *format >= PA_SAMPLE_MAX)
		*format = PA_SAMPLE_INVALID;

	return self;
}


static const char* paddlec_pulseaudio_sample_format_inspect_cstr(VALUE self)
{
	pa_sample_format_t *format;
	const char *s;

	format = paddlec_pulseaudio_sample_format_get_struct(self);

	switch (*format) {
		case PA_SAMPLE_U8:
			s = "PA_SAMPLE_U8"; break;
		case PA_SAMPLE_ALAW:
			s = "PA_SAMPLE_ALAW"; break;
		case PA_SAMPLE_ULAW:
			s = "PA_SAMPLE_ULAW"; break;
		case PA_SAMPLE_S16LE:
			s = "PA_SAMPLE_S16LE"; break;
		case PA_SAMPLE_S16BE:
			s = "PA_SAMPLE_S16BE"; break;
		case PA_SAMPLE_FLOAT32LE:
			s = "PA_SAMPLE_FLOAT32LE"; break;
		case PA_SAMPLE_FLOAT32BE:
			s = "PA_SAMPLE_FLOAT32BE"; break;
		case PA_SAMPLE_S32LE:
			s = "PA_SAMPLE_S32LE"; break;
		case PA_SAMPLE_S32BE:
			s = "PA_SAMPLE_S32BE"; break;
		case PA_SAMPLE_S24LE:
			s = "PA_SAMPLE_S24LE"; break;
		case PA_SAMPLE_S24BE:
			s = "PA_SAMPLE_S24BE"; break;
		case PA_SAMPLE_S24_32LE:
			s = "PA_SAMPLE_S24_32LE"; break;
		case PA_SAMPLE_S24_32BE:
			s = "PA_SAMPLE_S24_32BE"; break;
		case PA_SAMPLE_MAX:
			s = "PA_SAMPLE_MAX"; break;
		case PA_SAMPLE_INVALID:
			s = "PA_SAMPLE_INVALID"; break;
		default:
			s = "UNKNOWN";
	}

	return s;
}


/* Return the PulseAudio name of the sample format.
 * @return [String]
 */
static VALUE paddlec_pulseaudio_sample_format_inspect(VALUE self)
{
	const char *s;
	s = paddlec_pulseaudio_sample_format_inspect_cstr(self);
	return rb_utf8_str_new_cstr(s);
}


/* Tell if the format is a valid sample format.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_sample_format_is_valid(VALUE self)
{
	pa_sample_format_t *format;

	format = paddlec_pulseaudio_sample_format_get_struct(self);

	return (pa_sample_format_valid(*format) ? Qtrue : Qfalse);
}


/* Return a descriptive string for the sample format.
 * @return [String]
 */
static VALUE paddlec_pulseaudio_sample_format_to_s(VALUE self)
{
	VALUE res;
	pa_sample_format_t *format;
	const char *s;

	format = paddlec_pulseaudio_sample_format_get_struct(self);
	if (!pa_sample_format_valid(*format))
		res = rb_sprintf("Invalid sample format");
	else {
		s = pa_sample_format_to_string(*format);
		res = rb_utf8_str_new_cstr(s);
	}

	return res;
}


/* @return [Symbol, nil] a symbol, or +nil+ if the format is invalid
 */
static VALUE paddlec_pulseaudio_sample_format_to_sym(VALUE self)
{
	VALUE res;
	pa_sample_format_t *format;

	format = paddlec_pulseaudio_sample_format_get_struct(self);
	if (!pa_sample_format_valid(*format))
		res = Qnil;
	else {
		res = ID2SYM(rb_intern(pa_sample_format_to_string(*format)));
	}

	return res;
}


/* Returns +true+ when the format is little endian, +false+ when big endian.
 * Returns +nil+ when endianness does not apply to the format, or endianess is unknown.
 * @return [Boolean, nil]
 */
static VALUE paddlec_pulseaudio_sample_format_is_le(VALUE self)
{
	VALUE res;
	pa_sample_format_t *format;

	format = paddlec_pulseaudio_sample_format_get_struct(self);

	if (!pa_sample_format_valid(*format)) {
		res = Qnil;
	}
	else {
		switch (pa_sample_format_is_le(*format)) {
			case 1:
				res = Qtrue;
				break;
			case 0:
				res = Qfalse;
				break;
			default:
				res = Qnil;
		}
	}

	return res;
}


/* Returns +true+ when the format is big endian, +false+ when little endian.
 * Returns +nil+ when endianness does not apply to the format, or endianess is unknown.
 * @return [Boolean, nil]
 */
static VALUE paddlec_pulseaudio_sample_format_is_be(VALUE self)
{
	VALUE res;
	pa_sample_format_t *format;

	format = paddlec_pulseaudio_sample_format_get_struct(self);

	if (!pa_sample_format_valid(*format)) {
		res = Qnil;
	}
	else {
		switch (pa_sample_format_is_be(*format)) {
			case 1:
				res = Qtrue;
				break;
			case 0:
				res = Qfalse;
				break;
			default:
				res = Qnil;
		}
	}

	return res;
}


/* Returns size in bytes of the format.
 * Returns +nil+ when invalid.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_sample_format_size(VALUE self)
{
	VALUE res;
	pa_sample_format_t *format;

	format = paddlec_pulseaudio_sample_format_get_struct(self);

	if (!pa_sample_format_valid(*format)) {
		res = Qnil;
	}
	else {
		res = ULONG2NUM(pa_sample_size_of_format(*format));
	}

	return res;
}


/* @param other [PaddleC::PulseAudio::SampleFormat]
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_sample_format_isequ(VALUE self, VALUE other)
{
	const pa_sample_format_t *myformat, *hisformat;

	if (rb_class_of(other) != c_PASampleFormat)
		return Qfalse;

	myformat = paddlec_pulseaudio_sample_format_get_struct(self);
	hisformat = paddlec_pulseaudio_sample_format_get_struct(other);

	return ((*hisformat == *myformat) ? Qtrue : Qfalse);
}


/* Return an array of valid formats as symbols.
 * @return [Array<Symbol>] array of valid format symbols
 */
static VALUE paddlec_pulseaudio_sample_format_symbols(VALUE self)
{
	VALUE res;
	int i;

	res = rb_ary_new_capa(PA_SAMPLE_MAX);
	for (i = 0; i < (int)PA_SAMPLE_MAX; i++)
		rb_ary_store(res, i, ID2SYM(rb_intern(pa_sample_format_to_string((pa_sample_format_t)i))));

	return res;
}


/* Document-class: PaddleC::PulseAudio::SampleFormat
 *
 * Describe the data type in wich elements of a sample frame are stored.
 *
 */


static void Init_paddlec_pulseaudio_sample_format()
{
	int i;
	VALUE v, n;

	c_PASampleFormat = rb_define_class_under(m_PulseAudio, "SampleFormat", rb_cObject);
	rb_define_module_function(c_PASampleFormat, "symbols", paddlec_pulseaudio_sample_format_symbols, 0);
	rb_define_alloc_func(c_PASampleFormat, paddlec_pulseaudio_sample_format_alloc);
	rb_define_method(c_PASampleFormat, "initialize",     paddlec_pulseaudio_sample_format_initialize, 1);
	rb_define_method(c_PASampleFormat, "inspect",        paddlec_pulseaudio_sample_format_inspect,    0);
	rb_define_method(c_PASampleFormat, "to_s",           paddlec_pulseaudio_sample_format_to_s,       0);
	rb_define_method(c_PASampleFormat, "to_sym",         paddlec_pulseaudio_sample_format_to_sym,     0);
	rb_define_method(c_PASampleFormat, "valid?",         paddlec_pulseaudio_sample_format_is_valid,   0);
	rb_define_method(c_PASampleFormat, "little_endian?", paddlec_pulseaudio_sample_format_is_le,      0);
	rb_define_method(c_PASampleFormat, "big_endian?",    paddlec_pulseaudio_sample_format_is_be,      0);
	rb_define_method(c_PASampleFormat, "size",           paddlec_pulseaudio_sample_format_size,       0);
	rb_define_method(c_PASampleFormat, "==",             paddlec_pulseaudio_sample_format_isequ,      1);

	for (i = 0; i < (int)PA_SAMPLE_MAX; i++) {
		n = INT2NUM(i);
		v = rb_class_new_instance(1, &n, c_PASampleFormat);
		rb_define_const(c_PASampleFormat, paddlec_pulseaudio_sample_format_inspect_cstr(v), v);
	}
}


/* PaddleC::PulseAudio::SampleSpec */


/* @return [PaddleC::PulseAudio::SampleFormat] the sample format
 */
static VALUE paddlec_pulseaudio_sample_spec_get_format(VALUE self)
{
	return rb_iv_get(self, "@format");
}


/* @return [Integer] the sample rate in Hz
 */
static VALUE paddlec_pulseaudio_sample_spec_get_rate(VALUE self)
{
	return rb_iv_get(self, "@rate");
}


/* @return [Integer] the number of channels
 */
static VALUE paddlec_pulseaudio_sample_spec_get_channels(VALUE self)
{
	return rb_iv_get(self, "@channels");
}


/* @param format [PaddleC::PulseAudio::SampleFormat, String, Symbol, Integer] the sample format
 */
static VALUE paddlec_pulseaudio_sample_spec_set_format(VALUE self, VALUE format)
{
	if (rb_class_of(format) == c_PASampleFormat)
		rb_iv_set(self, "@format", format);
	else if (rb_class_of(format) == rb_cString || rb_class_of(format) == rb_cSymbol || rb_class_of(format) == rb_cInteger)
		rb_iv_set(self, "@format", rb_class_new_instance(1, &format, c_PASampleFormat));
	else
		rb_raise(rb_eArgError, "format must be a String, a Symbol, an Integer or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PASampleFormat), rb_class_name(rb_class_of(format)));

	return format;
}


/* @param rate [Integer] the sample rate in Hz
 */
static VALUE paddlec_pulseaudio_sample_spec_set_rate(VALUE self, VALUE rate)
{
	int crate;

	if (!rb_obj_is_kind_of(rate, rb_cNumeric))
		rb_raise(rb_eArgError, "rate must be an Integer, not a %"PRIsVALUE, rate);
	crate = NUM2INT(rate);
	if (!pa_sample_rate_valid((uint32_t)crate))
		rb_raise(rb_eArgError, "invalid sample rate: %"PRIsVALUE, rate);

	rb_iv_set(self, "@rate", rate);

	return rate;
}


/* @param channels [Integer] the number of channels
 */
static VALUE paddlec_pulseaudio_sample_spec_set_channels(VALUE self, VALUE channels)
{
	uint8_t cchannels;

	if (rb_class_of(channels) != rb_cInteger)
		rb_raise(rb_eArgError, "channels must be an Integer, not a %"PRIsVALUE, channels);
	cchannels = NUM2CHR(channels);
	if (!pa_channels_valid(cchannels))
		rb_raise(rb_eArgError, "invalid number of channels: %"PRIsVALUE, channels);

	rb_iv_set(self, "@channels", channels);

	return channels;
}


static VALUE paddlec_pulseaudio_sample_spec_newfrompa(const pa_sample_spec *ss)
{
	VALUE res, val;

	res = rb_class_new_instance(0, NULL, c_PASampleSpec);
	val = INT2NUM(ss->format);
	rb_iv_set(res, "@format",   rb_class_new_instance(1, &val, c_PASampleFormat));
	rb_iv_set(res, "@rate",     UINT2NUM(ss->rate));
	rb_iv_set(res, "@channels", UINT2NUM(ss->channels));

	return res;
}


/* @return [PaddleC::PulseAudio::SampleSpec]
 * @overload initialize(format: PaddleC::PulseAudio::SampleFormat::PA_SAMPLE_FLOAT32LE, rate: 44100, channels: 1)
 * @param format [PaddleC::PulseAudio::SampleFormat, String, Symbol, Integer] the sample format
 * @param rate [Integer] the sample rate in Hz
 * @param channels [Integer] the number of channels
 * @overload initialize(string)
 * @param string [String] something similar to the output of {SampleSpec#to_s}, such as +"float32le 1ch 44100Hz"+
 */
static VALUE paddlec_pulseaudio_sample_spec_initialize(int argc, VALUE *argv, VALUE self)
{
	VALUE opts, str, regexp;
	VALUE format_rate_channels[3] = {Qundef, Qundef, Qundef};
	const ID kwkeys[3] = {rb_intern("format"), rb_intern("rate"), rb_intern("channels")};

	rb_scan_args(argc, argv, "01:", &str, &opts);

	if (!NIL_P(opts))
		rb_get_kwargs(opts, kwkeys, 0, 3, format_rate_channels);

	if (!NIL_P(str)) {
		if (!rb_obj_is_kind_of(str, rb_cString))
			rb_raise(rb_eTypeError, "expecting only optional String and keywords arguments, not a %"PRIsVALUE, rb_class_name(rb_class_of(str)));
		regexp = rb_sprintf("^([^\\s]+)\\s+(\\d+)ch\\s+(\\d+)Hz$");
		regexp = rb_class_new_instance(1, &regexp, rb_cRegexp);
		regexp = rb_funcallv(str, rb_intern("match"), 1, &regexp);
		if (NIL_P(regexp))
			rb_raise(rb_eArgError, "cannot parse \"%"PRIsVALUE"\" into a %"PRIsVALUE, str, rb_class_name(c_PASampleSpec));
		format_rate_channels[0] = rb_funcall(regexp, rb_intern("[]"), 1, INT2NUM(1));
		format_rate_channels[2] = rb_funcallv(rb_funcall(regexp, rb_intern("[]"), 1, INT2NUM(2)), rb_intern("to_i"), 0, NULL);
		format_rate_channels[1] = rb_funcallv(rb_funcall(regexp, rb_intern("[]"), 1, INT2NUM(3)), rb_intern("to_i"), 0, NULL);
	}

	if (format_rate_channels[0] == Qundef)
		format_rate_channels[0] = rb_const_get(c_PASampleFormat, rb_intern("PA_SAMPLE_FLOAT32LE"));

	if (format_rate_channels[1] == Qundef)
		format_rate_channels[1] = INT2NUM(44100);

	if (format_rate_channels[2] == Qundef)
		format_rate_channels[2] = INT2NUM(1);

	paddlec_pulseaudio_sample_spec_set_format(self, format_rate_channels[0]);
	paddlec_pulseaudio_sample_spec_set_rate(self, format_rate_channels[1]);
	paddlec_pulseaudio_sample_spec_set_channels(self, format_rate_channels[2]);

	return self;
}


/* Creates a new {SampleSpec} from +self+.
 * @return [PaddleC::PulseAudio::SampleSpec]
 */
static VALUE paddlec_pulseaudio_sample_spec_dup(VALUE self)
{
	VALUE res;

	res = rb_class_new_instance(0, NULL, c_PASampleSpec);

	paddlec_pulseaudio_sample_spec_set_format(res, paddlec_pulseaudio_sample_spec_get_format(self));
	paddlec_pulseaudio_sample_spec_set_rate(res, paddlec_pulseaudio_sample_spec_get_rate(self));
	paddlec_pulseaudio_sample_spec_set_channels(res, paddlec_pulseaudio_sample_spec_get_channels(self));

	return res;
}


static pa_sample_spec paddlec_pulseaudio_sample_spec_get(VALUE self)
{
	pa_sample_spec spec;
	spec.format = paddlec_pulseaudio_sample_format_get_val(rb_iv_get(self, "@format"));
	spec.rate = (uint32_t)NUM2UINT(rb_iv_get(self, "@rate"));
	spec.channels = (uint8_t)NUM2CHR(rb_iv_get(self, "@channels"));
	return spec;
}


/* Return the amount of bytes that constitute playback of one second of audio, with the specified sample spec.
 * Return +nil+ if invalid.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_sample_spec_bytes_per_second(VALUE self)
{
	const pa_sample_spec spec = paddlec_pulseaudio_sample_spec_get(self);
	if (!pa_sample_spec_valid(&spec))
		return Qnil;
	return ULL2NUM(pa_bytes_per_second(&spec));
}


/* Return the size of a frame with the specific sample type.
 * Return +nil+ if invalid.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_sample_spec_frame_size(VALUE self)
{
	const pa_sample_spec spec = paddlec_pulseaudio_sample_spec_get(self);
	if (!pa_sample_spec_valid(&spec))
		return Qnil;
	return ULL2NUM(pa_frame_size(&spec));
}


/* Calculate the time it would take to play a buffer of the specified size with the specified sample type.
 * The return value will always be rounded down for non-integral return values.
 * Return +nil+ if invalid.
 * @param nb_bytes [Integer]
 * @return [Integer, nil] µs
 */
static VALUE paddlec_pulseaudio_sample_spec_bytes_to_usec(VALUE self, VALUE nb_bytes)
{
	const pa_sample_spec spec = paddlec_pulseaudio_sample_spec_get(self);
	if (!pa_sample_spec_valid(&spec))
		return Qnil;
	return ULL2NUM(pa_bytes_to_usec(NUM2ULL(nb_bytes), &spec));
}


/* Calculates the size of a buffer required, for playback duration of the time specified, with the specified sample type.
 * The return value will always be rounded down for non-integral return values.
 * Return +nil+ if invalid.
 * @param t [Integer, Float] time in µs (Integer) or seconds (Float)
 * @return [Integer, nil] number of bytes
 */
static VALUE paddlec_pulseaudio_sample_spec_usec_to_bytes(VALUE self, VALUE t)
{
	const pa_sample_spec spec = paddlec_pulseaudio_sample_spec_get(self);
	if (!pa_sample_spec_valid(&spec))
		return Qnil;
	if (rb_class_of(t) == rb_cFloat)
		return ULL2NUM(pa_usec_to_bytes((pa_usec_t)(1e6*NUM2DBL(t)), &spec));
	return ULL2NUM(pa_usec_to_bytes(NUM2ULL(t), &spec));
}


/* Return whether the sample type specification is valid or not.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_sample_spec_is_valid(VALUE self)
{
	const pa_sample_spec spec = paddlec_pulseaudio_sample_spec_get(self);
	return (pa_sample_spec_valid(&spec) ? Qtrue : Qfalse);
}


/* Return whether the two sample type specifications match.
 * @param other [PaddleC::PulseAudio::SampleSpec]
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_sample_spec_isequ(VALUE self, VALUE other)
{
	pa_sample_spec myspec;
	pa_sample_spec hisspec;

	if (rb_class_of(other) != c_PASampleSpec)
		return Qfalse;

	myspec = paddlec_pulseaudio_sample_spec_get(self);
	hisspec = paddlec_pulseaudio_sample_spec_get(other);

	return (pa_sample_spec_equal(&myspec, &hisspec) ? Qtrue : Qfalse);
}


/* Pretty print the sample type specification to a string.
 * Return +nil+ if invalid.
 * @return [String, nil]
 */
static VALUE paddlec_pulseaudio_sample_spec_to_s(VALUE self)
{
	char s[PA_SAMPLE_SPEC_SNPRINT_MAX] = {0};
	const pa_sample_spec spec = paddlec_pulseaudio_sample_spec_get(self);
	VALUE res;

	if (!pa_sample_spec_valid(&spec))
		return Qnil;
	
	pa_sample_spec_snprint(s, sizeof(s), &spec);
	res = rb_utf8_str_new_cstr(s);

	return res;
}


/* Document-class: PaddleC::PulseAudio::SampleSpec
 *
 * A sample format and attribute specification.
 *
 * @!attribute [rw] format
 *
 * @!attribute [rw] rate
 *
 * @!attribute [rw] channels
 */

static void Init_paddlec_pulseaudio_sample_spec()
{
	c_PASampleSpec = rb_define_class_under(m_PulseAudio, "SampleSpec", rb_cObject);
	rb_define_method(c_PASampleSpec, "initialize",       paddlec_pulseaudio_sample_spec_initialize,      -1);
	rb_define_method(c_PASampleSpec, "dup",              paddlec_pulseaudio_sample_spec_dup,              0);
	rb_define_method(c_PASampleSpec, "format",           paddlec_pulseaudio_sample_spec_get_format,       0);
	rb_define_method(c_PASampleSpec, "rate",             paddlec_pulseaudio_sample_spec_get_rate,         0);
	rb_define_method(c_PASampleSpec, "channels",         paddlec_pulseaudio_sample_spec_get_channels,     0);
	rb_define_method(c_PASampleSpec, "format=",          paddlec_pulseaudio_sample_spec_set_format,       1);
	rb_define_method(c_PASampleSpec, "rate=",            paddlec_pulseaudio_sample_spec_set_rate,         1);
	rb_define_method(c_PASampleSpec, "channels=",        paddlec_pulseaudio_sample_spec_set_channels,     1);
	rb_define_method(c_PASampleSpec, "bytes_per_second", paddlec_pulseaudio_sample_spec_bytes_per_second, 0);
	rb_define_method(c_PASampleSpec, "frame_size",       paddlec_pulseaudio_sample_spec_frame_size,       0);
	rb_define_method(c_PASampleSpec, "bytes_to_usec",    paddlec_pulseaudio_sample_spec_bytes_to_usec,    1);
	rb_define_method(c_PASampleSpec, "usec_to_bytes",    paddlec_pulseaudio_sample_spec_usec_to_bytes,    1);
	rb_define_method(c_PASampleSpec, "valid?",           paddlec_pulseaudio_sample_spec_is_valid,         0);
	rb_define_method(c_PASampleSpec, "==",               paddlec_pulseaudio_sample_spec_isequ,            1);
	rb_define_method(c_PASampleSpec, "to_s",             paddlec_pulseaudio_sample_spec_to_s,             0);
}


/* PaddleC::PulseAudio::ChannelMap */


static inline VALUE paddlec_pulseaudio_channel_position_to_sym(pa_channel_position_t pos)
{
	char s[32];
	int i;

	if (pos < 0 || pos >= PA_CHANNEL_POSITION_MAX)
		return Qnil;

	strncpy(s, pa_channel_position_to_string(pos), sizeof(s));
	s[31] = 0;

	for (i = 0; s[i]; i++) {
		if (s[i] == '-')
			s[i] = '_';
	}

	return ID2SYM(rb_intern(s));
}


static pa_channel_position_t paddlec_pulseaudio_channelmap_sym_to_channel_position(VALUE sym)
{
	char s[32];
	int i;

	if (rb_class_of(sym) == rb_cSymbol)
		sym = rb_funcallv(sym, id_to_s, 0, NULL);

	strncpy(s, StringValueCStr(sym), sizeof(s));
	s[31] = 0;

	for (i = 0; s[i]; i++) {
		if (s[i] == '_')
			s[i] = '-';
	}

	return pa_channel_position_from_string(s);
}


static void paddlec_pulseaudio_channelmap_free(void *p)
{
	ruby_xfree(p);
}


static size_t paddlec_pulseaudio_channelmap_size(const void* data)
{
	(void)data;
	return sizeof(pa_channel_map);
}


static const rb_data_type_t paddlec_pulseaudio_channelmap_type = {
	.wrap_struct_name = "paddlec_pulseaudio_channelmap_struct",
	.function = {
		.dmark = NULL,
		.dfree = paddlec_pulseaudio_channelmap_free,
		.dsize = paddlec_pulseaudio_channelmap_size,
	},
	.data  = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE paddlec_pulseaudio_channelmap_alloc(VALUE klass)
{
	VALUE obj;
	pa_channel_map *cm;

	cm = ruby_xmalloc(sizeof(pa_channel_map));
	pa_channel_map_init(cm);

	obj = TypedData_Wrap_Struct(klass, &paddlec_pulseaudio_channelmap_type, cm);

	return obj;
}


static pa_channel_map* paddlec_pulseaudio_channelmap_get_struct(VALUE obj)
{
	pa_channel_map *cm;
	TypedData_Get_Struct(obj, pa_channel_map, &paddlec_pulseaudio_channelmap_type, cm);
	return cm;
}


static VALUE paddlec_pulseaudio_channelmap_newfrompa(const pa_channel_map *cm)
{
	VALUE res, val;
	pa_channel_map *ncm;

	val = INT2NUM(0);
	res = rb_class_new_instance(1, &val, c_PAChannelMap);
	ncm = paddlec_pulseaudio_channelmap_get_struct(res);
	memcpy(ncm, cm, sizeof(pa_channel_map));

	return res;
}


/* Return a new {PaddleC::PulseAudio::ChannelMap}.
 * @return [PaddleC::PulseAudio::ChannelMap]
 * @overload initialize(nb_channels)
 * @param nb_channels [Integer] number of channels in the channel map
 * @overload initialize(str)
 * @param str [String] something like the output of {PaddleC::PulseAudio::ChannelMap#to_s}
 * @overload initialize(channel_map)
 * @param channel_map [PaddleC::PulseAudio::ChannelMap] another ChannelMap
 * @overload initialize(arr)
 * @param arr [Array<Symbol>] an array of channel positions
 */
static VALUE paddlec_pulseaudio_channelmap_initialize(VALUE self, VALUE val)
{
	pa_channel_map *cm;
	const pa_channel_map *ocm;
	int channels;
	const char *s;
	int i;
	VALUE el;
	pa_channel_position_t pos;

	cm = paddlec_pulseaudio_channelmap_get_struct(self);

	if (rb_class_of(val) == rb_cInteger) {
		channels = NUM2UINT(val);
		if (channels <= 0)
			return self;
		if (channels > (int)PA_CHANNELS_MAX)
			rb_raise(rb_eArgError, "a %"PRIsVALUE" cannot have more than %d positions", rb_class_name(c_PAChannelMap), PA_CHANNELS_MAX);
		//pa_channel_map_init_extend(cm, (unsigned int)channels, PA_CHANNEL_MAP_DEFAULT);
		pa_channel_map_init_extend(cm, (unsigned int)channels, PA_CHANNEL_MAP_ALSA);
	}
	else if (rb_class_of(val) == rb_cString) {
		s = StringValueCStr(val);
		if (!pa_channel_map_parse(cm, s))
			rb_raise(rb_eArgError, "cannot parse \"%"PRIsVALUE"\" to a %"PRIsVALUE, val, rb_class_name(c_PAChannelMap));
	}
	else if (rb_class_of(val) == c_PAChannelMap) {
		ocm = paddlec_pulseaudio_channelmap_get_struct(val);
		cm->channels = ocm->channels;
		memcpy(cm->map, ocm->map, PA_CHANNELS_MAX*sizeof(pa_channel_position_t));
	}
	else if (rb_class_of(val) == rb_cArray) {
		channels = rb_array_len(val);
		if (channels > (int)PA_CHANNELS_MAX)
			rb_raise(rb_eArgError, "a %"PRIsVALUE" cannot have more than %d positions", rb_class_name(c_PAChannelMap), PA_CHANNELS_MAX);
		for (i = 0; i < channels; i++) {
			el = rb_ary_entry(val, i);
			if (rb_class_of(el) != rb_cSymbol && rb_class_of(el) != rb_cString)
				rb_raise(rb_eTypeError, "expecting only Symbols or Strings in array, not %"PRIsVALUE, rb_class_name(rb_class_of(el)));
			pos = paddlec_pulseaudio_channelmap_sym_to_channel_position(el);
			if (pos < 0 || pos >= PA_CHANNEL_POSITION_MAX)
				rb_raise(rb_eArgError, "invalid channel position in array: %"PRIsVALUE, el);
			cm->map[i] = pos;
		}
		cm->channels = (uint8_t)channels;
	}
	else
		rb_raise(rb_eTypeError, "expecting an Integer, an Array, a String or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PAChannelMap), rb_class_name(rb_class_of(val)));

	return self;
}


/* Creates a new {ChannelMap} from +self+.
 * @return [PaddleC::PulseAudio::ChannelMap]
 */
static VALUE paddlec_pulseaudio_channelmap_dup(VALUE self)
{
	return rb_class_new_instance(1, &self, c_PAChannelMap);
}


/* @return [Integer] the number of positions in the channel map. 
 *  The length of the channel map can be modified using {PaddleC::PulseAudio::ChannelMap#resize!}
 */
static VALUE paddlec_pulseaudio_channelmap_length(VALUE self)
{
	const pa_channel_map *cm;
	cm = paddlec_pulseaudio_channelmap_get_struct(self);
	return INT2NUM((int)cm->channels);
}


/* @return [Array<Symbol, nil>]
 */
static VALUE paddlec_pulseaudio_channelmap_to_a(VALUE self)
{
	VALUE res;
	long i;
	const pa_channel_map *cm;

	cm = paddlec_pulseaudio_channelmap_get_struct(self);

	res = rb_ary_new_capa((long)cm->channels);

	for (i = 0; i < (long)cm->channels; i++)
		rb_ary_store(res, i, paddlec_pulseaudio_channel_position_to_sym(cm->map[i]));

	return res;
}


/* Returns a Hash with all the valid channel positions as keys, and human readable Strings as values.
 * @return [Hash{Symbol => String}]
 */
static VALUE paddlec_pulseaudio_channelmap_channel_positions(VALUE self)
{
	VALUE res;
	long i;
	(void)self;

	//res = rb_ary_new_capa((long)PA_CHANNEL_POSITION_MAX);
	//for (i = 0; i < (long)PA_CHANNEL_POSITION_MAX; i++)
	//	rb_ary_store(res, i, paddlec_pulseaudio_channel_position_to_sym((pa_channel_position_t)i));
	res = rb_hash_new();
	for (i = 0; i < (long)PA_CHANNEL_POSITION_MAX; i++)
		rb_hash_aset(res, paddlec_pulseaudio_channel_position_to_sym((pa_channel_position_t)i), rb_utf8_str_new_cstr(pa_channel_position_to_pretty_string((pa_channel_position_t)i)));

	return res;
}


/* Return a new PaddleC::PulseAudio::ChannelMap, from channel map positions as symbols.
 * @return [PaddleC::PulseAudio::ChannelMap]
 */
static VALUE paddlec_pulseaudio_channelmap_new_from_channel_positions(int argc, VALUE *argv, VALUE self)
{
	VALUE res;
	int i;
	pa_channel_position_t pos;
	pa_channel_map *cm;

	if (argc > (int)PA_CHANNELS_MAX)
		rb_raise(rb_eArgError, "a %"PRIsVALUE" cannot have more than %d positions", rb_class_name(c_PAChannelMap), PA_CHANNELS_MAX);

	res = rb_class_new_instance(0, NULL, c_PAChannelMap);
	cm = paddlec_pulseaudio_channelmap_get_struct(res);

	for (i = 0; i < argc; i++) {
		if (rb_class_of(argv[i]) != rb_cSymbol && rb_class_of(argv[i]) != rb_cString)
			rb_raise(rb_eTypeError, "expecting only Symbols or Strings, not %"PRIsVALUE, rb_class_name(rb_class_of(argv[i])));
		pos = paddlec_pulseaudio_channelmap_sym_to_channel_position(argv[i]);
		if (pos < 0 || pos >= PA_CHANNEL_POSITION_MAX)
			rb_raise(rb_eArgError, "invalid channel position: %"PRIsVALUE, argv[i]);
		cm->map[i] = pos;
	}
	cm->channels = (uint8_t)argc;

	return res;
}


/* Tell if the channel map is valid.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_channelmap_is_valid(VALUE self)
{
	pa_channel_map *cm;

	cm = paddlec_pulseaudio_channelmap_get_struct(self);

	return (pa_channel_map_valid(cm) ? Qtrue : Qfalse);
}


/* Return a descriptive string for the channel map.
 * @return [String]
 */
static VALUE paddlec_pulseaudio_channelmap_to_s(VALUE self)
{
	pa_channel_map *cm;
	char s[PA_CHANNEL_MAP_SNPRINT_MAX];

	cm = paddlec_pulseaudio_channelmap_get_struct(self);
	pa_channel_map_snprint(s, sizeof(s), cm);

	return rb_utf8_str_new_cstr(s);
}


/* @return [String]
 */
static VALUE paddlec_pulseaudio_channelmap_inspect(VALUE self)
{
	pa_channel_map *cm;
	char s[PA_CHANNEL_MAP_SNPRINT_MAX];

	cm = paddlec_pulseaudio_channelmap_get_struct(self);
	pa_channel_map_snprint(s, sizeof(s), cm);

	return rb_sprintf("[%s]", s);
}


/* Tries to find a well-known channel mapping name for this channel mapping, i.e. "stereo", "surround_71" and so on.
 * If the channel mapping is unknown +nil+ is returned.
 * @return [Symbol, nil]
 */
static VALUE paddlec_pulseaudio_channelmap_to_sym(VALUE self)
{
	pa_channel_map *cm;
	const char *s;
	char c[64];
	int i;

	cm = paddlec_pulseaudio_channelmap_get_struct(self);
	s = pa_channel_map_to_name(cm);
	if (!s)
		return Qnil;
	strncpy(c, s, sizeof(c));
	c[63] = 0;
	for (i = 0; c[i]; i++) {
		if (c[i] == '-')
			c[i] = '_';
	}

	return ID2SYM(rb_intern(c));
}


/* Tries to find a human readable text label for this channel mapping, i.e. "Stereo", "Surround 7.1" and so on.
 * If the channel mapping is unknown +nil+ is returned.
 * @return [String, nil]
 */
static VALUE paddlec_pulseaudio_channelmap_name(VALUE self)
{
	pa_channel_map *cm;
	const char *s;

	cm = paddlec_pulseaudio_channelmap_get_struct(self);
	s = pa_channel_map_to_pretty_name(cm);
	if (!s)
		return Qnil;

	return rb_utf8_str_new_cstr(s);
}


/* Get position of channel +index+.
 * @param index [Integer]
 * @return [Symbol, nil] the channel position as a Symbol, or +nil+ if invalid
 */
static VALUE paddlec_pulseaudio_channelmap_getat(VALUE self, VALUE index)
{
	const pa_channel_map *cm;
	int i;

	cm = paddlec_pulseaudio_channelmap_get_struct(self);

	if (rb_class_of(index) != rb_cInteger)
		rb_raise(rb_eTypeError, "expecting an Integer, not a %"PRIsVALUE, rb_class_name(rb_class_of(index)));
	i = NUM2INT(index);
	if (i < 0)
		i = (int)cm->channels + i;
	if (i < 0 || i >= (int)PA_CHANNELS_MAX)
		rb_raise(rb_eIndexError, "index out of bounds");

	return paddlec_pulseaudio_channel_position_to_sym(cm->map[i]);
}


/* Set position of channel +index+.
 * @param index [Integer]
 * @param position [Symbol, String]
 */
static VALUE paddlec_pulseaudio_channelmap_setat(VALUE self, VALUE index, VALUE position)
{
	pa_channel_map *cm;
	int i;
	pa_channel_position_t pos;

	rb_check_frozen(self);

	cm = paddlec_pulseaudio_channelmap_get_struct(self);

	if (rb_class_of(index) != rb_cInteger)
		rb_raise(rb_eTypeError, "expecting an Integer as index, not a %"PRIsVALUE, rb_class_name(rb_class_of(index)));
	i = NUM2INT(index);
	if (i < 0)
		i = (int)cm->channels + i;
	if (i < 0 || i >= (int)PA_CHANNELS_MAX)
		rb_raise(rb_eIndexError, "index out of bounds");

	if (rb_class_of(position) != rb_cSymbol && rb_class_of(position) != rb_cString)
		rb_raise(rb_eTypeError, "expecting a Symbol or a String as position, not a %"PRIsVALUE, rb_class_name(rb_class_of(position)));

	pos = paddlec_pulseaudio_channelmap_sym_to_channel_position(position);
	if (pos < 0 || pos >= PA_CHANNEL_POSITION_MAX)
		rb_raise(rb_eArgError, "invalid channel position \"%"PRIsVALUE"\"", position);

	cm->map[i] = pos;

	return position;
}


/* Resize the channel map.
 * @param new_length [Integer]
 * @return [self]
 */
static VALUE paddlec_pulseaudio_channelmap_resize(VALUE self, VALUE new_length)
{
	pa_channel_map *cm;
	int len, i, j, k, posok;
	pa_channel_position_t pos;

	rb_check_frozen(self);

	if (rb_class_of(new_length) != rb_cInteger)
		rb_raise(rb_eTypeError, "expecting an Integer as length, not a %"PRIsVALUE, rb_class_name(rb_class_of(new_length)));
	len = NUM2INT(new_length);
	if (len < 0 || len > (int)PA_CHANNELS_MAX)
		rb_raise(rb_eArgError, "length of %"PRIsVALUE" must be between 0 and %u", rb_class_name(c_PAChannelMap), PA_CHANNELS_MAX);

	cm = paddlec_pulseaudio_channelmap_get_struct(self);

	if (len <= (int)cm->channels) {
		cm->channels = (uint8_t)len;
		return self;
	}

	for (i = cm->channels; i < len; i++) {
		pos = PA_CHANNEL_POSITION_AUX0;
		for (j = 0; j < (int)PA_CHANNEL_POSITION_MAX; j++) {
			posok = 1;
			for (k = 0; k < i; k++) {
				if (cm->map[k] == pos) {
					posok = 0;
					break;
				}
			}
			if (posok) {
				cm->map[i] = pos;
				break;
			}
			pos = (pos + 1) % PA_CHANNEL_POSITION_MAX;
		}
	}
	cm->channels = (uint8_t)len;

	return self;
}


/* Generates a bit mask from the channel map.
 * @return [Integer]
 */
static VALUE paddlec_pulseaudio_channelmap_mask(VALUE self)
{
	const pa_channel_map *cm;

	cm = paddlec_pulseaudio_channelmap_get_struct(self);

	return ULL2NUM(pa_channel_map_mask(cm));
}


/* Compare +self+ with +other+.
 * @param other [PaddleC::PulseAudio::ChannelMap]
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_channelmap_isequ(VALUE self, VALUE other)
{
	const pa_channel_map *mcm, *ocm;

	if (rb_class_of(other) != c_PAChannelMap)
		return Qfalse;

	mcm = paddlec_pulseaudio_channelmap_get_struct(self);
	ocm = paddlec_pulseaudio_channelmap_get_struct(other);

	return (pa_channel_map_equal(mcm, ocm) ? Qtrue : Qfalse);
}


/* Whether the channel map is compatible with the specified sample spec.
 * @param spec [PaddleC::PulseAudio::SampleSpec]
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_channelmap_iscompatible(VALUE self, VALUE spec)
{
	const pa_channel_map *cm;
	pa_sample_spec cspec;

	if (rb_class_of(spec) != c_PASampleSpec)
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PASampleSpec), rb_class_name(rb_class_of(spec)));

	cm = paddlec_pulseaudio_channelmap_get_struct(self);
	cspec = paddlec_pulseaudio_sample_spec_get(spec);

	return (pa_channel_map_compatible(cm, &cspec) ? Qtrue : Qfalse);
}


/* Whether every channel defined in the channel map +other+ is also defined in +self+, 
 * or if the channel position +other+ is available at least once in +self+.
 * @param other [PaddleC::PulseAudio::ChannelMap, Symbol, String] a channel map or a channel position
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_channelmap_include(VALUE self, VALUE other)
{
	const pa_channel_map *cm, *ocm;
	pa_channel_position_t pos;
	VALUE res = Qfalse;

	cm = paddlec_pulseaudio_channelmap_get_struct(self);

	if (rb_class_of(other) == c_PAChannelMap) {
		ocm = paddlec_pulseaudio_channelmap_get_struct(other);
		if (pa_channel_map_superset(cm, ocm))
			res = Qtrue;
	}
	else if (rb_class_of(other) == rb_cSymbol || rb_class_of(other) == rb_cString) {
		pos = paddlec_pulseaudio_channelmap_sym_to_channel_position(other);
		if (pos < 0 || pos >= PA_CHANNEL_POSITION_MAX)
			rb_raise(rb_eArgError, "invalid channel position: %"PRIsVALUE, other);
		if (pa_channel_map_has_position(cm, pos))
			res = Qtrue;
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE", a Symbol or a String, not a %"PRIsVALUE, rb_class_name(c_PAChannelMap), rb_class_name(rb_class_of(other)));

	return res;
}


/* Whether it makes sense to apply a volume 'balance' with this mapping, i.e. if there are left/right channels available.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_channelmap_canbalance(VALUE self)
{
	const pa_channel_map *cm;
	cm = paddlec_pulseaudio_channelmap_get_struct(self);
	return (pa_channel_map_can_balance(cm) ? Qtrue : Qfalse);
}


/* Whether it makes sense to apply a volume 'fade' (i.e. 'balance' between front and rear) with this mapping, i.e. if there are front/rear channels available.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_channelmap_canfade(VALUE self)
{
	const pa_channel_map *cm;
	cm = paddlec_pulseaudio_channelmap_get_struct(self);
	return (pa_channel_map_can_fade(cm) ? Qtrue : Qfalse);
}


/* Whether it makes sense to apply a volume 'lfe balance' (i.e. 'balance' between LFE and non-LFE channels) with this mapping,
 * i.e. if there are LFE and non-LFE channels available.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_channelmap_canlfebalance(VALUE self)
{
	const pa_channel_map *cm;
	cm = paddlec_pulseaudio_channelmap_get_struct(self);
	return (pa_channel_map_can_lfe_balance(cm) ? Qtrue : Qfalse);
}


/* Document-class: PaddleC::PulseAudio::ChannelMap
 *
 * A channel map which can be used to attach labels to specific channels of a stream.
 * These values are relevant for conversion and mixing of streams.
 *
 * @!attribute [r] length
 *
 */


static void Init_paddlec_pulseaudio_channelmap()
{
	c_PAChannelMap = rb_define_class_under(m_PulseAudio, "ChannelMap", rb_cObject);
	rb_define_module_function(c_PAChannelMap, "channel_positions", paddlec_pulseaudio_channelmap_channel_positions,           0);
	rb_define_module_function(c_PAChannelMap, "[]",                paddlec_pulseaudio_channelmap_new_from_channel_positions, -1);
	rb_define_alloc_func(c_PAChannelMap, paddlec_pulseaudio_channelmap_alloc);
	rb_define_method(c_PAChannelMap, "initialize",        paddlec_pulseaudio_channelmap_initialize,    1);
	rb_define_method(c_PAChannelMap, "dup",               paddlec_pulseaudio_channelmap_dup,           0);
	rb_define_method(c_PAChannelMap, "length",            paddlec_pulseaudio_channelmap_length,        0);
	rb_define_method(c_PAChannelMap, "[]",                paddlec_pulseaudio_channelmap_getat,         1);
	rb_define_method(c_PAChannelMap, "[]=",               paddlec_pulseaudio_channelmap_setat,         2);
	rb_define_method(c_PAChannelMap, "resize!",           paddlec_pulseaudio_channelmap_resize,        1);
	rb_define_method(c_PAChannelMap, "to_a",              paddlec_pulseaudio_channelmap_to_a,          0);
	rb_define_method(c_PAChannelMap, "inspect",           paddlec_pulseaudio_channelmap_inspect,       0);
	rb_define_method(c_PAChannelMap, "valid?",            paddlec_pulseaudio_channelmap_is_valid,      0);
	rb_define_method(c_PAChannelMap, "to_s",              paddlec_pulseaudio_channelmap_to_s,          0);
	rb_define_method(c_PAChannelMap, "to_sym",            paddlec_pulseaudio_channelmap_to_sym,        0);
	rb_define_method(c_PAChannelMap, "name",              paddlec_pulseaudio_channelmap_name,          0);
	rb_define_method(c_PAChannelMap, "mask",              paddlec_pulseaudio_channelmap_mask,          0);
	rb_define_method(c_PAChannelMap, "==",                paddlec_pulseaudio_channelmap_isequ,         1);
	rb_define_method(c_PAChannelMap, "compatible_with?",  paddlec_pulseaudio_channelmap_iscompatible,  1);
	rb_define_method(c_PAChannelMap, "include?",          paddlec_pulseaudio_channelmap_include,       1);
	rb_define_method(c_PAChannelMap, "can_balance?",      paddlec_pulseaudio_channelmap_canbalance,    0);
	rb_define_method(c_PAChannelMap, "can_fade?",         paddlec_pulseaudio_channelmap_canfade,       0);
	rb_define_method(c_PAChannelMap, "can_LFE_balance?",  paddlec_pulseaudio_channelmap_canlfebalance, 0);
}


/* PaddleC::PulseAudio::BufferAttributes */


static void paddlec_pulseaudio_bufferattr_free(void *p)
{
	ruby_xfree(p);
}


static size_t paddlec_pulseaudio_bufferattr_size(const void* data)
{
	(void)data;
	return sizeof(pa_buffer_attr);
}


static const rb_data_type_t paddlec_pulseaudio_bufferattr_type = {
	.wrap_struct_name = "paddlec_pulseaudio_bufferattr_struct",
	.function = {
		.dmark = NULL,
		.dfree = paddlec_pulseaudio_bufferattr_free,
		.dsize = paddlec_pulseaudio_bufferattr_size,
	},
	.data  = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE paddlec_pulseaudio_bufferattr_alloc(VALUE klass)
{
	VALUE obj;
	pa_buffer_attr *battr;

	battr = ruby_xmalloc(sizeof(pa_buffer_attr));
	memset(battr, 0xff, sizeof(pa_buffer_attr));

	obj = TypedData_Wrap_Struct(klass, &paddlec_pulseaudio_bufferattr_type, battr);

	return obj;
}


static pa_buffer_attr* paddlec_pulseaudio_bufferattr_get_struct(VALUE obj)
{
	pa_buffer_attr *battr;
	TypedData_Get_Struct(obj, pa_buffer_attr, &paddlec_pulseaudio_bufferattr_type, battr);
	return battr;
}


/* Return a new {BufferAttributes} initialized with default values.
 * @return [PaddleC::PulseAudio::BufferAttributes]
 */
static VALUE paddlec_pulseaudio_bufferattr_initialize(VALUE self)
{
	return self;
}


/* Creates a new {BufferAttributes} from +self+.
 * @return [PaddleC::PulseAudio::BufferAttributes]
 */
static VALUE paddlec_pulseaudio_bufferattr_dup(VALUE self)
{
	const pa_buffer_attr *mbattr;
	pa_buffer_attr *nbattr;
	VALUE res;

	res = rb_class_new_instance(0, NULL, c_PABufferAttributes);

	mbattr = paddlec_pulseaudio_bufferattr_get_struct(self);
	nbattr = paddlec_pulseaudio_bufferattr_get_struct(res);

	memcpy(nbattr, mbattr, sizeof(pa_buffer_attr));

	return res;
}


static VALUE paddlec_pulseaudio_bufferattr_newfrompa(const pa_buffer_attr *battr)
{
	pa_buffer_attr *nbattr;
	VALUE res;

	res = rb_class_new_instance(0, NULL, c_PABufferAttributes);

	nbattr = paddlec_pulseaudio_bufferattr_get_struct(res);

	memcpy(nbattr, battr, sizeof(pa_buffer_attr));

	return res;
}


/* Maximum length of the buffer in bytes.
 *
 * Setting this to -1 will initialize this to the maximum value supported by server, which is recommended.
 *
 * In strict low-latency playback scenarios you might want to set this to a lower value, 
 * likely together with the PA_STREAM_ADJUST_LATENCY flag.
 * If you do so, you ensure that the latency doesn't grow beyond what is acceptable for the use case, 
 * at the cost of getting more underruns if the latency is lower than what the server can reliably handle.
 *
 * @return [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_maxlength(VALUE self)
{
	const pa_buffer_attr *battr;
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (battr->maxlength == 0xffffffff)
		return INT2NUM(-1);
	return UINT2NUM(battr->maxlength);
}


/* @param val [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_setmaxlength(VALUE self, VALUE val)
{
	pa_buffer_attr *battr;
	long long int v;

	rb_check_frozen(self);
	v = NUM2LL(val);
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (v < 0 || v >= 0xffffffff)
		battr->maxlength = 0xffffffff;
	else
		battr->maxlength = (uint32_t)v;
	return val;
}


/* Playback only: target length of the buffer.
 *
 * The server tries to assure that at least tlength bytes are always available in the per-stream server-side playback buffer.
 * The server will only send requests for more data as long as the buffer has less than this number of bytes of data.
 *
 * It is recommended to set this to -1, which will initialize this to a value that is deemed sensible by the server.
 * However, this value will default to something like 2s;
 * for applications that have specific latency requirements this value should be set to the maximum latency that the application can deal with.
 *
 * When PA_STREAM_ADJUST_LATENCY is not set this value will influence only the per-stream playback buffer size.
 * When PA_STREAM_ADJUST_LATENCY is set the overall latency of the sink plus the playback buffer size is configured to this value.
 * Set PA_STREAM_ADJUST_LATENCY if you are interested in adjusting the overall latency.
 * Don't set it if you are interested in configuring the server-side per-stream playback buffer size.
 *
 * @return [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_tlength(VALUE self)
{
	const pa_buffer_attr *battr;
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (battr->tlength == 0xffffffff)
		return INT2NUM(-1);
	return UINT2NUM(battr->tlength);
}


/* @param val [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_settlength(VALUE self, VALUE val)
{
	pa_buffer_attr *battr;
	long long int v;

	rb_check_frozen(self);
	v = NUM2LL(val);
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (v < 0 || v >= 0xffffffff)
		battr->tlength = 0xffffffff;
	else
		battr->tlength = (uint32_t)v;
	return val;
}


/* Playback only: pre-buffering.
 *
 * The server does not start with playback before at least prebuf bytes are available in the buffer.
 * It is recommended to set this to -1, which will initialize this to the same value as tlength, whatever that may be.
 *
 * Initialize to 0 to enable manual start/stop control of the stream.
 * This means that playback will not stop on underrun and playback will not start automatically, instead pa_stream_cork() needs to be called explicitly.
 * If you set this value to 0 you should also set PA_STREAM_START_CORKED.
 * Should underrun occur, the read index of the output buffer overtakes the write index, and hence the fill level of the buffer is negative.
 *
 * Start of playback can be forced using {PaddleC::PulseAudio::Stream#trigger} even though the prebuffer size hasn't been reached.
 * If a buffer underrun occurs, this prebuffering will be again enabled.
 *
 * @return [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_prebuf(VALUE self)
{
	const pa_buffer_attr *battr;
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (battr->prebuf == 0xffffffff)
		return INT2NUM(-1);
	return UINT2NUM(battr->prebuf);
}


/* @param val [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_setprebuf(VALUE self, VALUE val)
{
	pa_buffer_attr *battr;
	long long int v;

	rb_check_frozen(self);
	v = NUM2LL(val);
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (v < 0 || v >= 0xffffffff)
		battr->prebuf = 0xffffffff;
	else
		battr->prebuf = (uint32_t)v;
	return val;
}


/* Playback only: minimum request.
 *
 * The server does not request less than minreq bytes from the client, instead waits until the buffer is free enough to request more bytes at once.
 * It is recommended to set this to -1, which will initialize this to a value that is deemed sensible by the server.
 * This should be set to a value that gives PulseAudio enough time to move the data from the per-stream playback buffer into the hardware playback buffer.
 *
 * @return [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_minreq(VALUE self)
{
	const pa_buffer_attr *battr;
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (battr->minreq == 0xffffffff)
		return INT2NUM(-1);
	return UINT2NUM(battr->minreq);
}


/* @param val [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_setminreq(VALUE self, VALUE val)
{
	pa_buffer_attr *battr;
	long long int v;

	rb_check_frozen(self);
	v = NUM2LL(val);
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (v < 0 || v >= 0xffffffff)
		battr->minreq = 0xffffffff;
	else
		battr->minreq = (uint32_t)v;
	return val;
}


/* Recording only: fragment size.
 *
 * The server sends data in blocks of fragsize bytes size.
 * Large values diminish interactivity with other operations on the connection context but decrease control overhead.
 * It is recommended to set this to -1, which will initialize this to a value that is deemed sensible by the server.
 * However, this value will default to something like 2s; for applications that have specific latency requirements this value should be set to the maximum latency that the application can deal with.
 *
 * If PA_STREAM_ADJUST_LATENCY is set the overall source latency will be adjusted according to this value.
 * If it is not set the source latency is left unmodified.
 *
 * @return [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_fragsize(VALUE self)
{
	const pa_buffer_attr *battr;
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (battr->fragsize == 0xffffffff)
		return INT2NUM(-1);
	return UINT2NUM(battr->fragsize);
}


/* @param val [Integer]
 */
static VALUE paddlec_pulseaudio_bufferattr_setfragsize(VALUE self, VALUE val)
{
	pa_buffer_attr *battr;
	long long int v;

	rb_check_frozen(self);
	v = NUM2LL(val);
	battr = paddlec_pulseaudio_bufferattr_get_struct(self);
	if (v < 0 || v >= 0xffffffff)
		battr->fragsize = 0xffffffff;
	else
		battr->fragsize = (uint32_t)v;
	return val;
}


/* @return [String]
 */
static VALUE paddlec_pulseaudio_bufferattr_inspect(VALUE self)
{
	return rb_sprintf("#<%"PRIsVALUE":0x%016lx maxlength=%"PRIsVALUE", tlength=%"PRIsVALUE", prebuf=%"PRIsVALUE", minreq=%"PRIsVALUE", fragsize=%"PRIsVALUE">",
			rb_class_name(rb_class_of(self)),
			self,
			paddlec_pulseaudio_bufferattr_maxlength(self),
			paddlec_pulseaudio_bufferattr_tlength(self),
			paddlec_pulseaudio_bufferattr_prebuf(self),
			paddlec_pulseaudio_bufferattr_minreq(self),
			paddlec_pulseaudio_bufferattr_fragsize(self));
}


/* Document-class: PaddleC::PulseAudio::BufferAttributes
 *
 * Playback and record buffer metrics.
 *
 * @!attribute [rw] maxlength
 *
 * @!attribute [rw] tlength
 *
 * @!attribute [rw] prebuf
 *
 * @!attribute [rw] minreq
 *
 * @!attribute [rw] fragsize
 *
 */

static void Init_paddlec_pulseaudio_bufferattr()
{
	c_PABufferAttributes = rb_define_class_under(m_PulseAudio, "BufferAttributes", rb_cObject);
	rb_define_alloc_func(c_PABufferAttributes, paddlec_pulseaudio_bufferattr_alloc);
	rb_define_method(c_PABufferAttributes, "initialize",  paddlec_pulseaudio_bufferattr_initialize,    0);
	rb_define_method(c_PABufferAttributes, "dup",         paddlec_pulseaudio_bufferattr_dup,           0);
	rb_define_method(c_PABufferAttributes, "inspect",     paddlec_pulseaudio_bufferattr_inspect,       0);
	rb_define_method(c_PABufferAttributes, "maxlength",   paddlec_pulseaudio_bufferattr_maxlength,     0);
	rb_define_method(c_PABufferAttributes, "tlength",     paddlec_pulseaudio_bufferattr_tlength,       0);
	rb_define_method(c_PABufferAttributes, "prebuf",      paddlec_pulseaudio_bufferattr_prebuf,        0);
	rb_define_method(c_PABufferAttributes, "minreq",      paddlec_pulseaudio_bufferattr_minreq,        0);
	rb_define_method(c_PABufferAttributes, "fragsize",    paddlec_pulseaudio_bufferattr_fragsize,      0);
	rb_define_method(c_PABufferAttributes, "maxlength=",  paddlec_pulseaudio_bufferattr_setmaxlength,  1);
	rb_define_method(c_PABufferAttributes, "tlength=",    paddlec_pulseaudio_bufferattr_settlength,    1);
	rb_define_method(c_PABufferAttributes, "prebuf=",     paddlec_pulseaudio_bufferattr_setprebuf,     1);
	rb_define_method(c_PABufferAttributes, "minreq=",     paddlec_pulseaudio_bufferattr_setminreq,     1);
	rb_define_method(c_PABufferAttributes, "fragsize=",   paddlec_pulseaudio_bufferattr_setfragsize,   1);
}


/* PaddleC::PulseAudio::TimingInfo */

/* Document-class: PaddleC::PulseAudio::TimingInfo
 *
 * A structure for all kinds of timing information of a {PaddleC::PulseAudio::Stream}.
 *
 * See {PaddleC::PulseAudio::Stream#update_timing_info} and {PaddleC::PulseAudio::Stream#timing_info}.
 * The total output latency a sample that is written with {PaddleC::PulseAudio::Stream::Playback#write} takes to be played may be estimated by sink_usec+buffer_usec+transport_usec
 * (where buffer_usec is defined as pa_bytes_to_usec(write_index-read_index)).
 * The total input latency a sample that is recorded takes to be delivered to the application is: source_usec+buffer_usec+transport_usec-sink_usec. (Take care of sign issues!)
 * When connected to a monitor source sink_usec contains the latency of the owning sink. The two latency estimations described here are implemented in {PaddleC::PulseAudio::Stream#latency}.
 *
 * All time values are in the sound card clock domain, unless noted otherwise. The sound card clock usually runs at a slightly different rate than the system clock.
 */

static VALUE paddlec_pulseaudio_timinginfo_newfrompa(const pa_timing_info *i)
{
	VALUE res = rb_class_new_instance(0, NULL, c_PATimingInfo);

	rb_iv_set(res, "@timestamp",              DBL2NUM((double)i->timestamp.tv_sec + 1e-6*(double)i->timestamp.tv_usec));
	rb_iv_set(res, "@synchronized_clocks",    (i->synchronized_clocks) ? Qtrue : Qfalse);
	rb_iv_set(res, "@sink_usec",              ULL2NUM(i->sink_usec));
	rb_iv_set(res, "@source_usec",            ULL2NUM(i->source_usec));
	rb_iv_set(res, "@transport_usec",         ULL2NUM(i->transport_usec));
	rb_iv_set(res, "@playing",                (i->playing) ? Qtrue : Qfalse);
	rb_iv_set(res, "@write_index_corrupt",    (i->write_index_corrupt) ? Qtrue : Qfalse);
	rb_iv_set(res, "@write_index",            LL2NUM(i->write_index));
	rb_iv_set(res, "@read_index_corrupt",     (i->read_index_corrupt) ? Qtrue : Qfalse);
	rb_iv_set(res, "@read_index",             LL2NUM(i->read_index));
	rb_iv_set(res, "@configured_sink_usec",   ULL2NUM(i->configured_sink_usec));
	rb_iv_set(res, "@configured_source_usec", ULL2NUM(i->configured_source_usec));
	rb_iv_set(res, "@since_underrun",         LL2NUM(i->since_underrun));

	return res;
}

static void Init_paddlec_pulseaudio_timinginfo()
{
	c_PATimingInfo = rb_define_class_under(m_PulseAudio, "TimingInfo", rb_cObject);
	rb_undef_method(rb_class_of(c_PATimingInfo), "new");
	rb_define_attr(c_PATimingInfo, "timestamp",              1, 0); // @return [Float] The system clock time when this timing info structure was current.
	rb_define_attr(c_PATimingInfo, "synchronized_clocks",    1, 0); // @return [Boolean] Whether the local and the remote machine have synchronized clocks. If synchronized clocks are detected +transport_usec+ becomes much more reliable. However, the code that detects synchronized clocks is very limited and unreliable itself.
	rb_define_attr(c_PATimingInfo, "sink_usec",              1, 0); // @return [Integer] Time in µs a sample takes to be played on the sink. For playback streams and record streams connected to a monitor source.
	rb_define_attr(c_PATimingInfo, "source_usec",            1, 0); // @return [Integer] Time in µs a sample takes from being recorded to being delivered to the application. Only for record streams.
	rb_define_attr(c_PATimingInfo, "transport_usec",         1, 0); // @return [Integer] Estimated time in µs a sample takes to be transferred to/from the daemon. For both playback and record streams.
	rb_define_attr(c_PATimingInfo, "playing",                1, 0); // @return [Boolean] Whether the stream is currently not underrun and data is being passed on to the device. Only for playback streams. This field does not say whether the data is actually already being played. To determine this check whether +since_underrun+ (converted to usec) is larger than +sink_usec+.
	rb_define_attr(c_PATimingInfo, "write_index_corrupt",    1, 0); // @return [Boolean] Whether +write_index+ is not up-to-date because a local write command that corrupted it has been issued in the time since this latency info was current. Only write commands with SEEK_RELATIVE_ON_READ and SEEK_RELATIVE_END can corrupt write_index.
	rb_define_attr(c_PATimingInfo, "write_index",            1, 0); // @return [Integer] Current write index into the playback buffer in bytes. Think twice before using this for seeking purposes: it might be out of date at the time you want to use it. Consider using PA_SEEK_RELATIVE instead.
	rb_define_attr(c_PATimingInfo, "read_index_corrupt",     1, 0); // @return [Boolean] Whether +read_index+ is not up-to-date because a local pause or flush request that corrupted it has been issued in the time since this latency info was current.
	rb_define_attr(c_PATimingInfo, "read_index",             1, 0); // @return [Integer] Current read index into the playback buffer in bytes. Think twice before using this for seeking purposes: it might be out of date at the time you want to use it. Consider using PA_SEEK_RELATIVE_ON_READ instead.
	rb_define_attr(c_PATimingInfo, "configured_sink_usec",   1, 0); // @return [Integer] The configured latency for the sink, in µs.
	rb_define_attr(c_PATimingInfo, "configured_source_usec", 1, 0); // @return [Integer] The configured latency for the source, in µs.
	rb_define_attr(c_PATimingInfo, "since_underrun",         1, 0); // @return [Integer] Bytes that were handed to the sink since the last underrun happened, or since playback started again after the last underrun. Playing will tell you which case it is.
}


/* PaddleC::PulseAudio::ServerInfo */


/* Document-class: PaddleC::PulseAudio::ServerInfo
 * Server information, returned by {Connection#server_info}.
 */


static VALUE paddlec_pulseaudio_serverinfo_newfrompa(const pa_server_info *si)
{
	VALUE res = rb_class_new_instance(0, NULL, c_PAServerInfo);

	rb_iv_set(res, "@user_name",           rb_str_freeze(rb_utf8_str_new_cstr(si->user_name)));
	rb_iv_set(res, "@host_name",           rb_str_freeze(rb_utf8_str_new_cstr(si->host_name)));
	rb_iv_set(res, "@server_version",      rb_str_freeze(rb_utf8_str_new_cstr(si->server_version)));
	rb_iv_set(res, "@server_name",         rb_str_freeze(rb_utf8_str_new_cstr(si->server_name)));
	rb_iv_set(res, "@sample_spec",         rb_obj_freeze(paddlec_pulseaudio_sample_spec_newfrompa(&si->sample_spec)));
	rb_iv_set(res, "@default_sink_name",   rb_str_freeze(rb_utf8_str_new_cstr(si->default_sink_name)));
	rb_iv_set(res, "@default_source_name", rb_str_freeze(rb_utf8_str_new_cstr(si->default_source_name)));
	rb_iv_set(res, "@cookie",              UINT2NUM(si->cookie));
	rb_iv_set(res, "@channel_map",         rb_obj_freeze(paddlec_pulseaudio_channelmap_newfrompa(&si->channel_map)));

	return res;
}


static void Init_paddlec_pulseaudio_serverinfo()
{
	c_PAServerInfo = rb_define_class_under(m_PulseAudio, "ServerInfo", rb_cObject);
	rb_undef_method(rb_class_of(c_PAServerInfo), "new");
	rb_define_attr(c_PAServerInfo, "user_name",           1, 0); // @return [String] User name of the daemon process.
	rb_define_attr(c_PAServerInfo, "host_name",           1, 0); // @return [String] Host name the daemon is running on
	rb_define_attr(c_PAServerInfo, "server_version",      1, 0); // @return [String] Version string of the daemon
	rb_define_attr(c_PAServerInfo, "server_name",         1, 0); // @return [String] Server package name (usually "pulseaudio")
	rb_define_attr(c_PAServerInfo, "sample_spec",         1, 0); // @return [PaddleC::PulseAudio::SampleSpec] Default sample specification.
	rb_define_attr(c_PAServerInfo, "default_sink_name",   1, 0); // @return [String] Name of default sink.
	rb_define_attr(c_PAServerInfo, "default_source_name", 1, 0); // @return [String] Name of default source.
	rb_define_attr(c_PAServerInfo, "cookie",              1, 0); // @return [Integer] A random cookie for identifying this instance of PulseAudio.
	rb_define_attr(c_PAServerInfo, "channel_map",         1, 0); // @return [PaddleC::PulseAudio::ChannelMap] Default channel map.
}


/* PaddleC::PulseAudio::StatInfo */


/* Document-class: PaddleC::PulseAudio::StatInfo
 *
 * Memory block statistics, returned by {Connection#mem_stats}.
 */

static VALUE paddlec_pulseaudio_statinfo_newfrompa(const pa_stat_info *si)
{
	VALUE res = rb_class_new_instance(0, NULL, c_PAStatInfo);

	rb_iv_set(res, "@memblock_total",          UINT2NUM(si->memblock_total));
	rb_iv_set(res, "@memblock_total_size",     UINT2NUM(si->memblock_total_size));
	rb_iv_set(res, "@memblock_allocated",      UINT2NUM(si->memblock_allocated));
	rb_iv_set(res, "@memblock_allocated_size", UINT2NUM(si->memblock_allocated_size));
	rb_iv_set(res, "@scache_size",             UINT2NUM(si->scache_size));

	return res;
}


static void Init_paddlec_pulseaudio_statinfo()
{
	c_PAStatInfo = rb_define_class_under(m_PulseAudio, "StatInfo", rb_cObject);
	rb_undef_method(rb_class_of(c_PAStatInfo), "new");
	rb_define_attr(c_PAStatInfo, "memblock_total",          1, 0); // @return [Integer] Currently allocated memory blocks.
	rb_define_attr(c_PAStatInfo, "memblock_total_size",     1, 0); // @return [Integer] Current total size of allocated memory blocks.
	rb_define_attr(c_PAStatInfo, "memblock_allocated",      1, 0); // @return [Integer] Allocated memory blocks during the whole lifetime of the daemon.
	rb_define_attr(c_PAStatInfo, "memblock_allocated_size", 1, 0); // @return [Integer] Total size of all memory blocks allocated during the whole lifetime of the daemon.
	rb_define_attr(c_PAStatInfo, "scache_size",             1, 0); // @return [Integer] Total size of all sample cache entries.
}
 


/* proplist */

static VALUE paddlec_pulseaudio_proplist_to_hash(const pa_proplist *p)
{
	VALUE hash, rbkey, rbvalue;
	void *state = NULL;
	const char *key;
	const void *data;
	size_t nbytes;

	hash = rb_hash_new();

	for (;;) {
		key = pa_proplist_iterate((pa_proplist*)p, &state);
		if (!key)
			break;
		if (!pa_proplist_get((pa_proplist*)p, key, &data, &nbytes)) {
			rbkey = rb_utf8_str_new_cstr(key);
			rbvalue = rb_str_freeze(rb_utf8_str_new_cstr((const char*)data));
			rb_hash_aset(hash, rbkey, rbvalue);
		}
	}

	return rb_hash_freeze(hash);
}


/* Returns the volume as a Float. 1 is 100%
 */
static inline VALUE paddlec_pulseaudio_pa_volume_to_float(pa_volume_t v)
{
	return DBL2NUM((double)v / (double)PA_VOLUME_NORM);
}

static VALUE paddlec_pulseaudio_cvolume_to_array(const pa_cvolume *cv)
{
	uint8_t i;
	VALUE res = rb_ary_new_capa(cv->channels);
	for (i = 0; i < cv->channels; i++)
		rb_ary_store(res, i, paddlec_pulseaudio_pa_volume_to_float(cv->values[i]));
	return res;
}


/* PaddleC::PulseAudio::SinkInfo */

static VALUE paddlec_pulseaudio_sink_flags_to_a(pa_sink_flags_t f)
{
	VALUE res = rb_ary_new();

    if (f & PA_SINK_HW_VOLUME_CTRL)
		rb_ary_push(res, ID2SYM(rb_intern("hw_volume")));
    if (f & PA_SINK_LATENCY)
		rb_ary_push(res, ID2SYM(rb_intern("latency")));
    if (f & PA_SINK_HARDWARE)
		rb_ary_push(res, ID2SYM(rb_intern("hardware")));
    if (f & PA_SINK_NETWORK)
		rb_ary_push(res, ID2SYM(rb_intern("network")));
    if (f & PA_SINK_HW_MUTE_CTRL)
		rb_ary_push(res, ID2SYM(rb_intern("hw_mute_ctrl")));
    if (f & PA_SINK_DECIBEL_VOLUME)
		rb_ary_push(res, ID2SYM(rb_intern("decibel_volume")));
    if (f & PA_SINK_FLAT_VOLUME)
		rb_ary_push(res, ID2SYM(rb_intern("flat_volume")));
    if (f & PA_SINK_DYNAMIC_LATENCY)
		rb_ary_push(res, ID2SYM(rb_intern("dynamic_latency")));
    if (f & PA_SINK_SET_FORMATS)
		rb_ary_push(res, ID2SYM(rb_intern("set_formats")));

	return res;
}


static VALUE paddlec_pulseaudio_sink_state_to_sym(pa_sink_state_t s)
{
	switch (s) {
		case PA_SINK_RUNNING:
			return ID2SYM(rb_intern("running"));
		case PA_SINK_IDLE:
			return ID2SYM(rb_intern("idle"));
		case PA_SINK_SUSPENDED:
			return ID2SYM(rb_intern("suspended"));
		default:
			return ID2SYM(rb_intern("invalid_state"));
	}
}


/* Document-class: PaddleC::PulseAudio::FormatInfo
 *
 * Represents the format of data provided in a stream or processed by a sink.
 */

static VALUE paddlec_pulseaudio_pa_encoding_to_sym(pa_encoding_t e)
{
	const char *res;

	switch (e) {
		case PA_ENCODING_ANY:
			res = "ANY";
			break;
		case PA_ENCODING_PCM:
			res = "PCM";
			break;
		case PA_ENCODING_AC3_IEC61937:
			res = "AC3_IEC61937";
			break;
		case PA_ENCODING_EAC3_IEC61937:
			res = "EAC3_IEC61937";
			break;
		case PA_ENCODING_MPEG_IEC61937:
			res = "MPEG_IEC61937";
			break;
		case PA_ENCODING_DTS_IEC61937:
			res = "DTS_IEC61937";
			break;
		case PA_ENCODING_MPEG2_AAC_IEC61937:
			res = "MPEG2_AAC_IEC61937";
			break;
#ifdef PA_ENCODING_TRUEHD_IEC61937
		case PA_ENCODING_TRUEHD_IEC61937:
			res = "TRUEHD_IEC61937";
			break;
#endif
#ifdef PA_ENCODING_DTSHD_IEC61937
		case PA_ENCODING_DTSHD_IEC61937:
			res = "DTSHD_IEC61937";
			break;
#endif
		default:
			res = "invalid";
	}

	return ID2SYM(rb_intern(res));
}


/* Returns an array containing all valid endodings.
 * @return [Array<Symbol>] valid encodings
 */
static VALUE paddlec_pulseaudio_format_encodings(VALUE self)
{
	VALUE ar = rb_ary_new_capa(PA_ENCODING_MAX);
	long i;
	(void)self;

	for (i = 0; i < PA_ENCODING_MAX; i++)
		rb_ary_store(ar, i, paddlec_pulseaudio_pa_encoding_to_sym((pa_encoding_t)i));

	return ar;
}


static VALUE paddlec_pulseaudio_formatinfo_newfrompa(const pa_format_info *fi)
{
	VALUE res = rb_class_new_instance(0, NULL, c_PAFormatInfo);

	rb_iv_set(res, "@encoding", paddlec_pulseaudio_pa_encoding_to_sym(fi->encoding));
	rb_iv_set(res, "@plist", paddlec_pulseaudio_proplist_to_hash(fi->plist));

	return res;
}


static void Init_paddlec_pulseaudio_formatinfo()
{
	c_PAFormatInfo = rb_define_class_under(m_PulseAudio, "FormatInfo", rb_cObject);
	rb_define_module_function(c_PAFormatInfo, "encodings", paddlec_pulseaudio_format_encodings, 0);
	rb_undef_method(rb_class_of(c_PAFormatInfo), "new");
	rb_define_attr(c_PAFormatInfo, "encoding", 1, 0); // @return [Symbol] The encoding used for the format.
	rb_define_attr(c_PAFormatInfo, "plist",    1, 0); // @return [Hash{String=>String}] Additional encoding-specific properties such as sample rate, bitrate, etc.
}


/* Document-class: PaddleC::PulseAudio::SinkPortInfo
 *
 * Stores information about a specific port of a sink.
 */

static VALUE paddlec_pulseaudio_sinkportinfo_newfrompa(const pa_sink_port_info *si)
{
	VALUE res = rb_class_new_instance(0, NULL, c_PASinkPortInfo);

	rb_iv_set(res, "@name",                rb_str_freeze(rb_utf8_str_new_cstr(si->name)));
	rb_iv_set(res, "@description",         rb_str_freeze(rb_utf8_str_new_cstr(si->description)));
	rb_iv_set(res, "@priority",            UINT2NUM(si->priority));
	switch (si->available) {
		case PA_PORT_AVAILABLE_NO:
			rb_iv_set(res, "@available",   Qfalse);
			break;
		case PA_PORT_AVAILABLE_YES:
			rb_iv_set(res, "@available",   Qtrue);
			break;
		default:
			rb_iv_set(res, "@available",   Qnil);
	}

	return res;
}


static void Init_paddlec_pulseaudio_sinkportinfo()
{
	c_PASinkPortInfo = rb_define_class_under(m_PulseAudio, "SinkPortInfo", rb_cObject);
	rb_undef_method(rb_class_of(c_PASinkPortInfo), "new");
	rb_define_attr(c_PASinkPortInfo, "name",                1, 0); // @return [String] Name of this port.
	rb_define_attr(c_PASinkPortInfo, "description",         1, 0); // @return [String] Description of this port.
	rb_define_attr(c_PASinkPortInfo, "priority",            1, 0); // @return [Integer] The higher this value is, the more useful this port is as a default.
	rb_define_attr(c_PASinkPortInfo, "available",           1, 0); // @return [Boolean, nil] Indicate availability status of this port, or +nil+ if not known.
}


/* Document-class: PaddleC::PulseAudio::SinkInfo
 *
 * Information about sinks, returned by {Connection#sink} and {Connection#sinks}.
 */

static VALUE paddlec_pulseaudio_sinkinfo_newfrompa(const pa_sink_info *si)
{
	long i;
	VALUE ar, pspi;
	VALUE res = rb_class_new_instance(0, NULL, c_PASinkInfo);

	rb_iv_set(res, "@name",                rb_str_freeze(rb_utf8_str_new_cstr(si->name)));
	rb_iv_set(res, "@index",               UINT2NUM(si->index));
	rb_iv_set(res, "@description",         rb_str_freeze(rb_utf8_str_new_cstr(si->description)));
	rb_iv_set(res, "@sample_spec",         rb_obj_freeze(paddlec_pulseaudio_sample_spec_newfrompa(&si->sample_spec)));
	rb_iv_set(res, "@channel_map",         rb_obj_freeze(paddlec_pulseaudio_channelmap_newfrompa(&si->channel_map)));
	rb_iv_set(res, "@owner_module",        (si->owner_module == PA_INVALID_INDEX) ? Qnil : UINT2NUM(si->owner_module));
	rb_iv_set(res, "@volume",              paddlec_pulseaudio_cvolume_to_array(&si->volume));
	rb_iv_set(res, "@mute",                si->mute ? Qtrue : Qfalse);
	rb_iv_set(res, "@monitor_source",      UINT2NUM(si->monitor_source));
	rb_iv_set(res, "@monitor_source_name", rb_str_freeze(rb_utf8_str_new_cstr(si->monitor_source_name)));
	rb_iv_set(res, "@latency",             ULL2NUM(si->latency));
	rb_iv_set(res, "@driver",              rb_obj_freeze(rb_utf8_str_new_cstr(si->driver)));
	rb_iv_set(res, "@flags",               rb_ary_freeze(paddlec_pulseaudio_sink_flags_to_a(si->flags)));
	rb_iv_set(res, "@proplist",            paddlec_pulseaudio_proplist_to_hash(si->proplist));
	rb_iv_set(res, "@configured_latency",  ULL2NUM(si->configured_latency));
	rb_iv_set(res, "@base_volume",         paddlec_pulseaudio_pa_volume_to_float(si->base_volume));
	rb_iv_set(res, "@state",               paddlec_pulseaudio_sink_state_to_sym(si->state));
	rb_iv_set(res, "@n_volume_steps",      ULL2NUM(si->n_volume_steps));
	rb_iv_set(res, "@card",                (si->card == PA_INVALID_INDEX) ? Qnil : ULL2NUM(si->card));
	rb_iv_set(res, "@active_port",         Qnil);
	ar = rb_ary_new_capa((long)si->n_ports);
	for (i = 0; i < (long)si->n_ports; i++) {
		pspi = rb_obj_freeze(paddlec_pulseaudio_sinkportinfo_newfrompa(si->ports[i]));
		rb_ary_store(ar, i, pspi);
		if (si->ports[i] == si->active_port)
			rb_iv_set(res, "@active_port", pspi);
	}
	rb_iv_set(res, "@ports",               rb_ary_freeze(ar));
	ar = rb_ary_new_capa((long)si->n_formats);
	for (i = 0; i < (long)si->n_formats; i++) {
		rb_ary_store(ar, i, paddlec_pulseaudio_formatinfo_newfrompa(si->formats[i]));
	}
	rb_iv_set(res, "@formats",             rb_ary_freeze(ar));

	return res;
}


static void Init_paddlec_pulseaudio_sinkinfo()
{
	c_PASinkInfo = rb_define_class_under(m_PulseAudio, "SinkInfo", rb_cObject);
	rb_undef_method(rb_class_of(c_PASinkInfo), "new");
	rb_define_attr(c_PASinkInfo, "name",                1, 0); // @return [String] Name of the sink.
	rb_define_attr(c_PASinkInfo, "index",               1, 0); // @return [Integer] Index of the sink.
	rb_define_attr(c_PASinkInfo, "description",         1, 0); // @return [String] Description of this sink.
	rb_define_attr(c_PASinkInfo, "sample_spec",         1, 0); // @return [PaddleC::PulseAudio::SampleSpec] Sample specification of this sink.
	rb_define_attr(c_PASinkInfo, "channel_map",         1, 0); // @return [PaddleC::PulseAudio::ChannelMap] Channel map of this sink.
	rb_define_attr(c_PASinkInfo, "owner_module",        1, 0); // @return [Integer, nil] Index of the owning module of this sink, or +nil+.
	rb_define_attr(c_PASinkInfo, "volume",              1, 0); // @return [Array<Float>] Volume of the sink.
	rb_define_attr(c_PASinkInfo, "mute",                1, 0); // @return [Boolean] Mute switch of the sink.
	rb_define_attr(c_PASinkInfo, "monitor_source",      1, 0); // @return [Integer] Index of the monitor source connected to this sink.
	rb_define_attr(c_PASinkInfo, "monitor_source_name", 1, 0); // @return [String] The name of the monitor source.
	rb_define_attr(c_PASinkInfo, "latency",             1, 0); // @return [Integer] The length of queued audio in the output buffer, in µs.
	rb_define_attr(c_PASinkInfo, "driver",              1, 0); // @return [String] Driver name.
	rb_define_attr(c_PASinkInfo, "flags",               1, 0); // @return [Array<Symbol>] {https://freedesktop.org/software/pulseaudio/doxygen/def_8h.html#a20e0a15bebf78a29893f3b73ef7dadc0 Flags}.
	rb_define_attr(c_PASinkInfo, "proplist",            1, 0); // @return [Hash<String=>String>] Property list.
	rb_define_attr(c_PASinkInfo, "configured_latency",  1, 0); // @return [Integer] The latency this device has been configured to, in µs.
	rb_define_attr(c_PASinkInfo, "base_volume",         1, 0); // @return [Float] Some kind of "base" volume that refers to unamplified/unattenuated volume in the context of the output device.
	rb_define_attr(c_PASinkInfo, "state",               1, 0); // @return [Symbol] The sink state.
	rb_define_attr(c_PASinkInfo, "n_volume_steps",      1, 0); // @return [Integer] Number of volume steps for sinks which do not support arbitrary volumes.
	rb_define_attr(c_PASinkInfo, "card",                1, 0); // @return [Integer, nil] Card index, or +nil+.
	rb_define_attr(c_PASinkInfo, "ports",               1, 0); // @return [Array<PaddleC::PulseAudio::SinkPortInfo>] Array of available ports.
	rb_define_attr(c_PASinkInfo, "active_port",         1, 0); // @return [PaddleC::PulseAudio::SinkPortInfo, nil] The active port, or +nil+.
	rb_define_attr(c_PASinkInfo, "formats",             1, 0); // @return [Array<PaddleC::PulseAudio::FormatInfo>] Array of formats supported by the sink.
}


/* Document-class: PaddleC::PulseAudio::SourcePortInfo
 *
 * Stores information about a specific port of a source.
 */

static VALUE paddlec_pulseaudio_sourceportinfo_newfrompa(const pa_source_port_info *si)
{
	VALUE res = rb_class_new_instance(0, NULL, c_PASourcePortInfo);

	rb_iv_set(res, "@name",                rb_str_freeze(rb_utf8_str_new_cstr(si->name)));
	rb_iv_set(res, "@description",         rb_str_freeze(rb_utf8_str_new_cstr(si->description)));
	rb_iv_set(res, "@priority",            UINT2NUM(si->priority));
	switch (si->available) {
		case PA_PORT_AVAILABLE_NO:
			rb_iv_set(res, "@available",   Qfalse);
			break;
		case PA_PORT_AVAILABLE_YES:
			rb_iv_set(res, "@available",   Qtrue);
			break;
		default:
			rb_iv_set(res, "@available",   Qnil);
	}

	return res;
}


static void Init_paddlec_pulseaudio_sourceportinfo()
{
	c_PASourcePortInfo = rb_define_class_under(m_PulseAudio, "SourcePortInfo", rb_cObject);
	rb_undef_method(rb_class_of(c_PASourcePortInfo), "new");
	rb_define_attr(c_PASourcePortInfo, "name",        1, 0); // @return [String] Name of this port
	rb_define_attr(c_PASourcePortInfo, "description", 1, 0); // @return [String] Description of this port.
	rb_define_attr(c_PASourcePortInfo, "priority",    1, 0); // @return [Integer] The higher this value is, the more useful this port is as a default.
	rb_define_attr(c_PASourcePortInfo, "available",   1, 0); // @return [Boolean, nil] Availability status of this port, or +nil+ if unknown.
}


/* Document-class: PaddleC::PulseAudio::SourceInfo
 *
 * Information about sources, returned by {Connection#source} and {Connection#sources}.
 */


static VALUE paddlec_pulseaudio_source_flags_to_a(pa_source_flags_t f)
{
	VALUE res = rb_ary_new();

    if (f & PA_SOURCE_HW_VOLUME_CTRL)
		rb_ary_push(res, ID2SYM(rb_intern("hw_volume")));
    if (f & PA_SOURCE_LATENCY)
		rb_ary_push(res, ID2SYM(rb_intern("latency")));
    if (f & PA_SOURCE_HARDWARE)
		rb_ary_push(res, ID2SYM(rb_intern("hardware")));
    if (f & PA_SOURCE_NETWORK)
		rb_ary_push(res, ID2SYM(rb_intern("network")));
    if (f & PA_SOURCE_HW_MUTE_CTRL)
		rb_ary_push(res, ID2SYM(rb_intern("hw_mute_ctrl")));
    if (f & PA_SOURCE_DECIBEL_VOLUME)
		rb_ary_push(res, ID2SYM(rb_intern("decibel_volume")));
    if (f & PA_SOURCE_FLAT_VOLUME)
		rb_ary_push(res, ID2SYM(rb_intern("flat_volume")));
    if (f & PA_SOURCE_DYNAMIC_LATENCY)
		rb_ary_push(res, ID2SYM(rb_intern("dynamic_latency")));

	return res;
}


static VALUE paddlec_pulseaudio_source_state_to_sym(pa_source_state_t s)
{
	switch (s) {
		case PA_SOURCE_RUNNING:
			return ID2SYM(rb_intern("running"));
		case PA_SOURCE_IDLE:
			return ID2SYM(rb_intern("idle"));
		case PA_SOURCE_SUSPENDED:
			return ID2SYM(rb_intern("suspended"));
		default:
			return ID2SYM(rb_intern("invalid_state"));
	}
}


static VALUE paddlec_pulseaudio_sourceinfo_newfrompa(const pa_source_info *si)
{
	long i;
	VALUE ar, pspi;
	VALUE res = rb_class_new_instance(0, NULL, c_PASourceInfo);

	rb_iv_set(res, "@name",                 rb_str_freeze(rb_utf8_str_new_cstr(si->name)));
	rb_iv_set(res, "@index",                UINT2NUM(si->index));
	rb_iv_set(res, "@description",          rb_str_freeze(rb_utf8_str_new_cstr(si->description)));
	rb_iv_set(res, "@sample_spec",          rb_obj_freeze(paddlec_pulseaudio_sample_spec_newfrompa(&si->sample_spec)));
	rb_iv_set(res, "@channel_map",          rb_obj_freeze(paddlec_pulseaudio_channelmap_newfrompa(&si->channel_map)));
	rb_iv_set(res, "@owner_module",         (si->owner_module == PA_INVALID_INDEX) ? Qnil : UINT2NUM(si->owner_module));
	rb_iv_set(res, "@volume",               paddlec_pulseaudio_cvolume_to_array(&si->volume));
	rb_iv_set(res, "@mute",                 si->mute ? Qtrue : Qfalse);
	rb_iv_set(res, "@monitor_of_sink",      (si->monitor_of_sink == PA_INVALID_INDEX) ? Qnil : UINT2NUM(si->monitor_of_sink));
	rb_iv_set(res, "@monitor_of_sink_name", (si->monitor_of_sink == PA_INVALID_INDEX) ? Qnil : rb_str_freeze(rb_utf8_str_new_cstr(si->monitor_of_sink_name)));
	rb_iv_set(res, "@latency",              ULL2NUM(si->latency));
	rb_iv_set(res, "@driver",               rb_obj_freeze(rb_utf8_str_new_cstr(si->driver)));
	rb_iv_set(res, "@flags",                rb_ary_freeze(paddlec_pulseaudio_source_flags_to_a(si->flags)));
	rb_iv_set(res, "@proplist",             paddlec_pulseaudio_proplist_to_hash(si->proplist));
	rb_iv_set(res, "@configured_latency",   ULL2NUM(si->configured_latency));
	rb_iv_set(res, "@base_volume",          paddlec_pulseaudio_pa_volume_to_float(si->base_volume));
	rb_iv_set(res, "@state",                paddlec_pulseaudio_source_state_to_sym(si->state));
	rb_iv_set(res, "@n_volume_steps",       ULL2NUM(si->n_volume_steps));
	rb_iv_set(res, "@card",                 (si->card == PA_INVALID_INDEX) ? Qnil : ULL2NUM(si->card));
	rb_iv_set(res, "@active_port",          Qnil);
	ar = rb_ary_new_capa((long)si->n_ports);
	for (i = 0; i < (long)si->n_ports; i++) {
		pspi = rb_obj_freeze(paddlec_pulseaudio_sourceportinfo_newfrompa(si->ports[i]));
		rb_ary_store(ar, i, pspi);
		if (si->ports[i] == si->active_port)
			rb_iv_set(res, "@active_port", pspi);
	}
	rb_iv_set(res, "@ports",               rb_ary_freeze(ar));
	ar = rb_ary_new_capa((long)si->n_formats);
	for (i = 0; i < (long)si->n_formats; i++) {
		rb_ary_store(ar, i, paddlec_pulseaudio_formatinfo_newfrompa(si->formats[i]));
	}
	rb_iv_set(res, "@formats",             rb_ary_freeze(ar));

	return res;
}


static void Init_paddlec_pulseaudio_sourceinfo()
{
	c_PASourceInfo = rb_define_class_under(m_PulseAudio, "SourceInfo", rb_cObject);
	rb_undef_method(rb_class_of(c_PASourceInfo), "new");
	rb_define_attr(c_PASourceInfo, "name",                 1, 0); // @return [String] Name of the source.
	rb_define_attr(c_PASourceInfo, "index",                1, 0); // @return [Integer] Index of the source.
	rb_define_attr(c_PASourceInfo, "description",          1, 0); // @return [String] Description of this source.
	rb_define_attr(c_PASourceInfo, "sample_spec",          1, 0); // @return [PaddleC::PulseAudio::SampleSpec] Sample spec of this source.
	rb_define_attr(c_PASourceInfo, "channel_map",          1, 0); // @return [PaddleC::PulseAudio::ChannelMap] Channel map of this source.
	rb_define_attr(c_PASourceInfo, "owner_module",         1, 0); // @return [Integer, nil] Owning module index, or +nil+.
	rb_define_attr(c_PASourceInfo, "volume",               1, 0); // @return [Array<Float>] Volume of the source.
	rb_define_attr(c_PASourceInfo, "mute",                 1, 0); // @return [Boolean] Mute switch of the source.
	rb_define_attr(c_PASourceInfo, "monitor_of_sink",      1, 0); // @return [Integer, nil] If this is a monitor source, the index of the owning sink, otherwise +nil+.
	rb_define_attr(c_PASourceInfo, "monitor_of_sink_name", 1, 0); // @return [String] Name of the owning sink, or +nil+.
	rb_define_attr(c_PASourceInfo, "latency",              1, 0); // @return [Integer] Length of filled record buffer of this source.
	rb_define_attr(c_PASourceInfo, "driver",               1, 0); // @return [String] Driver name.
	rb_define_attr(c_PASourceInfo, "flags",                1, 0); // @return [Array<Symbol>] Flags.
	rb_define_attr(c_PASourceInfo, "proplist",             1, 0); // @return [Hash{String=>String}] Property list.
	rb_define_attr(c_PASourceInfo, "configured_latency",   1, 0); // @return [Integer] The latency this device has been configured to, in µs.
	rb_define_attr(c_PASourceInfo, "base_volume",          1, 0); // @return [Float] Some kind of "base" volume that refers to unamplified/unattenuated volume in the context of the input device.
	rb_define_attr(c_PASourceInfo, "state",                1, 0); // @return [Symbol] State.
	rb_define_attr(c_PASourceInfo, "n_volume_steps",       1, 0); // @return [Integer] Number of volume steps for sources which do not support arbitrary volumes.
	rb_define_attr(c_PASourceInfo, "card",                 1, 0); // @return [Integer, nil] Card index, or PA_INVALID_INDEX.
	rb_define_attr(c_PASourceInfo, "ports",                1, 0); // @return [Array<PaddleC::PulseAudio::SourcePortInfo>] Array of available ports.
	rb_define_attr(c_PASourceInfo, "active_port",          1, 0); // @return [PaddleC::PulseAudio::SourcePortInfo, nil] Active port in the array, or +nil+.
	rb_define_attr(c_PASourceInfo, "formats",              1, 0); // @return [Array<PaddleC::PulseAudio::FormatInfo>] Array of formats supported by the source.
}


/* Document-class: PaddleC::PulseAudio::ModuleInfo
 *
 * Stores information about modules.
 */

static VALUE paddlec_pulseaudio_moduleinfo_newfrompa(const pa_module_info *mi)
{
	VALUE res = rb_class_new_instance(0, NULL, c_PAModuleInfo);

	rb_iv_set(res, "@index",                UINT2NUM(mi->index));
	rb_iv_set(res, "@name",                 mi->name ? rb_str_freeze(rb_utf8_str_new_cstr(mi->name)) : Qnil);
	rb_iv_set(res, "@argument",             mi->argument ? rb_str_freeze(rb_utf8_str_new_cstr(mi->argument)) : Qnil);
	rb_iv_set(res, "@n_used",               (mi->n_used == PA_INVALID_INDEX) ? Qnil : UINT2NUM(mi->n_used));
	rb_iv_set(res, "@proplist",             mi->proplist ? paddlec_pulseaudio_proplist_to_hash(mi->proplist) : Qnil);

	return res;
}


static void Init_paddlec_pulseaudio_moduleinfo()
{
	c_PAModuleInfo = rb_define_class_under(m_PulseAudio, "ModuleInfo", rb_cObject);
	rb_undef_method(rb_class_of(c_PAModuleInfo), "new");
	rb_define_attr(c_PAModuleInfo, "index",    1, 0); // @return [Integer] Index of the module.
	rb_define_attr(c_PAModuleInfo, "name",     1, 0); // @return [String] Name of the module.
	rb_define_attr(c_PAModuleInfo, "argument", 1, 0); // @return [String, nil] Argument string of the module.
	rb_define_attr(c_PAModuleInfo, "n_used",   1, 0); // @return [Integer, nil] Usage counter, or +nil+.
	rb_define_attr(c_PAModuleInfo, "proplist", 1, 0); // @return [Hash{String=>String}] Property list.
}


/* PaddleC::PulseAudio::Stream */

/* Document-module: PaddleC::PulseAudio::Stream
 *
 * Audio streams for input, output and sample upload. Streams requiere a {Connection}.
 *
 * This module provides common methods for {PaddleC::PulseAudio::Stream::Playback} and {PaddleC::PulseAudio::Stream::Record}.
 */


typedef struct {
	pa_stream *stream;
	VALUE      connection;
	VALUE      latency_update_callback_proc;
	VALUE      write_callback_proc;
	VALUE      state_callback_proc;
	VALUE      read_callback_proc;
	VALUE      overflow_callback_proc;
	VALUE      underflow_callback_proc;
	VALUE      started_callback_proc;
	VALUE      moved_callback_proc;
	VALUE      suspended_callback_proc;
	VALUE      event_callback_proc;
	VALUE      buffer_attr_callback_proc;
} paddlec_pulseaudio_stream_t;


static void paddlec_pulseaudio_stream_free(void *p)
{
	paddlec_pulseaudio_stream_t *st = (paddlec_pulseaudio_stream_t*)p;
	if (st->stream) {
		pa_stream_disconnect(st->stream);
		pa_stream_unref(st->stream);
	}
	ruby_xfree(st);
}


static size_t paddlec_pulseaudio_stream_size(const void *p)
{
	(void)p;
	return sizeof(paddlec_pulseaudio_stream_t);
}


static void paddlec_pulseaudio_stream_mark(void *p)
{
	paddlec_pulseaudio_stream_t *st = (paddlec_pulseaudio_stream_t*)p;
	rb_gc_mark(st->connection);
	rb_gc_mark(st->latency_update_callback_proc);
	rb_gc_mark(st->write_callback_proc);
	rb_gc_mark(st->state_callback_proc);
	rb_gc_mark(st->read_callback_proc);
	rb_gc_mark(st->overflow_callback_proc);
	rb_gc_mark(st->underflow_callback_proc);
	rb_gc_mark(st->started_callback_proc);
	rb_gc_mark(st->moved_callback_proc);
	rb_gc_mark(st->suspended_callback_proc);
	rb_gc_mark(st->event_callback_proc);
	rb_gc_mark(st->buffer_attr_callback_proc);
}


static const rb_data_type_t paddlec_pulseaudio_stream_type = {
	.wrap_struct_name = "paddlec_pulseaudio_stream_struct",
	.function = {
		.dmark = paddlec_pulseaudio_stream_mark,
		.dfree = paddlec_pulseaudio_stream_free,
		.dsize = paddlec_pulseaudio_stream_size,
	},
	.data  = NULL,
	.flags = 0, //RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE paddlec_pulseaudio_stream_alloc(VALUE klass)
{
	VALUE obj;
	paddlec_pulseaudio_stream_t *st;

	//st = ruby_xmalloc(sizeof(paddlec_pulseaudio_stream_t));
	st = malloc(sizeof(paddlec_pulseaudio_stream_t));
	st->stream = NULL;
	st->connection = Qnil;
	st->latency_update_callback_proc = Qnil;
	st->write_callback_proc = Qnil;
	st->state_callback_proc = Qnil;
	st->read_callback_proc = Qnil;
	st->overflow_callback_proc = Qnil;
	st->underflow_callback_proc = Qnil;
	st->started_callback_proc = Qnil;
	st->moved_callback_proc = Qnil;
	st->suspended_callback_proc = Qnil;
	st->event_callback_proc = Qnil;
	st->buffer_attr_callback_proc = Qnil;

	obj = TypedData_Wrap_Struct(klass, &paddlec_pulseaudio_stream_type, st);

	return obj;
}


static paddlec_pulseaudio_stream_t* paddlec_pulseaudio_stream_get_struct2(VALUE obj)
{
	paddlec_pulseaudio_stream_t *st;
	TypedData_Get_Struct(obj, paddlec_pulseaudio_stream_t, &paddlec_pulseaudio_stream_type, st);
	return st;
}


static pa_stream* paddlec_pulseaudio_stream_get_struct(VALUE obj)
{
	paddlec_pulseaudio_stream_t *st;
	TypedData_Get_Struct(obj, paddlec_pulseaudio_stream_t, &paddlec_pulseaudio_stream_type, st);
	return st->stream;
}


/* Disconnect the stream from a sink/source.
 * @return [Boolean] returns +true+ if it was connected, false otherwise.
 */
static VALUE paddlec_pulseaudio_stream_disconnect(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qfalse;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	pa_stream_disconnect(st->stream);
	pa_stream_unref(st->stream);
	st->stream = NULL;
	paddlec_pulseaudio_connection_cunlock(pac);

	return Qtrue;
}


/* Get the {Connection} this stream is bound to.
 * @return [PaddleC::PulseAudio::Connection]
 */
static VALUE paddlec_pulseaudio_stream_connection(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	return st->connection;
}


/* Return the sink input (resp source output) index this stream is identified in the server with, or +nil+ if disconnected.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_stream_index(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	uint32_t r;
	int error = 0;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	r = pa_stream_get_index(st->stream);
	if (r == PA_INVALID_INDEX)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (r == PA_INVALID_INDEX)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
	return UINT2NUM(r);
}


/* Return the index of the sink or source this stream is connected to in the server, or +nil+ if disconnected.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_stream_device_index(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	uint32_t r;
	int error = 0;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	r = pa_stream_get_device_index(st->stream);
	if (r == PA_INVALID_INDEX)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (r == PA_INVALID_INDEX)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
	return UINT2NUM(r);
}


/* Return the name of the sink or source this stream is connected to in the server, or +nil+ if disconnected.
 * @return [String, nil]
 */
static VALUE paddlec_pulseaudio_stream_device_name(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	const char *s;
	int error = 0;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	s = pa_stream_get_device_name(st->stream);
	if (!s)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (!s)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
	return rb_utf8_str_new_cstr(s);
}


/* Whether the sink or source this stream is connected to has been suspended or not, or +nil+ if disconnected.
 * @return [Boolean, nil]
 */
static VALUE paddlec_pulseaudio_stream_issuspended(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	int r, error = 0;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	r = pa_stream_is_suspended(st->stream);
	if (r < 0)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (r < 0)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
	return (r ? Qtrue : Qfalse);
}


/* Whether this stream has been corked or not, or +nil+ if disconnected.
 * @return [Boolean, nil]
 */
static VALUE paddlec_pulseaudio_stream_iscorked(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	int r, error = 0;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	r = pa_stream_is_corked(st->stream);
	if (r < 0)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (r < 0)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
	return (r ? Qtrue : Qfalse);
}


/* Whether the stream is connected or not.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_stream_isconnected(VALUE self)
{
	pa_stream *st;
	st = paddlec_pulseaudio_stream_get_struct(self);
	if (st)
		return Qtrue;
	return Qfalse;
}


/* Whether the stream is disconnected or not.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_stream_isdisconnected(VALUE self)
{
	pa_stream *st;
	st = paddlec_pulseaudio_stream_get_struct(self);
	if (st)
		return Qfalse;
	return Qtrue;
}


static VALUE paddlec_pulseaudio_stream_state_to_symbol(pa_stream_state_t st)
{
	const char *s;

	switch (st) {
		case PA_STREAM_UNCONNECTED:
			s = "unconnected"; break;
		case PA_STREAM_CREATING:
			s = "creating"; break;
		case PA_STREAM_READY:
			s = "ready"; break;
		case PA_STREAM_FAILED:
			s = "failed"; break;
		case PA_STREAM_TERMINATED:
			s = "terminated"; break;
		default:
			s = "invalid";
	}

	return ID2SYM(rb_intern(s));
}


/* Tell whether this method is called from a PulseAudio callback (helper thread) or not.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_stream_isincallback(VALUE self)
{
	return (paddlec_pulseaudio_connection_in_worker_thread(paddlec_pulseaudio_connection_get_struct(paddlec_pulseaudio_stream_get_struct2(self)->connection)) ? Qtrue : Qfalse);
}


/* Return the current state of the stream.
 * - +:unconnected+ : The stream is not yet connected to any sink or source.
 * - +:creating+ : The stream is being created.
 * - +:ready+ : The stream is established, you may pass audio data to it now.
 * - +:failed+ : An error occurred that made the stream invalid.
 * - +:ternimated+ : The stream has been terminated cleanly.
 * - +:invalid+ : unknown state.
 * @return [Symbol]
 */
static VALUE paddlec_pulseaudio_stream_state(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_stream_state_t state;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return ID2SYM(rb_intern("unconnected"));
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	state = pa_stream_get_state(st->stream);
	paddlec_pulseaudio_connection_cunlock(pac);
	return paddlec_pulseaudio_stream_state_to_symbol(state);
}


/* Return a copy of the stream's sample specification, or +nil+ if disconnected.
 * @return [PaddleC::PulseAudio::SampleSpec, nil]
 */
static VALUE paddlec_pulseaudio_stream_get_sample_spec(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	VALUE res;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	res = paddlec_pulseaudio_sample_spec_newfrompa(pa_stream_get_sample_spec(st->stream));
	paddlec_pulseaudio_connection_cunlock(pac);
	return res;
}


/* Return a copy of the stream's channel map, or +nil+ if disconnected.
 * @return [PaddleC::PulseAudio::ChannelMap, nil]
 */
static VALUE paddlec_pulseaudio_stream_get_channel_map(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	VALUE res;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	res = paddlec_pulseaudio_channelmap_newfrompa(pa_stream_get_channel_map(st->stream));
	paddlec_pulseaudio_connection_cunlock(pac);
	return res;
}


/* Return a copy of the stream's format, or +nil+ if disconnected.
 * @return [PaddleC::PulseAudio::FormatInfo, nil]
 */
static VALUE paddlec_pulseaudio_stream_get_formatinfo(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	VALUE res;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	res = paddlec_pulseaudio_formatinfo_newfrompa(pa_stream_get_format_info(st->stream));
	paddlec_pulseaudio_connection_cunlock(pac);
	return res;
}


/* Return a copy of the per-stream server-side buffer metrics of the stream, or +nil+ if disconnected.
 * @return [PaddleC::PulseAudio::BufferAttributes, nil]
 */
static VALUE paddlec_pulseaudio_stream_get_bufferattr(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	VALUE res;
	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	res = paddlec_pulseaudio_bufferattr_newfrompa(pa_stream_get_buffer_attr(st->stream));
	paddlec_pulseaudio_connection_cunlock(pac);
	return res;
}


/* Return the number of bytes requested by the server that have not yet been written.
 * It is possible to write more than this amount, up to the stream's +buffer_attributes.maxlength+ bytes.
 * This is usually not desirable, though, as it would increase stream latency to be higher than requested (+buffer_attributes.tlength+).
 * Return +nil+ if disconnected.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_playback_writable_bytes(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	int error = 0;
	size_t nb_bytes;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	nb_bytes = pa_stream_writable_size(st->stream);
	if (nb_bytes == (size_t)-1)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (nb_bytes == (size_t)-1)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return ULL2NUM(nb_bytes);
}


/* Return the number of samples requested by the server that have not yet been written.
 *
 * For example, if this method returns 4 and +self.sample_spec == 'u8 3ch 32000Hz'+, it means that 12 bytes can be written: 4 samples x 3 channels of 1 byte. 
 *
 * It is possible to write more than this amount, up to the stream's +buffer_attributes.maxlength+ bytes.
 * This is usually not desirable, though, as it would increase stream latency to be higher than requested (+buffer_attributes.tlength+).
 *
 * Return +nil+ if disconnected.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_playback_writable_samples(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	const pa_sample_spec *sample_spec;
	int error = 0;
	size_t nb_bytes;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	sample_spec = pa_stream_get_sample_spec(st->stream);
	nb_bytes = pa_stream_writable_size(st->stream);
	if (nb_bytes == (size_t)-1)
		error = pa_context_errno(pac->context);
	nb_bytes /= pa_frame_size(sample_spec);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return ULL2NUM(nb_bytes);
}


static void my_latency_update_cb(pa_stream *p, void *userdata)
{
	VALUE rbstream = (VALUE)userdata;
	paddlec_pulseaudio_stream_t *st = paddlec_pulseaudio_stream_get_struct2(rbstream);
	if (st->latency_update_callback_proc != Qnil)
		rb_funcallv(st->latency_update_callback_proc, rb_intern("call"), 1, &rbstream);
}

/* Set the callback block that is called (from the helper thread) whenever a latency information update happens.
 * If called without a block, removes the previously set callback.
 *
 * @yield [stream] the given block must accept one argument, which is the stream itself.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_stream_set_latency_update_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->latency_update_callback_proc = rb_block_proc();
	else
		st->latency_update_callback_proc = Qnil;

	if (!st->stream)
		return self;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	if (st->latency_update_callback_proc == Qnil)
		pa_stream_set_latency_update_callback(st->stream, NULL, (void*)self);
	else
		pa_stream_set_latency_update_callback(st->stream, my_latency_update_cb, (void*)self);
	paddlec_pulseaudio_connection_cunlock(pac);
	
	return self;
}


static void my_write_cb(pa_stream *p, size_t nbytes, void *userdata)
{
	VALUE st_s_e_b[4] = {(VALUE)userdata, Qundef, Qundef, Qundef};
	const pa_sample_spec *ss;
	paddlec_pulseaudio_stream_t *st = paddlec_pulseaudio_stream_get_struct2((VALUE)userdata);

	if (st->write_callback_proc != Qnil) {
		ss = pa_stream_get_sample_spec(p);
		st_s_e_b[1] = ULL2NUM(nbytes / pa_frame_size(ss));
		st_s_e_b[2] = ULL2NUM(nbytes / pa_sample_size(ss));
		st_s_e_b[3] = ULL2NUM(nbytes);
		rb_funcallv(st->write_callback_proc, rb_intern("call"), 4, st_s_e_b);
	}
}

/* Set the callback block that is called (from the helper thread) when new data may be written to the stream.
 * If called without a block, removes the previously set callback.
 *
 * @yieldparam playback [PaddleC::PulseAudio::Stream::Playback] the playback stream.
 * @yieldparam nsamples [Integer] number of samples that can be written (+nsamples * playback.sample_spec.channels+ elements).
 * @yieldparam nelems [Integer] number of elements that can be written (+nelems+ elements).
 * @yieldparam nbytes [Integer] number of bytes that can be written (+nelems * playback.sample_spec.format.size+ bytes).
 * @return [self]
 */
static VALUE paddlec_pulseaudio_playback_set_write_request_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->write_callback_proc = rb_block_proc();
	else
		st->write_callback_proc = Qnil;

	if (!st->stream)
		return self;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	if (st->write_callback_proc == Qnil)
		pa_stream_set_write_callback(st->stream, NULL, (void*)self);
	else
		pa_stream_set_write_callback(st->stream, my_write_cb, (void*)self);
	paddlec_pulseaudio_connection_cunlock(pac);
	
	return self;
}


static void my_overflow_cb(pa_stream *p, void *userdata)
{
	VALUE rbstream = (VALUE)userdata;
	paddlec_pulseaudio_stream_t *st = paddlec_pulseaudio_stream_get_struct2(rbstream);
	if (st->overflow_callback_proc != Qnil)
		rb_funcallv(st->overflow_callback_proc, rb_intern("call"), 1, &rbstream);
}

/* Set the callback block that is called (from the helper thread) when a buffer overflow happens.
 * If called without a block, removes the previously set callback.
 *
 * @yield [stream] the given block must accept one argument, which is the stream itself.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_playback_set_overflow_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->overflow_callback_proc = rb_block_proc();
	else
		st->overflow_callback_proc = Qnil;

	if (!st->stream)
		return self;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	if (st->overflow_callback_proc == Qnil)
		pa_stream_set_overflow_callback(st->stream, NULL, (void*)self);
	else
		pa_stream_set_overflow_callback(st->stream, my_overflow_cb, (void*)self);
	paddlec_pulseaudio_connection_cunlock(pac);
	
	return self;
}


static void my_underflow_cb(pa_stream *p, void *userdata)
{
	VALUE rbstream = (VALUE)userdata;
	paddlec_pulseaudio_stream_t *st = paddlec_pulseaudio_stream_get_struct2(rbstream);
	if (st->underflow_callback_proc != Qnil)
		rb_funcallv(st->underflow_callback_proc, rb_intern("call"), 1, &rbstream);
}

/* Set the callback block that is called (from the helper thread) when a buffer underflow happens.
 * If called without a block, removes the previously set callback.
 *
 * @yield [stream] the given block must accept one argument, which is the stream itself.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_playback_set_underflow_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->underflow_callback_proc = rb_block_proc();
	else
		st->underflow_callback_proc = Qnil;

	if (!st->stream)
		return self;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	if (st->underflow_callback_proc == Qnil)
		pa_stream_set_underflow_callback(st->stream, NULL, (void*)self);
	else
		pa_stream_set_underflow_callback(st->stream, my_underflow_cb, (void*)self);
	paddlec_pulseaudio_connection_cunlock(pac);
	
	return self;
}


static void my_started_cb(pa_stream *p, void *userdata)
{
	VALUE rbstream = (VALUE)userdata;
	paddlec_pulseaudio_stream_t *st = paddlec_pulseaudio_stream_get_struct2(rbstream);
	if (st->started_callback_proc != Qnil)
		rb_funcallv(st->started_callback_proc, rb_intern("call"), 1, &rbstream);
}

/* Set the callback block that is called (from the helper thread) when the server starts playback after an underrun or on initial startup.
 * This only informs that audio is flowing again, it is no indication that audio started to reach the speakers already.
 * If called without a block, removes the previously set callback.
 *
 * @yield [stream] the given block must accept one argument, which is the stream itself.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_playback_set_started_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->started_callback_proc = rb_block_proc();
	else
		st->started_callback_proc = Qnil;

	if (!st->stream)
		return self;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	if (st->started_callback_proc == Qnil)
		pa_stream_set_started_callback(st->stream, NULL, (void*)self);
	else
		pa_stream_set_started_callback(st->stream, my_started_cb, (void*)self);
	paddlec_pulseaudio_connection_cunlock(pac);
	
	return self;
}


static void my_moved_cb(pa_stream *p, void *userdata)
{
	VALUE rbstream = (VALUE)userdata;
	paddlec_pulseaudio_stream_t *st = paddlec_pulseaudio_stream_get_struct2(rbstream);
	if (st->moved_callback_proc != Qnil)
		rb_funcallv(st->moved_callback_proc, rb_intern("call"), 1, &rbstream);
}

/* Set the callback block that is called (from the helper thread) whenever the stream is moved to a different sink/source.
 *
 * Use {PaddleC::PulseAudio::Stream#device_name} or {PaddleC::PulseAudio::Stream#device_index} to query the new sink/source.
 * This notification is only generated when the server is at least 0.9.8.
 *
 * If called without a block, removes the previously set callback.
 *
 * @yield [stream] the given block must accept one argument, which is the stream itself.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_stream_set_moved_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->moved_callback_proc = rb_block_proc();
	else
		st->moved_callback_proc = Qnil;

	if (!st->stream)
		return self;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	if (st->moved_callback_proc == Qnil)
		pa_stream_set_moved_callback(st->stream, NULL, (void*)self);
	else
		pa_stream_set_moved_callback(st->stream, my_moved_cb, (void*)self);
	paddlec_pulseaudio_connection_cunlock(pac);
	
	return self;
}


static void my_suspended_cb(pa_stream *p, void *userdata)
{
	VALUE rbstream = (VALUE)userdata;
	paddlec_pulseaudio_stream_t *st = paddlec_pulseaudio_stream_get_struct2(rbstream);
	if (st->suspended_callback_proc != Qnil)
		rb_funcallv(st->suspended_callback_proc, rb_intern("call"), 1, &rbstream);
}

/* Set the callback block that is called (from the helper thread) whenever the sink/source this stream is connected to is suspended or resumed.
 *
 * Use {PaddleC::PulseAudio::Stream#suspended} to query the new suspend status.
 * Please note that the suspend status might also change when the stream is moved between devices.
 * Thus if you call this function you very likely want to call {PaddleC::PulseAudio::Stream#on_moved} too.
 * This notification is only generated when the server is at least 0.9.8.
 *
 * If called without a block, removes the previously set callback.
 *
 * @yield [stream] the given block must accept one argument, which is the stream itself.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_stream_set_suspended_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->suspended_callback_proc = rb_block_proc();
	else
		st->suspended_callback_proc = Qnil;

	if (!st->stream)
		return self;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	if (st->suspended_callback_proc == Qnil)
		pa_stream_set_suspended_callback(st->stream, NULL, (void*)self);
	else
		pa_stream_set_suspended_callback(st->stream, my_suspended_cb, (void*)self);
	paddlec_pulseaudio_connection_cunlock(pac);
	
	return self;
}


static void my_buffer_attr_cb(pa_stream *p, void *userdata)
{
	VALUE rbstream = (VALUE)userdata;
	paddlec_pulseaudio_stream_t *st = paddlec_pulseaudio_stream_get_struct2(rbstream);
	if (st->buffer_attr_callback_proc != Qnil)
		rb_funcallv(st->buffer_attr_callback_proc, rb_intern("call"), 1, &rbstream);
}

/* Set the callback block that is called (from the helper thread) whenever the buffer attributes on the server side change.
 *
 * Please note that the buffer attributes can change when moving a stream to a different sink/source too,
 * hence if you use this callback you should use {PaddleC::PulseAudio::Stream#on_moved} as well.
 *
 * If called without a block, removes the previously set callback.
 *
 * @yield [stream] the given block must accept one argument, which is the stream itself.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_stream_set_buffer_attr_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->buffer_attr_callback_proc = rb_block_proc();
	else
		st->buffer_attr_callback_proc = Qnil;

	if (!st->stream)
		return self;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	if (st->buffer_attr_callback_proc == Qnil)
		pa_stream_set_buffer_attr_callback(st->stream, NULL, (void*)self);
	else
		pa_stream_set_buffer_attr_callback(st->stream, my_buffer_attr_cb, (void*)self);
	paddlec_pulseaudio_connection_cunlock(pac);
	
	return self;
}


static void my_event_cb(pa_stream *p, const char *event_name, pa_proplist *pl, void *userdata)
{
	VALUE rbstream = (VALUE)userdata;
	paddlec_pulseaudio_stream_t *st = paddlec_pulseaudio_stream_get_struct2(rbstream);
	if (st->event_callback_proc != Qnil)
		rb_funcall(st->event_callback_proc, rb_intern("call"), 3, rbstream, rb_utf8_str_new_cstr(event_name), paddlec_pulseaudio_proplist_to_hash(pl));
}

/* Set the callback block that is called (from the helper thread) whenever a meta/policy control event is received.
 *
 * If called without a block, removes the previously set callback.
 *
 * @yieldparam stream [PaddleC::PulseAudio::Stream] the stream itself
 * @yieldparam event [String] the name of the event
 * @yieldparam proplist [Hash{String=>String}] a property list
 * @return [self]
 */
static VALUE paddlec_pulseaudio_stream_set_event_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->event_callback_proc = rb_block_proc();
	else
		st->event_callback_proc = Qnil;

	if (!st->stream)
		return self;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	if (st->event_callback_proc == Qnil)
		pa_stream_set_event_callback(st->stream, NULL, (void*)self);
	else
		pa_stream_set_event_callback(st->stream, my_event_cb, (void*)self);
	paddlec_pulseaudio_connection_cunlock(pac);
	
	return self;
}


/* Return the current playback/recording time, in µs.
 *
 * The returned time is in the sound card clock domain, which usually runs at a slightly different rate than the system clock.
 * The data from the last timing update is used for an estimation of the current playback/recording time 
 * based on the local time that passed since the timing info structure has been acquired (see {PaddleC::PulseAudio::Stream#on_latency_update}).
 *
 * The time value returned by this function is guaranteed to increase monotonically (the returned value is always greater or equal to the value returned by the last call). 
 *
 * The time interpolator favours 'smooth' time graphs over accurate ones to improve the smoothness of UI operations that are tied to the audio clock.
 * If accuracy is more important to you, you might need to estimate your timing based on the data from {PaddleC::PulseAudio::Stream#timing_info} 
 * yourself or not work with interpolated timing at all and instead always query the server side for the most up to date timing with {PaddleC::PulseAudio::Stream#update_timing_info}.
 *
 * If no timing information has been received yet this call will return nil.
 * @return [Integer, nil] the time in µs, or +nil+ if disconnected or no time is available.
 */
static VALUE paddlec_pulseaudio_stream_time(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	int r, error = 0;
	pa_usec_t rusec;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	r = pa_stream_get_time(st->stream, &rusec);
	if (r)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (r)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	if (rusec == (pa_usec_t)-PA_ERR_NODATA)
		return Qnil;

	return ULL2NUM(rusec);
}


/* Determine the total stream latency, in µs.
 *
 * This function is based {PaddleC::PulseAudio::Stream#time}.
 * The returned time is in the sound card clock domain, which usually runs at a slightly different rate than the system clock.
 *
 * In case the stream is a monitoring stream the result can be negative, i.e. the captured samples are not yet played.
 *
 * If no timing information has been received yet or if the stream is not connected, this call will return +nil+.
 *
 * @return [Integer, nil] the stream latency in µs, or nil if disconnected or not timing information is available yet.
 */
static VALUE paddlec_pulseaudio_stream_latency(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	int r, negative, error = 0;
	pa_usec_t rusec;
	int64_t sr;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	r = pa_stream_get_latency(st->stream, &rusec, &negative);
	if (r)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (r)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	if (rusec == (pa_usec_t)-PA_ERR_NODATA)
		return Qnil;
	sr = (int64_t)rusec;
	if (negative)
		sr = -sr;

	return LL2NUM(sr);
}


/* Return the latest raw timing data structure.
 *
 * An in-place update to this data structure may be requested using {PaddleC::PulseAudio::Stream#update_timing_info}.
 *
 * If no timing information has been received before or if the stream is disconnected, this method will return nil.
 *
 * Please note that the +write_index+ member field (and only this field) is updated on each {PaddleC::PulseAudio::Stream::Playback#write} call, not just when a timing update has been received.
 * @return [PaddleC::PulseAudio::TimingInfo, nil]
 */
static VALUE paddlec_pulseaudio_stream_timing_info(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	const pa_timing_info *ti;
	VALUE res;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	ti = pa_stream_get_timing_info(st->stream);
	res = paddlec_pulseaudio_timinginfo_newfrompa(ti);
	paddlec_pulseaudio_connection_cunlock(pac);

	return res;
}


/* Return at what position the latest underflow occurred, or 
 * +false+ if this information is not known (e.g. if no underflow has occurred, or server is older than 1.0).
 *
 * Can be used inside the underflow callback to get information about the current underflow. (Only for playback streams)
 * @return [Integer, false, nil]
 */
static VALUE paddlec_pulseaudio_stream_underflow_index(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	VALUE res;
	int64_t r;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	paddlec_pulseaudio_connection_clock(pac);
	r = pa_stream_get_underflow_index(st->stream);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (r == -1)
		res = Qfalse;
	else
		res = LL2NUM(r);

	return res;
}


/* Write some data to the server.
 * @param data [String, PaddleC::FloatBuffer, PaddleC::ComplexBuffer] the data to be written
 * @return [self, Qnil] returns +self+, or +nil+ if the connection is closed
 */
static VALUE paddlec_pulseaudio_playback_write(VALUE self, VALUE data)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	int error = 0;
	const void *ptr;
	size_t nbytes;
	const pdlc_buffer_t *fbuf;
	const pdlc_complex_buffer_t *cbuf;
	void   *pdata = NULL;
	size_t  pnbytes;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;

	if (rb_obj_is_kind_of(data, rb_cString)) {
		ptr = StringValuePtr(data);
		nbytes = (size_t)RSTRING_LEN(data);
	}
	else if (rb_obj_is_kind_of(data, c_FloatBuffer)) {
		fbuf = paddlec_float_buffer_get_struct(data);
		ptr = fbuf->data;
		nbytes = fbuf->length * sizeof(float);
	}
	else if (rb_obj_is_kind_of(data, c_ComplexBuffer)) {
		cbuf = paddlec_complex_buffer_get_struct(data);
		ptr = cbuf->data;
		nbytes = cbuf->length * sizeof(pdlc_complex_t);
	}
	else
		rb_raise(rb_eTypeError, "expecting a String, a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, 
				rb_class_name(c_FloatBuffer), rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(data)));

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	pnbytes = nbytes;
	paddlec_pulseaudio_connection_clock(pac);
	//fprintf(stderr, "pa_stream_begin_write()\n");
	if (pa_stream_begin_write(st->stream, &pdata, &pnbytes)) {
		error = pa_context_errno(pac->context);
		paddlec_pulseaudio_connection_cunlock(pac);
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
	}
	if (pnbytes >= nbytes) {
		//fprintf(stderr, "pa_stream_write(%lu / %lu)\n\n", nbytes, pnbytes);
		memcpy(pdata, ptr, nbytes);
		if (pa_stream_write(st->stream, pdata, nbytes, NULL, 0, PA_SEEK_RELATIVE)) {
			error = pa_context_errno(pac->context);
			paddlec_pulseaudio_connection_cunlock(pac);
			rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
		}
	}
	else {
		//fprintf(stderr, "pa_stream_cancel_write(%lu)\n", pnbytes);
		//fprintf(stderr, "pa_stream_write(%lu)\n\n", nbytes);
		if (pa_stream_cancel_write(st->stream) || pa_stream_write(st->stream, ptr, nbytes, NULL, 0, PA_SEEK_RELATIVE)) {
			error = pa_context_errno(pac->context);
			paddlec_pulseaudio_connection_cunlock(pac);
			rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
		}
	}
	paddlec_pulseaudio_connection_cunlock(pac);

	RB_GC_GUARD(data);

	return self;
}


static void my_pa_stream_success_cb(pa_stream *s, int success, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	pac->success = &success;
	paddlec_pulseaudio_connection_signal(pac, 1);
	pac->success = NULL;
}


/* Change the buffer metrics of the stream during playback.
 *
 * The server might have chosen different buffer metrics than requested.
 * The selected metrics may be queried with {PaddleC::PulseAudio::Stream#buffer_attributes} as soon as the callback is called.
 * Only valid after the stream has been connected successfully and if the server is at least PulseAudio 0.9.8.
 * Please be aware of the slightly different semantics of the call depending whether PA_STREAM_ADJUST_LATENCY is set or not.
 * @param battr [PaddleC::PulseAudio::BufferAttributes] the buffer attributes to be set
 * @return [PaddleC::PulseAudio::BufferAttributes]
 */
static VALUE paddlec_pulseaudio_stream_set_bufferattr(VALUE self, VALUE battr)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	const pa_buffer_attr *attr = NULL;
	int error = 0;

	if (rb_class_of(battr) != c_PABufferAttributes)
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PABufferAttributes), rb_class_name(rb_class_of(battr)));
	attr = paddlec_pulseaudio_bufferattr_get_struct(battr);

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return battr;

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_stream_set_buffer_attr(st->stream, attr, my_pa_stream_success_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return battr;
}


/* Request immediate start of playback on this stream.
 *
 * This disables prebuffering temporarily if specified in the {PaddleC::PulseAudio::BufferAttributes} structure.
 * @return [self, nil] +nil+ if disconnected, +self+ otherwise.
 */
static VALUE paddlec_pulseaudio_playback_trigger(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error = 0;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_stream_trigger(st->stream, my_pa_stream_success_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* Drain a playback stream.
 *
 * Use this for notification when the playback buffer is empty after playing all the audio in the buffer.
 * Please note that only one drain operation per stream may be issued at a time.
 * @return [self, nil] +nil+ if disconnected, +self+ otherwise.
 */
static VALUE paddlec_pulseaudio_playback_drain(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error = 0;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_stream_drain(st->stream, my_pa_stream_success_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* Request a {PaddleC::PulseAudio::TimingInfo} structure update for this stream.
 *
 * Use this method to get access to the raw timing data, or {PaddleC::PulseAudio::Stream#time} or {PaddleC::PulseAudio::Stream#latency} to get cleaned up values.
 * @return [self, nil] +nil+ if disconnected, +self+ otherwise.
 */
static VALUE paddlec_pulseaudio_stream_updatetiminginfo(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error = 0;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_stream_update_timing_info(st->stream, my_pa_stream_success_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* Pause the playback or record of this stream.
 * @return [self, nil] +nil+ if disconnected, +self+ otherwise.
 */
static VALUE paddlec_pulseaudio_stream_pause(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error = 0;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_stream_cork(st->stream, 1, my_pa_stream_success_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* Resume the playback or record of this stream.
 * @return [self, nil] +nil+ if disconnected, +self+ otherwise.
 */
static VALUE paddlec_pulseaudio_stream_resume(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error = 0;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_stream_cork(st->stream, 0, my_pa_stream_success_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* Flush the playback or record buffer of this stream.
 *
 * This discards any audio data in the buffer.
 * @return [self, nil] +nil+ if disconnected, +self+ otherwise.
 */
static VALUE paddlec_pulseaudio_stream_flush(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error = 0;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_stream_flush(st->stream, my_pa_stream_success_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* Reenable prebuffering if specified in the {PaddleC::PulseAudio::BufferAttributes} structure.
 * @return [self, nil] +nil+ if disconnected, +self+ otherwise.
 */
static VALUE paddlec_pulseaudio_playback_prebuf(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error = 0;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_stream_prebuf(st->stream, my_pa_stream_success_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* Rename the stream.
 * @param name [String] new name of the stream
 * @return [String, nil] +nil+ if disconnected (name not set), +name+ otherwise.
 */
static VALUE paddlec_pulseaudio_stream_set_name(VALUE self, VALUE name)
{
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error = 0;

	if (!rb_obj_is_kind_of(name, rb_cString))
		rb_raise(rb_eTypeError, "expecting a string, not a %"PRIsVALUE, rb_class_name(rb_class_of(name)));

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (!st->stream)
		return Qnil;

	pac = paddlec_pulseaudio_connection_get_struct(st->connection);
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_stream_set_name(st->stream, rb_string_value_cstr(&name), my_pa_stream_success_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return name;
}


static void my_state_change_cb(pa_stream *p, void *userdata)
{
	VALUE rbstream = (VALUE)userdata;
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_stream_state_t state;
	int mustunlocklock;

	if (paddlec_pulseaudio_interrupted)
		return;

	st = paddlec_pulseaudio_stream_get_struct2(rbstream);
	state = pa_stream_get_state(p);
	pac = paddlec_pulseaudio_connection_get_struct(st->connection);

	switch (state) {
		case PA_STREAM_UNCONNECTED:
			//fprintf(stderr, "PA_STREAM_UNCONNECTED\n");
			break;
		case PA_STREAM_CREATING:
			//fprintf(stderr, "PA_STREAM_CREATING\n");
			break;
		case PA_STREAM_READY:
			//fprintf(stderr, "PA_STREAM_READY\n");
			paddlec_pulseaudio_connection_signal(pac, 0);
			break;
		case PA_STREAM_FAILED:
			//fprintf(stderr, "PA_STREAM_FAILED\n");
			paddlec_pulseaudio_connection_signal(pac, 0);
			break;
		case PA_STREAM_TERMINATED:
			//fprintf(stderr, "PA_STREAM_TERMINATED\n");
			if (st->stream) {
				pa_stream_unref(st->stream);
				st->stream = NULL;
			}
			break;
		default:
			//fprintf(stderr, "invalid stream state!!!\n");
			paddlec_pulseaudio_connection_signal(pac, 0);
	}

	if (st->state_callback_proc != Qnil) {
		mustunlocklock = (!paddlec_pulseaudio_connection_in_worker_thread(pac) && rb_mutex_locked_p(pac->mutex));
		if (mustunlocklock)
			paddlec_pulseaudio_connection_unlock(pac);
		rb_funcall(st->state_callback_proc, rb_intern("call"), 2, rbstream, paddlec_pulseaudio_stream_state_to_symbol(state));
		if (mustunlocklock)
			paddlec_pulseaudio_connection_lock(pac);
	}
}


/* Set the callback block that is called (from the helper thread) whenever the state of the stream changes.
 * If called without a block, removes the previously set callback.
 *
 * @yieldparam stream [PaddleC::PulseAudio::Stream::Playback, PaddleC::PulseAudio::Stream::Record] the stream (+self+).
 * @yieldparam state [Symbol] the state.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_stream_set_state_change_cb(VALUE self)
{
	paddlec_pulseaudio_stream_t *st;

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (rb_block_given_p())
		st->state_callback_proc = rb_block_proc();
	else
		st->state_callback_proc = Qnil;
	
	return self;
}


/* Connect the playback stream to a sink.
 * @overload connect(name: self.class.name, device: nil, sample_spec: 'float32 1ch 44100Hz', channel_map: nil, buffer_attributes: nil, adjust_latency: false)
 * @param name [String, nil] a descriptive name for this stream (application name, song title, ...)
 * @param device [String, nil] sink name, or +nil+ for default
 * @param sample_spec [PaddleC::PulseAudio::SampleSpec, String] the sample type to use, or a string describing a sample specification (see {SampleSpec#initialize})
 * @param channel_map [PaddleC::PulseAudio::ChannelMap, Integer, String, Array<Symbol>, nil] the channel map to use, or an argument of {ChannelMap#initialize}, or +nil+ for default
 * @param buffer_attributes [PaddleC::PulseAudio::BufferAttributes, nil] buffering attributes, or +nil+ for default
 * @param adjust_latency [Boolean] Try to adjust the latency of the sink/source based on the requested buffer attributes metrics and adjust buffer metrics accordingly.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_playback_connect(int argc, VALUE *argv, VALUE self)
{
	VALUE opts, rbcon;
	VALUE n_d_ss_cm_ba[6] = {Qundef, Qundef, Qundef, Qundef, Qundef, Qundef};
	const ID kwkeys[6] = {rb_intern("name"), rb_intern("device"), rb_intern("sample_spec"), rb_intern("channel_map"), rb_intern("buffer_attributes"), rb_intern("adjust_latency")};
	paddlec_pulseaudio_stream_t *st;
	paddlec_pulseaudio_connection_t *pac;
	pa_sample_spec ss;
	const pa_channel_map *cm = NULL;
	const pa_buffer_attr *attr = NULL;
	int r, error = 0;
	pa_stream_flags_t adjust_latency = 0;
	pa_stream_state_t state = PA_STREAM_UNCONNECTED;


	rb_scan_args(argc, argv, ":", &opts);

	if (!NIL_P(opts))
		rb_get_kwargs(opts, kwkeys, 0, 5, n_d_ss_cm_ba);

	if (n_d_ss_cm_ba[0] != Qundef && n_d_ss_cm_ba[0] != Qnil) {
		if (!rb_obj_is_kind_of(n_d_ss_cm_ba[0], rb_cString))
			rb_raise(rb_eTypeError, "stream name must be a string, not a %"PRIsVALUE, rb_class_name(rb_class_of(n_d_ss_cm_ba[0])));
	}
	else
		n_d_ss_cm_ba[0] = rb_class_name(c_PAPlayback);

	if (n_d_ss_cm_ba[1] != Qundef) {
		if (!rb_obj_is_kind_of(n_d_ss_cm_ba[1], rb_cString) && n_d_ss_cm_ba[1] != Qnil)
			rb_raise(rb_eTypeError, "device must be a string, not a %"PRIsVALUE, rb_class_name(rb_class_of(n_d_ss_cm_ba[1])));
	}
	else
		n_d_ss_cm_ba[1] = Qnil;

	if (n_d_ss_cm_ba[2] == Qnil || n_d_ss_cm_ba[2] == Qundef)
		n_d_ss_cm_ba[2] = rb_sprintf("float32 1ch 44100Hz");
	if (rb_class_of(n_d_ss_cm_ba[2]) != c_PASampleSpec)
		n_d_ss_cm_ba[2] = rb_class_new_instance(1, &n_d_ss_cm_ba[2], c_PASampleSpec);
	ss = paddlec_pulseaudio_sample_spec_get(n_d_ss_cm_ba[2]);

	if (n_d_ss_cm_ba[3] == Qundef)
		n_d_ss_cm_ba[3] = Qnil;
	if (n_d_ss_cm_ba[3] != Qnil && rb_class_of(n_d_ss_cm_ba[3]) != c_PAChannelMap)
		n_d_ss_cm_ba[3] = rb_class_new_instance(1, &n_d_ss_cm_ba[3], c_PAChannelMap); 
	if (n_d_ss_cm_ba[3] != Qnil)
		cm = paddlec_pulseaudio_channelmap_get_struct(n_d_ss_cm_ba[3]);

	if (n_d_ss_cm_ba[4] == Qundef)
		n_d_ss_cm_ba[4] = Qnil;
	if (n_d_ss_cm_ba[4] != Qnil && rb_class_of(n_d_ss_cm_ba[4]) != c_PABufferAttributes)
		rb_raise(rb_eTypeError, "buffer_attributes must be nil or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PABufferAttributes), rb_class_name(rb_class_of(n_d_ss_cm_ba[4])));
	if (n_d_ss_cm_ba[4] != Qnil)
		attr = paddlec_pulseaudio_bufferattr_get_struct(n_d_ss_cm_ba[4]);

	if (n_d_ss_cm_ba[5] != Qnil && n_d_ss_cm_ba[5] != Qfalse)
		adjust_latency = PA_STREAM_ADJUST_LATENCY;

	if (cm && !pa_channel_map_compatible(cm, &ss))
		rb_raise(rb_eRuntimeError, "Channel map [%"PRIsVALUE"] is not compatible with sample specification [%"PRIsVALUE"]", n_d_ss_cm_ba[3], n_d_ss_cm_ba[2]);

	st = paddlec_pulseaudio_stream_get_struct2(self);
	if (st->stream)
		rb_raise(rb_eRuntimeError, "the stream is already connected");
	rbcon = st->connection;
	pac = paddlec_pulseaudio_connection_get_struct(rbcon);
	if (pac->thread == Qnil)
		rb_raise(rb_eRuntimeError, "the connection this stream is bound to is closed");
	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");


	paddlec_pulseaudio_connection_lock(pac);

	st->stream = pa_stream_new(
			pac->context,
			rb_string_value_cstr(&n_d_ss_cm_ba[0]),
			&ss,
			cm);
	if (st->stream == NULL) {
		paddlec_pulseaudio_connection_unlock(pac);
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(pa_context_errno(pac->context)));
	}

	if (!NIL_P(st->latency_update_callback_proc))
		pa_stream_set_latency_update_callback(st->stream, my_latency_update_cb, (void*)self);
	if (!NIL_P(st->write_callback_proc))
		pa_stream_set_write_callback(st->stream, my_write_cb, (void*)self);
	pa_stream_set_state_callback(st->stream, my_state_change_cb, (void*)self);
	//if (!NIL_P(st->read_callback_proc))
	if (!NIL_P(st->overflow_callback_proc))
		pa_stream_set_overflow_callback(st->stream, my_overflow_cb, (void*)self);
	if (!NIL_P(st->underflow_callback_proc))
		pa_stream_set_underflow_callback(st->stream, my_underflow_cb, (void*)self);
	if (!NIL_P(st->started_callback_proc))
		pa_stream_set_started_callback(st->stream, my_started_cb, (void*)self);
	if (!NIL_P(st->moved_callback_proc))
		pa_stream_set_moved_callback(st->stream, my_moved_cb, (void*)self);
	if (!NIL_P(st->suspended_callback_proc))
		pa_stream_set_suspended_callback(st->stream, my_suspended_cb, (void*)self);
	if (!NIL_P(st->event_callback_proc))
		pa_stream_set_event_callback(st->stream, my_event_cb, (void*)self);
	if (!NIL_P(st->buffer_attr_callback_proc))
		pa_stream_set_buffer_attr_callback(st->stream, my_buffer_attr_cb, (void*)self);

	r = pa_stream_connect_playback(
			st->stream,
			(n_d_ss_cm_ba[1] == Qnil) ? NULL : rb_string_value_cstr(&n_d_ss_cm_ba[1]),
			attr,
			PA_STREAM_INTERPOLATE_TIMING | PA_STREAM_AUTO_TIMING_UPDATE | adjust_latency,
			NULL,
			NULL);

	if (r) {
		error = pa_context_errno(pac->context);
		pa_stream_disconnect(st->stream);
		pa_stream_unref(st->stream);
		st->stream = NULL;
		paddlec_pulseaudio_connection_unlock(pac);
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
	}
	
	while ((state = pa_stream_get_state(st->stream)) != PA_STREAM_READY && state != PA_STREAM_FAILED)
		paddlec_pulseaudio_connection_wait(pac);

	paddlec_pulseaudio_connection_unlock(pac);

	return self;
}


/* Document-class: PaddleC::PulseAudio::Stream::Playback
 *
 * A playback stream for sending audio to be played by the server.
 */


static void Init_paddlec_pulseaudio_stream()
{
	m_PAStream = rb_define_module_under(m_PulseAudio, "Stream");
	rb_define_method(m_PAStream, "disconnect",            paddlec_pulseaudio_stream_disconnect,             0);
	rb_define_method(m_PAStream, "connection",            paddlec_pulseaudio_stream_connection,             0);
	rb_define_method(m_PAStream, "index",                 paddlec_pulseaudio_stream_index,                  0);
	rb_define_method(m_PAStream, "device_index",          paddlec_pulseaudio_stream_device_index,           0);
	rb_define_method(m_PAStream, "device_name",           paddlec_pulseaudio_stream_device_name,            0);
	rb_define_method(m_PAStream, "suspended?",            paddlec_pulseaudio_stream_issuspended,            0);
	rb_define_method(m_PAStream, "corked?",               paddlec_pulseaudio_stream_iscorked,               0);
	rb_define_method(m_PAStream, "connected?",            paddlec_pulseaudio_stream_isconnected,            0);
	rb_define_method(m_PAStream, "disconnected?",         paddlec_pulseaudio_stream_isdisconnected,         0);
	rb_define_method(m_PAStream, "state",                 paddlec_pulseaudio_stream_state,                  0);
	rb_define_method(m_PAStream, "sample_spec",           paddlec_pulseaudio_stream_get_sample_spec,        0);
	rb_define_method(m_PAStream, "channel_map",           paddlec_pulseaudio_stream_get_channel_map,        0);
	rb_define_method(m_PAStream, "format",                paddlec_pulseaudio_stream_get_formatinfo,         0);
	rb_define_method(m_PAStream, "buffer_attributes",     paddlec_pulseaudio_stream_get_bufferattr,         0);
	rb_define_method(m_PAStream, "buffer_attributes=",    paddlec_pulseaudio_stream_set_bufferattr,         1);
	rb_define_method(m_PAStream, "time",                  paddlec_pulseaudio_stream_time,                   0);
	rb_define_method(m_PAStream, "latency",               paddlec_pulseaudio_stream_latency,                0);
	rb_define_method(m_PAStream, "timing_info",           paddlec_pulseaudio_stream_timing_info,            0);
	rb_define_method(m_PAStream, "update_timing_info",    paddlec_pulseaudio_stream_updatetiminginfo,       0);
	rb_define_method(m_PAStream, "underflow_index",       paddlec_pulseaudio_stream_underflow_index,        0);
	rb_define_method(m_PAStream, "pause",                 paddlec_pulseaudio_stream_pause,                  0);
	rb_define_method(m_PAStream, "resume",                paddlec_pulseaudio_stream_resume,                 0);
	rb_define_method(m_PAStream, "flush",                 paddlec_pulseaudio_stream_flush,                  0);
	rb_define_method(m_PAStream, "name=",                 paddlec_pulseaudio_stream_set_name,               1);
	rb_define_method(m_PAStream, "in_callback?",          paddlec_pulseaudio_stream_isincallback,           0);
	rb_define_method(m_PAStream, "on_latency_update",     paddlec_pulseaudio_stream_set_latency_update_cb,  0);
	rb_define_method(m_PAStream, "on_state_change",       paddlec_pulseaudio_stream_set_state_change_cb,    0);
	rb_define_method(m_PAStream, "on_moved",              paddlec_pulseaudio_stream_set_moved_cb,           0);
	rb_define_method(m_PAStream, "on_suspended",          paddlec_pulseaudio_stream_set_suspended_cb,       0);
	rb_define_method(m_PAStream, "on_buffer_attr_change", paddlec_pulseaudio_stream_set_buffer_attr_cb,     0);
	rb_define_method(m_PAStream, "on_event",              paddlec_pulseaudio_stream_set_event_cb,           0);


	c_PAPlayback = rb_define_class_under(m_PAStream, "Playback", rb_cObject);
	rb_define_alloc_func(c_PAPlayback, paddlec_pulseaudio_stream_alloc);
	rb_include_module(c_PAPlayback, m_PAStream);
	rb_undef_method(rb_class_of(c_PAPlayback), "new");
	rb_define_method(c_PAPlayback, "connect",             paddlec_pulseaudio_playback_connect,             -1);
	rb_define_method(c_PAPlayback, "writable_bytes",      paddlec_pulseaudio_playback_writable_bytes,       0);
	rb_define_method(c_PAPlayback, "writable_samples",    paddlec_pulseaudio_playback_writable_samples,     0);
	rb_define_method(c_PAPlayback, "write",               paddlec_pulseaudio_playback_write,                1);
	rb_define_alias(c_PAPlayback,  "<<", "write");        
	rb_define_method(c_PAPlayback, "trigger",             paddlec_pulseaudio_playback_trigger,              0);
	rb_define_method(c_PAPlayback, "drain",               paddlec_pulseaudio_playback_drain,                0);
	rb_define_method(c_PAPlayback, "prebuf",              paddlec_pulseaudio_playback_prebuf,               0);
	rb_define_method(c_PAPlayback, "on_write_request",    paddlec_pulseaudio_playback_set_write_request_cb, 0);
	rb_define_method(c_PAPlayback, "on_overflow",         paddlec_pulseaudio_playback_set_overflow_cb,      0);
	rb_define_method(c_PAPlayback, "on_underflow",        paddlec_pulseaudio_playback_set_underflow_cb,     0);
	rb_define_method(c_PAPlayback, "on_started",          paddlec_pulseaudio_playback_set_started_cb,       0);
}

// TODO: callbacks, volume, flags, rate, rate=, proplist, record //

/* PaddleC::PulseAudio::Connection */



static inline int paddlec_pulseaudio_connection_in_worker_thread(paddlec_pulseaudio_connection_t *pac)
{
	if (rb_thread_current() == pac->thread)
		return 1;
	return 0;
}


/* Make sure that this function is not called from the helper thread */
static inline void paddlec_pulseaudio_connection_lock(paddlec_pulseaudio_connection_t *pac)
{
    /* Make sure that this function is not called from the helper thread */
    assert(!paddlec_pulseaudio_connection_in_worker_thread(pac));
	rb_mutex_lock(pac->mutex);
}


/* Make sure that this function is not called from the helper thread */
static inline void paddlec_pulseaudio_connection_unlock(paddlec_pulseaudio_connection_t *pac)
{
    /* Make sure that this function is not called from the helper thread */
    assert(!paddlec_pulseaudio_connection_in_worker_thread(pac));
	rb_mutex_unlock(pac->mutex);
}


/* Does nothing if called from the helper thread */
static inline void paddlec_pulseaudio_connection_clock(paddlec_pulseaudio_connection_t *pac)
{
    if (pac->thread != rb_thread_current())
		rb_mutex_lock(pac->mutex);
}


/* Does nothing if called from the helper thread */
static inline void paddlec_pulseaudio_connection_cunlock(paddlec_pulseaudio_connection_t *pac)
{
    if (pac->thread != rb_thread_current())
		rb_mutex_unlock(pac->mutex);
}


static inline int paddlec_pulseaudio_connection_is_locked(paddlec_pulseaudio_connection_t *pac)
{
	return ((pac->mutex != Qnil) && rb_mutex_locked_p(pac->mutex));
}


/* Called with the lock taken */
static inline void paddlec_pulseaudio_connection_signal(paddlec_pulseaudio_connection_t *pac, int wait_for_accept)
{
	/* Called with the lock taken */
	assert(paddlec_pulseaudio_connection_is_locked(pac));

	rb_funcallv(pac->cond, rb_intern("broadcast"), 0, NULL);

    if (wait_for_accept) {
        pac->n_waiting_for_accept++;
        while (pac->n_waiting_for_accept > 0)
			rb_funcallv(pac->accept_cond, rb_intern("wait"), 1, &pac->mutex);
    }
}


/* Called with the lock taken.
 * Make sure that this function is not called from the helper thread 
 */
static inline void paddlec_pulseaudio_connection_wait(paddlec_pulseaudio_connection_t *pac)
{
	/* Called with the lock taken */
	assert(paddlec_pulseaudio_connection_is_locked(pac));
    /* Make sure that this function is not called from the helper thread */
    assert(!paddlec_pulseaudio_connection_in_worker_thread(pac));

    pac->n_waiting++;
	rb_funcallv(pac->cond, rb_intern("wait"), 1, &pac->mutex);
    assert(pac->n_waiting > 0);
    pac->n_waiting--;
}


/* Called with the lock taken.
 * Make sure that this function is not called from the helper thread.
 */
static inline void paddlec_pulseaudio_connection_accept(paddlec_pulseaudio_connection_t *pac)
{
	/* Called with the lock taken */
	assert(paddlec_pulseaudio_connection_is_locked(pac));
    /* Make sure that this function is not called from the helper thread */
    assert(!paddlec_pulseaudio_connection_in_worker_thread(pac));

    assert(pac->n_waiting_for_accept > 0);
    pac->n_waiting_for_accept--;
	rb_funcallv(pac->accept_cond, rb_intern("signal"), 0, NULL);
}


static void paddlec_pulseaudio_connection_mark(void *p)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)p;
	rb_gc_mark(pac->thread);
	rb_gc_mark(pac->mutex);
	rb_gc_mark(pac->cond);
	rb_gc_mark(pac->accept_cond);
	rb_gc_mark(pac->rbval);
}


static void paddlec_pulseaudio_connection_free(void *p)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)p;

	if (pac->context) {
		pa_context_set_state_callback(pac->context, NULL, NULL);
		//fprintf(stderr, "> disconnecting\n");
		pa_context_disconnect(pac->context);
		pa_context_unref(pac->context);
	}
	if (pac->thread != Qnil) {
		//fprintf(stderr, "> Stopping thread\n");
		pa_mainloop_quit(pac->mainloop, 0);
		rb_funcallv(pac->thread, rb_intern("join"), 0, NULL);
		pac->thread = Qnil;
	}
	if (pac->mainloop) {
		//fprintf(stderr, "> free mainloop\n");
		pa_mainloop_free(pac->mainloop);
	}
	if (pac->mutex != Qnil && paddlec_pulseaudio_connection_is_locked(pac)) {
		rb_mutex_unlock(pac->mutex);
	}

	ruby_xfree(pac);
}


static size_t paddlec_pulseaudio_connection_size(const void* data)
{
	(void)data;
	return sizeof(paddlec_pulseaudio_connection_t);
}


static const rb_data_type_t paddlec_pulseaudio_connection_type = {
	.wrap_struct_name = "paddlec_pulseaudio_connection_struct",
	.function = {
		.dmark = paddlec_pulseaudio_connection_mark,
		.dfree = paddlec_pulseaudio_connection_free,
		.dsize = paddlec_pulseaudio_connection_size,
	},
	.data = NULL,
	.flags = 0,
};


static VALUE paddlec_pulseaudio_connection_alloc(VALUE klass)
{
	VALUE obj;
	paddlec_pulseaudio_connection_t *pac;

	//pac = ruby_xmalloc(sizeof(paddlec_pulseaudio_connection_t));
	pac = malloc(sizeof(paddlec_pulseaudio_connection_t));
	pac->thread = Qnil;
	pac->mutex = Qnil;
	pac->cond = Qnil;
	pac->accept_cond = Qnil;
	pac->mainloop = NULL;
	pac->context = NULL;
	pac->n_waiting = 0;
	pac->n_waiting_for_accept = 0;
	pac->rbval = Qnil;

	obj = TypedData_Wrap_Struct(klass, &paddlec_pulseaudio_connection_type, pac);

	return obj;
}


static paddlec_pulseaudio_connection_t* paddlec_pulseaudio_connection_get_struct(VALUE obj)
{
	paddlec_pulseaudio_connection_t *pac;
	TypedData_Get_Struct(obj, paddlec_pulseaudio_connection_t, &paddlec_pulseaudio_connection_type, pac);
	return pac;
}


/* Returns a new {Connection}.
 * @return [PaddleC::PulseAudio::Connection]
 */
static VALUE paddlec_pulseaudio_connection_initialize(VALUE self)
{
	return self;
}


struct poll_args {
	struct pollfd *ufds;
	unsigned long nfds;
	int timeout;
	int ret;
};


static void* my_poll_func(void *args)
{
	struct poll_args *pa = (struct poll_args*)args;
	pa->ret = poll(pa->ufds, pa->nfds, pa->timeout);
	return NULL;
}


static void my_ubf(void *args)
{
	pa_mainloop *ml = (pa_mainloop*)args;
	paddlec_pulseaudio_interrupted = 1;
	pa_mainloop_quit(ml, 1);
}


int my_pa_poll_func(struct pollfd *ufds, unsigned long nfds, int timeout, void *userdata)
{
	struct poll_args args;
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;

	args.ufds = ufds;
	args.nfds = nfds;
	args.timeout = timeout;
	args.ret = -1;

	rb_mutex_unlock(pac->mutex);
	rb_thread_call_without_gvl2(my_poll_func, &args, my_ubf, pac->mainloop);
	rb_mutex_lock(pac->mutex);

	return args.ret;
}


static VALUE paddlec_pulseaudio_connection_thread_func(paddlec_pulseaudio_connection_t *pac)
{
	int r;
	int retval = 0;

	//printf("Starting PA main loop in thread\n");
	rb_mutex_lock(pac->mutex);
	r = pa_mainloop_run(pac->mainloop, &retval);
	rb_mutex_unlock(pac->mutex);
	//printf("Stoping PA main loop, returned %d, retval: %d\n", r, retval);
	if (r >= 0)
		r = retval;
	//fprintf(stderr, "\e[1;32mbye from thread\n\e[0m");

	return INT2NUM(r);
}


static const char* paddlec_pulseaudio_connection_state_to_string(pa_context_state_t state)
{
	switch (state) {
		case PA_CONTEXT_UNCONNECTED:
			return "The context hasn't been connected yet.";
		case PA_CONTEXT_CONNECTING:
			return "A connection is being established.";
		case PA_CONTEXT_AUTHORIZING:
			return "The client is authorizing itself to the daemon.";
		case PA_CONTEXT_SETTING_NAME:
			return "The client is passing its application name to the daemon.";
		case PA_CONTEXT_READY:
			return "The connection is established, the context is ready to execute operations.";
		case PA_CONTEXT_FAILED:
			return "The connection failed or was disconnected.";
		case PA_CONTEXT_TERMINATED:
			return "The connection was terminated cleanly.";
		default:
			return "UNKNOWN CONNECTION STATE";
	}
}


static VALUE paddlec_pulseaudio_connection_is_closed(VALUE self)
{
	const paddlec_pulseaudio_connection_t *pac;
	pac = paddlec_pulseaudio_connection_get_struct(self);
	return ((pac->thread == Qnil) ? Qtrue : Qfalse);
}


static void my_pa_context_success_cb(pa_context *c, int success, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	pac->success = &success;
	paddlec_pulseaudio_connection_signal(pac, 1);
	pac->success = NULL;
}


/* Tell if some data is pending to be written to the connection.
 * Returns +nil+ if the connection is closed.
 * @return [Boolean, nil]
 */
static VALUE paddlec_pulseaudio_connection_is_pending(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	int r;

	pac = paddlec_pulseaudio_connection_get_struct(self);

	if (pac->thread == Qnil)
		return Qnil;

	paddlec_pulseaudio_connection_clock(pac);
	r = pa_context_is_pending(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);

	return (r ? Qtrue : Qfalse);
}


/* Tells if the connection is to a local daemon.
 * Returns +nil+ if the connection is closed or hasn't been made yet.
 * @return [Boolean, nil]
 */
static VALUE paddlec_pulseaudio_connection_is_local(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	int r;

	pac = paddlec_pulseaudio_connection_get_struct(self);

	if (pac->thread == Qnil)
		return Qnil;

	paddlec_pulseaudio_connection_clock(pac);
	r = pa_context_is_local(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);

	return ((r > 0) ? Qtrue : ((r == 0) ? Qfalse : Qnil));
}


/* Returns the serveur connection path, or +nil+ if not connected or closed.
 * @return [String, nil]
 */
static VALUE paddlec_pulseaudio_connection_server_name(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	const char *s;

	pac = paddlec_pulseaudio_connection_get_struct(self);

	if (pac->thread == Qnil)
		return Qnil;

	paddlec_pulseaudio_connection_clock(pac);
	s = pa_context_get_server(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);

	return rb_utf8_str_new_cstr(s);
}


/* Return the protocol version of the client library, or +nil+ if the connection is closed.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_connection_protocol_version(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	uint32_t p; 

	pac = paddlec_pulseaudio_connection_get_struct(self);

	if (pac->thread == Qnil)
		return Qnil;

	paddlec_pulseaudio_connection_clock(pac);
	p = pa_context_get_protocol_version(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);

	return UINT2NUM(p);
}


/* Return the protocol version of the connected server, or +nil+ if the connection is closed.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_connection_server_protocol_version(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	uint32_t p; 
	int error = 0;

	pac = paddlec_pulseaudio_connection_get_struct(self);

	if (pac->thread == Qnil)
		return Qnil;

	paddlec_pulseaudio_connection_clock(pac);
	p = pa_context_get_server_protocol_version(pac->context);
	if (p == PA_INVALID_INDEX)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (p == PA_INVALID_INDEX)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return UINT2NUM(p);
}


/* Return the client index this context is identified in the server with, or +nil+ if the connection is closed.
 * This is useful for usage with the introspection functions.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_connection_index(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	uint32_t p; 
	int error = 0;

	pac = paddlec_pulseaudio_connection_get_struct(self);

	if (pac->thread == Qnil)
		return Qnil;

	paddlec_pulseaudio_connection_clock(pac);
	p = pa_context_get_index(pac->context);
	if (p == PA_INVALID_INDEX)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);
	if (p == PA_INVALID_INDEX)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return UINT2NUM(p);
}


/* Set a different application name for context on the server.
 * @param name [String]
 * @return [String]
 */
static VALUE paddlec_pulseaudio_connection_set_name(VALUE self, VALUE name)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error;
	int s;

	if (rb_class_of(name) != rb_cString)
		rb_raise(rb_eArgError, "expecting a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(name)));

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		rb_raise(rb_eRuntimeError, "connection is closed");

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_context_set_name(pac->context, StringValueCStr(name), my_pa_context_success_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	s = *pac->success;
	if (!s)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (!s)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return name;
}


/* Set the name of the default sink, server wide.
 * @param default_sink [String]
 * @return [String]
 */
static VALUE paddlec_pulseaudio_connection_set_default_sink(VALUE self, VALUE default_sink)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error;
	int s;

	if (rb_class_of(default_sink) != rb_cString)
		rb_raise(rb_eArgError, "expecting a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(default_sink)));

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		rb_raise(rb_eRuntimeError, "connection is closed");

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_context_set_default_sink(pac->context, StringValueCStr(default_sink), my_pa_context_success_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	s = *pac->success;
	if (!s)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (!s)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return default_sink;
}


/* Set the name of the default source, server wide.
 * @param default_source [String]
 * @return [String]
 */
static VALUE paddlec_pulseaudio_connection_set_default_source(VALUE self, VALUE default_source)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error;
	int s;

	if (rb_class_of(default_source) != rb_cString)
		rb_raise(rb_eArgError, "expecting a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(default_source)));

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		rb_raise(rb_eRuntimeError, "connection is closed");

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_context_set_default_source(pac->context, StringValueCStr(default_source), my_pa_context_success_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	s = *pac->success;
	if (!s)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (!s)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return default_source;
}


/* Tell the PulseAudio daemon (the server) to exit.
 * The operation is unlikely to complete successfully, since the daemon probably died before returning a success notification.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_connection_exit_daemon(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int error;
	int s;

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		rb_raise(rb_eRuntimeError, "connection is closed");

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_context_exit_daemon(pac->context, my_pa_context_success_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	s = *pac->success;
	if (!s)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (!s)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


static void my_pa_context_drain_notify_cb(pa_context *c, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	paddlec_pulseaudio_connection_signal(pac, 0);
}


/* Drain the context.
 * Returns +true+ if something was drained, +flase+ otherwise, or +nil+ if the connection is closed.
 * @return [Boolean, nil]
 */
static VALUE paddlec_pulseaudio_connection_drain(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	VALUE res;

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	op = pa_context_drain(pac->context, my_pa_context_drain_notify_cb, (void*)pac);
	if (op) {
		while (pa_operation_get_state(op) == PA_OPERATION_RUNNING)
			paddlec_pulseaudio_connection_wait(pac);
		pa_operation_unref(op);
		res = Qtrue;
	}
	else {
		res = Qfalse;
	}
	paddlec_pulseaudio_connection_unlock(pac);

	return res;
}


/* Return the optimal block size for passing around audio buffers.
 * It is recommended to allocate buffers of the size returned here when writing audio data to playback streams, if the latency constraints permit this.
 * It is not recommended writing larger blocks than this because usually they will then be split up internally into chunks of this size.
 * It is not recommended writing smaller blocks than this (unless required due to latency demands) because this increases CPU usage.
 *
 * If +sample_spec+ is +nil+ you will be returned the byte-exact tile size.
 * If +sample_spec+ is invalid or the connection is closed, +nil+ will be returned.
 * If you pass a valid +sample_spec+, then the tile size will be rounded down to multiple of the frame size.
 *
 * @param sample_spec [PaddleC::PulseAudio::SampleSpec, String, nil]
 * @return [Integer, nil] optimal block size in bytes for passing around audio buffers.
 */
static VALUE paddlec_pulseaudio_connection_tile_size(VALUE self, VALUE sample_spec)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_sample_spec spec;
	size_t res;

	if (rb_class_of(sample_spec) == rb_cString)
		sample_spec = rb_class_new_instance(1, &sample_spec, c_PASampleSpec);
	if (sample_spec != Qnil && rb_class_of(sample_spec) != c_PASampleSpec)
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or nil, not a %"PRIsVALUE, rb_class_name(c_PASampleSpec), rb_class_name(rb_class_of(sample_spec)));
	if (sample_spec != Qnil) {
		spec = paddlec_pulseaudio_sample_spec_get(sample_spec);
		if (!pa_sample_spec_valid(&spec))
			return Qnil;
	}

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	paddlec_pulseaudio_connection_clock(pac);
	if (sample_spec != Qnil)
		res = pa_context_get_tile_size(pac->context, &spec);
	else
		res = pa_context_get_tile_size(pac->context, NULL);
	paddlec_pulseaudio_connection_cunlock(pac);

	return ULL2NUM(res);
}


/* Return the optimal block size for passing around audio buffers.
 * It is recommended to allocate buffers of the size returned here when writing audio data to playback streams, if the latency constraints permit this.
 * It is not recommended writing larger blocks than this because usually they will then be split up internally into chunks of this size.
 * It is not recommended writing smaller blocks than this (unless required due to latency demands) because this increases CPU usage.
 *
 * If +sample_spec+ is invalid or the connection is closed, +nil+ will be returned.
 *
 * @param sample_spec [PaddleC::PulseAudio::SampleSpec, String]
 * @return [Integer, nil] optimal number of frames for passing around audio buffers.
 */
static VALUE paddlec_pulseaudio_connection_tile_length(VALUE self, VALUE sample_spec)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_sample_spec spec;
	size_t res, frame_size;

	if (rb_class_of(sample_spec) == rb_cString)
		sample_spec = rb_class_new_instance(1, &sample_spec, c_PASampleSpec);
	if (rb_class_of(sample_spec) != c_PASampleSpec)
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PASampleSpec), rb_class_name(rb_class_of(sample_spec)));
	spec = paddlec_pulseaudio_sample_spec_get(sample_spec);
	if (!pa_sample_spec_valid(&spec))
		return Qnil;
	frame_size = pa_frame_size(&spec);

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	paddlec_pulseaudio_connection_clock(pac);
	res = pa_context_get_tile_size(pac->context, &spec) / frame_size;
	paddlec_pulseaudio_connection_cunlock(pac);

	return ULL2NUM(res);
}


/* @return [Symbol]
*/
static VALUE paddlec_pulseaudio_connection_state(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_context_state_t state;

	pac = paddlec_pulseaudio_connection_get_struct(self);

	if (pac->thread == Qnil)
		return ID2SYM(rb_intern("closed"));

	paddlec_pulseaudio_connection_clock(pac);
	state = pa_context_get_state(pac->context);
	paddlec_pulseaudio_connection_cunlock(pac);

	switch (state) {
		case PA_CONTEXT_UNCONNECTED:
			return ID2SYM(rb_intern("unconnected"));
		case PA_CONTEXT_CONNECTING:
			return ID2SYM(rb_intern("connecting"));
		case PA_CONTEXT_AUTHORIZING:
			return ID2SYM(rb_intern("authorizing"));
		case PA_CONTEXT_SETTING_NAME:
			return ID2SYM(rb_intern("setting_name"));
		case PA_CONTEXT_READY:
			return ID2SYM(rb_intern("ready"));
		case PA_CONTEXT_FAILED:
			return ID2SYM(rb_intern("failed"));
		case PA_CONTEXT_TERMINATED:
			return ID2SYM(rb_intern("terminated"));
	}

	return ID2SYM(rb_intern("unknown"));
}


static void my_pa_context_notify_cb(pa_context *c, void *userdata)
{
	pa_context_state_t state;
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	
	state = pa_context_get_state(c);
	switch (state) {
		case PA_CONTEXT_UNCONNECTED:
			//printf(" State callback: The context hasn't been connected yet.\n");
			break;
		case PA_CONTEXT_CONNECTING:
			//printf(" State callback: A connection is being established.\n");
			break;
		case PA_CONTEXT_AUTHORIZING:
			//printf(" State callback: The client is authorizing itself to the daemon.\n");
			break;
		case PA_CONTEXT_SETTING_NAME:
			//printf(" State callback: The client is passing its application name to the daemon.\n");
			break;
		case PA_CONTEXT_READY:
			//printf(" State callback: The connection is established, the context is ready to execute operations.\n");
			paddlec_pulseaudio_connection_signal(pac, 0);
			break;
		case PA_CONTEXT_FAILED:
			//printf(" State callback: The connection failed or was disconnected.\n");
			paddlec_pulseaudio_connection_signal(pac, 0);
			break;
		case PA_CONTEXT_TERMINATED:
			//printf(" State callback: The connection was terminated cleanly.\n");
			break;
		default:
			//printf(" State callback: UNKNOWN CONNECTION STATE: %d\n", state);
			paddlec_pulseaudio_connection_signal(pac, 0);
	}

}


/* Open the connection to a PulseAudio server.
 * @return [self]
 * @overload open(server: nil, name: $0)
 * @param server [String, nil] the path of the server, +nil+ for default
 * @param name [String, nil] specify the application name to the server side
 */
static VALUE paddlec_pulseaudio_connection_open(int argc, VALUE *argv, VALUE self)
{
	int r;
	VALUE rbOptHash; 
	VALUE server_and_name[2] = {Qundef, Qundef};
	const ID kwkeys[2] = {rb_intern("server"), rb_intern("name")};
	paddlec_pulseaudio_connection_t *pac;
	const char *server = NULL;
	const char *name = NULL;
	VALUE threadName;
	pa_context_state_t state;
	pa_proplist *proplist;
	

	rb_scan_args(argc, argv, ":", &rbOptHash);
	if (rbOptHash != Qnil)
		rb_get_kwargs(rbOptHash, kwkeys, 0, 2, server_and_name);
	if (server_and_name[0] != Qundef) {
		if (!rb_obj_is_kind_of(server_and_name[0], rb_cString))
			rb_raise(rb_eTypeError, "keyword argument \"server\" must be a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(server_and_name[0])));
		server = StringValueCStr(server_and_name[0]);
	}
	if (server_and_name[1] != Qundef) {
		if (!rb_obj_is_kind_of(server_and_name[1], rb_cString))
			rb_raise(rb_eTypeError, "keyword argument \"name\" must be a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(server_and_name[1])));
		threadName = server_and_name[1];
	}
	else
		threadName = rb_funcall(rb_cFile, rb_intern("basename"), 1, rb_gv_get("$0"));
	name = StringValueCStr(threadName);

	pac = paddlec_pulseaudio_connection_get_struct(self);

	if (pac->thread != Qnil)
		rb_raise(rb_eRuntimeError, "connection is already openned");

	pac->mutex = rb_mutex_new();
	pac->cond = rb_class_new_instance(0, NULL, rb_cConditionVariable);
	pac->accept_cond = rb_class_new_instance(0, NULL, rb_cConditionVariable);
	pac->n_waiting = 0;
	pac->n_waiting_for_accept = 0;
	pac->mainloop = pa_mainloop_new();
	pa_mainloop_set_poll_func(pac->mainloop, my_pa_poll_func, (void*)pac);
	proplist = pa_proplist_new();
	pa_proplist_sets(proplist, PA_PROP_APPLICATION_LANGUAGE, "Ruby");
	pa_proplist_sets(proplist, PA_PROP_APPLICATION_ICON_NAME, "application-x-ruby");
	pac->context = pa_context_new_with_proplist(pa_mainloop_get_api(pac->mainloop), name, proplist);
	pa_proplist_free(proplist);
	pa_context_set_state_callback(pac->context, my_pa_context_notify_cb, pac);

	r = pa_context_connect(pac->context, server, PA_CONTEXT_NOAUTOSPAWN, NULL);
	if (r < 0) {
		r = pa_context_errno(pac->context);
		pac->thread = Qnil;
		pa_context_disconnect(pac->context);
		pa_context_unref(pac->context);
		pac->context = NULL;
		pa_mainloop_free(pac->mainloop);
		pac->mainloop = NULL;
		pac->mutex = Qnil;
		pac->cond = Qnil;
		pac->accept_cond = Qnil;
		pac->n_waiting = 0;
		pac->n_waiting_for_accept = 0;
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(r));
	}

	pac->thread = rb_thread_create(paddlec_pulseaudio_connection_thread_func, pac);
	threadName = rb_sprintf("%"PRIsVALUE"_pa_mainloop", threadName);
	rb_funcallv(pac->thread, rb_intern("name="), 1, &threadName);

	paddlec_pulseaudio_connection_lock(pac);
	while ((state = pa_context_get_state(pac->context)) != PA_CONTEXT_READY && state != PA_CONTEXT_FAILED)
		paddlec_pulseaudio_connection_wait(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (state != PA_CONTEXT_READY) {
		paddlec_pulseaudio_connection_lock(pac);
		pa_mainloop_quit(pac->mainloop, 0);
		paddlec_pulseaudio_connection_unlock(pac);
		rb_funcallv(pac->thread, rb_intern("join"), 0, NULL);
		pac->thread = Qnil;
		pa_context_disconnect(pac->context);
		pa_context_unref(pac->context);
		pac->context = NULL;
		pa_mainloop_free(pac->mainloop);
		pac->mainloop = NULL;
		if (paddlec_pulseaudio_connection_is_locked(pac))
			paddlec_pulseaudio_connection_unlock(pac);
		pac->mutex = Qnil;
		pac->cond = Qnil;
		pac->accept_cond = Qnil;
		pac->n_waiting = 0;
		pac->n_waiting_for_accept = 0;
		rb_raise(rb_eRuntimeError, paddlec_pulseaudio_connection_state_to_string(state));
	}

	return self;
}


/* Close the connection to the PulseAudio server.
 * After +#close+, the connection can be opened again.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_connection_close(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;

	pac = paddlec_pulseaudio_connection_get_struct(self);

	if (pac->thread == Qnil)
		rb_raise(rb_eRuntimeError, "connection is already closed");

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pa_mainloop_quit(pac->mainloop, 0);
	paddlec_pulseaudio_connection_unlock(pac);

	rb_funcallv(pac->thread, rb_intern("join"), 0, NULL);
	pac->thread = Qnil;

	pa_context_disconnect(pac->context);
	pa_context_unref(pac->context);
	pac->context = NULL;

	pa_mainloop_free(pac->mainloop);
	pac->mainloop = NULL;

	if (paddlec_pulseaudio_connection_is_locked(pac))
		paddlec_pulseaudio_connection_unlock(pac);
	pac->mutex = Qnil;
	pac->cond = Qnil;
	pac->accept_cond = Qnil;
	pac->n_waiting = 0;
	pac->n_waiting_for_accept = 0;

	return self;
}

 
static void my_pa_server_info_cb(pa_context *c, const pa_server_info *i, void *userdata)
{
	int s = 0;
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	if (i) {
		pac->rbval = paddlec_pulseaudio_serverinfo_newfrompa(i);
		s = 1;
	}
	pac->success = &s;
	paddlec_pulseaudio_connection_signal(pac, 1);
	pac->rbval = Qnil;
	pac->success = NULL;
}


/* Return server information, or +nil+ if the connection is closed.
 * @return [PaddleC::PulseAudio::ServerInfo] server information
 */
static VALUE paddlec_pulseaudio_connection_server_info(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	VALUE res;
	int s, error = 0;

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->rbval = Qnil;
	pac->success = NULL;
	op = pa_context_get_server_info(pac->context, my_pa_server_info_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	res = pac->rbval;
	s = *pac->success;
	if (!s)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (!s)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return res;
}

 
static void my_pa_stat_info_cb(pa_context *c, const pa_stat_info *i, void *userdata)
{
	int s = 0;
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	if (i) {
		pac->rbval = paddlec_pulseaudio_statinfo_newfrompa(i);
		s = 1;
	}
	pac->success = &s;
	paddlec_pulseaudio_connection_signal(pac, 1);
	pac->rbval = Qnil;
	pac->success = NULL;
}


/* Return daemon memory block statistics, or +nil+ if the connection is closed.
 * @return [PaddleC::PulseAudio::StatInfo] memory blocks statistics
 */
static VALUE paddlec_pulseaudio_connection_stat_info(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	VALUE res;
	int s, error = 0;

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->rbval = Qnil;
	pac->success = NULL;
	op = pa_context_stat(pac->context, my_pa_stat_info_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	res = pac->rbval;
	s = *pac->success;
	if (!s)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (!s)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return res;
}

 
static void my_pa_sink_info_cb(pa_context *c, const pa_sink_info *si, int eol, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	if (si)
		pac->rbval = paddlec_pulseaudio_sinkinfo_newfrompa(si);
	if (eol < 0 || eol > 0) {
		pac->success = &eol;
		paddlec_pulseaudio_connection_signal(pac, 1);
		pac->rbval = Qnil;
		pac->success = NULL;
	}
}


/* Get information about a sink by its name or index, or return +nil+ if the connection is closed.
 * @overload sink(id = 1)
 * @param id [String, Integer] the sink name or its index (starting at 1)
 * @return [PaddleC::PulseAudio::SinkInfo, nil]
 */
static VALUE paddlec_pulseaudio_connection_sink(int argc, VALUE *argv, VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	VALUE res, id;
	int is_int, s, error = 0;

	rb_scan_args(argc, argv, "01", &id);
	if (NIL_P(id))
		id = INT2NUM(0);

	if (rb_class_of(id) == rb_cInteger)
		is_int = 1;
	else if (rb_class_of(id) == rb_cString)
		is_int = 0;
	else
	rb_raise(rb_eTypeError, "expecting an Integer or a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(id)));

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->rbval = Qnil;
	pac->success = NULL;
	if (is_int)
		op = pa_context_get_sink_info_by_index(pac->context, NUM2UINT(id), my_pa_sink_info_cb, (void*)pac);
	else
		op = pa_context_get_sink_info_by_name(pac->context, rb_string_value_cstr(&id), my_pa_sink_info_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	res = pac->rbval;
	s = *pac->success;
	if (s < 0)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (s < 0)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return res;
}

 
static void my_pa_sinks_info_cb(pa_context *c, const pa_sink_info *si, int eol, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	if (pac->rbval == Qnil)
		pac->rbval = rb_ary_new();
	if (si)
		rb_ary_push(pac->rbval, paddlec_pulseaudio_sinkinfo_newfrompa(si));
	if (eol < 0 || eol > 0) {
		pac->success = &eol;
		paddlec_pulseaudio_connection_signal(pac, 1);
		pac->rbval = Qnil;
		pac->success = NULL;
	}
}


/* Get the complete sink list, or return +nil+ if the connection is closed.
 * @return [Array<PaddleC::PulseAudio::SinkInfo>, nil]
 */
static VALUE paddlec_pulseaudio_connection_sinks(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	VALUE res;
	int s, error = 0;

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->rbval = Qnil;
	pac->success = NULL;
	op = pa_context_get_sink_info_list(pac->context, my_pa_sinks_info_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	res = pac->rbval;
	s = *pac->success;
	if (s < 0)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (s < 0)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return res;
}

 
static void my_pa_source_info_cb(pa_context *c, const pa_source_info *si, int eol, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	if (si)
		pac->rbval = paddlec_pulseaudio_sourceinfo_newfrompa(si);
	if (eol < 0 || eol > 0) {
		pac->success = &eol;
		paddlec_pulseaudio_connection_signal(pac, 1);
		pac->rbval = Qnil;
		pac->success = NULL;
	}
}


/* Get information about a source by its name or index, or return +nil+ if the connection is closed.
 * @overload source(id = 1)
 * @param id [String, Integer] the source name or its index (starting at 1)
 * @return [PaddleC::PulseAudio::SourceInfo, nil]
 */
static VALUE paddlec_pulseaudio_connection_source(int argc, VALUE *argv, VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	VALUE res, id;
	int is_int, s, error = 0;

	rb_scan_args(argc, argv, "01", &id);
	if (NIL_P(id))
		id = INT2NUM(0);

	if (rb_class_of(id) == rb_cInteger)
		is_int = 1;
	else if (rb_class_of(id) == rb_cString)
		is_int = 0;
	else
	rb_raise(rb_eTypeError, "expecting an Integer or a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(id)));

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->rbval = Qnil;
	pac->success = NULL;
	if (is_int)
		op = pa_context_get_source_info_by_index(pac->context, NUM2UINT(id), my_pa_source_info_cb, (void*)pac);
	else
		op = pa_context_get_source_info_by_name(pac->context, rb_string_value_cstr(&id), my_pa_source_info_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	res = pac->rbval;
	s = *pac->success;
	if (s < 0)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (s < 0)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return res;
}

 
static void my_pa_sources_info_cb(pa_context *c, const pa_source_info *si, int eol, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	if (pac->rbval == Qnil)
		pac->rbval = rb_ary_new();
	if (si)
		rb_ary_push(pac->rbval, paddlec_pulseaudio_sourceinfo_newfrompa(si));
	if (eol < 0 || eol > 0) {
		pac->success = &eol;
		paddlec_pulseaudio_connection_signal(pac, 1);
		pac->rbval = Qnil;
		pac->success = NULL;
	}
}


/* Get the complete source list, or return +nil+ if the connection is closed.
 * @return [Array<PaddleC::PulseAudio::SourceInfo>, nil]
 */
static VALUE paddlec_pulseaudio_connection_sources(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	VALUE res;
	int s, error = 0;

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->rbval = Qnil;
	pac->success = NULL;
	op = pa_context_get_source_info_list(pac->context, my_pa_sources_info_cb, (void*)pac);
	pa_operation_unref(op);
	while (!pac->success)
		paddlec_pulseaudio_connection_wait(pac);
	res = pac->rbval;
	s = *pac->success;
	if (s < 0)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (s < 0)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return res;
}


static void my_pa_modules_info_cb(pa_context *c, const pa_module_info *mi, int eol, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	if (pac->rbval == Qnil)
		pac->rbval = rb_ary_new();
	if (mi)
		rb_ary_push(pac->rbval, paddlec_pulseaudio_moduleinfo_newfrompa(mi));
	if (eol < 0 || eol > 0) {
		pac->success = &eol;
		paddlec_pulseaudio_connection_signal(pac, 1);
		pac->rbval = Qnil;
		pac->success = NULL;
	}
}


/* Get the complete list of currently loaded modules, or return +nil+ if the connection is closed.
 * @return [Array<PaddleC::PulseAudio::ModuleInfo>, nil]
 */
static VALUE paddlec_pulseaudio_connection_modules(VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	VALUE res;
	int error = 0;

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->rbval = Qnil;
	pac->success = NULL;
	op = pa_context_get_module_info_list(pac->context, my_pa_modules_info_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	res = pac->rbval;
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return res;
}

 
static void my_pa_module_info_cb(pa_context *c, const pa_module_info *mi, int eol, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	if (mi)
		pac->rbval = paddlec_pulseaudio_moduleinfo_newfrompa(mi);
	if (eol < 0 || eol > 0) {
		pac->success = &eol;
		paddlec_pulseaudio_connection_signal(pac, 1);
		pac->rbval = Qnil;
		pac->success = NULL;
	}
}


/* Get some information about a module by its index or name.
 * @param module [Integer, String] the module index or its name
 * @return [PaddleC::PulseAudio::ModuleInfo, nil] the module info, or +nil+ if not found or the connection is closed.
 */
static VALUE paddlec_pulseaudio_connection_getmodulebyind(VALUE self, VALUE module)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	VALUE res, ar, mi;
	int ind, error = 0;
	long i, arlen;


	if (rb_class_of(module) == rb_cString) {
		ar = paddlec_pulseaudio_connection_modules(self);
		if (ar == Qnil)
			return Qnil;
		arlen = rb_array_len(ar);
		for (i = 0; i < arlen; i++) {
			mi = rb_ary_entry(ar, i);
			if (rb_funcallv(rb_iv_get(mi, "@name"), rb_intern("=="), 1, &module) == Qtrue)
				return mi;
		}
		return Qnil;
	}
	else if (!rb_obj_is_kind_of(module, rb_cInteger))
		rb_raise(rb_eTypeError, "expecting an integer or a string, not a %"PRIsVALUE, rb_class_name(rb_class_of(module)));
	ind = NUM2INT(module);
	if (ind < 0)
		rb_raise(rb_eRangeError, "module index must be positive");

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->rbval = Qnil;
	pac->success = NULL;
	op = pa_context_get_module_info(pac->context, (uint32_t)ind, my_pa_module_info_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	res = pac->rbval;
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return res;
}


static void my_pa_load_module_cb(pa_context *c, uint32_t idx, void *userdata)
{
	int s = 1;
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	pac->rbval = UINT2NUM(idx);
	pac->success = &s;
	paddlec_pulseaudio_connection_signal(pac, 1);
	pac->rbval = Qnil;
	pac->success = NULL;
}


/* Load a module.
 * @return [Integer, nil] the loaded module index, or +nil+ if disconnected.
 * @overload load_module(module_name, module_args = nil)
 * @param module_name [String] the name of the module to load
 * @param module_args [String, nil] module arguments
 * @overload load_module(moduleinfo)
 * @param moduleinfo [PaddleC::PulseAudio::ModuleInfo] takes name and arguments from a {PaddleC::PulseAudio::ModuleInfo}
 */
static VALUE paddlec_pulseaudio_connection_loadmodule(int argc, VALUE *argv, VALUE self)
{
	paddlec_pulseaudio_connection_t *pac;
	VALUE name_or_mi, arguments, res;
	pa_operation *op;
	int error = 0;
	const char *name = NULL;
	const char *args = NULL;

	rb_scan_args(argc, argv, "11", &name_or_mi, &arguments);

	if (rb_class_of(name_or_mi) == c_PAModuleInfo) {
		arguments = rb_iv_get(name_or_mi, "@argument");
		if (rb_class_of(arguments) == rb_cString)
			args = rb_string_value_cstr(&arguments);
		name_or_mi = rb_iv_get(name_or_mi, "@name");
		name = rb_string_value_cstr(&name_or_mi);
	}
	else if (rb_obj_is_kind_of(name_or_mi, rb_cString)) {
		name = rb_string_value_cstr(&name_or_mi);
		if (rb_obj_is_kind_of(arguments, rb_cString))
			args = rb_string_value_cstr(&arguments);
		else if (arguments != Qnil)
			rb_raise(rb_eTypeError, "module arguments must be a string, not a %"PRIsVALUE, rb_class_name(rb_class_of(arguments)));
	}
	else
		rb_raise(rb_eTypeError, "module must be a string or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PAModuleInfo), rb_class_name(rb_class_of(arguments)));

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return Qnil;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->rbval = Qnil;
	pac->success = NULL;
	op = pa_context_load_module(pac->context, name, args, my_pa_load_module_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	res = pac->rbval;
	if (!pac->success || !(*pac->success) || NUM2UINT(res) == PA_INVALID_INDEX)
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return res;
}


static void my_pa_unload_module_cb(pa_context *c, int success, void *userdata)
{
	paddlec_pulseaudio_connection_t *pac = (paddlec_pulseaudio_connection_t*)userdata;
	pac->success = &success;
	paddlec_pulseaudio_connection_signal(pac, 1);
	pac->success = NULL;
}


/* Unload a module.
 * @param module [PaddleC::PulseAudio::ModuleInfo, Integer, String] a module info or its index or its name
 * @return [self]
 */
static VALUE paddlec_pulseaudio_connection_unloadmodule(VALUE self, VALUE module)
{
	paddlec_pulseaudio_connection_t *pac;
	pa_operation *op;
	int ind = -1;
	int error = 0;
	VALUE ar, mi;
	long arlen, i;

	if (rb_class_of(module) == c_PAModuleInfo)
		ind = NUM2INT(rb_iv_get(module, "@index"));
	else if (rb_class_of(module) == rb_cString) {
		ar = paddlec_pulseaudio_connection_modules(self);
		if (ar == Qnil)
			return self;
		arlen = rb_array_len(ar);
		for (i = 0; i < arlen; i++) {
			mi = rb_ary_entry(ar, i);
			if (rb_funcallv(rb_iv_get(mi, "@name"), rb_intern("=="), 1, &module) == Qtrue) {
				ind = NUM2INT(rb_iv_get(mi, "@index"));
				break;
			}
		}
		if (ind < 0)
			return self;
	}
	else
		ind = NUM2INT(module);
	if (ind < 0)
		rb_raise(rb_eRangeError, "module index must be positive");

	pac = paddlec_pulseaudio_connection_get_struct(self);
	if (pac->thread == Qnil)
		return self;

	if (paddlec_pulseaudio_connection_in_worker_thread(pac))
		rb_raise(rb_eRuntimeError, "this method cannot be called from a PulseAudio callback");

	paddlec_pulseaudio_connection_lock(pac);
	pac->success = NULL;
	op = pa_context_unload_module(pac->context, (uint32_t)ind, my_pa_unload_module_cb, (void*)pac);
	while (!pac->success && pa_operation_get_state(op) == PA_OPERATION_RUNNING)
		paddlec_pulseaudio_connection_wait(pac);
	pa_operation_unref(op);
	if (!pac->success || !(*pac->success))
		error = pa_context_errno(pac->context);
	paddlec_pulseaudio_connection_accept(pac);
	paddlec_pulseaudio_connection_unlock(pac);

	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* If no block is provided, returns an opened {Connection}
 *
 * If a block is given, creates a new {Connection}, opens it, yield it and close it before returning the result of the block.
 *
 * @overload open(server: nil, name: $0)
 *  @param server [String, nil] the path of the server, +nil+ for default
 *  @param name [String, nil] specify the application name to the server side
 *  @return [PaddleC::PulseAudio::Connection] the new connection, already opened
 * @overload open(...)
 *  @yield [connection]  opens a newly created {Connection}, yield it and close it, then return the result of the block
 *  @return [Object] the result of the block
 */
static VALUE paddlec_pulseaudio_connection_class_open(int argc, VALUE *argv, VALUE self)
{
	VALUE connection, res;

	connection = rb_class_new_instance(0, NULL, c_PAConnection);

	paddlec_pulseaudio_connection_open(argc, argv, connection);

	res = connection;

	if (rb_block_given_p()) {
		res = rb_yield(connection);
		paddlec_pulseaudio_connection_close(connection);
	}

	return res;
}


/* Return a new unconnected {PaddleC::PulseAudio::Stream::Playback} stream bound to this connection.
 * @return [PaddleC::PulseAudio::Stream::Playback]
 */
static VALUE paddlec_pulseaudio_connection_newplayback(VALUE self)
{
	VALUE res;
	paddlec_pulseaudio_stream_t *st;

	res = rb_class_new_instance(0, NULL, c_PAPlayback);
	st = paddlec_pulseaudio_stream_get_struct2(res);
	st->connection = self;

	return res;
}


/* Create a new {PaddleC::PulseAudio::Stream::Playback} stream and connect it.
 * @see PaddleC::PulseAudio::Stream::Playback#connect
 *
 * If no block is provided, returns the connected new {PaddleC::PulseAudio::Stream::Playback}
 *
 * If a block is given, creates a new {PaddleC::PulseAudio::Stream::Playback}, connects it, yield it and disconnect it before returning the result of the block.
 *
 * @overload new_playback_connect(name: self.class.name, device: nil, sample_spec: 'float32 1ch 44100Hz', channel_map: nil, buffer_attributes: nil, adjust_latency: false)
 *  @param name [String, nil] a descriptive name for this stream (application name, song title, ...)
 *  @param device [String, nil] sink name, or +nil+ for default
 *  @param sample_spec [PaddleC::PulseAudio::SampleSpec, String] the sample type to use, or a string describing a sample specification (see {SampleSpec#initialize})
 *  @param channel_map [PaddleC::PulseAudio::ChannelMap, Integer, String, Array<Symbol>, nil] the channel map to use, or an argument of {ChannelMap#initialize}, or +nil+ for default
 *  @param buffer_attributes [PaddleC::PulseAudio::BufferAttributes, nil] buffering attributes, or +nil+ for default
 *  @param adjust_latency [Boolean] Try to adjust the latency of the sink/source based on the requested buffer attributes metrics and adjust buffer metrics accordingly.
 *  @return [PaddleC::PulseAudio::Stream::Playback] a connected playback stream
 * @overload new_playback_connect(...)
 *  @yield [playback] the newly created and connected {PaddleC::PulseAudio::Stream::Playback} stream, it will be disconnected before this method returns
 *  @return [Object] the result of the block
 */
static VALUE paddlec_pulseaudio_connection_newplaybackconnect(int argc, VALUE *argv, VALUE self)
{
	VALUE playback, res;

	playback = paddlec_pulseaudio_connection_newplayback(self); 
	paddlec_pulseaudio_playback_connect(argc, argv, playback);
	res = playback;

	if (rb_block_given_p()) {
		res = rb_yield(playback);
		paddlec_pulseaudio_stream_disconnect(playback);
	}

	return res;
}


/* Document-class: PaddleC::PulseAudio::Connection
 *
 * Connection contexts for asynchronous communication with a server.
 *
 * A {Connection} object wraps a connection to a PulseAudio server using its native protocol.
 *
 * This Ruby implementation mimics the PulseAudio client API's {https://freedesktop.org/software/pulseaudio/doxygen/threaded_mainloop.html threaded_mainloop}:
 * when the {Connection} is opened, a Ruby worker thread is created to handle asynchronous communication with the server.
 * The Ruby VM's GVL is released just before the +poll()+ function and reaquiered just after so that the communication thread does not block other ruby threads. 
 */

static void Init_paddlec_pulseaudio_connection()
{
	rb_cConditionVariable = rb_const_get(rb_cObject, rb_intern("ConditionVariable"));

	c_PAConnection = rb_define_class_under(m_PulseAudio, "Connection", rb_cObject);
	rb_define_module_function(c_PAConnection, "open",           paddlec_pulseaudio_connection_class_open,             -1);
	rb_define_alloc_func(c_PAConnection, paddlec_pulseaudio_connection_alloc);
	rb_define_method(c_PAConnection, "initialize",              paddlec_pulseaudio_connection_initialize,              0);
	rb_define_method(c_PAConnection, "open",                    paddlec_pulseaudio_connection_open,                   -1);
	rb_define_method(c_PAConnection, "close",                   paddlec_pulseaudio_connection_close,                   0);
	rb_define_method(c_PAConnection, "state",                   paddlec_pulseaudio_connection_state,                   0);
	rb_define_method(c_PAConnection, "closed?",                 paddlec_pulseaudio_connection_is_closed,               0);
	rb_define_method(c_PAConnection, "pending?",                paddlec_pulseaudio_connection_is_pending,              0);
	rb_define_method(c_PAConnection, "local?",                  paddlec_pulseaudio_connection_is_local,                0);
	rb_define_method(c_PAConnection, "path",                    paddlec_pulseaudio_connection_server_name,             0);
	rb_define_method(c_PAConnection, "protocol_version",        paddlec_pulseaudio_connection_protocol_version,        0);
	rb_define_method(c_PAConnection, "server_protocol_version", paddlec_pulseaudio_connection_server_protocol_version, 0);
	rb_define_method(c_PAConnection, "index",                   paddlec_pulseaudio_connection_index,                   0);
	rb_define_method(c_PAConnection, "name=",                   paddlec_pulseaudio_connection_set_name,                1);
	rb_define_method(c_PAConnection, "drain",                   paddlec_pulseaudio_connection_drain,                   0);
	rb_define_method(c_PAConnection, "default_sink=",           paddlec_pulseaudio_connection_set_default_sink,        1);
	rb_define_method(c_PAConnection, "default_source=",         paddlec_pulseaudio_connection_set_default_source,      1);
	rb_define_method(c_PAConnection, "exit_daemon",             paddlec_pulseaudio_connection_exit_daemon,             0);
	rb_define_method(c_PAConnection, "tile_size",               paddlec_pulseaudio_connection_tile_size,               1);
	rb_define_method(c_PAConnection, "tile_length",             paddlec_pulseaudio_connection_tile_length,             1);
	rb_define_method(c_PAConnection, "server_info",             paddlec_pulseaudio_connection_server_info,             0);
	rb_define_method(c_PAConnection, "mem_stats",               paddlec_pulseaudio_connection_stat_info,               0);
	rb_define_method(c_PAConnection, "sink",                    paddlec_pulseaudio_connection_sink,                   -1);
	rb_define_method(c_PAConnection, "sinks",                   paddlec_pulseaudio_connection_sinks,                   0);
	rb_define_method(c_PAConnection, "source",                  paddlec_pulseaudio_connection_source,                 -1);
	rb_define_method(c_PAConnection, "sources",                 paddlec_pulseaudio_connection_sources,                 0);
	rb_define_method(c_PAConnection, "module",                  paddlec_pulseaudio_connection_getmodulebyind,          1);
	rb_define_method(c_PAConnection, "modules",                 paddlec_pulseaudio_connection_modules,                 0);
	rb_define_method(c_PAConnection, "load_module",             paddlec_pulseaudio_connection_loadmodule,             -1);
	rb_define_method(c_PAConnection, "unload_module",           paddlec_pulseaudio_connection_unloadmodule,            1);
	rb_define_method(c_PAConnection, "new_playback",            paddlec_pulseaudio_connection_newplayback,             0);
	rb_define_method(c_PAConnection, "new_playback_connect",    paddlec_pulseaudio_connection_newplaybackconnect,     -1);
}


/* PaddleC::PulseAudio::Simple */


/* Document-module: PaddleC::PulseAudio::Simple
 *
 * This is a wrapper to the PulseAudio client library's {https://freedesktop.org/software/pulseaudio/doxygen/simple.html simple API}.
 * The synchronous simple API is designed for applications with very basic sound playback or capture needs.
 * It can only support a single stream per connection and has no support for handling of complex features like events, channel mappings and volume control.
 * It is, however, very simple to use and quite sufficient for many programs.
 *
 * This module provides common functions for {PaddleC::PulseAudio::Simple::Sink} and {PaddleC::PulseAudio::Simple::Source}.
 *
 * @!attribute [rw] server
 *  The server name (connection path), or +nil+ for default. 
 *
 *  The server name cannot be set when the connection is opened, otherwithe an exception is raised.
 *  @return [String, nil]
 *  
 * @!attribute [rw] name
 *  A descriptive name for this client (application name, ...).
 *
 *  The connection name cannot be set when the connection is opened, otherwithe an exception is raised.
 *  @return [String, nil]
 *
 * @!attribute [rw] device
 *  Sink (resp. source) name, or +nil+ for default.
 *
 *  The device name cannot be set when the connection is opened, otherwithe an exception is raised.
 *  @return [String, nil]
 *
 * @!attribute [rw] stream_name
 *  A descriptive name for this stream (application name, song title, ...).
 *
 *  The stream name cannot be set when the connection is opened, otherwithe an exception is raised.
 *  @return [String, nil]
 *
 * @!attribute [rw] sample_spec
 *  The sample type to use.
 *
 *  The sample specification cannot be set when the connection is opened, otherwithe an exception is raised.
 *  @return [PaddleC::PulseAudio::SampleSpec, nil]
 *
 * @!attribute [rw] channel_map
 *  The channel map to use, or +nil+ for default.
 *
 *  The channel map cannot be set when the connection is opened, otherwithe an exception is raised.
 *  @return [PaddleC::PulseAudio::ChannelMap, nil]
 *
 * @!attribute [rw] buffer_attributes
 *  Buffering attributes, or +nil+ for default.
 *
 *  The buffer attributes cannot be set when the connection is opened, otherwithe an exception is raised.
 *  @return [PaddleC::PulseAudio::BufferAttributes, nil]
 *
 */


static void paddlec_pulseaudio_simple_free(void *p)
{
	pa_simple **pas = (pa_simple**)p;
	if (*pas)
		pa_simple_free(*pas);
	ruby_xfree(pas);
}


static size_t paddlec_pulseaudio_simple_size(const void *p)
{
	(void)p;
	return sizeof(pa_simple*);
}


static const rb_data_type_t paddlec_pulseaudio_simple_type = {
	.wrap_struct_name = "paddlec_pulseaudio_simple_struct",
	.function = {
		.dmark = NULL,
		.dfree = paddlec_pulseaudio_simple_free,
		.dsize = paddlec_pulseaudio_simple_size,
	},
	.data  = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE paddlec_pulseaudio_simple_alloc(VALUE klass)
{
	VALUE obj;
	pa_simple **pas;

	pas = ruby_xmalloc(sizeof(pa_simple*));
	*pas = NULL;

	obj = TypedData_Wrap_Struct(klass, &paddlec_pulseaudio_simple_type, pas);

	return obj;
}


static pa_simple** paddlec_pulseaudio_simple_get_pointer(VALUE obj)
{
	pa_simple **pas;
	TypedData_Get_Struct(obj, pa_simple*, &paddlec_pulseaudio_simple_type, pas);
	return pas;
}


static pa_simple* paddlec_pulseaudio_simple_get_struct(VALUE obj)
{
	pa_simple **pas;
	TypedData_Get_Struct(obj, pa_simple*, &paddlec_pulseaudio_simple_type, pas);
	return *pas;
}


static VALUE paddlec_pulseaudio_simple_set_server(VALUE self, VALUE server)
{
	const pa_simple *pas;

	if (!NIL_P(server) && rb_class_of(server) != rb_cString)
		rb_raise(rb_eTypeError, "expecting nil or a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(server)));

	pas = paddlec_pulseaudio_simple_get_struct(self);
	if (pas)
		rb_raise(rb_eRuntimeError, "cannot set the server name while the connection is opened");

	if (!NIL_P(server))
		server = rb_str_dup(server);
	//server = rb_obj_freeze(server);

	rb_iv_set(self, "@server", server);

	return server;
}


static VALUE paddlec_pulseaudio_simple_get_server(VALUE self)
{
	return rb_iv_get(self, "@server");
}


static VALUE paddlec_pulseaudio_simple_set_name(VALUE self, VALUE name)
{
	const pa_simple *pas;

	if (!NIL_P(name) && rb_class_of(name) != rb_cString)
		rb_raise(rb_eTypeError, "expecting nil or a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(name)));

	pas = paddlec_pulseaudio_simple_get_struct(self);
	if (pas)
		rb_raise(rb_eRuntimeError, "cannot set the connection name while the connection is opened");

	if (!NIL_P(name))
		name = rb_str_dup(name);
	//name = rb_obj_freeze(name);

	rb_iv_set(self, "@name", name);

	return name;
}


static VALUE paddlec_pulseaudio_simple_get_name(VALUE self)
{
	return rb_iv_get(self, "@name");
}


static VALUE paddlec_pulseaudio_simple_set_device(VALUE self, VALUE device)
{
	const pa_simple *pas;

	if (!NIL_P(device) && rb_class_of(device) != rb_cString)
		rb_raise(rb_eTypeError, "expecting nil or a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(device)));

	pas = paddlec_pulseaudio_simple_get_struct(self);
	if (pas)
		rb_raise(rb_eRuntimeError, "cannot set the device name while the connection is opened");

	if (!NIL_P(device))
		device = rb_str_dup(device);
	//device = rb_obj_freeze(device);

	rb_iv_set(self, "@device", device);

	return device;
}


static VALUE paddlec_pulseaudio_simple_get_device(VALUE self)
{
	return rb_iv_get(self, "@device");
}


static VALUE paddlec_pulseaudio_simple_set_streamname(VALUE self, VALUE stream_name)
{
	const pa_simple *pas;

	if (!NIL_P(stream_name) && rb_class_of(stream_name) != rb_cString)
		rb_raise(rb_eTypeError, "expecting nil or a String, not a %"PRIsVALUE, rb_class_name(rb_class_of(stream_name)));

	pas = paddlec_pulseaudio_simple_get_struct(self);
	if (pas)
		rb_raise(rb_eRuntimeError, "cannot set the stream name while the connection is opened");

	if (!NIL_P(stream_name))
		stream_name = rb_str_dup(stream_name);
	//stream_name = rb_obj_freeze(stream_name);

	rb_iv_set(self, "@stream_name", stream_name);

	return stream_name;
}


static VALUE paddlec_pulseaudio_simple_get_streamname(VALUE self)
{
	return rb_iv_get(self, "@samplespec");
}


static VALUE paddlec_pulseaudio_simple_set_samplespec(VALUE self, VALUE samplespec)
{
	const pa_simple *pas;

//	if (!NIL_P(samplespec) && rb_class_of(samplespec) != c_PASampleSpec)
//		rb_raise(rb_eTypeError, "expecting nil or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PASampleSpec), rb_class_name(rb_class_of(samplespec)));

	pas = paddlec_pulseaudio_simple_get_struct(self);
	if (pas)
		rb_raise(rb_eRuntimeError, "cannot set the sample specification while the connection is opened");

	if (rb_class_of(samplespec) == c_PASampleSpec)
		samplespec = paddlec_pulseaudio_sample_spec_dup(samplespec);
	else if (!NIL_P(samplespec))
		samplespec = rb_class_new_instance(1, &samplespec, c_PASampleSpec);
	//samplespec = rb_obj_freeze(samplespec);

	rb_iv_set(self, "@sample_spec", samplespec);

	return samplespec;
}


static VALUE paddlec_pulseaudio_simple_get_samplespec(VALUE self)
{
	return rb_iv_get(self, "@sample_spec");
}


static VALUE paddlec_pulseaudio_simple_set_channelmap(VALUE self, VALUE channelmap)
{
	const pa_simple *pas;

//	if (!NIL_P(channelmap) && rb_class_of(channelmap) != c_PAChannelMap)
//		rb_raise(rb_eTypeError, "expecting nil or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PAChannelMap), rb_class_name(rb_class_of(channelmap)));

	pas = paddlec_pulseaudio_simple_get_struct(self);
	if (pas)
		rb_raise(rb_eRuntimeError, "cannot set the channel map while the connection is opened");

	if (rb_class_of(channelmap) == c_PAChannelMap)
		channelmap = paddlec_pulseaudio_channelmap_dup(channelmap);
	else if (!NIL_P(channelmap))
		channelmap = rb_class_new_instance(1, &channelmap, c_PAChannelMap);
	//channelmap = rb_obj_freeze(channelmap);

	rb_iv_set(self, "@channel_map", channelmap);

	return channelmap;
}


static VALUE paddlec_pulseaudio_simple_get_channelmap(VALUE self)
{
	return rb_iv_get(self, "@channel_map");
}


static VALUE paddlec_pulseaudio_simple_set_bufferattributes(VALUE self, VALUE bufferattributes)
{
	const pa_simple *pas;

	if (!NIL_P(bufferattributes) && rb_class_of(bufferattributes) != c_PABufferAttributes)
		rb_raise(rb_eTypeError, "expecting nil or a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_PABufferAttributes), rb_class_name(rb_class_of(bufferattributes)));

	pas = paddlec_pulseaudio_simple_get_struct(self);
	if (pas)
		rb_raise(rb_eRuntimeError, "cannot set the buffer attributes while the connection is opened");

	if (!NIL_P(bufferattributes))
		bufferattributes = paddlec_pulseaudio_bufferattr_dup(bufferattributes);
	//bufferattributes = rb_obj_freeze(bufferattributes);

	rb_iv_set(self, "@buffer_attributes", bufferattributes);

	return bufferattributes;
}


static VALUE paddlec_pulseaudio_simple_get_bufferattributes(VALUE self)
{
	return rb_iv_get(self, "@buffer_attributes");
}


/* Close the connection. Returns +false+ if the connection was already closed, +true+ otherwise.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_simple_close(VALUE self)
{
	pa_simple **pas;

	pas = paddlec_pulseaudio_simple_get_pointer(self);

	if (*pas == NULL)
		return Qfalse;

	pa_simple_free(*pas);
	*pas = NULL;

	return Qtrue;
}


/* Wait until all data already written is played by the daemon.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_simplesink_drain(VALUE self)
{
	pa_simple *pas;
	int error;

	pas = paddlec_pulseaudio_simple_get_struct(self);

	if (pas) {
		if (pa_simple_drain(pas, &error))
			rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
	}

	return self;
}


/* Flush the playback or record buffer, discarding any audio in the buffer.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_simple_flush(VALUE self)
{
	pa_simple *pas;
	int error;

	pas = paddlec_pulseaudio_simple_get_struct(self);

	if (pas) {
		if (pa_simple_flush(pas, &error))
			rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));
	}

	return self;
}


/* Return the playback or record latency in µs, or +nil+ if the connection is closed.
 * @return [Integer, nil]
 */
static VALUE paddlec_pulseaudio_simple_latency(VALUE self)
{
	pa_simple *pas;
	int error = 0;
	pa_usec_t latency;

	pas = paddlec_pulseaudio_simple_get_struct(self);

	if (!pas)
		return Qnil;

	latency = pa_simple_get_latency(pas, &error);
	if (error)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return ULL2NUM(latency);
}


/* Whether the connection is closed or not.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_simple_closed(VALUE self)
{
	pa_simple *pas;
	pas = paddlec_pulseaudio_simple_get_struct(self);
	return (pas ? Qfalse : Qtrue);
}


/* Whether the connection is opened or not.
 * @return [Boolean]
 */
static VALUE paddlec_pulseaudio_simple_opened(VALUE self)
{
	pa_simple *pas;
	pas = paddlec_pulseaudio_simple_get_struct(self);
	return (pas ? Qtrue : Qfalse);
}


/* Document-class: PaddleC::PulseAudio::Simple::Sink
 *
 * An audio sink with a simple synchronous API.
 *
 * @example With Sink::open(...) {|s| ... }
 *  audio = PaddleC::FloatBuffer.new(1024)
 *  PaddleC::PulseAudio::Simple::Sink.open(sample_spec: 'float32le 1ch 32000Hz') do |sink|
 * 	    loop do
 * 	        ...
 * 	        sink << audio
 * 	        break if ...
 * 	    end
 *  end
 *
 * @example With Sink.new, open and close
 *  audio = PaddleC::FloatBuffer.new(1024)
 *  ss = PaddleC::PulseAudio::SampleSpec.new(format: :float32le, channels: 1, rate: 32e3)
 *  sink = PaddleC::PulseAudio::Simple::Sink.new(name: "test with PaddleC")
 *  sink.sample_spec = ss
 *  sink.stream_name = 'my audio stream'
 *  sink.open
 *  ...
 *  sink << audio
 *  ...
 *  sink << audio
 *  ...
 *  sink << audio
 *  sink.drain
 *  sink.close
 *
 */


/* Open the connection to the server.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_simplesink_open(VALUE self)
{
	const char *server = NULL;
	const char *name   = NULL;
	const char *device = NULL;
	const char *stream_name = NULL;
	const pa_sample_spec *sample_spec = NULL;
	const pa_channel_map *channel_map = NULL;
	const pa_buffer_attr *buffer_attributes = NULL;
	VALUE rbserver, rbname, rbdevice, rbstream, rbss, rbcm, rbba;
	pa_sample_spec ss;
	pa_simple **pas;
	int error = 0;

	pas = paddlec_pulseaudio_simple_get_pointer(self);

	if (*pas)
		rb_raise(rb_eRuntimeError, "Connection is already opened");

	rbserver = rb_iv_get(self, "@server");
	rbname   = rb_iv_get(self, "@name");
	rbdevice = rb_iv_get(self, "@device");
	rbstream = rb_iv_get(self, "@stream_name");
	rbss     = rb_iv_get(self, "@sample_spec");
	rbcm     = rb_iv_get(self, "@channel_map");
	rbba     = rb_iv_get(self, "@buffer_attributes");

	if (rbserver != Qnil)
		server = StringValueCStr(rbserver);
	if (rbname != Qnil)
		name = StringValueCStr(rbname);
	if (rbdevice != Qnil)
		device = StringValueCStr(rbdevice);
	if (rbstream != Qnil)
		stream_name = StringValueCStr(rbstream);
	if (rbss != Qnil) {
		ss = paddlec_pulseaudio_sample_spec_get(rbss);
		sample_spec = &ss;
	}
	if (rbcm != Qnil)
		channel_map = paddlec_pulseaudio_channelmap_get_struct(rbcm);
	if (rbba != Qnil)
		buffer_attributes = paddlec_pulseaudio_bufferattr_get_struct(rbba);

	if (channel_map && sample_spec && !pa_channel_map_compatible(channel_map, sample_spec))
		rb_raise(rb_eRuntimeError, "Channel map [%"PRIsVALUE"] is not compatible with sample specification [%"PRIsVALUE"]", rbcm, rbss);

	*pas = pa_simple_new(server, name, PA_STREAM_PLAYBACK, device, stream_name, sample_spec, channel_map, buffer_attributes, &error);
	if (*pas == NULL)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* Returns a new {PaddleC::PulseAudio::Simple::Sink}.
 * @overload initialize(server: nil, name: $0, device: nil, stream_name: Sink.class.name, sample_spec: 'float32le 1ch 44100Hz', channel_map: nil, buffer_attributes: nil)
 * @param server [String, nil] the server name (connection path), or +nil+ for default
 * @param name [String, nil] a descriptive name for this client
 * @param device [String, nil] sink name, or +nil+ for default
 * @param stream_name [String, nil] a descriptive name for this stream (application name, song title, ...)
 * @param sample_spec [PaddleC::PulseAudio::SampleSpec, String, nil] the sample type to use, or a string describing a sample specification (see {SampleSpec#initialize})
 * @param channel_map [PaddleC::PulseAudio::ChannelMap, Integer, String, Array<Symbol>, nil] the channel map to use, or an argument of {ChannelMap#initialize}, or +nil+ for default
 * @param buffer_attributes [PaddleC::PulseAudio::BufferAttributes, nil] buffering attributes, or +nil+ for default
 * @return [PaddleC::PulseAudio::Simple::Sink] the new sink, not opened yet
 */
static VALUE paddlec_pulseaudio_simplesink_initialize(int argc, VALUE *argv, VALUE self)
{
	VALUE opts;
	VALUE se_n_d_st_ss_cm_a[7] = {Qundef, Qundef, Qundef, Qundef, Qundef, Qundef, Qundef};
	const ID kwkeys[7] = {rb_intern("server"), rb_intern("name"), rb_intern("device"), rb_intern("stream_name"), rb_intern("sample_spec"), rb_intern("channel_map"), rb_intern("buffer_attributes")};

	rb_scan_args(argc, argv, ":", &opts);

	if (!NIL_P(opts))
		rb_get_kwargs(opts, kwkeys, 0, 7, se_n_d_st_ss_cm_a);

	if (se_n_d_st_ss_cm_a[0] != Qundef)
		paddlec_pulseaudio_simple_set_server(self, se_n_d_st_ss_cm_a[0]);
	else
		rb_iv_set(self, "@server", Qnil);

	if (se_n_d_st_ss_cm_a[1] != Qundef)
		paddlec_pulseaudio_simple_set_name(self, se_n_d_st_ss_cm_a[1]);
	else
		rb_iv_set(self, "@name", rb_funcall(rb_cFile, rb_intern("basename"), 1, rb_gv_get("$0")));

	if (se_n_d_st_ss_cm_a[2] != Qundef)
		paddlec_pulseaudio_simple_set_device(self, se_n_d_st_ss_cm_a[2]);
	else
		rb_iv_set(self, "@device", Qnil);

	if (se_n_d_st_ss_cm_a[3] != Qundef)
		paddlec_pulseaudio_simple_set_streamname(self, se_n_d_st_ss_cm_a[3]);
	else
		rb_iv_set(self, "@stream_name", rb_class_name(rb_class_of(self)));

	if (se_n_d_st_ss_cm_a[4] != Qundef)
		paddlec_pulseaudio_simple_set_samplespec(self, se_n_d_st_ss_cm_a[4]);
	else
		rb_iv_set(self, "@sample_spec", rb_class_new_instance(0, NULL, c_PASampleSpec));

	if (se_n_d_st_ss_cm_a[5] != Qundef)
		paddlec_pulseaudio_simple_set_channelmap(self, se_n_d_st_ss_cm_a[5]);
	else
		rb_iv_set(self, "@channel_map", Qnil);

	if (se_n_d_st_ss_cm_a[6] != Qundef)
		paddlec_pulseaudio_simple_set_bufferattributes(self, se_n_d_st_ss_cm_a[6]);
	else
		rb_iv_set(self, "@buffer_attributes", Qnil);

	return self;
}


/* If no block is provided, returns an opened {Sink}
 *
 * If a block is given, creates a new {Sink}, opens it, yield it, drain it and close it before returning the result of the block.
 *
 * @overload open(server: nil, name: $0, device: nil, stream_name: Sink.class.name, sample_spec: 'float32le 1ch 44100Hz', channel_map: nil, buffer_attributes: nil)
 *  @param server [String, nil] the server name (connection path), or +nil+ for default
 *  @param name [String, nil] a descriptive name for this client
 *  @param device [String, nil] sink name, or +nil+ for default
 *  @param stream_name [String, nil] a descriptive name for this stream (application name, song title, ...)
 *  @param sample_spec [PaddleC::PulseAudio::SampleSpec, String, nil] the sample type to use, or a string describing a sample specification (see {SampleSpec#initialize})
 *  @param channel_map [PaddleC::PulseAudio::ChannelMap, Integer, String, Array<Symbol>, nil] the channel map to use, or an argument of {ChannelMap#initialize}, or +nil+ for default
 *  @param buffer_attributes [PaddleC::PulseAudio::BufferAttributes, nil] buffering attributes, or +nil+ for default
 *  @return [PaddleC::PulseAudio::Simple::Sink] the new sink, already opened
 * @overload open(...)
 *  @yield [sink]  opens a newly created {Sink}, yield it, drain it and close it, then return the result of the block
 *  @return [Object] the result of the block
 */
static VALUE paddlec_pulseaudio_simplesinkclass_open(int argc, VALUE *argv, VALUE self)
{
	VALUE sink, res;

	sink = rb_class_new_instance(argc, argv, c_PASimpleSink);
	res = sink;

	paddlec_pulseaudio_simplesink_open(sink);

	if (rb_block_given_p()) {
		res = rb_yield(sink);
		paddlec_pulseaudio_simplesink_drain(sink);
		paddlec_pulseaudio_simple_close(sink);
	}

	return res;
}


/* Write some data to the server.
 * @param data [String, PaddleC::FloatBuffer, PaddleC::ComplexBuffer] the data to be written
 * @return [self, Qnil] returns +self+, or +nil+ if the connection is closed
 */
static VALUE paddlec_pulseaudio_simplesink_write(VALUE self, VALUE data)
{
	pa_simple *pas;
	int error = 0;
	const void *ptr;
	size_t nbytes;
	const pdlc_buffer_t *fbuf;
	const pdlc_complex_buffer_t *cbuf;

	pas = paddlec_pulseaudio_simple_get_struct(self);

	if (!pas)
		return Qnil;

	if (rb_obj_is_kind_of(data, rb_cString)) {
		ptr = StringValuePtr(data);
		nbytes = (size_t)RSTRING_LEN(data);
	}
	else if (rb_obj_is_kind_of(data, c_FloatBuffer)) {
		fbuf = paddlec_float_buffer_get_struct(data);
		ptr = fbuf->data;
		nbytes = fbuf->length * sizeof(float);
	}
	else if (rb_obj_is_kind_of(data, c_ComplexBuffer)) {
		cbuf = paddlec_complex_buffer_get_struct(data);
		ptr = cbuf->data;
		nbytes = cbuf->length * sizeof(pdlc_complex_t);
	}
	else
		rb_raise(rb_eTypeError, "expecting a String, a %"PRIsVALUE" or a %"PRIsVALUE", not a %"PRIsVALUE, 
				rb_class_name(c_FloatBuffer), rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(data)));

	if (pa_simple_write(pas, ptr, nbytes, &error))
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* Document-class: PaddleC::PulseAudio::Simple::Source
 *
 * An audio source with a simple synchronous API.
 *
 * @example With Source::open(...) {|s| ... }
 *  audio = PaddleC::FloatBuffer.new(1024)
 *  PaddleC::PulseAudio::Simple::Source.open(sample_spec: 'float32le 1ch 32000Hz') do |source|
 * 	    loop do
 * 	        source >> audio
 * 	        ...
 * 	        break if ...
 * 	    end
 *  end
 *
 * @example With Source.new, open and close
 *  audio = PaddleC::FloatBuffer.new(1024)
 *  ss = PaddleC::PulseAudio::SampleSpec.new(format: :float32le, channels: 1, rate: 32e3)
 *  source = PaddleC::PulseAudio::Simple::Source.new(name: "test with PaddleC")
 *  source.sample_spec = ss
 *  source.stream_name = 'my audio stream'
 *  source.open
 *  source >> audio
 *  ...
 *  source >> audio
 *  ...
 *  source >> audio
 *  ...
 *  source.close
 *
 */


/* Returns a new {PaddleC::PulseAudio::Simple::Source}.
 * @overload initialize(server: nil, name: $0, device: nil, stream_name: Source.class.name, sample_spec: 'float32le 1ch 44100Hz', channel_map: nil, buffer_attributes: nil)
 * @param server [String, nil] the server name (connection path), or +nil+ for default
 * @param name [String, nil] a descriptive name for this client
 * @param device [String, nil] source name, or +nil+ for default
 * @param stream_name [String, nil] a descriptive name for this stream (application name, song title, ...)
 * @param sample_spec [PaddleC::PulseAudio::SampleSpec, String, nil] the sample type to use, or a string describing a sample specification (see {SampleSpec#initialize})
 * @param channel_map [PaddleC::PulseAudio::ChannelMap, Integer, String, Array<Symbol>, nil] the channel map to use, or an argument of {ChannelMap#initialize}, or +nil+ for default
 * @param buffer_attributes [PaddleC::PulseAudio::BufferAttributes, nil] buffering attributes, or +nil+ for default
 * @return [PaddleC::PulseAudio::Simple::Source] the new source, not opened yet
 */
static VALUE paddlec_pulseaudio_simplesource_initialize(int argc, VALUE *argv, VALUE self)
{
	VALUE opts;
	VALUE se_n_d_st_ss_cm_a[7] = {Qundef, Qundef, Qundef, Qundef, Qundef, Qundef, Qundef};
	const ID kwkeys[7] = {rb_intern("server"), rb_intern("name"), rb_intern("device"), rb_intern("stream_name"), rb_intern("sample_spec"), rb_intern("channel_map"), rb_intern("buffer_attributes")};

	rb_scan_args(argc, argv, ":", &opts);

	if (!NIL_P(opts))
		rb_get_kwargs(opts, kwkeys, 0, 7, se_n_d_st_ss_cm_a);

	if (se_n_d_st_ss_cm_a[0] != Qundef)
		paddlec_pulseaudio_simple_set_server(self, se_n_d_st_ss_cm_a[0]);
	else
		rb_iv_set(self, "@server", Qnil);

	if (se_n_d_st_ss_cm_a[1] != Qundef)
		paddlec_pulseaudio_simple_set_name(self, se_n_d_st_ss_cm_a[1]);
	else
		rb_iv_set(self, "@name", rb_funcall(rb_cFile, rb_intern("basename"), 1, rb_gv_get("$0")));

	if (se_n_d_st_ss_cm_a[2] != Qundef)
		paddlec_pulseaudio_simple_set_device(self, se_n_d_st_ss_cm_a[2]);
	else
		rb_iv_set(self, "@device", Qnil);

	if (se_n_d_st_ss_cm_a[3] != Qundef)
		paddlec_pulseaudio_simple_set_streamname(self, se_n_d_st_ss_cm_a[3]);
	else
		rb_iv_set(self, "@stream_name", rb_class_name(rb_class_of(self)));

	if (se_n_d_st_ss_cm_a[4] != Qundef)
		paddlec_pulseaudio_simple_set_samplespec(self, se_n_d_st_ss_cm_a[4]);
	else
		rb_iv_set(self, "@sample_spec", rb_class_new_instance(0, NULL, c_PASampleSpec));

	if (se_n_d_st_ss_cm_a[5] != Qundef)
		paddlec_pulseaudio_simple_set_channelmap(self, se_n_d_st_ss_cm_a[5]);
	else
		rb_iv_set(self, "@channel_map", Qnil);

	if (se_n_d_st_ss_cm_a[6] != Qundef)
		paddlec_pulseaudio_simple_set_bufferattributes(self, se_n_d_st_ss_cm_a[6]);
	else
		rb_iv_set(self, "@buffer_attributes", Qnil);

	return self;
}


/* Open the connection to the server.
 * @return [self]
 */
static VALUE paddlec_pulseaudio_simplesource_open(VALUE self)
{
	const char *server = NULL;
	const char *name   = NULL;
	const char *device = NULL;
	const char *stream_name = NULL;
	const pa_sample_spec *sample_spec = NULL;
	const pa_channel_map *channel_map = NULL;
	const pa_buffer_attr *buffer_attributes = NULL;
	VALUE rbserver, rbname, rbdevice, rbstream, rbss, rbcm, rbba;
	pa_sample_spec ss;
	pa_simple **pas;
	int error = 0;

	pas = paddlec_pulseaudio_simple_get_pointer(self);

	if (*pas)
		rb_raise(rb_eRuntimeError, "Connection is already opened");

	rbserver = rb_iv_get(self, "@server");
	rbname   = rb_iv_get(self, "@name");
	rbdevice = rb_iv_get(self, "@device");
	rbstream = rb_iv_get(self, "@stream_name");
	rbss     = rb_iv_get(self, "@sample_spec");
	rbcm     = rb_iv_get(self, "@channel_map");
	rbba     = rb_iv_get(self, "@buffer_attributes");

	if (rbserver != Qnil)
		server = StringValueCStr(rbserver);
	if (rbname != Qnil)
		name = StringValueCStr(rbname);
	if (rbdevice != Qnil)
		device = StringValueCStr(rbdevice);
	if (rbstream != Qnil)
		stream_name = StringValueCStr(rbstream);
	if (rbss != Qnil) {
		ss = paddlec_pulseaudio_sample_spec_get(rbss);
		sample_spec = &ss;
	}
	if (rbcm != Qnil)
		channel_map = paddlec_pulseaudio_channelmap_get_struct(rbcm);
	if (rbba != Qnil)
		buffer_attributes = paddlec_pulseaudio_bufferattr_get_struct(rbba);

	if (channel_map && sample_spec && !pa_channel_map_compatible(channel_map, sample_spec))
		rb_raise(rb_eRuntimeError, "Channel map [%"PRIsVALUE"] is not compatible with sample specification [%"PRIsVALUE"]", rbcm, rbss);

	*pas = pa_simple_new(server, name, PA_STREAM_RECORD, device, stream_name, sample_spec, channel_map, buffer_attributes, &error);
	if (*pas == NULL)
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return self;
}


/* If no block is provided, returns an opened {Source}
 *
 * If a block is given, creates a new {Source}, opens it, yield it, drain it and close it before returning the result of the block.
 *
 * @overload open(server: nil, name: $0, device: nil, stream_name: Source.class.name, sample_spec: 'float32le 1ch 44100Hz', channel_map: nil, buffer_attributes: nil)
 *  @param server [String, nil] the server name (connection path), or +nil+ for default
 *  @param name [String, nil] a descriptive name for this client
 *  @param device [String, nil] source name, or +nil+ for default
 *  @param stream_name [String, nil] a descriptive name for this stream (application name, song title, ...)
 *  @param sample_spec [PaddleC::PulseAudio::SampleSpec, String, nil] the sample type to use, or a string describing a sample specification (see {SampleSpec#initialize})
 *  @param channel_map [PaddleC::PulseAudio::ChannelMap, Integer, String, Array<Symbol>, nil] the channel map to use, or an argument of {ChannelMap#initialize}, or +nil+ for default
 *  @param buffer_attributes [PaddleC::PulseAudio::BufferAttributes, nil] buffering attributes, or +nil+ for default
 *  @return [PaddleC::PulseAudio::Simple::Source] the new source, already opened
 * @overload open(...)
 *  @yield [source]  opens a newly created {Source}, yield it and close it, then return the result of the block
 *  @return [Object] the result of the block
 */
static VALUE paddlec_pulseaudio_simplesourceclass_open(int argc, VALUE *argv, VALUE self)
{
	VALUE source, res;

	source = rb_class_new_instance(argc, argv, c_PASimpleSource);
	res = source;

	paddlec_pulseaudio_simplesource_open(source);

	if (rb_block_given_p()) {
		res = rb_yield(source);
		paddlec_pulseaudio_simple_close(source);
	}

	return res;
}


/* Read some data from the server.
 * @overload read(out = nil, len: nil, seconds: nil)
 * @param out [PaddleC::FloatBuffer, PaddleC::ComplexBuffer, String, Class, nil]
 *  - If +out+ is a {FloatBuffer}, fills it with data and returns it. If +len+ and +seconds+ are +nil+, reads +out.length * 4+ bytes into it.
 *  - If +out+ is a {ComplexBuffer}, fills it with data and returns it. If +len+ and +seconds+ are +nil+, reads +out.length * 8+ bytes into it.
 *  - If +out+ is a String, fills the string with data and returns it. If +len+ and +seconds+ are +nil+, reads +out.bytesize+ bytes into it.
 *  - If +out+ is the class {FloatBuffer}, returns a new FloatBuffer filled with data.
 *  - If +out+ is the class {ComplexBuffer}, returns a new ComplexBuffer filled with data.
 *  - If +out+ is the class String, returns a new String filled with data.
 *  - If +out+ is not provided:
 *    - if +self.channel_map.format+ is +:float32le+
 *      - returns a new {ComplexBuffer} if +self.channel_map.channels+ is 2
 *      - returns a new {FloatBuffer} otherwise.
 *    - returns a new String otherwise.
 * @param len [Integer] Number of samples to read (read +len * self.sample_spec.frame_size+ bytes). Exclusive with +seconds:+
 * @param seconds [Float] reads +seconds+ of audio data (read +self.sample_spec.usec_to_bytes(seconds * 1e6)+ bytes). Exclusive with +len:+
 * @return [PaddleC::FloatBuffer, PaddleC::ComplexBuffer, String, Qnil] depending on +out+, returns a String, a {FloatBuffer}, a ComplexBuffer}, or +nil+ if the connection is closed
 */
static VALUE paddlec_pulseaudio_simplesource_read(int argc, VALUE *argv, VALUE self)
{
	pa_simple *pas;
	VALUE opts, out;
	VALUE len_seconds[2] = {Qundef, Qundef};
	const ID kwkeys[2] = {rb_intern("len"), rb_intern("seconds")};
	int len = 0;
	float seconds = 0.0f;
	pa_sample_spec ss;
	size_t nbytes = 0;
	pdlc_buffer_t *fbuf;
	pdlc_complex_buffer_t *cbuf;
	void *ptr = NULL;
	int error = 0;

	rb_scan_args(argc, argv, "01:", &out, &opts);

	pas = paddlec_pulseaudio_simple_get_struct(self);
	if (!pas)
		return Qnil;

	ss = paddlec_pulseaudio_sample_spec_get(rb_iv_get(self, "@sample_spec"));

	if (!NIL_P(opts)) {
		rb_get_kwargs(opts, kwkeys, 0, 2, len_seconds);
		if (len_seconds[0] != Qundef) {
			len = NUM2INT(len_seconds[0]);
			if (len > 0)
				nbytes = (size_t)len * pa_sample_size(&ss);
		}
		if (len_seconds[1] != Qundef) {
			if (len > 0)
				rb_raise(rb_eArgError, "seconds: and len: are exclusive");
			seconds = (float)NUM2DBL(len_seconds[1]);
			if (seconds > 0.0f)
				nbytes = pa_usec_to_bytes((pa_usec_t)(seconds * 1000000.0f), &ss);
		}
	}

	if (NIL_P(out)) {
		if (ss.format == PA_SAMPLE_FLOAT32LE) {
			if (ss.channels == 2)
				out = c_ComplexBuffer;
			else
				out = c_FloatBuffer;
		}
		else
			out = rb_cString;
	}

	if (rb_obj_is_kind_of(out, c_FloatBuffer)) {
		fbuf = paddlec_float_buffer_get_struct(out);
		if (nbytes)
			pdlc_buffer_resize(fbuf, (nbytes+3) >> 2, 0);
		else if (fbuf->length > 0)
			nbytes = fbuf->length*4;
		else {
			nbytes = pa_usec_to_bytes(100000, &ss);
			pdlc_buffer_resize(fbuf, (nbytes+3) >> 2, 0);
		}
		ptr = fbuf->data;
	}
	else if (rb_obj_is_kind_of(out, c_ComplexBuffer)) {
		cbuf = paddlec_complex_buffer_get_struct(out);
		if (nbytes)
			pdlc_complex_buffer_resize(cbuf, (nbytes+7) >> 3, 0);
		else if (cbuf->length > 0)
			nbytes = cbuf->length*8;
		else {
			nbytes = pa_usec_to_bytes(100000, &ss);
			pdlc_complex_buffer_resize(cbuf, (nbytes+7) >> 3, 0);
		}
		ptr = cbuf->data;
	}
	else if (rb_obj_is_kind_of(out, rb_cString)) {
		rb_funcall(out, rb_intern("force_encoding"), 1, rb_sprintf("ASCII"));
		if (nbytes)
			out = rb_str_resize(out, nbytes);
		else if (RSTRING_LEN(out) > 0)
			nbytes = RSTRING_LEN(out);
		else {
			nbytes = pa_usec_to_bytes(100000, &ss);
			out = rb_str_resize(out, nbytes);
		}
		ptr = rb_string_value_ptr(&out);
	}
	else if (out == c_FloatBuffer) {
		if (!nbytes)
			nbytes = pa_usec_to_bytes(100000, &ss);
		out = rb_class_new_instance(0, NULL, c_FloatBuffer);
		fbuf = paddlec_float_buffer_get_struct(out);
		pdlc_buffer_resize(fbuf, (nbytes+3) >> 2, 0);
		ptr = fbuf->data;
	}
	else if (out == c_ComplexBuffer) {
		if (!nbytes)
			nbytes = pa_usec_to_bytes(100000, &ss);
		out = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		cbuf = paddlec_complex_buffer_get_struct(out);
		pdlc_complex_buffer_resize(cbuf, (nbytes+7) >> 3, 0);
		ptr = cbuf->data;
	}
	else if (out == rb_cString) {
		if (!nbytes)
			nbytes = pa_usec_to_bytes(100000, &ss);
		out = rb_usascii_str_new(NULL, 0);
		out = rb_str_resize(out, nbytes);
		ptr = rb_string_value_ptr(&out);
	}
	else
		rb_raise(rb_eArgError, "expecting only nil, a FloatBuffer, a ComplexBuffer, a String or the class FloatBuffer, ComplexBuffer or String ; not a %"PRIsVALUE, rb_class_name(rb_class_of(out)));

	if (pa_simple_read(pas, ptr, nbytes, &error))
		rb_raise(rb_eRuntimeError, "PulseAudio: %s", pa_strerror(error));

	return out;
}


static void Init_paddlec_pulseaudio_simple()
{
	m_PASimple = rb_define_module_under(m_PulseAudio, "Simple");
	rb_define_method(m_PASimple, "server",             paddlec_pulseaudio_simple_get_server,           0);
	rb_define_method(m_PASimple, "server=",            paddlec_pulseaudio_simple_set_server,           1);
	rb_define_method(m_PASimple, "name",               paddlec_pulseaudio_simple_get_name,             0);
	rb_define_method(m_PASimple, "name=",              paddlec_pulseaudio_simple_set_name,             1);
	rb_define_method(m_PASimple, "device",             paddlec_pulseaudio_simple_get_device,           0);
	rb_define_method(m_PASimple, "device=",            paddlec_pulseaudio_simple_set_device,           1);
	rb_define_method(m_PASimple, "stream_name",        paddlec_pulseaudio_simple_get_streamname,       0);
	rb_define_method(m_PASimple, "stream_name=",       paddlec_pulseaudio_simple_set_streamname,       1);
	rb_define_method(m_PASimple, "sample_spec",        paddlec_pulseaudio_simple_get_samplespec,       0);
	rb_define_method(m_PASimple, "sample_spec=",       paddlec_pulseaudio_simple_set_samplespec,       1);
	rb_define_method(m_PASimple, "channel_map",        paddlec_pulseaudio_simple_get_channelmap,       0);
	rb_define_method(m_PASimple, "channel_map=",       paddlec_pulseaudio_simple_set_channelmap,       1);
	rb_define_method(m_PASimple, "buffer_attributes",  paddlec_pulseaudio_simple_get_bufferattributes, 0);
	rb_define_method(m_PASimple, "buffer_attributes=", paddlec_pulseaudio_simple_set_bufferattributes, 1);
	rb_define_method(m_PASimple, "close",              paddlec_pulseaudio_simple_close,                0);
	rb_define_method(m_PASimple, "flush",              paddlec_pulseaudio_simple_flush,                0);
	rb_define_method(m_PASimple, "latency",            paddlec_pulseaudio_simple_latency,              0);
	rb_define_method(m_PASimple, "opened?",            paddlec_pulseaudio_simple_opened,               0);
	rb_define_method(m_PASimple, "closed?",            paddlec_pulseaudio_simple_closed,               0);

	c_PASimpleSink = rb_define_class_under(m_PASimple, "Sink", rb_cObject);
	rb_define_alloc_func(c_PASimpleSink, paddlec_pulseaudio_simple_alloc);
	rb_include_module(c_PASimpleSink, m_PASimple);
	rb_define_module_function(c_PASimpleSink, "open", paddlec_pulseaudio_simplesinkclass_open,  -1);
	rb_define_method(c_PASimpleSink, "initialize",    paddlec_pulseaudio_simplesink_initialize, -1);
	rb_define_method(c_PASimpleSink, "open",          paddlec_pulseaudio_simplesink_open,        0);
	rb_define_method(c_PASimpleSink, "write",         paddlec_pulseaudio_simplesink_write,       1);
	rb_define_alias(c_PASimpleSink,  "<<", "write");
	rb_define_method(c_PASimpleSink, "drain",         paddlec_pulseaudio_simplesink_drain,       0);

	c_PASimpleSource = rb_define_class_under(m_PASimple, "Source", rb_cObject);
	rb_define_alloc_func(c_PASimpleSource, paddlec_pulseaudio_simple_alloc);
	rb_include_module(c_PASimpleSource, m_PASimple);
	rb_define_module_function(c_PASimpleSource, "open", paddlec_pulseaudio_simplesourceclass_open,  -1);
	rb_define_method(c_PASimpleSource, "initialize",    paddlec_pulseaudio_simplesource_initialize, -1);
	rb_define_method(c_PASimpleSource, "open",          paddlec_pulseaudio_simplesource_open,        0);
	rb_define_method(c_PASimpleSource, "read",          paddlec_pulseaudio_simplesource_read,       -1);
	rb_define_alias(c_PASimpleSource,  ">>", "read");
}


#endif
