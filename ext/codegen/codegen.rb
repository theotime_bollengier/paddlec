#!/usr/bin/env ruby

=begin


Comparison
==========


Ask
===

eql? === true or false, element by element comparison


Float
=====


Complex
=======

polar


Object
======

clone
dup
select!
collect!
reject!
push, append,
pop, shift, 
unshift, prepend
rotate, rotate!
clear
compact, compact! -> removes all NANs
replace_nans
count(), count{}
first, last
new {}
keep_if
concat
concat!
<< Append—Pushes the given object on to the end of this array. This expression returns the array itself, so several appends may be chained together.


noise
pink_noise
gaussian_noise
pink_gaussian_noise
[] []= 
linspace
complex#normalize


=end


defs = [
#	## Min Max ##
#	{
#		pdlc_name: :min,
#		method: :min,
#		inplace: :min!,
#		symmetric: false,
#		ff: 'res = fminf(lhs, rhs)',
#		cc: 'res.real = fminf(lhs.real, rhs.real) | res.imag = fminf(lhs.imag, rhs.imag)',
#		cf: 'res.real = fminf(lhs.real, rhs) | res.imag = fminf(lhs.imag, rhs)',
#	},
#	{
#		pdlc_name: :max,
#		method: :max,
#		inplace: :max!,
#		symmetric: false,
#		ff: 'res = fmaxf(lhs, rhs)',
#		cc: 'res.real = fmaxf(lhs.real, rhs.real) | res.imag = fmaxf(lhs.imag, rhs.imag)',
#		cf: 'res.real = fmaxf(lhs.real, rhs) | res.imag = fmaxf(lhs.imag, rhs)',
#	},

#   ## Arithmetic ##
##	{
##		pdlc_name: :unaryplus,
##		method: :+@,
##		f: 'res = param',
##		c: 'res.real = param.real | res.imag = param.imag',
##		doc: 'Unary plus, returns the receiver.'
##	},
#	{
#		pdlc_name: :unaryminus,
#		method: :-@,
#		inplace: :negate!,
#		f: 'res = -param',
#		c: 'res.real = -param.real | res.imag = -param.imag',
#		doc: 'Unary minus, returns the receiver, negated.'
#	},
#	{
#		pdlc_name: :sub,
#		method: :-,
#		alias: :sub,
#		inplace: :sub!,
#		symmetric: false,
#		ff: 'res = lhs - rhs',
#		cc: 'res.real = lhs.real - rhs.real | res.imag = lhs.imag - rhs.imag',
#		fc: 'res.real = lhs - rhs.real | res.imag = -rhs.imag',
#		cf: 'res.real = lhs.real - rhs | res.imag = lhs.imag',
#	},
#	{
#		pdlc_name: :esub,
#		method: :esub,
#		inplace: :esub!,
#		symmetric: false,
#		ff: 'res = lhs - rhs',
#		cc: 'res.real = lhs.real - rhs.real | res.imag = lhs.imag - rhs.imag',
#		fc: 'res.real = lhs - rhs.real | res.imag = lhs - rhs.imag',
#		cf: 'res.real = lhs.real - rhs | res.imag = lhs.imag - rhs',
#		doc: 'Element by element soustraction.'
#	},
#	{
#		pdlc_name: :add,
#		method: :+,
#		alias: :add,
#		inplace: :add!,
#		ff: 'res = lhs + rhs',
#		cc: 'res.real = lhs.real + rhs.real | res.imag = lhs.imag + rhs.imag',
#		fc: 'res.real = lhs + rhs.real | res.imag = rhs.imag',
#		cf: 'res.real = lhs.real + rhs | res.imag = lhs.imag',
#	},
#	{
#		pdlc_name: :eadd,
#		method: :eadd,
#		inplace: :eadd!,
#		ff: 'res = lhs + rhs',
#		cc: 'res.real = lhs.real + rhs.real | res.imag = lhs.imag + rhs.imag',
#		fc: 'res.real = lhs + rhs.real | res.imag = lhs + rhs.imag',
#		cf: 'res.real = lhs.real + rhs | res.imag = lhs.imag + rhs',
#		doc: 'Element by element addition.'
#	},
#	{
#		pdlc_name: :mult,
#		method: :*,
#		alias: :mult,
#		inplace: :mult!,
#		ff: 'res = lhs * rhs',
#		cc: 'tmp.real = lhs.real * rhs.real - lhs.imag * rhs.imag | tmp.imag = lhs.real * rhs.imag + lhs.imag * rhs.real | res = tmp',
#		fc: 'res.real = lhs * rhs.real | res.imag = lhs * rhs.imag',
#		cf: 'res.real = lhs.real * rhs | res.imag = lhs.imag * rhs',
#		doc: 'Performs multiplication.'
#	},
#	{
#		pdlc_name: :emult,
#		method: :emult,
#		inplace: :emul!,
#		ff: 'res = lhs * rhs',
#		cc: 'res.real = lhs.real * rhs.real | res.imag = lhs.imag * rhs.imag',
#		fc: 'res.real = lhs * rhs.real | res.imag = lhs * rhs.imag',
#		cf: 'res.real = lhs.real * rhs | res.imag = lhs.imag * rhs',
#		doc: 'Element by element multiplication.'
#	},
#	{
#		pdlc_name: :div,
#		method: :/,
#		alias: :div,
#		inplace: :div!,
#		symmetric: false,
#		ff: 'res = lhs / rhs',
#		cc: 'tmp.real = (lhs.real*rhs.real + lhs.imag*rhs.imag) / (rhs.real*rhs.real + rhs.imag*rhs.imag) | tmp.imag = (lhs.imag*rhs.real - lhs.real*rhs.imag) / (rhs.real*rhs.real + rhs.imag*rhs.imag) | res = tmp',
#		fc: 'res.real = (lhs*rhs.real) / (rhs.real*rhs.real + rhs.imag*rhs.imag) | res.imag = -lhs*rhs.imag / (rhs.real*rhs.real + rhs.imag*rhs.imag)',
#		cf: 'res.real = (lhs.real*rhs) / (rhs*rhs) | res.imag = (lhs.imag*rhs) / (rhs*rhs)',
#		doc: 'Performs division.'
#	},
#	{
#		pdlc_name: :ediv,
#		method: :ediv,
#		inplace: :ediv!,
#		symmetric: false,
#		ff: 'res = lhs / rhs',
#		cc: 'res.real = lhs.real / rhs.real | res.imag = lhs.imag / rhs.imag',
#		fc: 'res.real = lhs / rhs.real | res.imag = lhs / rhs.imag',
#		cf: 'res.real = lhs.real / rhs | res.imag = lhs.imag / rhs',
#		doc: 'Element by element division.'
#	},
#	{
#		pdlc_name: :pow,
#		method: :**,
#		alias: :pow,
#		inplace: :pow!,
#		symmetric: false,
#		ff: 'res = powf(lhs, rhs)',
#		cf: 'tmp.real = powf(hypotf(lhs.real, lhs.imag), rhs) | tmp.imag = atan2f(lhs.imag, lhs.real) * rhs | res.real = tmp.real * cosf(tmp.imag) | res.imag = tmp.real * sinf(tmp.imag)',
#		doc: 'Performs exponentiation. Raises +self+ to the power of +rhs+.'
#	},
#	{
#		pdlc_name: :modulo,
#		method: :%,
#		alias: :modulo,
#		inplace: :modulo!,
#		symmetric: false,
#		ff: 'res = pdlc_fmodf(lhs, rhs)',
#		doc: 'Returns the modulo after division of values of +lhs+ by values of +rhs+.'
#	},
#	{
#		pdlc_name: :emod,
#		method: :emod,
#		inplace: :emod!,
#		symmetric: false,
#		ff: 'res = pdlc_fmodf(lhs, rhs)',
#		cc: 'res.real = pdlc_fmodf(lhs.real, rhs.real) | res.imag = pdlc_fmodf(lhs.imag, rhs.imag)',
#		fc: 'res.real = pdlc_fmodf(lhs, rhs.real) | res.imag = pdlc_fmodf(lhs, rhs.imag)',
#		cf: 'res.real = pdlc_fmodf(lhs.real, rhs) | res.imag = pdlc_fmodf(lhs.imag, rhs)',
#		doc: 'Element by element modulo.'
#	},
#	{
#		pdlc_name: :epow,
#		method: :epow,
#		inplace: :epow!,
#		symmetric: false,
#		ff: 'res = powf(lhs, rhs)',
#		cc: 'res.real = powf(lhs.real, rhs.real) | res.imag = powf(lhs.imag, rhs.imag)',
#		fc: 'res.real = powf(lhs, rhs.real) | res.imag = powf(lhs, rhs.imag)',
#		cf: 'res.real = powf(lhs.real, rhs) | res.imag = powf(lhs.imag, rhs)',
#		doc: 'Element by element exponentiation.'
#	},

	## Comparisons ##
#	{
#		pdlc_name: :cmpless,
#		method: :<,
#		symmetric: false,
#		ff: 'res = (lhs < rhs) ? 1.0f : 0.0f',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.'
#	},
#	{
#		pdlc_name: :cmplessequ,
#		method: :<=,
#		symmetric: false,
#		ff: 'res = (lhs <= rhs) ? 1.0f : 0.0f',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.'
#	},
#	{
#		pdlc_name: :cmpgrt,
#		method: :>,
#		symmetric: false,
#		ff: 'res = (lhs > rhs) ? 1.0f : 0.0f',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.'
#	},
#	{
#		pdlc_name: :cmpgrtequ,
#		method: :>=,
#		symmetric: false,
#		ff: 'res = (lhs >= rhs) ? 1.0f : 0.0f',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.'
#	},
#	{
#		pdlc_name: :equ,
#		method: :==,
#		symmetric: false,
#		ff: 'res = (lhs == rhs) ? 1.0f : 0.0f',
#		cc: 'res = ((lhs.real == rhs.real) && (lhs.imag == rhs.imag)) ? 1.0f : 0.0f',
#		fc: 'res = ((lhs == rhs.real) && (rhs.imag == 0.0f)) ? 1.0f : 0.0f',
#		cf: 'res = ((lhs.real == rhs) && (lhs.imag == 0.0f)) ? 1.0f : 0.0f',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.'
#	},
#	{
#		pdlc_name: :different,
#		method: :!=,
#		symmetric: false,
#		ff: 'res = (lhs != rhs) ? 1.0f : 0.0f',
#		cc: 'res = ((lhs.real != rhs.real) || (lhs.imag != rhs.imag)) ? 1.0f : 0.0f',
#		fc: 'res = ((lhs != rhs.real) || (rhs.imag != 0.0f)) ? 1.0f : 0.0f',
#		cf: 'res = ((lhs.real != rhs) && (lhs.imag != 0.0f)) ? 1.0f : 0.0f',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 for true, 0.0 for false.'
#	},

#	## Ask? ##
#	{
#		pdlc_name: :finite,
#		method: :finite?,
#		f: 'res = (isfinite(param) ? 1.0f : 0.0f)',
#		c: 'res = ((isfinite(param.real) && isfinite(param.imag)) ? 1.0f : 0.0f)',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 for values which are valid IEEE floating point number, i.e. which are not infinite or NaN, 0.0 otherwize.'
#	},
#	{
#		pdlc_name: :infinite,
#		method: :infinite?,
#		f: 'res = ((isinf(param)) ? 1.0f : 0.0f)',
#		c: 'res = ((isinf(param.real) || isinf(param.imag)) ? 1.0f : 0.0f)',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 for values which are positive or negative infinity, 0.0 otherwize.'
#	},
#	{
#		pdlc_name: :nan,
#		method: :nan?,
#		f: 'res = ((isnan(param)) ? 1.0f : 0.0f)',
#		c: 'res = ((isnan(param.real) || isnan(param.imag)) ? 1.0f : 0.0f)',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 for values which are NaN, 0.0 otherwize.'
#	},
#	{
#		pdlc_name: :integer,
#		method: :integer?,
#		f: 'res = ((isfinite(param) && (param == truncf(param))) ? 1.0f : 0.0f)',
#		c: 'res = ((isfinite(param.real) && isfinite(param.imag) && (param.real == truncf(param.real)) && (param.imag == truncf(param.imag))) ? 1.0f : 0.0f)',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 if +value == value.truncate+, 0.0 otherwise.'
#	},
#	{
#		pdlc_name: :negative,
#		method: :negative?,
#		f: 'res = ((param < 0.0f) ? 1.0f : 0.0f)',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 if +value < 0+, 0.0 otherwise.'
#	},
#	{
#		pdlc_name: :positive,
#		method: :positive?,
#		f: 'res = ((param > 0.0f) ? 1.0f : 0.0f)',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 if +value > 0+, 0.0 otherwise.'
#	},
#	{
#		pdlc_name: :nonzero,
#		method: :nonzero?,
#		f: 'res = ((param == 0.0f) ? 0.0f : 1.0f)',
#		c: 'res = ((param.real == 0.0f && param.imag == 0.0f) ? 0.0f : 1.0f)',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 if +value != 0+, 0.0 otherwise.'
#	},
#	{
#		pdlc_name: :zero,
#		method: :zero?,
#		f: 'res = ((param == 0.0f) ? 1.0f : 0.0f)',
#		c: 'res = ((param.real == 0.0f && param.imag == 0.0f) ? 1.0f : 0.0f)',
#		doc: 'Returns a {PaddleC::FloatBuffer}. 1.0 if +value == 0+, 0.0 otherwise.'
#	},

#	## Roundings ##
#	{
#		pdlc_name: :ceil,
#		method: :ceil,
#		inplace: :ceil!,
#		f: 'res = ceilf(param)',
#		c: 'res.real = ceilf(param.real) | res.imag = ceilf(param.imag)',
#		doc: 'Returns the smallest integral values that are not less than values of +self+.'
#	},
#	{
#		pdlc_name: :floor,
#		method: :floor,
#		inplace: :floor!,
#		f: 'res = floorf(param)',
#		c: 'res.real = floorf(param.real) | res.imag = floorf(param.imag)',
#		doc: 'Returns the largest integral values that are not greater than values of +self+.'
#	},
#	{
#		pdlc_name: :truncate,
#		method: :truncate,
#		inplace: :truncate!,
#		f: 'res = truncf(param)',
#		c: 'res.real = truncf(param.real) | res.imag = truncf(param.imag)',
#		doc: 'Rounds values of +self+ to the nearest integers not larger in absolute value.'
#	},
#	{
#		pdlc_name: :round,
#		method: :round,
#		inplace: :round!,
#		f: 'res = roundf(param)',
#		c: 'res.real = roundf(param.real) | res.imag = roundf(param.imag)',
#		doc: 'Rounds values of +self+ to the nearest integers, but round halfway cases away from zero.'
#	},
#	{
#		pdlc_name: :ceil_digits,
#		method: :ceil,
#		inplace: :ceil!,
#		f: 'res = ceilf(param*d)/d',
#		c: 'res.real = ceilf(param.real*d)/d | res.imag = ceilf(param.imag*d)/d',
#		doc: 'Returns the smallest integral values that are not less than values of +self+.'
#	},
#	{
#		pdlc_name: :floor_digits,
#		method: :floor,
#		inplace: :floor!,
#		f: 'res = floorf(param*d)/d',
#		c: 'res.real = floorf(param.real*d)/d | res.imag = floorf(param.imag*d)/d',
#		doc: 'Returns the largest integral values that are not greater than values of +self+.'
#	},
#	{
#		pdlc_name: :truncate_digits,
#		method: :truncate,
#		inplace: :truncate!,
#		f: 'res = truncf(param*d)/d',
#		c: 'res.real = truncf(param.real*d)/d | res.imag = truncf(param.imag*d)/d',
#		doc: 'Rounds values of +self+ to the nearest integers not larger in absolute value.'
#	},
#	{
#		pdlc_name: :round_digits,
#		method: :round,
#		inplace: :round!,
#		f: 'res = roundf(param*d)/d',
#		c: 'res.real = roundf(param.real*d)/d | res.imag = roundf(param.imag*d)/d',
#		doc: 'Rounds values of +self+ to the nearest integers, but round halfway cases away from zero.'
#	},

#	## Complex ##
#	{
#		pdlc_name: :abs,
#		method: :abs,
#		alias: :magnitude,
#		inplace: :abs!,
#		f: 'res = fabsf(param)',
#		c: 'res = hypotf(param.real, param.imag)',
#		doc: 'Returns the absolute value / the absolute part of its polar form.'
#	},
#	{
#		pdlc_name: :abs2,
#		method: :abs2,
#		inplace: :square!,
#		f: 'res = param * param',
#		c: 'res = param.real * param.real + param.imag * param.imag',
#		doc: 'Returns the square of the absolute value / the square of the absolute part of its polar form.'
#	},
#	{
#		pdlc_name: :arg,
#		method: :arg,
#		alias: [:angle, :phase],
#		f: 'res = (param < 0.0f) ? (float)M_PI : 0.0f',
#		c: 'res = atan2f(param.imag, param.real)',
#		doc: 'Returns the angle part of its polar form.'
#	},
#	{
#		pdlc_name: :conjugate,
#		method: :conjugate,
#		inplace: :conjugate!,
#		alias: :conj,
#		f: 'res = param',
#		c: 'res.real = param.real | res.imag = -param.imag',
#		doc: 'Returns the complex conjugate.'
#	},
#	{
#		pdlc_name: :imag,
#		method: :imaginary,
#		alias: :imag,
#		f: 'res = 0.0f',
#		c: 'res = param.imag',
#		doc: 'Returns the imaginary part.'
#	},
#	{
#		pdlc_name: :real,
#		method: :real,
#		f: 'res = param',
#		c: 'res = param.real',
#		doc: 'Returns the real part.'
#	},
#	{
#		pdlc_name: :swapIQ,
#		method: :swapIQ,
#		alias: :swapRI,
#		inplace: :swapIQ!,
#		f: 'res.real = 0.0f | res.imag = param',
#		c: 'tmp.real = param.imag | tmp.imag = param.real | res = tmp',
#		doc: 'Swap the real and imaginary parts.'
#	},

#	## Math ##
#	{
#		pdlc_name: :acos,
#		method: :acos,
#		inplace: :acos!,
#		f: 'res = acosf(param)',
#		doc: 'Computes the arc cosine of values of +self+. Returns 0..PI.'
#	},
#	{
#		pdlc_name: :acosh,
#		method: :acosh,
#		inplace: :acosh!,
#		f: 'res = acoshf(param)',
#		doc: 'Computes the inverse hyperbolic cosine of values of +self+.'
#	},
#	{
#		pdlc_name: :asin,
#		method: :asin,
#		inplace: :asin!,
#		f: 'res = asinf(param)',
#		doc: 'Computes the arc sine of values of +self+. Returns -PI/2..PI/2.'
#	},
#	{
#		pdlc_name: :asinh,
#		method: :asinh,
#		inplace: :asinh!,
#		f: 'res = asinhf(param)',
#		doc: 'Computes the inverse hyperbolic sine of values of +self+.'
#	},
#	{
#		pdlc_name: :atan,
#		method: :atan,
#		inplace: :atan!,
#		f: 'res = atanf(param)',
#		doc: 'Computes the arc tangent of values of +self+.'
#	},
#	{
#		pdlc_name: :atanh,
#		method: :atanh,
#		inplace: :atanh!,
#		f: 'res = atanhf(param)',
#		doc: 'Computes the inverse hyperbolic tangent of values of +self+.'
#	},
#	{
#		pdlc_name: :cbrt,
#		method: :cbrt,
#		inplace: :cbrt!,
#		f: 'res = cbrtf(param)',
#		doc: 'Returns the cube root of values of +self+.'
#	},
#	{
#		pdlc_name: :cos,
#		method: :cos,
#		inplace: :cos!,
#		f: 'res = cosf(param)',
#		doc: 'Computes the cosine of values of +self+ (expressed in radians). Returns a Float in the range -1.0..1.0.'
#	},
#	{
#		pdlc_name: :cosh,
#		method: :cosh,
#		inplace: :cosh!,
#		f: 'res = coshf(param)',
#		doc: 'Computes the hyperbolic cosine of values of +self+.'
#	},
#	{
#		pdlc_name: :erf,
#		method: :erf,
#		inplace: :erf!,
#		f: 'res = erff(param)',
#		doc: 'Calculates the error function of values of +self+.'
#	},
#	{
#		pdlc_name: :erfc,
#		method: :erfc,
#		inplace: :erfc!,
#		f: 'res = erfcf(param)',
#		doc: 'Calculates the complementary error function of values of +self+.'
#	},
#	{
#		pdlc_name: :exp,
#		method: :exp,
#		inplace: :exp!,
#		f: 'res = expf(param)',
#		doc: 'Calculates the value of e (the base of natural logarithms) raised to the power of values of +self+.'
#	},
#	{
#		pdlc_name: :log,
#		method: :log,
#		inplace: :log!,
#		f: 'res = logf(param)',
#		doc: 'Calculates the natural logarithm of values of +self+.'
#	},
#	{
#		pdlc_name: :log10,
#		method: :log10,
#		inplace: :log10!,
#		f: 'res = log10f(param)',
#		doc: 'Calculates the base 10 logarithm of values of +self+.'
#	},
#	{
#		pdlc_name: :log2,
#		method: :log2,
#		inplace: :log2!,
#		f: 'res = log2f(param)',
#		doc: 'Calculate the base 2 logarithm of values of +self+.'
#	},
#	{
#		pdlc_name: :sin,
#		method: :sin,
#		inplace: :sin!,
#		f: 'res = sinf(param)',
#		doc: 'Computes the sine of values of +self+ (expressed in radians). Returns a Float in the range -1.0..1.0.'
#	},
#	{
#		pdlc_name: :sinh,
#		method: :sinh,
#		inplace: :sinh!,
#		f: 'res = sinhf(param)',
#		doc: 'Computes the hyperbolic sine of values of +self+.'
#	},
#	{
#		pdlc_name: :sqrt,
#		method: :sqrt,
#		inplace: :sqrt!,
#		f: 'res = sqrtf(param)',
#		doc: 'Computes the non-negative square root of values of +self+.'
#	},
#	{
#		pdlc_name: :tan,
#		method: :tan,
#		inplace: :tan!,
#		f: 'res = tanf(param)',
#		doc: 'Computes the tangent of values of +self+ (expressed in radians).'
#	},
#	{
#		pdlc_name: :tanh,
#		method: :tanh,
#		inplace: :tanh!,
#		f: 'res = tanhf(param)',
#		doc: 'Computes the hyperbolic tangent of values of +self+.'
#	},
]


pdlc_c = File.open('pdlc_funcs.c', 'w')
pdlc_h = File.open('pdlc_funcs.h', 'w')
paddlec_float_c = File.open('float_buffer_funcs.c', 'w')
paddlec_float_rb = File.open('float_buffer_funcs_methods.c', 'w')
paddlec_complex_c = File.open('complex_buffer_funcs.c', 'w')
paddlec_complex_rb = File.open('complex_buffer_funcs_methods.c', 'w')

pdlc_c.puts '// Start generated code'
pdlc_h.puts '// Start generated code'
paddlec_float_c.puts '// Start generated code'
paddlec_float_rb.puts '// Start generated code'
paddlec_complex_c.puts '// Start generated code'
paddlec_complex_rb.puts '// Start generated code'

defs.each do |h|
	h[:cf] = h[:fc] if (h[:symmetric] != false and h[:cf].nil? and not(h[:fc].nil?))
	h[:fc] = h[:cf] if (h[:symmetric] != false and h[:fc].nil? and not(h[:cf].nil?))

	[:ff, :cc, :fc, :cf].each do |ntdoubleton|
		spec = h[ntdoubleton]
		next if spec.nil?

		two_args = (spec =~ /rhs/ and spec =~ /lhs/)
		tmp = not(not(spec =~ /tmp/))
		ctmp = not(not(spec =~ /tmp\.real/ or spec =~ /tmp\.imag/))
		complex_result = not(not(spec =~ /res\.real/ or spec =~ /res\.imag/ or (tmp and ctmp and spec =~ /res\s*=\s*tmp(?!\.(real|imag))/)))

		nta = ntdoubleton.to_s.split('').collect{|s| s.to_sym}

		lntype = nta.first
		rntype = nta.last
		if two_args then
			btypes = [[:b, :b], [:b, :s]]
			#btypes << [:s, :b] unless h[:symmetric] == false
			btypes.each do |bta|
				lbtype = bta.first
				rbtype = bta.last
				[:new, :inplace].each do |out|
					next if out == :inplace and ((lbtype == :b and ((complex_result and lntype == :f) or (not(complex_result) and lntype == :c))) or (lbtype == :s and ((complex_result and rntype == :f) or (not(complex_result) and rntype == :c))))
					### Declaration ###

					declaration = "pdlc_#{complex_result ? 'complex_':''}buffer_t* pdlc_#{lntype}#{lbtype}_#{rntype}#{rbtype}_#{h[:pdlc_name]}#{out == :inplace ? '_inplace':''}("
					declaration << 'const ' if out == :new and lbtype != :s
					if lbtype == :b then
						declaration << "pdlc_#{lntype == :c ? 'complex_':''}buffer_t *"
					else
						if lntype == :c then
							declaration << 'pdlc_complex_t '
						else
							declaration << 'float '
						end
					end
					declaration << 'lhs, '
					declaration << 'const ' if rbtype != :s and (lbtype != :s or out == :new)
					if rbtype == :b then
						declaration << "pdlc_#{rntype == :c ? 'complex_':''}buffer_t *"
					else
						if rntype == :c then
							declaration << 'pdlc_complex_t '
						else
							declaration << 'float '
						end
					end
					declaration << 'rhs'
					if out == :new then
						declaration << ", pdlc_#{complex_result ? 'complex_':''}buffer_t *res"
					end
					declaration << ')'

					pdlc_h.puts declaration + ';'


					### Definition ###

					tmp = not(not(spec =~ /tmp/))
					ctmp = not(not(spec =~ /tmp\.real/) and not(spec =~ /tmp\.imag/))

					lines = spec.split(/(?<!\|)\s*\|\s*(?!\|)/).collect do |s| 
						l = s
						l = l.gsub(/lhs/, 'lhs->data[i]') if lbtype == :b
						l = l.gsub(/rhs/, 'rhs->data[i]') if rbtype == :b
						if out == :new then
							l = l.gsub(/res/, 'res->data[i]')
						else
							if lbtype == :b then
								l = l.gsub(/res/, 'lhs->data[i]')
							else
								l = l.gsub(/res/, 'rhs->data[i]')
							end
						end
						l = "\t\t#{l};\n"
					end

					definition = <<EOS
#{declaration}
{
	size_t i;
EOS
					definition += "	#{ctmp ? 'pdlc_complex_t' : 'float'} tmp;\n" if tmp

					definition += <<EOS
	#{(lbtype == :s or rbtype == :s) ? 'const ':''}size_t len = #{(lbtype == :s) ? 'r':'l'}hs->length;

EOS
					if lbtype == :b and rbtype == :b then
						definition += <<EOS
	if (len > rhs->length)
		len = rhs->length;

EOS
					end

					if out == :new then
						definition += <<EOS
	if (!res)
		res = pdlc_#{complex_result ? 'complex_':''}buffer_new(len);
	else
		pdlc_#{complex_result ? 'complex_':''}buffer_resize(res, len, 0);

EOS
					end

					definition += <<EOS
	for (i = 0; i < len; i++)#{lines.length > 1 ? ' {':''}
#{lines.join}#{lines.length > 1 ? "\t}\n":''}
	return #{(out == :new) ? 'res' : ((lbtype == :b) ? 'lhs' : 'rhs')};
}
EOS
					pdlc_c.puts
					pdlc_c.puts definition
					pdlc_c.puts
					##################
				end
			end
		end
	end

	## Number of arguments ##
	two_args = [:ff, :cc, :fc, :cf].collect{|ntdoubleton|
		spec = h[ntdoubleton]
		if spec.nil? then
			nil
		else
			not(not(spec =~ /rhs/) or not(spec =~ /lhs/))
		end
	}.reject{|v| v.nil?}.uniq
	raise "method \"#{h[:method]}\": two args and one arg mixed. #{two_args.inspect}" if two_args.length > 1
	two_args = two_args.first





	if two_args then
		[:f, :c].each do |receiver|
			rettype = {float: [], complex: []}
			[:ff, :cc, :fc, :cf].select{|s| s.to_s.split('').first.to_sym == receiver}.each do |ntdoubleton|
				spec = h[ntdoubleton]
				next if spec.nil?
				if spec =~ /res\.real/ or spec =~ /res\.imag/ or ((spec =~ /tmp\.real/ or spec =~ /tmp\.imag/) and spec =~ /res\s*=\s*tmp(?!\.(real|imag))/) then
					rettype[:complex] << ntdoubleton
				else
					rettype[:float] << ntdoubleton
				end
			end

			next if [:ff, :cc, :fc, :cf].count{|s| (s.to_s.split('').first.to_sym == receiver) and h[s]} < 1

			## Return new ##########################################################

			definition = "/* #{h[:doc].nil? ? '' : (h[:doc].split(/\s*\n\s*/).join("\n * ") + "\n * ")}@return [#{rettype[:float].empty? ? '' : 'PaddleC::FloatBuffer'}#{rettype[:complex].empty? ? '' : ((rettype[:float].empty? ? '' : ', ') + 'PaddleC::ComplexBuffer')}]\n"

			unless rettype[:float].empty? then
				othertypes = rettype[:float].sort{|ntda, ntdb|
					a = ntda.to_s.split('').last.to_sym
					b = ntdb.to_s.split('').last.to_sym
					if a == b then
						0
					elsif a == :c then
						-1
					elsif b == :c then
						1
					else
						a <=> b
					end
				}.collect do |ntdoubleton|
					case ntdoubleton.to_s.split('').last.to_sym
					when :f
						'PaddleC::FloatBuffer, Numeric'
					when :c
						'PaddleC::ComplexBuffer, Complex'
					else
						raise "expecting only :f or :c"
					end
				end

				definition += <<EOS
 * @overload #{h[:method]}(other, buffer = nil)
 *  @param other [#{othertypes.join(', ')}]
 *  @param buffer [nil, PaddleC::FloatBuffer] if a {FloatBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::FloatBuffer]
EOS
			end

			unless rettype[:complex].empty? then
				othertypes = rettype[:complex].sort{|ntda, ntdb|
					a = ntda.to_s.split('').last.to_sym
					b = ntdb.to_s.split('').last.to_sym
					if a == b then
						0
					elsif a == :c then
						-1
					elsif b == :c then
						1
					else
						a <=> b
					end
				}.collect do |ntdoubleton|
					case ntdoubleton.to_s.split('').last.to_sym
					when :f
						'PaddleC::FloatBuffer, Numeric'
					when :c
						'PaddleC::ComplexBuffer, Complex'
					else
						raise "expecting only :f or :c"
					end
				end

				definition += <<EOS
 * @overload #{h[:method]}(other, buffer = nil)
 *  @param other [#{othertypes.join(', ')}]
 *  @param buffer [nil, PaddleC::ComplexBuffer] if a {ComplexBuffer} is provided, it is filled with the output
 * 	@return [PaddleC::ComplexBuffer]
EOS
			end

			definition += <<EOS
 */
static VALUE paddlec_#{receiver == :f ? 'float' : 'complex'}_buffer_#{h[:pdlc_name]}(int argc, VALUE *argv, VALUE self)
{
	const pdlc_#{receiver == :f ? '' : 'complex_'}buffer_t *m#{receiver}buf;
EOS
			definition += "	pdlc_buffer_t *rfbuf;\n" unless rettype[:float].empty?
			definition += "	pdlc_complex_buffer_t *rcbuf;\n" unless rettype[:complex].empty?
			if receiver == :f then
				unless h[:ff].nil? then
					definition += "	const pdlc_buffer_t *ofbuf;\n"
					definition += "	float of;\n"
				end
				unless h[:fc].nil? then
					definition += "	const pdlc_complex_buffer_t *ocbuf;\n"
					definition += "	pdlc_complex_t oc;\n"
				end
			else
				unless h[:cc].nil? then
					definition += "	const pdlc_complex_buffer_t *ocbuf;\n"
					definition += "	pdlc_complex_t oc;\n"
				end
				unless h[:cf].nil? then
					definition += "	const pdlc_buffer_t *ofbuf;\n"
					definition += "	float of;\n"
				end
			end
			definition += <<EOS
	VALUE other, buffer;

	rb_scan_args(argc, argv, "11", &other, &buffer);

	m#{receiver}buf = paddlec_#{receiver == :f ? 'float' : 'complex'}_buffer_get_struct(self);

EOS
			unless rettype[:complex].empty? then
				othertypes = rettype[:complex].collect{|ntdoubleton|
					case ntdoubleton.to_s.split('').last.to_sym
					when :f
						['c_FloatBuffer', 'rb_cNumeric']
					when :c
						['c_ComplexBuffer', 'rb_cComplex']
					else
						raise "expecting only :f or :c"
					end
				}.flatten.sort do |a, b| 
					if a == b then
						0
					elsif a =~ /Complex/ then
						-1
					elsif b =~ /Complex/ then
						1
					else
						a <=> b
					end
				end
				definition += "	if (#{othertypes.collect{|s| "rb_obj_is_kind_of(other, #{s})"}.join(' || ')}) {\n"
				definition += <<EOS
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_ComplexBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_ComplexBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_ComplexBuffer);
		rcbuf = paddlec_complex_buffer_get_struct(buffer);
EOS
				definition += '		' + rettype[:complex].sort{|ntda, ntdb|
					a = ntda.to_s.split('').last.to_sym
					b = ntdb.to_s.split('').last.to_sym
					if a == b then
						0
					elsif a == :c then
						-1
					elsif b == :c then
						1
					else
						a <=> b
					end
				}.collect{|ntdoubleton|
					case ntdoubleton.to_s.split('').last.to_sym
					when :f
						<<EOS
if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_#{receiver}b_fb_#{h[:pdlc_name]}(m#{receiver}buf, ofbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_#{receiver}b_fs_#{h[:pdlc_name]}(m#{receiver}buf, of, rcbuf);
		}
EOS
					when :c
						<<EOS
if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_#{receiver}b_cb_#{h[:pdlc_name]}(m#{receiver}buf, ocbuf, rcbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_#{receiver}b_cs_#{h[:pdlc_name]}(m#{receiver}buf, oc, rcbuf);
		}
EOS
					else
						raise "expecting only :f or :c"
					end
				}.join("		else ")
				definition += <<EOS
	}
EOS
			end

			unless rettype[:float].empty? then
				definition += rettype[:complex].empty? ? '	' : '	else '
				othertypes = rettype[:float].collect{|ntdoubleton|
					case ntdoubleton.to_s.split('').last.to_sym
					when :f
						['c_FloatBuffer', 'rb_cNumeric']
					when :c
						['c_ComplexBuffer', 'rb_cComplex']
					else
						raise "expecting only :f or :c"
					end
				}.flatten.sort do |a, b| 
					if a == b then
						0
					elsif a =~ /Complex/ then
						-1
					elsif b =~ /Complex/ then
						1
					else
						a <=> b
					end
				end
				definition += "if (#{othertypes.collect{|s| "rb_obj_is_kind_of(other, #{s})"}.join(' || ')}) {\n"
				definition += <<EOS
		if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_FloatBuffer)) {
			rb_warn("second argument (buffer) is expected to be a %"PRIsVALUE", not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(buffer)));
			buffer = Qnil;
		}
		if (buffer == Qnil)
			buffer = rb_class_new_instance(0, NULL, c_FloatBuffer);
		rfbuf = paddlec_float_buffer_get_struct(buffer);
EOS
				definition += '		' + rettype[:float].sort{|ntda, ntdb|
					a = ntda.to_s.split('').last.to_sym
					b = ntdb.to_s.split('').last.to_sym
					if a == b then
						0
					elsif a == :c then
						-1
					elsif b == :c then
						1
					else
						a <=> b
					end
				}.collect{|ntdoubleton|
					case ntdoubleton.to_s.split('').last.to_sym
					when :f
						<<EOS
if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
			ofbuf = paddlec_float_buffer_get_struct(other);
			pdlc_#{receiver}b_fb_#{h[:pdlc_name]}(m#{receiver}buf, ofbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
			of = (float)NUM2DBL(other);
			pdlc_#{receiver}b_fs_#{h[:pdlc_name]}(m#{receiver}buf, of, rfbuf);
		}
EOS
					when :c
						<<EOS
if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
			ocbuf = paddlec_complex_buffer_get_struct(other);
			pdlc_#{receiver}b_cb_#{h[:pdlc_name]}(m#{receiver}buf, ocbuf, rfbuf);
		}
		else if (rb_obj_is_kind_of(other, rb_cComplex)) {
			oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
			oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
			pdlc_#{receiver}b_cs_#{h[:pdlc_name]}(m#{receiver}buf, oc, rfbuf);
		}
EOS
					else
						raise "expecting only :f or :c"
					end
				}.join("		else ")
				definition += <<EOS
	}
EOS
			end

			othertypes = (rettype[:complex] + rettype[:float]).collect{|ntdoubleton|
				case ntdoubleton.to_s.split('').last.to_sym
				when :f
					['c_FloatBuffer', 'rb_cNumeric']
				when :c
					['c_ComplexBuffer', 'rb_cComplex']
				else
					raise "expecting only :f or :c"
				end
			}.flatten.uniq.sort do |a, b| 
				if a == b then
					0
				elsif a =~ /Complex/ then
					-1
				elsif b =~ /Complex/ then
					1
				else
					a <=> b
				end
			end
			definition += <<EOS
	else
		rb_raise(rb_eTypeError, "First argument is expected to be #{othertypes.length.times.collect{"a %\"PRIsVALUE\""}.join(' or ')}, not a %"PRIsVALUE, #{othertypes.collect{|s| "rb_class_name(#{s})"}.join(', ')}, rb_class_name(rb_class_of(other)));

	return buffer;
}
EOS

			if receiver == :f then
				paddlec_float_c.puts
				paddlec_float_c.puts definition
				paddlec_float_c.puts
				paddlec_float_rb.puts "	rb_define_method(c_FloatBuffer, #{"\"#{h[:method]}\",".ljust(21)}#{"paddlec_float_buffer_#{h[:pdlc_name]},".ljust(41)}-1);\n"
				if h[:alias] then
					(h[:alias].is_a?(Array) ? h[:alias] : [h[:alias]]).each do |al|
						paddlec_float_rb.puts "	rb_define_alias(c_FloatBuffer,  \"#{al}\", \"#{h[:method]}\");\n" 
					end
				end
			else
				paddlec_complex_c.puts
				paddlec_complex_c.puts definition
				paddlec_complex_c.puts
				paddlec_complex_rb.puts "	rb_define_method(c_ComplexBuffer, #{"\"#{h[:method]}\",".ljust(22)}#{"paddlec_complex_buffer_#{h[:pdlc_name]},".ljust(41)}-1);\n"
				if h[:alias] then
					(h[:alias].is_a?(Array) ? h[:alias] : [h[:alias]]).each do |al|
						paddlec_complex_rb.puts "	rb_define_alias(c_ComplexBuffer,  \"#{al}\", \"#{h[:method]}\");\n"
					end
				end
			end


			## In place ############################################################


			next unless h[:inplace]

			inpntdoubletons = rettype[(receiver == :f) ? :float : :complex]
			#puts "#{receiver} #{h[:inplace]} -> #{inpntdoubletons.inspect}"
			next if inpntdoubletons.empty?
			othertypes = inpntdoubletons.collect{|ntdoubleton|
				case ntdoubleton.to_s.split('').last.to_sym
				when :f
					['c_FloatBuffer', 'rb_cNumeric']
				when :c
					['c_ComplexBuffer', 'rb_cComplex']
				else
					raise "expecting only :f or :c"
				end
			}.flatten.uniq.sort do |a, b| 
				if a == b then
					0
				elsif a =~ /Complex/ then
					-1
				elsif b =~ /Complex/ then
					1
				else
					a <=> b
				end
			end

			definition = <<EOS
/* #{h[:doc].nil? ? '' : (h[:doc].split(/\s*\n\s*/).join("\n * ") + "\n * ")}@param other [#{othertypes.join(', ')}]
 * @return [self]
 */
static VALUE paddlec_#{receiver == :f ? 'float' : 'complex'}_buffer_#{h[:pdlc_name]}_inplace(VALUE self, VALUE other)
{
	pdlc_#{receiver == :f ? '' : 'complex_'}buffer_t *m#{receiver}buf;
EOS
			othertypes = inpntdoubletons.collect{|ntdoubleton| ntdoubleton.to_s.split('').last.to_sym}.uniq
			if othertypes.include?(:f) then
				definition += "	const pdlc_buffer_t *ofbuf;\n"
				definition += "	float of;\n"
			end
			if othertypes.include?(:c) then
				definition += "	const pdlc_complex_buffer_t *ocbuf;\n"
				definition += "	pdlc_complex_t oc;\n"
			end
			definition += <<EOS

	m#{receiver}buf = paddlec_#{receiver == :f ? 'float' : 'complex'}_buffer_get_struct(self);

EOS
			
			definition += '	' + inpntdoubletons.sort{|ntda, ntdb|
				a = ntda.to_s.split('').last.to_sym
				b = ntdb.to_s.split('').last.to_sym
				if a == b then
					0
				elsif a == :c then
					-1
				elsif b == :c then
					1
				else
					a <=> b
				end
			}.collect{|ntdoubleton|
				case ntdoubleton.to_s.split('').last.to_sym
				when :f
					<<EOS
if (rb_obj_is_kind_of(other, c_FloatBuffer)) {
		ofbuf = paddlec_float_buffer_get_struct(other);
		pdlc_#{receiver}b_fb_#{h[:pdlc_name]}_inplace(m#{receiver}buf, ofbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cNumeric)) {
		of = (float)NUM2DBL(other);
		pdlc_#{receiver}b_fs_#{h[:pdlc_name]}_inplace(m#{receiver}buf, of);
	}
EOS
				when :c
					<<EOS
if (rb_obj_is_kind_of(other, c_ComplexBuffer)) {
		ocbuf = paddlec_complex_buffer_get_struct(other);
		pdlc_#{receiver}b_cb_#{h[:pdlc_name]}_inplace(m#{receiver}buf, ocbuf);
	}
	else if (rb_obj_is_kind_of(other, rb_cComplex)) {
		oc.real = (float)NUM2DBL(rb_funcallv(other, id_real, 0, NULL));
		oc.imag = (float)NUM2DBL(rb_funcallv(other, id_imag, 0, NULL));
		pdlc_#{receiver}b_cs_#{h[:pdlc_name]}_inplace(m#{receiver}buf, oc);
	}
EOS
				else
					raise "expecting only :f or :c"
				end
			}.join("	else ")

			othertypes.collect!{|s| case s when :f then ['c_FloatBuffer', 'rb_cNumeric'] when :c then ['c_ComplexBuffer', 'rb_cComplex'] end}.flatten!.uniq!
			definition += <<EOS
	else
		rb_raise(rb_eTypeError, "expecting #{othertypes.length.times.collect{"a %\"PRIsVALUE\""}.join(' or ')}, not a %"PRIsVALUE, #{othertypes.collect{|s| "rb_class_name(#{s})"}.join(', ')}, rb_class_name(rb_class_of(other)));

	return self;
}
EOS

			if receiver == :f then
				paddlec_float_c.puts
				paddlec_float_c.puts definition
				paddlec_float_c.puts
				paddlec_float_rb.puts "	rb_define_method(c_FloatBuffer, #{"\"#{h[:inplace]}\",".ljust(21)}#{"paddlec_float_buffer_#{h[:pdlc_name]}_inplace,".ljust(42)}1);\n"
			else
				paddlec_complex_c.puts
				paddlec_complex_c.puts definition
				paddlec_complex_c.puts
				paddlec_complex_rb.puts "	rb_define_method(c_ComplexBuffer, #{"\"#{h[:inplace]}\",".ljust(22)}#{"paddlec_complex_buffer_#{h[:pdlc_name]}_inplace,".ljust(42)}1);\n"
			end
		end
	end


	## One argument ############################################################
	
	[:f, :c].each do |nt|
		spec = h[nt]
		next if spec.nil?
		tmp = not(not(spec =~ /tmp/))
		ctmp = not(not(spec =~ /tmp\.real/ or spec =~ /tmp\.imag/))
		complex_result = not(not(spec =~ /res\.real/ or spec =~ /res\.imag/ or (tmp and ctmp and spec =~ /res\s*=\s*tmp(?!\.(real|imag))/)))
		complex_param = not(not(spec =~ /param\.real/ or spec =~ /param\.imag/))
		#puts "#{h[:pdlc_name]} -> #{complex_result ? 'complex' : 'float'} = f(#{complex_param ? 'complex' : 'float'})"
		tmp = not(not(spec =~ /tmp/))
		ctmp = not(not(spec =~ /tmp\.real/) and not(spec =~ /tmp\.imag/))

		[:new, :inplace].each do |out|
			next if (out == :inplace and complex_result != complex_param)
			declaration = "pdlc_#{complex_result ? 'complex_':''}buffer_t* pdlc_#{complex_param ? 'complex_':''}buffer_#{h[:pdlc_name]}#{out == :inplace ? '_inplace':''}(#{out == :new ? 'const ':''}pdlc_#{complex_param ? 'complex_':''}buffer_t *#{complex_param ? 'c':'f'}buf#{out == :new ? ", pdlc_#{complex_result ? 'complex_':''}buffer_t *o#{complex_result ? 'c':'f'}buf":''})"

			lines = spec.split(/(?<!\|)\s*\|\s*(?!\|)/).collect do |s| 
				l = s
				if out == :new then
					l = l.gsub(/res/, "o#{complex_result ? 'c':'f'}buf->data[i]")
				else
					l = l.gsub(/res/, "#{complex_param ? 'c':'f'}buf->data[i]")
				end
				l = l.gsub(/param/, "#{complex_param ? 'c':'f'}buf->data[i]")
				l = "\t\t#{l};\n"
			end


			definition = <<EOS
#{declaration}
{
	size_t i;
EOS
			definition += "	#{ctmp ? 'pdlc_complex_t' : 'float'} tmp;\n" if tmp
			definition += <<EOS

EOS
			if out == :new then
				definition += <<EOS
	if (!o#{complex_result ? 'c':'f'}buf)
		o#{complex_result ? 'c':'f'}buf = pdlc_#{complex_result ? 'complex_':''}buffer_new(#{complex_param ? 'c':'f'}buf->length);
	else
		pdlc_#{complex_result ? 'complex_':''}buffer_resize(o#{complex_result ? 'c':'f'}buf, #{complex_param ? 'c':'f'}buf->length, 0);

EOS
			end

			definition += <<EOS
	for (i = 0; i < #{complex_param ? 'c':'f'}buf->length; i++)#{lines.length > 1 ? ' {':''}
#{lines.join}#{lines.length > 1 ? "\t}\n":''}
	return #{out == :new ? 'o':''}#{complex_result ? 'c':'f'}buf;
}
EOS
			pdlc_h.puts declaration + ';'
			pdlc_c.puts
			pdlc_c.puts definition
			pdlc_c.puts


			next if out == :inplace and not(h[:inplace])
			if out == :new then
				definition = <<EOS
/* #{h[:doc].nil? ? '' : (h[:doc].split(/\s*\n\s*/).join("\n * ") + "\n * ")}@overload #{h[:method]}(buffer = nil)
 * @param buffer [nil, PaddleC::#{complex_result ? 'Complex' : 'Float'}Buffer] if provided, the result is written into +buffer+.
 * @return [PaddleC::#{complex_result ? 'Complex' : 'Float'}Buffer]
 */
static VALUE paddlec_#{complex_param ? 'complex' : 'float'}_buffer_#{h[:pdlc_name]}(int argc, VALUE *argv, VALUE self)
{
	const pdlc_#{complex_param ? 'complex_' : ''}buffer_t *m#{complex_param ? 'c' : 'f'}buf;
	pdlc_#{complex_result ? 'complex_' : ''}buffer_t *r#{complex_result ? 'c' : 'f'}buf;
	VALUE buffer;

	m#{complex_param ? 'c' : 'f'}buf = paddlec_#{complex_param ? 'complex' : 'float'}_buffer_get_struct(self);

	rb_scan_args(argc, argv, "01", &buffer);

	if (buffer != Qnil && !rb_obj_is_kind_of(buffer, c_#{complex_result ? 'Complex' : 'Float'}Buffer)) {
		rb_warn("expecting a %"PRIsVALUE" as buffer, not a %"PRIsVALUE, rb_class_name(c_#{complex_result ? 'Complex' : 'Float'}Buffer), rb_class_name(rb_class_of(buffer)));
		buffer = Qnil;
	}
	if (buffer == Qnil)
		buffer = rb_class_new_instance(0, NULL, c_#{complex_result ? 'Complex' : 'Float'}Buffer);
	r#{complex_result ? 'c' : 'f'}buf = paddlec_#{complex_result ? 'complex' : 'float'}_buffer_get_struct(buffer);

	pdlc_#{complex_param ? 'complex_':''}buffer_#{h[:pdlc_name]}(m#{complex_param ? 'c':'f'}buf, r#{complex_result ? 'c':'f'}buf);

	return buffer;
}
EOS
			else
				definition = <<EOS
/* #{h[:doc].nil? ? '' : (h[:doc].split(/\s*\n\s*/).join("\n * ") + "\n * ")}@return [self]
 */
static VALUE paddlec_#{complex_param ? 'complex' : 'float'}_buffer_#{h[:pdlc_name]}_inplace(VALUE self)
{
	pdlc_#{complex_param ? 'complex_' : ''}buffer_t *m#{complex_param ? 'c' : 'f'}buf;

	m#{complex_param ? 'c' : 'f'}buf = paddlec_#{complex_param ? 'complex' : 'float'}_buffer_get_struct(self);

	pdlc_#{complex_param ? 'complex_':''}buffer_#{h[:pdlc_name]}_inplace(m#{complex_param ? 'c':'f'}buf);

	return self;
}
EOS
			end

			if complex_param then
				paddlec_complex_c.puts
				paddlec_complex_c.puts definition
				paddlec_complex_c.puts
				if out == :new then
					paddlec_complex_rb.puts "	rb_define_method(c_ComplexBuffer, #{"\"#{h[:method]}\",".ljust(22)}#{"paddlec_complex_buffer_#{h[:pdlc_name]},".ljust(41)}-1);\n"
					if h[:alias] then
						(h[:alias].is_a?(Array) ? h[:alias] : [h[:alias]]).each do |al|
							paddlec_complex_rb.puts "	rb_define_alias(c_ComplexBuffer,  \"#{al}\", \"#{h[:method]}\");\n" 
						end
					end
				else
					paddlec_complex_rb.puts "	rb_define_method(c_ComplexBuffer, #{"\"#{h[:inplace]}\",".ljust(22)}#{"paddlec_complex_buffer_#{h[:pdlc_name]}_inplace,".ljust(42)}0);\n"
				end
			else
				paddlec_float_c.puts
				paddlec_float_c.puts definition
				paddlec_float_c.puts
				if out == :new then
					paddlec_float_rb.puts "	rb_define_method(c_FloatBuffer, #{"\"#{h[:method]}\",".ljust(21)}#{"paddlec_float_buffer_#{h[:pdlc_name]},".ljust(41)}-1);\n"
					if h[:alias] then
						(h[:alias].is_a?(Array) ? h[:alias] : [h[:alias]]).each do |al|
							paddlec_float_rb.puts "	rb_define_alias(c_FloatBuffer,  \"#{al}\", \"#{h[:method]}\");\n" 
						end
					end
				else
					paddlec_float_rb.puts "	rb_define_method(c_FloatBuffer, #{"\"#{h[:inplace]}\",".ljust(21)}#{"paddlec_float_buffer_#{h[:pdlc_name]}_inplace,".ljust(42)}0);\n"
				end
			end
		end
	end
end

pdlc_c.puts '// End generated code'
pdlc_h.puts '// End generated code'
paddlec_float_c.puts '// End generated code'
paddlec_float_rb.puts '// End generated code'
paddlec_complex_c.puts '// End generated code'
paddlec_complex_rb.puts '// End generated code'

pdlc_c.close
pdlc_h.close
paddlec_float_c.close
paddlec_float_rb.close
paddlec_complex_c.close
paddlec_complex_rb.close

