#!/usr/bin/env ruby

defs = [
	{
		pdlc_name: :acos,
		method: :acos,
		inplace: :acos!,
		f: 'res = acosf(param)',
		doc: 'Computes the arc cosine of x. Returns 0..PI.'
	},
	{
		pdlc_name: :acosh,
		method: :acosh,
		inplace: :acosh!,
		f: 'res = acoshf(param)',
		doc: 'Computes the inverse hyperbolic cosine of x.'
	},
	{
		pdlc_name: :asin,
		method: :asin,
		inplace: :asin!,
		f: 'res = asinf(param)',
		doc: 'Computes the arc sine of x. Returns -PI/2..PI/2.'
	},
	{
		pdlc_name: :asinh,
		method: :asinh,
		inplace: :asinh!,
		f: 'res = asinhf(param)',
		doc: 'Computes the inverse hyperbolic sine of x.'
	},
	{
		pdlc_name: :atan,
		method: :atan,
		inplace: :atan!,
		f: 'res = atanf(param)',
		doc: 'Computes the arc tangent of x.'
	},
	{
		pdlc_name: :atanh,
		method: :atanh,
		inplace: :atanh!,
		f: 'res = atanhf(param)',
		doc: 'Computes the inverse hyperbolic tangent of x.'
	},
	{
		pdlc_name: :cbrt,
		method: :cbrt,
		inplace: :cbrt!,
		f: 'res = cbrtf(param)',
		doc: 'Returns the cube root of x.'
	},
	{
		pdlc_name: :cos,
		method: :cos,
		inplace: :cos!,
		f: 'res = cosf(param)',
		doc: 'Computes the cosine of x (expressed in radians). Returns a Float in the range -1.0..1.0.'
	},
	{
		pdlc_name: :cosh,
		method: :cosh,
		inplace: :cosh!,
		f: 'res = coshf(param)',
		doc: 'Computes the hyperbolic cosine of x.'
	},
	{
		pdlc_name: :erf,
		method: :erf,
		inplace: :erf!,
		f: 'res = erff(param)',
		doc: 'Calculates the error function of x.'
	},
	{
		pdlc_name: :erfc,
		method: :erfc,
		inplace: :erfc!,
		f: 'res = erfcf(param)',
		doc: 'Calculates the complementary error function of x.'
	},
	{
		pdlc_name: :exp,
		method: :exp,
		inplace: :exp!,
		f: 'res = expf(param)',
		doc: 'Calculates the value of e (the base of natural logarithms) raised to the power of x.'
	},
	{
		pdlc_name: :log,
		method: :log,
		inplace: :log!,
		f: 'res = logf(param)',
		doc: 'Calculates the natural logarithm of x.'
	},
	{
		pdlc_name: :log10,
		method: :log10,
		inplace: :log10!,
		f: 'res = log10f(param)',
		doc: 'Calculates the base 10 logarithm of x.'
	},
	{
		pdlc_name: :log2,
		method: :log2,
		inplace: :log2!,
		f: 'res = log2f(param)',
		doc: 'Calculate the base 2 logarithm of x.'
	},
	{
		pdlc_name: :sin,
		method: :sin,
		inplace: :sin!,
		f: 'res = sinf(param)',
		doc: 'Computes the sine of x (expressed in radians). Returns a Float in the range -1.0..1.0.'
	},
	{
		pdlc_name: :sinh,
		method: :sinh,
		inplace: :sinh!,
		f: 'res = sinhf(param)',
		doc: 'Computes the hyperbolic sine of x.'
	},
	{
		pdlc_name: :sqrt,
		method: :sqrt,
		inplace: :sqrt!,
		f: 'res = sqrtf(param)',
		doc: 'Computes the non-negative square root of x.'
	},
	{
		pdlc_name: :tan,
		method: :tan,
		inplace: :tan!,
		f: 'res = tanf(param)',
		doc: 'Computes the tangent of x (expressed in radians).'
	},
	{
		pdlc_name: :tanh,
		method: :tanh,
		inplace: :tanh!,
		f: 'res = tanhf(param)',
		doc: 'Computes the hyperbolic tangent of x.'
	}
]


paddlec_c = File.open('paddlec_math.c', 'w')
paddlec_rb = File.open('paddlec_math_methods.c', 'w')

paddlec_c.puts  '// Start generated code'
paddlec_rb.puts '// Start generated code'


defs.each do |h|
	func = h[:f].sub(/^res\s+=\s+/, '').sub(/\(param\)\s*/, '')
	definition = <<EOS
/* #{h[:doc]}
 * @return [PaddleC::FloatBuffer, Float]
 * @overload #{h[:method]}(x)
 *  @param x [PaddleC::FloatBuffer]
 *  @return [PaddleC::FloatBuffer]
 * @overload #{h[:method]}(x)
 *  @param x [Float]
 *  @return [Float]
 */
static VALUE paddlec_math_#{h[:method]}(VALUE self, VALUE x)
{
	VALUE res;
	float sf;

	if (rb_obj_is_kind_of(x, c_FloatBuffer))
		res = rb_funcallv(x, id_#{h[:method]}, 0, NULL);
	else if (rb_obj_is_kind_of(x, rb_cNumeric) && !rb_obj_is_kind_of(x, rb_cComplex)) {
		sf = (float)NUM2DBL(x);
		res = DBL2NUM((double)#{func}(sf));
	}
	else
		rb_raise(rb_eTypeError, "expecting a %"PRIsVALUE" or a non complex Numeric, not a %"PRIsVALUE, rb_class_name(c_FloatBuffer), rb_class_name(rb_class_of(x)));

	return res;
}
EOS
	paddlec_c.puts
	paddlec_c.puts definition
	paddlec_c.puts
end

paddlec_rb.puts
defs.each do |h|
	paddlec_rb.puts "ID id_#{h[:method]};"
end
paddlec_rb.puts
defs.each do |h|
	paddlec_rb.puts "	id_#{h[:method]} = rb_intern(\"#{h[:method]}\");"
end
paddlec_rb.puts
defs.each do |h|
	paddlec_rb.puts "	rb_define_module_function(m_PaddleC, #{"\"#{h[:method]}\",".ljust(15)}#{"paddlec_math_#{h[:method]},".ljust(29)}1);"
end
paddlec_rb.puts

paddlec_c.puts '// End generated code'
paddlec_rb.puts '// End generated code'

paddlec_c.close
paddlec_rb.close

