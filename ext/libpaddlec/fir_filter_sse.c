/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <xmmintrin.h>
#ifdef __SSE3__
#include <pmmintrin.h>
#endif


void pdlc_fir_filter_inspect(pdlc_fir_filter_t* fir)
{
	size_t i, j;

	printf("nb_coefs: %u, state_len: %u, coef_len: %u, index_mask: 0x%x, index: %u\n",
			fir->nb_coefs, fir->state_len, fir->coef_len, fir->index_mask, fir->index);
	printf("state:    [%.7g", fir->stater[0]);
	for (i = 1; i < fir->state_len; i++)
		printf(", %.7g", fir->stater[i]);
	printf("]\n");
	for (j = 0; j < 4; j++) {
		printf("coefs: {%lu}[%.7g", j, fir->coefs[j][0]);
		for (i = 1; i < fir->coef_len; i++)
			printf(", %.7g", fir->coefs[j][i]);
		printf("]\n");
	}
}


void pdlc_fir_filter_initialize(pdlc_fir_filter_t* fir, int order)
{
	int i;

	if (fir->coefs) {
		for (i = 0; i < 4; i++)
			if (fir->coefs[i])
				_mm_free(fir->coefs[i]);
		free(fir->coefs);
		fir->coefs = NULL;
	}

	if (fir->stater)
		_mm_free(fir->stater);
	fir->stater = NULL;

	if (fir->statei)
		_mm_free(fir->statei);
	fir->statei = NULL;

	fir->nb_coefs = 0;
	fir->state_len = 0;
	fir->coef_len = 0;
	fir->index = 0;
	fir->index_mask = 0;
	fir->counter = 0;
	fir->max_counter = 1;

	if (order < 0)
		return;

	if (order > 67108863) {
		fprintf(stderr, "ERROR: libpaddlec: Filter order cannot be greater than 67108864\n");
		exit(EXIT_FAILURE);
	}

	fir->nb_coefs = (unsigned int)(order + 1);
	fir->coef_len = ((fir->nb_coefs + 3 + 3) >> 2) << 2;
	fir->state_len = (unsigned int)(pow(2.0, ceil(log2(fir->coef_len))));
	fir->index = 0;
	fir->index_mask = fir->state_len - 1;

	fir->coefs = malloc(4*sizeof(float*));
	if (fir->coefs == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", 4 * sizeof(float*));
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < 4; i++) {
		fir->coefs[i] = _mm_malloc(fir->coef_len * sizeof(float), sizeof(__m128));
		if (fir->coefs[i] == NULL) {
			fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->coef_len * sizeof(float));
			exit(EXIT_FAILURE);
		}
	}

	fir->stater = _mm_malloc(fir->state_len * sizeof(float), sizeof(__m128));
	if (fir->stater == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->state_len * sizeof(float));
		exit(EXIT_FAILURE);
	}

	fir->statei = _mm_malloc(fir->state_len * sizeof(float), sizeof(__m128));
	if (fir->statei == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->state_len * sizeof(float));
		exit(EXIT_FAILURE);
	}

	memset(fir->stater,   0, fir->state_len * sizeof(float));
	memset(fir->statei,   0, fir->state_len * sizeof(float));
	for (i = 0; i < 4; i++)
		memset(fir->coefs[i], 0, fir->coef_len  * sizeof(float));
}


void pdlc_fir_filter_free(pdlc_fir_filter_t* fir)
{
	int i;

	if (!fir)
		return;

	if (fir->coefs) {
		for (i = 0; i < 4; i++)
			if (fir->coefs[i])
				_mm_free(fir->coefs[i]);
		free(fir->coefs);
	}

	if (fir->stater)
		_mm_free(fir->stater);

	if (fir->statei)
		_mm_free(fir->statei);

	free(fir);
}


size_t pdlc_fir_filter_size(pdlc_fir_filter_t* fir)
{
	size_t res;

	res  = sizeof(pdlc_fir_filter_t);
	res += sizeof(float*)* 4;
	res += sizeof(float) * fir->state_len * 2;
	res += sizeof(float) * fir->coef_len * 4;

	return res;
}


int pdlc_fir_filter_set_coef_at(pdlc_fir_filter_t* fir, int index, float  value)
{
	int i;

	if (index < 0 || index >= (int)fir->nb_coefs)
		return -1;

	for (i = 0; i < 4; i++)
		fir->coefs[i][(fir->nb_coefs - 1 - index + i) % fir->coef_len] = value;

	return 0;
}


float pdlc_fir_filter_filter_float(pdlc_fir_filter_t* fir, float sample, float *delayed)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	const unsigned int middle_index = (start_index + nb_coefs / 2) & mask;
	const unsigned int lensimd = fir->coef_len >> 2;
	const unsigned int startsimd = start_index >> 2;
	const unsigned int masksimd = mask >> 2;
	unsigned int i, j;
	register __m128 acc, prod;
	const __m128 *coefs = (__m128*)fir->coefs[start_index & 3];
	__m128 *stater = (__m128*)fir->stater;

	fir->stater[fir->index] = sample;
	fir->index = (fir->index + 1) & mask;

	if (delayed) {
		if (nb_coefs & 1)
			*delayed = fir->stater[middle_index];
		else
			*delayed = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
	}

	acc  = _mm_setzero_ps();
	j = startsimd;
	for (i = 0; i < lensimd; i++) {
		prod = _mm_mul_ps(coefs[i], stater[j]);
		acc  = _mm_add_ps(acc, prod);
		j = (j+1) & masksimd;
	}

	return acc[0] + acc[1] + acc[2] + acc[3];
}


pdlc_complex_t pdlc_fir_filter_filter_complex(pdlc_fir_filter_t* fir, pdlc_complex_t sample, pdlc_complex_t *delayed)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	const unsigned int middle_index = (start_index + nb_coefs / 2) & mask;
	const unsigned int lensimd = fir->coef_len >> 2;
	const unsigned int startsimd = start_index >> 2;
	const unsigned int masksimd = mask >> 2;
	unsigned int i, j;
	pdlc_complex_t res = {0.0f, 0.0f};
	register __m128 accr, acci, prodr, prodi;
	const __m128 *coefs = (__m128*)fir->coefs[start_index & 3];
	__m128 *stater = (__m128*)fir->stater;
	__m128 *statei = (__m128*)fir->statei;

	fir->stater[fir->index] = sample.real;
	fir->statei[fir->index] = sample.imag;
	fir->index = (fir->index + 1) & mask;

	accr = _mm_setzero_ps();
	acci = _mm_setzero_ps();
	j = startsimd;
	for (i = 0; i < lensimd; i++) {
		prodr = _mm_mul_ps(coefs[i], stater[j]);
		prodi = _mm_mul_ps(coefs[i], statei[j]);
		accr  = _mm_add_ps(accr, prodr);
		acci  = _mm_add_ps(acci, prodi);
		j = (j+1) & masksimd;
	}
	res.real = accr[0] + accr[1] + accr[2] + accr[3];
	res.imag = acci[0] + acci[1] + acci[2] + acci[3];

	if (delayed) {
		if (nb_coefs & 1) {
			delayed->real = fir->stater[middle_index];
			delayed->imag = fir->statei[middle_index];
		}
		else {
			delayed->real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			delayed->imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
		}
	}

	return res;
}


pdlc_buffer_t* pdlc_fir_filter_filter_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf, pdlc_buffer_t *delayed)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int masksimd = mask >> 2;
	const size_t ibuflen = ifbuf->length;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 2;
	unsigned int middle_index;
	unsigned int i, j;
	size_t k;
	register __m128 acc0, acc1, acc2, acc3;
	register __m128 prod0, prod1, prod2, prod3;
	register __m128 statereal;
	const __m128 *coefs0, *coefs1, *coefs2, *coefs3;
	__m128 *stater = (__m128*)fir->stater;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(ibuflen);
	else if (ofbuf->length != ibuflen)
		pdlc_buffer_resize(ofbuf, ibuflen, 0);

	if (delayed) {
		if (delayed->length != ibuflen)
			pdlc_buffer_resize(delayed, ibuflen, 0);
		middle_index = (start_index + nb_coefs / 2) & mask;
		if (nb_coefs & 1) {
			k = 0;
			while ((start_index & 3) && k < ibuflen) {
				fir->stater[fir->index] = ifbuf->data[k];
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m128*)fir->coefs[start_index & 3];
				acc0 = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					prod0 = _mm_mul_ps(coefs0[i], stater[j]);
					acc0  = _mm_add_ps(acc0, prod0);
					j = (j+1) & masksimd;
				}
				ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 2;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			while (k + 4 <= ibuflen) {
				fir->stater[ fir->index            ] = ifbuf->data[k + 0];
				fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
				fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
				fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
				fir->index = (fir->index + 4) & mask;
				coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
				coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
				coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
				coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
				acc0 = _mm_setzero_ps();
				acc1 = _mm_setzero_ps();
				acc2 = _mm_setzero_ps();
				acc3 = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					statereal = stater[j];
					prod0 = _mm_mul_ps(coefs0[i], statereal);
					acc0  = _mm_add_ps(acc0, prod0);
					prod1 = _mm_mul_ps(coefs1[i], statereal);
					acc1  = _mm_add_ps(acc1, prod1);
					prod2 = _mm_mul_ps(coefs2[i], statereal);
					acc2  = _mm_add_ps(acc2, prod2);
					prod3 = _mm_mul_ps(coefs3[i], statereal);
					acc3  = _mm_add_ps(acc3, prod3);
					j = (j+1) & masksimd;
				}
#ifdef __SSE3__
				acc0 = _mm_hadd_ps(acc0, acc1);
				acc2 = _mm_hadd_ps(acc2, acc3);
				acc0 = _mm_hadd_ps(acc0, acc2);
				ofbuf->data[k+0] = acc0[0];
				ofbuf->data[k+1] = acc0[1];
				ofbuf->data[k+2] = acc0[2];
				ofbuf->data[k+3] = acc0[3];
#else
				ofbuf->data[k+0] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
				ofbuf->data[k+1] = acc1[0] + acc1[1] + acc1[2] + acc1[3];
				ofbuf->data[k+2] = acc2[0] + acc2[1] + acc2[2] + acc2[3];
				ofbuf->data[k+3] = acc3[0] + acc3[1] + acc3[2] + acc3[3];
#endif
				start_index = (start_index + 4) & mask;
				startsimd = start_index >> 2;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			for (; k < ibuflen; k++) {
				fir->stater[fir->index] = ifbuf->data[k];
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m128*)fir->coefs[start_index & 3];
				acc0 = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					prod0 = _mm_mul_ps(coefs0[i], stater[j]);
					acc0  = _mm_add_ps(acc0, prod0);
					j = (j+1) & masksimd;
				}
				ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 2;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
			}
		}
		else {
			k = 0;
			while ((start_index & 3) && k < ibuflen) {
				fir->stater[fir->index] = ifbuf->data[k];
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m128*)fir->coefs[start_index & 3];
				acc0 = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					prod0 = _mm_mul_ps(coefs0[i], stater[j]);
					acc0  = _mm_add_ps(acc0, prod0);
					j = (j+1) & masksimd;
				}
				ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 2;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			while (k + 4 <= ibuflen) {
				fir->stater[ fir->index            ] = ifbuf->data[k + 0];
				fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
				fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
				fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
				fir->index = (fir->index + 4) & mask;
				coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
				coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
				coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
				coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
				acc0 = _mm_setzero_ps();
				acc1 = _mm_setzero_ps();
				acc2 = _mm_setzero_ps();
				acc3 = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					statereal = stater[j];
					prod0 = _mm_mul_ps(coefs0[i], statereal);
					acc0  = _mm_add_ps(acc0, prod0);
					prod1 = _mm_mul_ps(coefs1[i], statereal);
					acc1  = _mm_add_ps(acc1, prod1);
					prod2 = _mm_mul_ps(coefs2[i], statereal);
					acc2  = _mm_add_ps(acc2, prod2);
					prod3 = _mm_mul_ps(coefs3[i], statereal);
					acc3  = _mm_add_ps(acc3, prod3);
					j = (j+1) & masksimd;
				}
#ifdef __SSE3__
				acc0 = _mm_hadd_ps(acc0, acc1);
				acc2 = _mm_hadd_ps(acc2, acc3);
				acc0 = _mm_hadd_ps(acc0, acc2);
				ofbuf->data[k+0] = acc0[0];
				ofbuf->data[k+1] = acc0[1];
				ofbuf->data[k+2] = acc0[2];
				ofbuf->data[k+3] = acc0[3];
#else
				ofbuf->data[k+0] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
				ofbuf->data[k+1] = acc1[0] + acc1[1] + acc1[2] + acc1[3];
				ofbuf->data[k+2] = acc2[0] + acc2[1] + acc2[2] + acc2[3];
				ofbuf->data[k+3] = acc3[0] + acc3[1] + acc3[2] + acc3[3];
#endif
				start_index = (start_index + 4) & mask;
				startsimd = start_index >> 2;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			for (; k < ibuflen; k++) {
				fir->stater[fir->index] = ifbuf->data[k];
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m128*)fir->coefs[start_index & 3];
				acc0 = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					prod0 = _mm_mul_ps(coefs0[i], stater[j]);
					acc0  = _mm_add_ps(acc0, prod0);
					j = (j+1) & masksimd;
				}
				ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 2;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
			}
		}
	}
	else {
		k = 0;
		while ((start_index & 3) && k < ibuflen) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m128*)fir->coefs[start_index & 3];
			acc0 = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				prod0 = _mm_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm_add_ps(acc0, prod0);
				j = (j+1) & masksimd;
			}
			ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 2;
			k++;
		}
		while (k + 4 <= ibuflen) {
			fir->stater[ fir->index            ] = ifbuf->data[k + 0];
			fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
			fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
			fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
			fir->index = (fir->index + 4) & mask;
			coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
			coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
			coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
			coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
			acc0 = _mm_setzero_ps();
			acc1 = _mm_setzero_ps();
			acc2 = _mm_setzero_ps();
			acc3 = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				statereal = stater[j];
				prod0 = _mm_mul_ps(coefs0[i], statereal);
				acc0  = _mm_add_ps(acc0, prod0);
				prod1 = _mm_mul_ps(coefs1[i], statereal);
				acc1  = _mm_add_ps(acc1, prod1);
				prod2 = _mm_mul_ps(coefs2[i], statereal);
				acc2  = _mm_add_ps(acc2, prod2);
				prod3 = _mm_mul_ps(coefs3[i], statereal);
				acc3  = _mm_add_ps(acc3, prod3);
				j = (j+1) & masksimd;
			}
#ifdef __SSE3__
			acc0 = _mm_hadd_ps(acc0, acc1);
			acc2 = _mm_hadd_ps(acc2, acc3);
			acc0 = _mm_hadd_ps(acc0, acc2);
			ofbuf->data[k+0] = acc0[0];
			ofbuf->data[k+1] = acc0[1];
			ofbuf->data[k+2] = acc0[2];
			ofbuf->data[k+3] = acc0[3];
#else
			ofbuf->data[k+0] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			ofbuf->data[k+1] = acc1[0] + acc1[1] + acc1[2] + acc1[3];
			ofbuf->data[k+2] = acc2[0] + acc2[1] + acc2[2] + acc2[3];
			ofbuf->data[k+3] = acc3[0] + acc3[1] + acc3[2] + acc3[3];
#endif
			start_index = (start_index + 4) & mask;
			startsimd = start_index >> 2;
			k += 4;
		}
		for (; k < ibuflen; k++) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m128*)fir->coefs[start_index & 3];
			acc0 = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				prod0 = _mm_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm_add_ps(acc0, prod0);
				j = (j+1) & masksimd;
			}
			ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 2;
		}
	}

	return ofbuf;
}


pdlc_complex_buffer_t* pdlc_fir_filter_filter_complex_buffer(pdlc_fir_filter_t* fir, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf, pdlc_complex_buffer_t *delayed)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int masksimd = mask >> 2;
	const size_t ibuflen = icbuf->length;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 2;
	unsigned int middle_index;
	unsigned int i, j;
	size_t k;
	register __m128 acc0r, acc1r, acc2r, acc3r;
	register __m128 acc0i, acc1i, acc2i, acc3i;
	register __m128 prod0r, prod1r, prod2r, prod3r;
	register __m128 prod0i, prod1i, prod2i, prod3i;
	register __m128 statereal;
	register __m128 stateimag;
	const __m128 *coefs0, *coefs1, *coefs2, *coefs3;
	__m128 *stater = (__m128*)fir->stater;
	__m128 *statei = (__m128*)fir->statei;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(ibuflen);
	else if (ocbuf->length != ibuflen)
		pdlc_complex_buffer_resize(ocbuf, ibuflen, 0);

	if (delayed) {
		if (delayed->length != ibuflen)
			pdlc_complex_buffer_resize(delayed, ibuflen, 0);
		middle_index = (start_index + nb_coefs / 2) & mask;
		if (nb_coefs & 1) {
				//delayed->data[k] = fir->stater[middle_index];
				//middle_index = (middle_index + 1) & mask;
			k = 0;
			while ((start_index & 3) && k < ibuflen) {
				fir->stater[fir->index] = icbuf->data[k].real;
				fir->statei[fir->index] = icbuf->data[k].imag;
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m128*)fir->coefs[start_index & 3];
				acc0r = _mm_setzero_ps();
				acc0i = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					prod0r = _mm_mul_ps(coefs0[i], stater[j]);
					prod0i = _mm_mul_ps(coefs0[i], statei[j]);
					acc0r  = _mm_add_ps(acc0r, prod0r);
					acc0i  = _mm_add_ps(acc0i, prod0i);
					j = (j+1) & masksimd;
				}
				ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
				ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 2;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			while (k + 4 <= ibuflen) {
				fir->stater[ fir->index            ] = icbuf->data[k + 0].real;
				fir->stater[(fir->index + 1) & mask] = icbuf->data[k + 1].real;
				fir->stater[(fir->index + 2) & mask] = icbuf->data[k + 2].real;
				fir->stater[(fir->index + 3) & mask] = icbuf->data[k + 3].real;
				fir->statei[ fir->index            ] = icbuf->data[k + 0].imag;
				fir->statei[(fir->index + 1) & mask] = icbuf->data[k + 1].imag;
				fir->statei[(fir->index + 2) & mask] = icbuf->data[k + 2].imag;
				fir->statei[(fir->index + 3) & mask] = icbuf->data[k + 3].imag;
				fir->index = (fir->index + 4) & mask;
				coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
				coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
				coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
				coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
				acc0r = _mm_setzero_ps();
				acc0i = _mm_setzero_ps();
				acc1r = _mm_setzero_ps();
				acc1i = _mm_setzero_ps();
				acc2r = _mm_setzero_ps();
				acc2i = _mm_setzero_ps();
				acc3r = _mm_setzero_ps();
				acc3i = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					statereal = stater[j];
					prod0r = _mm_mul_ps(coefs0[i], statereal);
					acc0r  = _mm_add_ps(acc0r, prod0r);
					prod1r = _mm_mul_ps(coefs1[i], statereal);
					acc1r  = _mm_add_ps(acc1r, prod1r);
					prod2r = _mm_mul_ps(coefs2[i], statereal);
					acc2r  = _mm_add_ps(acc2r, prod2r);
					prod3r = _mm_mul_ps(coefs3[i], statereal);
					acc3r  = _mm_add_ps(acc3r, prod3r);
					stateimag = statei[j];
					prod0i = _mm_mul_ps(coefs0[i], stateimag);
					acc0i  = _mm_add_ps(acc0i, prod0i);
					prod1i = _mm_mul_ps(coefs1[i], stateimag);
					acc1i  = _mm_add_ps(acc1i, prod1i);
					prod2i = _mm_mul_ps(coefs2[i], stateimag);
					acc2i  = _mm_add_ps(acc2i, prod2i);
					prod3i = _mm_mul_ps(coefs3[i], stateimag);
					acc3i  = _mm_add_ps(acc3i, prod3i);
					j = (j+1) & masksimd;
				}
#ifdef __SSE3__
				acc0r = _mm_hadd_ps(acc0r, acc1r);
				acc0i = _mm_hadd_ps(acc0i, acc1i);
				acc2r = _mm_hadd_ps(acc2r, acc3r);
				acc2i = _mm_hadd_ps(acc2i, acc3i);
				acc0r = _mm_hadd_ps(acc0r, acc2r);
				acc0i = _mm_hadd_ps(acc0i, acc2i);
				ocbuf->data[k+0].real = acc0r[0];
				ocbuf->data[k+0].imag = acc0i[0];
				ocbuf->data[k+1].real = acc0r[1];
				ocbuf->data[k+1].imag = acc0i[1];
				ocbuf->data[k+2].real = acc0r[2];
				ocbuf->data[k+2].imag = acc0i[2];
				ocbuf->data[k+3].real = acc0r[3];
				ocbuf->data[k+3].imag = acc0i[3];
#else
				ocbuf->data[k+0].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
				ocbuf->data[k+0].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
				ocbuf->data[k+1].real = acc1r[0] + acc1r[1] + acc1r[2] + acc1r[3];
				ocbuf->data[k+1].imag = acc1i[0] + acc1i[1] + acc1i[2] + acc1i[3];
				ocbuf->data[k+2].real = acc2r[0] + acc2r[1] + acc2r[2] + acc2r[3];
				ocbuf->data[k+2].imag = acc2i[0] + acc2i[1] + acc2i[2] + acc2i[3];
				ocbuf->data[k+3].real = acc3r[0] + acc3r[1] + acc3r[2] + acc3r[3];
				ocbuf->data[k+3].imag = acc3i[0] + acc3i[1] + acc3i[2] + acc3i[3];
#endif
				start_index = (start_index + 4) & mask;
				startsimd = start_index >> 2;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			for (; k < ibuflen; k++) {
				fir->stater[fir->index] = icbuf->data[k].real;
				fir->statei[fir->index] = icbuf->data[k].imag;
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m128*)fir->coefs[start_index & 3];
				acc0r = _mm_setzero_ps();
				acc0i = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					prod0r = _mm_mul_ps(coefs0[i], stater[j]);
					acc0r  = _mm_add_ps(acc0r, prod0r);
					prod0i = _mm_mul_ps(coefs0[i], statei[j]);
					acc0i  = _mm_add_ps(acc0i, prod0i);
					j = (j+1) & masksimd;
				}
				ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
				ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 2;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
			}
		}
		else {
			k = 0;
			while ((start_index & 3) && k < ibuflen) {
				fir->stater[fir->index] = icbuf->data[k].real;
				fir->statei[fir->index] = icbuf->data[k].imag;
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m128*)fir->coefs[start_index & 3];
				acc0r = _mm_setzero_ps();
				acc0i = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					prod0r = _mm_mul_ps(coefs0[i], stater[j]);
					prod0i = _mm_mul_ps(coefs0[i], statei[j]);
					acc0r  = _mm_add_ps(acc0r, prod0r);
					acc0i  = _mm_add_ps(acc0i, prod0i);
					j = (j+1) & masksimd;
				}
				ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
				ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 2;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			while (k + 4 <= ibuflen) {
				fir->stater[ fir->index            ] = icbuf->data[k + 0].real;
				fir->stater[(fir->index + 1) & mask] = icbuf->data[k + 1].real;
				fir->stater[(fir->index + 2) & mask] = icbuf->data[k + 2].real;
				fir->stater[(fir->index + 3) & mask] = icbuf->data[k + 3].real;
				fir->statei[ fir->index            ] = icbuf->data[k + 0].imag;
				fir->statei[(fir->index + 1) & mask] = icbuf->data[k + 1].imag;
				fir->statei[(fir->index + 2) & mask] = icbuf->data[k + 2].imag;
				fir->statei[(fir->index + 3) & mask] = icbuf->data[k + 3].imag;
				fir->index = (fir->index + 4) & mask;
				coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
				coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
				coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
				coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
				acc0r = _mm_setzero_ps();
				acc0i = _mm_setzero_ps();
				acc1r = _mm_setzero_ps();
				acc1i = _mm_setzero_ps();
				acc2r = _mm_setzero_ps();
				acc2i = _mm_setzero_ps();
				acc3r = _mm_setzero_ps();
				acc3i = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					statereal = stater[j];
					prod0r = _mm_mul_ps(coefs0[i], statereal);
					acc0r  = _mm_add_ps(acc0r, prod0r);
					prod1r = _mm_mul_ps(coefs1[i], statereal);
					acc1r  = _mm_add_ps(acc1r, prod1r);
					prod2r = _mm_mul_ps(coefs2[i], statereal);
					acc2r  = _mm_add_ps(acc2r, prod2r);
					prod3r = _mm_mul_ps(coefs3[i], statereal);
					acc3r  = _mm_add_ps(acc3r, prod3r);
					stateimag = statei[j];
					prod0i = _mm_mul_ps(coefs0[i], stateimag);
					acc0i  = _mm_add_ps(acc0i, prod0i);
					prod1i = _mm_mul_ps(coefs1[i], stateimag);
					acc1i  = _mm_add_ps(acc1i, prod1i);
					prod2i = _mm_mul_ps(coefs2[i], stateimag);
					acc2i  = _mm_add_ps(acc2i, prod2i);
					prod3i = _mm_mul_ps(coefs3[i], stateimag);
					acc3i  = _mm_add_ps(acc3i, prod3i);
					j = (j+1) & masksimd;
				}
#ifdef __SSE3__
				acc0r = _mm_hadd_ps(acc0r, acc1r);
				acc0i = _mm_hadd_ps(acc0i, acc1i);
				acc2r = _mm_hadd_ps(acc2r, acc3r);
				acc2i = _mm_hadd_ps(acc2i, acc3i);
				acc0r = _mm_hadd_ps(acc0r, acc2r);
				acc0i = _mm_hadd_ps(acc0i, acc2i);
				ocbuf->data[k+0].real = acc0r[0];
				ocbuf->data[k+0].imag = acc0i[0];
				ocbuf->data[k+1].real = acc0r[1];
				ocbuf->data[k+1].imag = acc0i[1];
				ocbuf->data[k+2].real = acc0r[2];
				ocbuf->data[k+2].imag = acc0i[2];
				ocbuf->data[k+3].real = acc0r[3];
				ocbuf->data[k+3].imag = acc0i[3];
#else
				ocbuf->data[k+0].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
				ocbuf->data[k+0].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
				ocbuf->data[k+1].real = acc1r[0] + acc1r[1] + acc1r[2] + acc1r[3];
				ocbuf->data[k+1].imag = acc1i[0] + acc1i[1] + acc1i[2] + acc1i[3];
				ocbuf->data[k+2].real = acc2r[0] + acc2r[1] + acc2r[2] + acc2r[3];
				ocbuf->data[k+2].imag = acc2i[0] + acc2i[1] + acc2i[2] + acc2i[3];
				ocbuf->data[k+3].real = acc3r[0] + acc3r[1] + acc3r[2] + acc3r[3];
				ocbuf->data[k+3].imag = acc3i[0] + acc3i[1] + acc3i[2] + acc3i[3];
#endif
				start_index = (start_index + 4) & mask;
				startsimd = start_index >> 2;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			for (; k < ibuflen; k++) {
				fir->stater[fir->index] = icbuf->data[k].real;
				fir->statei[fir->index] = icbuf->data[k].imag;
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m128*)fir->coefs[start_index & 3];
				acc0r = _mm_setzero_ps();
				acc0i = _mm_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					prod0r = _mm_mul_ps(coefs0[i], stater[j]);
					acc0r  = _mm_add_ps(acc0r, prod0r);
					prod0i = _mm_mul_ps(coefs0[i], statei[j]);
					acc0i  = _mm_add_ps(acc0i, prod0i);
					j = (j+1) & masksimd;
				}
				ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
				ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 2;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
			}
		}
	}
	else {
		k = 0;
		while ((start_index & 3) && k < ibuflen) {
			fir->stater[fir->index] = icbuf->data[k].real;
			fir->statei[fir->index] = icbuf->data[k].imag;
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m128*)fir->coefs[start_index & 3];
			acc0r = _mm_setzero_ps();
			acc0i = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				prod0r = _mm_mul_ps(coefs0[i], stater[j]);
				prod0i = _mm_mul_ps(coefs0[i], statei[j]);
				acc0r  = _mm_add_ps(acc0r, prod0r);
				acc0i  = _mm_add_ps(acc0i, prod0i);
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
			ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 2;
			k++;
		}
		while (k + 4 <= ibuflen) {
			fir->stater[ fir->index            ] = icbuf->data[k + 0].real;
			fir->stater[(fir->index + 1) & mask] = icbuf->data[k + 1].real;
			fir->stater[(fir->index + 2) & mask] = icbuf->data[k + 2].real;
			fir->stater[(fir->index + 3) & mask] = icbuf->data[k + 3].real;
			fir->statei[ fir->index            ] = icbuf->data[k + 0].imag;
			fir->statei[(fir->index + 1) & mask] = icbuf->data[k + 1].imag;
			fir->statei[(fir->index + 2) & mask] = icbuf->data[k + 2].imag;
			fir->statei[(fir->index + 3) & mask] = icbuf->data[k + 3].imag;
			fir->index = (fir->index + 4) & mask;
			coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
			coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
			coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
			coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
			acc0r = _mm_setzero_ps();
			acc0i = _mm_setzero_ps();
			acc1r = _mm_setzero_ps();
			acc1i = _mm_setzero_ps();
			acc2r = _mm_setzero_ps();
			acc2i = _mm_setzero_ps();
			acc3r = _mm_setzero_ps();
			acc3i = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				statereal = stater[j];
				prod0r = _mm_mul_ps(coefs0[i], statereal);
				acc0r  = _mm_add_ps(acc0r, prod0r);
				prod1r = _mm_mul_ps(coefs1[i], statereal);
				acc1r  = _mm_add_ps(acc1r, prod1r);
				prod2r = _mm_mul_ps(coefs2[i], statereal);
				acc2r  = _mm_add_ps(acc2r, prod2r);
				prod3r = _mm_mul_ps(coefs3[i], statereal);
				acc3r  = _mm_add_ps(acc3r, prod3r);
				stateimag = statei[j];
				prod0i = _mm_mul_ps(coefs0[i], stateimag);
				acc0i  = _mm_add_ps(acc0i, prod0i);
				prod1i = _mm_mul_ps(coefs1[i], stateimag);
				acc1i  = _mm_add_ps(acc1i, prod1i);
				prod2i = _mm_mul_ps(coefs2[i], stateimag);
				acc2i  = _mm_add_ps(acc2i, prod2i);
				prod3i = _mm_mul_ps(coefs3[i], stateimag);
				acc3i  = _mm_add_ps(acc3i, prod3i);
				j = (j+1) & masksimd;
			}
#ifdef __SSE3__
			acc0r = _mm_hadd_ps(acc0r, acc1r);
			acc0i = _mm_hadd_ps(acc0i, acc1i);
			acc2r = _mm_hadd_ps(acc2r, acc3r);
			acc2i = _mm_hadd_ps(acc2i, acc3i);
			acc0r = _mm_hadd_ps(acc0r, acc2r);
			acc0i = _mm_hadd_ps(acc0i, acc2i);
			ocbuf->data[k+0].real = acc0r[0];
			ocbuf->data[k+0].imag = acc0i[0];
			ocbuf->data[k+1].real = acc0r[1];
			ocbuf->data[k+1].imag = acc0i[1];
			ocbuf->data[k+2].real = acc0r[2];
			ocbuf->data[k+2].imag = acc0i[2];
			ocbuf->data[k+3].real = acc0r[3];
			ocbuf->data[k+3].imag = acc0i[3];
#else
			ocbuf->data[k+0].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
			ocbuf->data[k+0].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
			ocbuf->data[k+1].real = acc1r[0] + acc1r[1] + acc1r[2] + acc1r[3];
			ocbuf->data[k+1].imag = acc1i[0] + acc1i[1] + acc1i[2] + acc1i[3];
			ocbuf->data[k+2].real = acc2r[0] + acc2r[1] + acc2r[2] + acc2r[3];
			ocbuf->data[k+2].imag = acc2i[0] + acc2i[1] + acc2i[2] + acc2i[3];
			ocbuf->data[k+3].real = acc3r[0] + acc3r[1] + acc3r[2] + acc3r[3];
			ocbuf->data[k+3].imag = acc3i[0] + acc3i[1] + acc3i[2] + acc3i[3];
#endif
			start_index = (start_index + 4) & mask;
			startsimd = start_index >> 2;
			k += 4;
		}
		for (; k < ibuflen; k++) {
			fir->stater[fir->index] = icbuf->data[k].real;
			fir->statei[fir->index] = icbuf->data[k].imag;
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m128*)fir->coefs[start_index & 3];
			acc0r = _mm_setzero_ps();
			acc0i = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				prod0r = _mm_mul_ps(coefs0[i], stater[j]);
				acc0r  = _mm_add_ps(acc0r, prod0r);
				prod0i = _mm_mul_ps(coefs0[i], statei[j]);
				acc0i  = _mm_add_ps(acc0i, prod0i);
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
			ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 2;
		}
	}

	return ocbuf;
}


pdlc_buffer_t* pdlc_fir_filter_interpolate_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int masksimd = mask >> 2;
	const size_t ibuflen = ifbuf->length;
	const size_t obuflen = ibuflen*fir->max_counter;
	const float ffactor = (float)(fir->max_counter);
	const size_t mcounter = fir->max_counter;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 2;
	unsigned int i, j;
	size_t k = 0, l = 0;
	register __m128 acc0, acc1, acc2, acc3;
	register __m128 prod0, prod1, prod2, prod3;
	register __m128 statereal;
	const __m128 *coefs0, *coefs1, *coefs2, *coefs3;
	__m128 *stater = (__m128*)fir->stater;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(obuflen);
	else if (ofbuf->length != obuflen)
		pdlc_buffer_resize(ofbuf, obuflen, 0);


	while ((start_index & 3) && k < obuflen) {
		if ((k % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m128*)fir->coefs[start_index & 3];
		acc0 = _mm_setzero_ps();
		j = startsimd;
		for (i = 0; i < lensimd; i++) {
			prod0 = _mm_mul_ps(coefs0[i], stater[j]);
			acc0  = _mm_add_ps(acc0, prod0);
			j = (j+1) & masksimd;
		}
		ofbuf->data[k] = (acc0[0] + acc0[1] + acc0[2] + acc0[3]) * ffactor;
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 2;
		k++;
	}
	while (k + 4 <= obuflen) {
		if (((k+0) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+1) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+2) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+3) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
		coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
		coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
		coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
		acc0 = _mm_setzero_ps();
		acc1 = _mm_setzero_ps();
		acc2 = _mm_setzero_ps();
		acc3 = _mm_setzero_ps();
		j = startsimd;
		for (i = 0; i < lensimd; i++) {
			statereal = stater[j];
			prod0 = _mm_mul_ps(coefs0[i], statereal);
			acc0  = _mm_add_ps(acc0, prod0);
			prod1 = _mm_mul_ps(coefs1[i], statereal);
			acc1  = _mm_add_ps(acc1, prod1);
			prod2 = _mm_mul_ps(coefs2[i], statereal);
			acc2  = _mm_add_ps(acc2, prod2);
			prod3 = _mm_mul_ps(coefs3[i], statereal);
			acc3  = _mm_add_ps(acc3, prod3);
			j = (j+1) & masksimd;
		}
#ifdef __SSE3__
		acc0 = _mm_hadd_ps(acc0, acc1);
		acc2 = _mm_hadd_ps(acc2, acc3);
		acc0 = _mm_hadd_ps(acc0, acc2);
		acc1 = _mm_set_ps1(ffactor);
		acc0 = _mm_mul_ps(acc0, acc1);
		ofbuf->data[k+0] = acc0[0];
		ofbuf->data[k+1] = acc0[1];
		ofbuf->data[k+2] = acc0[2];
		ofbuf->data[k+3] = acc0[3];
#else
		ofbuf->data[k+0] = (acc0[0] + acc0[1] + acc0[2] + acc0[3]) * ffactor;
		ofbuf->data[k+1] = (acc1[0] + acc1[1] + acc1[2] + acc1[3]) * ffactor;
		ofbuf->data[k+2] = (acc2[0] + acc2[1] + acc2[2] + acc2[3]) * ffactor;
		ofbuf->data[k+3] = (acc3[0] + acc3[1] + acc3[2] + acc3[3]) * ffactor;
#endif
		start_index = (start_index + 4) & mask;
		startsimd = start_index >> 2;
		k += 4;
	}
	for (; k < obuflen; k++) {
		if ((k % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m128*)fir->coefs[start_index & 3];
		acc0 = _mm_setzero_ps();
		j = startsimd;
		for (i = 0; i < lensimd; i++) {
			prod0 = _mm_mul_ps(coefs0[i], stater[j]);
			acc0  = _mm_add_ps(acc0, prod0);
			j = (j+1) & masksimd;
		}
		ofbuf->data[k] = (acc0[0] + acc0[1] + acc0[2] + acc0[3]) * ffactor;
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 2;
	}

	return ofbuf;
}



pdlc_complex_buffer_t* pdlc_fir_filter_interpolate_complex_buffer(pdlc_fir_filter_t* fir, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int masksimd = mask >> 2;
	const size_t ibuflen = icbuf->length;
	const size_t obuflen = ibuflen*fir->max_counter;
	const float ffactor = (float)(fir->max_counter);
	const size_t mcounter = fir->max_counter;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 2;
	unsigned int i, j;
	size_t k = 0, l = 0;
	register __m128 acc0r, acc1r, acc2r, acc3r;
	register __m128 acc0i, acc1i, acc2i, acc3i;
	register __m128 prod0r, prod1r, prod2r, prod3r;
	register __m128 prod0i, prod1i, prod2i, prod3i;
	register __m128 statereal;
	register __m128 stateimag;
	const __m128 *coefs0, *coefs1, *coefs2, *coefs3;
	__m128 *stater = (__m128*)fir->stater;
	__m128 *statei = (__m128*)fir->statei;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(obuflen);
	else if (ocbuf->length != obuflen)
		pdlc_complex_buffer_resize(ocbuf, obuflen, 0);


	while ((start_index & 3) && k < obuflen) {
		if ((k % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m128*)fir->coefs[start_index & 3];
		acc0r = _mm_setzero_ps();
		acc0i = _mm_setzero_ps();
		j = startsimd;
		for (i = 0; i < lensimd; i++) {
			prod0r = _mm_mul_ps(coefs0[i], stater[j]);
			prod0i = _mm_mul_ps(coefs0[i], statei[j]);
			acc0r  = _mm_add_ps(acc0r, prod0r);
			acc0i  = _mm_add_ps(acc0i, prod0i);
			j = (j+1) & masksimd;
		}
		ocbuf->data[k].real = (acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3]) * ffactor;
		ocbuf->data[k].imag = (acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3]) * ffactor;
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 2;
		k++;
	}
	while (k + 4 <= obuflen) {
		if (((k+0) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+1) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+2) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+3) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
		coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
		coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
		coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
		acc0r = _mm_setzero_ps();
		acc0i = _mm_setzero_ps();
		acc1r = _mm_setzero_ps();
		acc1i = _mm_setzero_ps();
		acc2r = _mm_setzero_ps();
		acc2i = _mm_setzero_ps();
		acc3r = _mm_setzero_ps();
		acc3i = _mm_setzero_ps();
		j = startsimd;
		for (i = 0; i < lensimd; i++) {
			statereal = stater[j];
			prod0r = _mm_mul_ps(coefs0[i], statereal);
			acc0r  = _mm_add_ps(acc0r, prod0r);
			prod1r = _mm_mul_ps(coefs1[i], statereal);
			acc1r  = _mm_add_ps(acc1r, prod1r);
			prod2r = _mm_mul_ps(coefs2[i], statereal);
			acc2r  = _mm_add_ps(acc2r, prod2r);
			prod3r = _mm_mul_ps(coefs3[i], statereal);
			acc3r  = _mm_add_ps(acc3r, prod3r);
			stateimag = statei[j];
			prod0i = _mm_mul_ps(coefs0[i], stateimag);
			acc0i  = _mm_add_ps(acc0i, prod0i);
			prod1i = _mm_mul_ps(coefs1[i], stateimag);
			acc1i  = _mm_add_ps(acc1i, prod1i);
			prod2i = _mm_mul_ps(coefs2[i], stateimag);
			acc2i  = _mm_add_ps(acc2i, prod2i);
			prod3i = _mm_mul_ps(coefs3[i], stateimag);
			acc3i  = _mm_add_ps(acc3i, prod3i);
			j = (j+1) & masksimd;
		}
#ifdef __SSE3__
		acc0r = _mm_hadd_ps(acc0r, acc1r);
		acc0i = _mm_hadd_ps(acc0i, acc1i);
		acc2r = _mm_hadd_ps(acc2r, acc3r);
		acc2i = _mm_hadd_ps(acc2i, acc3i);
		acc0r = _mm_hadd_ps(acc0r, acc2r);
		acc0i = _mm_hadd_ps(acc0i, acc2i);
		acc1r = _mm_set_ps1(ffactor);
		acc0r = _mm_mul_ps(acc0r, acc1r);
		acc0i = _mm_mul_ps(acc0i, acc1r);
		ocbuf->data[k+0].real = acc0r[0];
		ocbuf->data[k+0].imag = acc0i[0];
		ocbuf->data[k+1].real = acc0r[1];
		ocbuf->data[k+1].imag = acc0i[1];
		ocbuf->data[k+2].real = acc0r[2];
		ocbuf->data[k+2].imag = acc0i[2];
		ocbuf->data[k+3].real = acc0r[3];
		ocbuf->data[k+3].imag = acc0i[3];
#else
		ocbuf->data[k+0].real = (acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3]) * ffactor;
		ocbuf->data[k+0].imag = (acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3]) * ffactor;
		ocbuf->data[k+1].real = (acc1r[0] + acc1r[1] + acc1r[2] + acc1r[3]) * ffactor;
		ocbuf->data[k+1].imag = (acc1i[0] + acc1i[1] + acc1i[2] + acc1i[3]) * ffactor;
		ocbuf->data[k+2].real = (acc2r[0] + acc2r[1] + acc2r[2] + acc2r[3]) * ffactor;
		ocbuf->data[k+2].imag = (acc2i[0] + acc2i[1] + acc2i[2] + acc2i[3]) * ffactor;
		ocbuf->data[k+3].real = (acc3r[0] + acc3r[1] + acc3r[2] + acc3r[3]) * ffactor;
		ocbuf->data[k+3].imag = (acc3i[0] + acc3i[1] + acc3i[2] + acc3i[3]) * ffactor;
#endif
		start_index = (start_index + 4) & mask;
		startsimd = start_index >> 2;
		k += 4;
	}
	for (; k < obuflen; k++) {
		if ((k % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m128*)fir->coefs[start_index & 3];
		acc0r = _mm_setzero_ps();
		acc0i = _mm_setzero_ps();
		j = startsimd;
		for (i = 0; i < lensimd; i++) {
			prod0r = _mm_mul_ps(coefs0[i], stater[j]);
			prod0i = _mm_mul_ps(coefs0[i], statei[j]);
			acc0r  = _mm_add_ps(acc0r, prod0r);
			acc0i  = _mm_add_ps(acc0i, prod0i);
			j = (j+1) & masksimd;
		}
		ocbuf->data[k].real = (acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3]) * ffactor;
		ocbuf->data[k].imag = (acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3]) * ffactor;
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 2;
	}

	return ocbuf;
}


pdlc_buffer_t* pdlc_fir_filter_decimate_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int masksimd = mask >> 2;
	const int mcounter = fir->max_counter;
	const size_t ibuflen = ifbuf->length;
	const size_t obuflen = (size_t)ceil(((double)ibuflen - (double)((mcounter - fir->counter) % mcounter)) / (double)mcounter);
	unsigned int start_index = (flt_len + fir->index + fir->counter + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 2;
	unsigned int i0, i1, j0, j1;
	size_t k, l;
	register __m128 acc0, acc1;
	register __m128 prod0, prod1;
	const __m128 *coefs;
	__m128 *stater = (__m128*)fir->stater;


	if (!ofbuf)
		ofbuf = pdlc_buffer_new(obuflen);
	else if (ofbuf->length != obuflen)
		pdlc_buffer_resize(ofbuf, obuflen, 0);


	for (k = 0, l = 0; k < ibuflen; k++) {
		fir->stater[fir->index] = ifbuf->data[k];
		fir->index = (fir->index + 1) & mask;
		if (fir->counter == 0) {
			coefs = (__m128*)fir->coefs[start_index & 3];
			acc0 = _mm_setzero_ps();
			acc1 = _mm_setzero_ps();
			j0 = startsimd;
			j1 = (startsimd+1) & masksimd;
			i0 = 0;
			i1 = 1;
			while (i1 < lensimd) {
				prod0 = _mm_mul_ps(coefs[i0], stater[j0]);
				acc0  = _mm_add_ps(acc0, prod0);
				prod1 = _mm_mul_ps(coefs[i1], stater[j1]);
				acc1  = _mm_add_ps(acc1, prod1);
				i0 += 2;
				i1 += 2;
				j0 = (j0+2) & masksimd;
				j1 = (j1+2) & masksimd;
			}
			while (i0 < lensimd) {
				prod0 = _mm_mul_ps(coefs[i0], stater[j0]);
				acc0  = _mm_add_ps(acc0, prod0);
				i0 += 2;
				j0 = (j0+2) & masksimd;
			}
			acc0 = _mm_add_ps(acc0, acc1);
			ofbuf->data[l++] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			start_index = (start_index + mcounter) & mask;
			startsimd = start_index >> 2;
		}
		fir->counter = (fir->counter + 1) % mcounter;
	}

	return ofbuf;
}


pdlc_complex_buffer_t* pdlc_fir_filter_decimate_complex_buffer(pdlc_fir_filter_t* fir, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int masksimd = mask >> 2;
	const int mcounter = fir->max_counter;
	const size_t ibuflen = icbuf->length;
	const size_t obuflen = (size_t)ceil(((double)ibuflen - (double)((mcounter - fir->counter) % mcounter)) / (double)mcounter);
	unsigned int start_index = (flt_len + fir->index + fir->counter + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 2;
	unsigned int i0, j0, i1, j1;
	size_t k, l;
	register __m128 acc0r, acc0i, acc1r, acc1i;
	register __m128 prod0r, prod0i, prod1r, prod1i;
	const __m128 *coefs;
	__m128 *stater = (__m128*)fir->stater;
	__m128 *statei = (__m128*)fir->statei;


	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(obuflen);
	else if (ocbuf->length != obuflen)
		pdlc_complex_buffer_resize(ocbuf, obuflen, 0);


	for (k = 0, l = 0; k < ibuflen; k++) {
		fir->stater[fir->index] = icbuf->data[k].real;
		fir->statei[fir->index] = icbuf->data[k].imag;
		fir->index = (fir->index + 1) & mask;
		if (fir->counter == 0) {
			coefs = (__m128*)fir->coefs[start_index & 3];
			acc0r = _mm_setzero_ps();
			acc0i = _mm_setzero_ps();
			acc1r = _mm_setzero_ps();
			acc1i = _mm_setzero_ps();
			j0 = startsimd;
			j1 = (startsimd+1) & masksimd;
			i0 = 0;
			i1 = 1;
			while (i1 < lensimd) {
				prod0r = _mm_mul_ps(coefs[i0], stater[j0]);
				acc0r  = _mm_add_ps(acc0r, prod0r);
				prod0i = _mm_mul_ps(coefs[i0], statei[j0]);
				acc0i  = _mm_add_ps(acc0i, prod0i);
				prod1r = _mm_mul_ps(coefs[i1], stater[j1]);
				acc1r  = _mm_add_ps(acc1r, prod1r);
				prod1i = _mm_mul_ps(coefs[i1], statei[j1]);
				acc1i  = _mm_add_ps(acc1i, prod1i);
				i0 += 2;
				i1 += 2;
				j0 = (j0+2) & masksimd;
				j1 = (j1+2) & masksimd;
			}
			while (i0 < lensimd) {
				prod0r = _mm_mul_ps(coefs[i0], stater[j0]);
				acc0r  = _mm_add_ps(acc0r, prod0r);
				prod0i = _mm_mul_ps(coefs[i0], statei[j0]);
				acc0i  = _mm_add_ps(acc0i, prod0i);
				i0 += 2;
				j0 = (j0+2) & masksimd;
			}
			acc0r = _mm_add_ps(acc0r, acc1r);
			acc0i = _mm_add_ps(acc0i, acc1i);
			ocbuf->data[l].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3];
			ocbuf->data[l].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3];
			l++;
			start_index = (start_index + mcounter) & mask;
			startsimd = start_index >> 2;
		}
		fir->counter = (fir->counter + 1) % mcounter;
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_fir_filter_transform(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_complex_buffer_t *ocbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int masksimd = mask >> 2;
	const size_t ibuflen = ifbuf->length;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 2;
	unsigned int middle_index = (start_index + nb_coefs / 2) & mask;
	unsigned int i, j;
	size_t k = 0;
	register __m128 acc0, acc1, acc2, acc3;
	register __m128 prod0, prod1, prod2, prod3;
	register __m128 statereal;
	const __m128 *coefs0, *coefs1, *coefs2, *coefs3;
	__m128 *stater = (__m128*)fir->stater;


	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(ifbuf->length);
	else if (ocbuf->length != ifbuf->length)
		pdlc_complex_buffer_resize(ocbuf, ifbuf->length, 0);


	if (nb_coefs & 1) {
		while ((start_index & 3) && k < ibuflen) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m128*)fir->coefs[start_index & 3];
			acc0 = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				prod0 = _mm_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm_add_ps(acc0, prod0);
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 2;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
		}
		while (k + 4 <= ibuflen) {
			fir->stater[ fir->index            ] = ifbuf->data[k + 0];
			fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
			fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
			fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
			fir->index = (fir->index + 4) & mask;
			coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
			coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
			coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
			coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
			acc0 = _mm_setzero_ps();
			acc1 = _mm_setzero_ps();
			acc2 = _mm_setzero_ps();
			acc3 = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				statereal = stater[j];
				prod0 = _mm_mul_ps(coefs0[i], statereal);
				acc0  = _mm_add_ps(acc0, prod0);
				prod1 = _mm_mul_ps(coefs1[i], statereal);
				acc1  = _mm_add_ps(acc1, prod1);
				prod2 = _mm_mul_ps(coefs2[i], statereal);
				acc2  = _mm_add_ps(acc2, prod2);
				prod3 = _mm_mul_ps(coefs3[i], statereal);
				acc3  = _mm_add_ps(acc3, prod3);
				j = (j+1) & masksimd;
			}
#ifdef __SSE3__
			acc0 = _mm_hadd_ps(acc0, acc1);
			acc2 = _mm_hadd_ps(acc2, acc3);
			acc0 = _mm_hadd_ps(acc0, acc2);
			ocbuf->data[k+0].imag = acc0[0];
			ocbuf->data[k+1].imag = acc0[1];
			ocbuf->data[k+2].imag = acc0[2];
			ocbuf->data[k+3].imag = acc0[3];
#else
			ocbuf->data[k+0].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			ocbuf->data[k+1].imag = acc1[0] + acc1[1] + acc1[2] + acc1[3];
			ocbuf->data[k+2].imag = acc2[0] + acc2[1] + acc2[2] + acc2[3];
			ocbuf->data[k+3].imag = acc3[0] + acc3[1] + acc3[2] + acc3[3];
#endif
			start_index = (start_index + 4) & mask;
			startsimd = start_index >> 2;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
		}
		for (; k < ibuflen; k++) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m128*)fir->coefs[start_index & 3];
			acc0 = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				prod0 = _mm_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm_add_ps(acc0, prod0);
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 2;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
		}
	}
	else {
		while ((start_index & 3) && k < ibuflen) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m128*)fir->coefs[start_index & 3];
			acc0 = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				prod0 = _mm_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm_add_ps(acc0, prod0);
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 2;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
		}
		while (k + 4 <= ibuflen) {
			fir->stater[ fir->index            ] = ifbuf->data[k + 0];
			fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
			fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
			fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
			fir->index = (fir->index + 4) & mask;
			coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
			coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
			coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
			coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
			acc0 = _mm_setzero_ps();
			acc1 = _mm_setzero_ps();
			acc2 = _mm_setzero_ps();
			acc3 = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				statereal = stater[j];
				prod0 = _mm_mul_ps(coefs0[i], statereal);
				acc0  = _mm_add_ps(acc0, prod0);
				prod1 = _mm_mul_ps(coefs1[i], statereal);
				acc1  = _mm_add_ps(acc1, prod1);
				prod2 = _mm_mul_ps(coefs2[i], statereal);
				acc2  = _mm_add_ps(acc2, prod2);
				prod3 = _mm_mul_ps(coefs3[i], statereal);
				acc3  = _mm_add_ps(acc3, prod3);
				j = (j+1) & masksimd;
			}
#ifdef __SSE3__
			acc0 = _mm_hadd_ps(acc0, acc1);
			acc2 = _mm_hadd_ps(acc2, acc3);
			acc0 = _mm_hadd_ps(acc0, acc2);
			ocbuf->data[k+0].imag = acc0[0];
			ocbuf->data[k+1].imag = acc0[1];
			ocbuf->data[k+2].imag = acc0[2];
			ocbuf->data[k+3].imag = acc0[3];
#else
			ocbuf->data[k+0].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			ocbuf->data[k+1].imag = acc1[0] + acc1[1] + acc1[2] + acc1[3];
			ocbuf->data[k+2].imag = acc2[0] + acc2[1] + acc2[2] + acc2[3];
			ocbuf->data[k+3].imag = acc3[0] + acc3[1] + acc3[2] + acc3[3];
#endif
			start_index = (start_index + 4) & mask;
			startsimd = start_index >> 2;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
		}
		for (; k < ibuflen; k++) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m128*)fir->coefs[start_index & 3];
			acc0 = _mm_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				prod0 = _mm_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm_add_ps(acc0, prod0);
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 2;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
		}
	}

	return ocbuf;
}
