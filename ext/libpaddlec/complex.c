/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include "libpaddlec.h"


pdlc_buffer_t* pdlc_buffer_abs(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = fabsf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_abs_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = fabsf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_complex_buffer_abs(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = hypotf(cbuf->data[i].real, cbuf->data[i].imag);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_abs2(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = fbuf->data[i] * fbuf->data[i];

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_abs2_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = fbuf->data[i] * fbuf->data[i];

	return fbuf;
}


pdlc_buffer_t* pdlc_complex_buffer_abs2(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = cbuf->data[i].real * cbuf->data[i].real + cbuf->data[i].imag * cbuf->data[i].imag;

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_arg(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = (fbuf->data[i] < 0.0f) ? (float)M_PI : 0.0f;

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_arg_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = (fbuf->data[i] < 0.0f) ? (float)M_PI : 0.0f;

	return fbuf;
}


pdlc_buffer_t* pdlc_complex_buffer_arg(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = atan2f(cbuf->data[i].imag, cbuf->data[i].real);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_conjugate(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = fbuf->data[i];

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_conjugate_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = fbuf->data[i];

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_conjugate(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf)
{
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = cbuf->data[i].real;
		ocbuf->data[i].imag = -cbuf->data[i].imag;
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_conjugate_inplace(pdlc_complex_buffer_t *cbuf)
{
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = cbuf->data[i].real;
		cbuf->data[i].imag = -cbuf->data[i].imag;
	}

	return cbuf;
}


pdlc_buffer_t* pdlc_buffer_imag(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = 0.0f;

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_imag_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = 0.0f;

	return fbuf;
}


pdlc_buffer_t* pdlc_complex_buffer_imag(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = cbuf->data[i].imag;

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_real(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = fbuf->data[i];

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_real_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = fbuf->data[i];

	return fbuf;
}


pdlc_buffer_t* pdlc_complex_buffer_real(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(cbuf->length);
	else
		pdlc_buffer_resize(ofbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++)
		ofbuf->data[i] = cbuf->data[i].real;

	return ofbuf;
}


pdlc_complex_buffer_t* pdlc_buffer_swapIQ(const pdlc_buffer_t *fbuf, pdlc_complex_buffer_t *ocbuf)
{
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(fbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++) {
		ocbuf->data[i].real = 0.0f;
		ocbuf->data[i].imag = fbuf->data[i];
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_swapIQ(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf)
{
	size_t i;
	pdlc_complex_t tmp;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		tmp.real = cbuf->data[i].imag;
		tmp.imag = cbuf->data[i].real;
		ocbuf->data[i] = tmp;
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_swapIQ_inplace(pdlc_complex_buffer_t *cbuf)
{
	size_t i;
	pdlc_complex_t tmp;

	for (i = 0; i < cbuf->length; i++) {
		tmp.real = cbuf->data[i].imag;
		tmp.imag = cbuf->data[i].real;
		cbuf->data[i] = tmp;
	}

	return cbuf;
}


