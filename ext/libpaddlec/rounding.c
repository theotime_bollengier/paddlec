/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include "libpaddlec.h"


pdlc_buffer_t* pdlc_buffer_ceil(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = ceilf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_ceil_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = ceilf(fbuf->data[i]);

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_ceil(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf)
{
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = ceilf(cbuf->data[i].real);
		ocbuf->data[i].imag = ceilf(cbuf->data[i].imag);
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_ceil_inplace(pdlc_complex_buffer_t *cbuf)
{
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = ceilf(cbuf->data[i].real);
		cbuf->data[i].imag = ceilf(cbuf->data[i].imag);
	}

	return cbuf;
}


pdlc_buffer_t* pdlc_buffer_floor(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = floorf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_floor_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = floorf(fbuf->data[i]);

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_floor(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf)
{
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = floorf(cbuf->data[i].real);
		ocbuf->data[i].imag = floorf(cbuf->data[i].imag);
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_floor_inplace(pdlc_complex_buffer_t *cbuf)
{
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = floorf(cbuf->data[i].real);
		cbuf->data[i].imag = floorf(cbuf->data[i].imag);
	}

	return cbuf;
}


pdlc_buffer_t* pdlc_buffer_truncate(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = truncf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_truncate_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = truncf(fbuf->data[i]);

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_truncate(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf)
{
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = truncf(cbuf->data[i].real);
		ocbuf->data[i].imag = truncf(cbuf->data[i].imag);
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_truncate_inplace(pdlc_complex_buffer_t *cbuf)
{
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = truncf(cbuf->data[i].real);
		cbuf->data[i].imag = truncf(cbuf->data[i].imag);
	}

	return cbuf;
}


pdlc_buffer_t* pdlc_buffer_round(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = roundf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_round_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = roundf(fbuf->data[i]);

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_round(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf)
{
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = roundf(cbuf->data[i].real);
		ocbuf->data[i].imag = roundf(cbuf->data[i].imag);
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_round_inplace(pdlc_complex_buffer_t *cbuf)
{
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = roundf(cbuf->data[i].real);
		cbuf->data[i].imag = roundf(cbuf->data[i].imag);
	}

	return cbuf;
}


pdlc_buffer_t* pdlc_buffer_ceil_digits(const pdlc_buffer_t *fbuf, int digits, pdlc_buffer_t *ofbuf)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = ceilf(fbuf->data[i]*d)/d;

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_ceil_digits_inplace(pdlc_buffer_t *fbuf, int digits)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = ceilf(fbuf->data[i]*d)/d;

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_ceil_digits(const pdlc_complex_buffer_t *cbuf, int digits, pdlc_complex_buffer_t *ocbuf)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = ceilf(cbuf->data[i].real*d)/d;
		ocbuf->data[i].imag = ceilf(cbuf->data[i].imag*d)/d;
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_ceil_digits_inplace(pdlc_complex_buffer_t *cbuf, int digits)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = ceilf(cbuf->data[i].real*d)/d;
		cbuf->data[i].imag = ceilf(cbuf->data[i].imag*d)/d;
	}

	return cbuf;
}


pdlc_buffer_t* pdlc_buffer_floor_digits(const pdlc_buffer_t *fbuf, int digits, pdlc_buffer_t *ofbuf)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = floorf(fbuf->data[i]*d)/d;

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_floor_digits_inplace(pdlc_buffer_t *fbuf, int digits)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = floorf(fbuf->data[i]*d)/d;

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_floor_digits(const pdlc_complex_buffer_t *cbuf, int digits, pdlc_complex_buffer_t *ocbuf)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = floorf(cbuf->data[i].real*d)/d;
		ocbuf->data[i].imag = floorf(cbuf->data[i].imag*d)/d;
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_floor_digits_inplace(pdlc_complex_buffer_t *cbuf, int digits)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = floorf(cbuf->data[i].real*d)/d;
		cbuf->data[i].imag = floorf(cbuf->data[i].imag*d)/d;
	}

	return cbuf;
}


pdlc_buffer_t* pdlc_buffer_truncate_digits(const pdlc_buffer_t *fbuf, int digits, pdlc_buffer_t *ofbuf)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = truncf(fbuf->data[i]*d)/d;

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_truncate_digits_inplace(pdlc_buffer_t *fbuf, int digits)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = truncf(fbuf->data[i]*d)/d;

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_truncate_digits(const pdlc_complex_buffer_t *cbuf, int digits, pdlc_complex_buffer_t *ocbuf)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = truncf(cbuf->data[i].real*d)/d;
		ocbuf->data[i].imag = truncf(cbuf->data[i].imag*d)/d;
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_truncate_digits_inplace(pdlc_complex_buffer_t *cbuf, int digits)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = truncf(cbuf->data[i].real*d)/d;
		cbuf->data[i].imag = truncf(cbuf->data[i].imag*d)/d;
	}

	return cbuf;
}


pdlc_buffer_t* pdlc_buffer_round_digits(const pdlc_buffer_t *fbuf, int digits, pdlc_buffer_t *ofbuf)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = roundf(fbuf->data[i]*d)/d;

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_round_digits_inplace(pdlc_buffer_t *fbuf, int digits)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = roundf(fbuf->data[i]*d)/d;

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_round_digits(const pdlc_complex_buffer_t *cbuf, int digits, pdlc_complex_buffer_t *ocbuf)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = roundf(cbuf->data[i].real*d)/d;
		ocbuf->data[i].imag = roundf(cbuf->data[i].imag*d)/d;
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_round_digits_inplace(pdlc_complex_buffer_t *cbuf, int digits)
{
	const float d = powf(10.0f, (float)digits);
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = roundf(cbuf->data[i].real*d)/d;
		cbuf->data[i].imag = roundf(cbuf->data[i].imag*d)/d;
	}

	return cbuf;
}


