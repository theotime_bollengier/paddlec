/* Copyright (C) 2020 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "libpaddlec.h"


pdlc_delay_t* pdlc_delay_new(int delay)
{
	pdlc_delay_t *del;

	del = malloc(sizeof(pdlc_delay_t));
	del->delay = 0;
	del->index_mask = 0;
	del->index = 0;
	del->data = NULL;

	if (delay > 0)
		pdlc_delay_initialize(del, delay);

	return del;
}


void pdlc_delay_initialize(pdlc_delay_t* del, int delay)
{
	unsigned int length;

	if (del->data)
		free(del->data);

	del->delay = 0;
	del->index_mask = 0;
	del->index = 0;
	del->data = NULL;

	if (delay <= 0)
		return;

	if (delay > 134217728) {
		fprintf(stderr, "ERROR: libpaddlec: Delay cannot be greater than 134217728\n");
		exit(EXIT_FAILURE);
	}

	del->delay = (unsigned int)delay;
	length = (unsigned int)(pow(2.0, ceil(log2(del->delay))));
	del->index_mask = length - 1;

	del->data = malloc(length*sizeof(pdlc_complex_t));
	if (del->data == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for the delay!\n", length * sizeof(pdlc_complex_t));
		exit(EXIT_FAILURE);
	}

	memset(del->data, 0, length*sizeof(pdlc_complex_t));
}


void pdlc_delay_free(pdlc_delay_t* del)
{
	if (del->data)
		free(del->data);
}


size_t pdlc_delay_size(const pdlc_delay_t* del)
{
	return sizeof(pdlc_delay_t) + (del->index_mask + 1)*sizeof(pdlc_complex_t);
}


void pdlc_delay_reset(pdlc_delay_t* del)
{
	memset(del->data, 0, (del->index_mask + 1)*sizeof(pdlc_complex_t));
	del->index = 0;
}


float pdlc_delay_delay_float(pdlc_delay_t* del, float value)
{
	const unsigned int mask = del->index_mask;
	const unsigned int start_index = (mask + del->index + 1 - del->delay) & mask;
	float *ptr = (float*)del->data;
	float res;

	res = ptr[start_index];
	ptr[del->index] = value;
	del->index = (del->index + 1) & mask;

	return res;
}


pdlc_complex_t pdlc_delay_delay_complex(pdlc_delay_t* del, pdlc_complex_t value)
{
	const unsigned int mask = del->index_mask;
	const unsigned int start_index = (mask + del->index + 1 - del->delay) & mask;
	pdlc_complex_t *ptr = del->data;
	pdlc_complex_t res;

	res = ptr[start_index];
	ptr[del->index] = value;
	del->index = (del->index + 1) & mask;

	return res;
}


pdlc_buffer_t* pdlc_delay_delay_float_buffer(pdlc_delay_t* del, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int mask = del->index_mask;
	const unsigned int length = mask + 1;
	const unsigned int delay = del->delay;
	const unsigned int start_index = (length + del->index - delay) & mask;
	float *ptr = (float*)del->data;
	unsigned int len, tocopy, dests = 0, srcs = 0;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(ifbuf->length);
	else if (ofbuf->length != ifbuf->length)
		pdlc_buffer_resize(ofbuf, ifbuf->length, 0);
	
	/* Delay line to output buffer */
	len = delay;
	if (ifbuf->length < len)
		len = ifbuf->length;
	tocopy = len;
	if ((start_index + tocopy) > length) {
		tocopy = length - start_index;
		memcpy(ofbuf->data + dests, ptr + start_index, tocopy*sizeof(float));
		dests += tocopy;
		tocopy = len - tocopy;
		memcpy(ofbuf->data + dests, ptr, tocopy*sizeof(float));
	}
	else
		memcpy(ofbuf->data + dests, ptr + start_index, tocopy*sizeof(float));
	dests += tocopy;

	if (ifbuf->length >= delay)
		del->index = 0;

	/* Input buffer to output buffer */
	if (ifbuf->length > delay) {
		tocopy = ifbuf->length - len;
		memcpy(ofbuf->data + dests, ifbuf->data, tocopy*sizeof(float));
		srcs = tocopy;
	}

	/* Input buffer to delay line */
	tocopy = len;
	if ((del->index + tocopy) > length) {
		tocopy = length - del->index;
		memcpy(ptr + del->index, ifbuf->data + srcs, tocopy*sizeof(float));
		srcs += tocopy;
		tocopy = len - tocopy;
		memcpy(ptr, ifbuf->data + srcs, tocopy*sizeof(float));
	}
	else
		memcpy(ptr + del->index, ifbuf->data + srcs, tocopy*sizeof(float));
	del->index = (del->index + len) & mask;
	
	return ofbuf;
}


pdlc_complex_buffer_t* pdlc_delay_delay_complex_buffer(pdlc_delay_t* del, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf)
{
	const unsigned int mask = del->index_mask;
	const unsigned int length = mask + 1;
	const unsigned int delay = del->delay;
	const unsigned int start_index = (length + del->index - delay) & mask;
	pdlc_complex_t *ptr = del->data;
	unsigned int len, tocopy, dests = 0, srcs = 0;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(icbuf->length);
	else if (ocbuf->length != icbuf->length)
		pdlc_complex_buffer_resize(ocbuf, icbuf->length, 0);
	
	/* Delay line to output buffer */
	len = delay;
	if (icbuf->length < len)
		len = icbuf->length;
	tocopy = len;
	if ((start_index + tocopy) > length) {
		tocopy = length - start_index;
		memcpy(ocbuf->data + dests, ptr + start_index, tocopy*sizeof(pdlc_complex_t));
		dests += tocopy;
		tocopy = len - tocopy;
		memcpy(ocbuf->data + dests, ptr, tocopy*sizeof(pdlc_complex_t));
	}
	else
		memcpy(ocbuf->data + dests, ptr + start_index, tocopy*sizeof(pdlc_complex_t));
	dests += tocopy;

	if (icbuf->length >= delay)
		del->index = 0;

	/* Input buffer to output buffer */
	if (icbuf->length > delay) {
		tocopy = icbuf->length - len;
		memcpy(ocbuf->data + dests, icbuf->data, tocopy*sizeof(pdlc_complex_t));
		srcs = tocopy;
	}

	/* Input buffer to delay line */
	tocopy = len;
	if ((del->index + tocopy) > length) {
		tocopy = length - del->index;
		memcpy(ptr + del->index, icbuf->data + srcs, tocopy*sizeof(pdlc_complex_t));
		srcs += tocopy;
		tocopy = len - tocopy;
		memcpy(ptr, icbuf->data + srcs, tocopy*sizeof(pdlc_complex_t));
	}
	else
		memcpy(ptr + del->index, icbuf->data + srcs, tocopy*sizeof(pdlc_complex_t));
	del->index = (del->index + len) & mask;
	
	return ocbuf;
}


