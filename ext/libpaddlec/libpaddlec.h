/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIBPADDLEC_H
#define LIBPADDLEC_H

const char* pdlc_accelerator();

typedef struct {
	float real;
	float imag;
} pdlc_complex_t;


typedef struct {
	size_t capacity;
	size_t length;
	float *data;
} pdlc_buffer_t;


typedef struct {
	size_t capacity;
	size_t length;
	pdlc_complex_t *data;
} pdlc_complex_buffer_t;


pdlc_buffer_t* pdlc_buffer_new(size_t length);
pdlc_buffer_t* pdlc_buffer_copy(const pdlc_buffer_t *orig);
void pdlc_buffer_resize(pdlc_buffer_t *buf, size_t new_length, int copy_old_and_zero_excess);
void pdlc_buffer_set(pdlc_buffer_t *buf, float val);
void pdlc_buffer_free(pdlc_buffer_t *buf);
pdlc_buffer_t* pdlc_buffer_reverse(const pdlc_buffer_t *fbuf, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_buffer_reverse_inplace(pdlc_buffer_t *fbuf);

pdlc_complex_buffer_t* pdlc_complex_buffer_new(size_t length);
pdlc_complex_buffer_t* pdlc_complex_buffer_copy(const pdlc_complex_buffer_t *orig);
void pdlc_complex_buffer_resize(pdlc_complex_buffer_t *buf, size_t new_length, int copy_old_and_zero_excess);
void pdlc_complex_buffer_set(pdlc_complex_buffer_t *buf, pdlc_complex_t val);
void pdlc_complex_buffer_free(pdlc_complex_buffer_t *buf);
pdlc_complex_buffer_t* pdlc_complex_buffer_reverse(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_complex_buffer_reverse_inplace(pdlc_complex_buffer_t *cbuf);


typedef struct {
	float      **coefs;
	float       *stater;
	float       *statei;
	unsigned int nb_coefs;
	unsigned int state_len;
	unsigned int coef_len;
	unsigned int index;
	unsigned int index_mask;
	int          counter;
	int          max_counter;
} pdlc_fir_filter_t;


pdlc_fir_filter_t*     pdlc_fir_filter_new(int order);
void                   pdlc_fir_filter_initialize(pdlc_fir_filter_t* fir, int order);
void                   pdlc_fir_filter_free(pdlc_fir_filter_t* fir);
size_t                 pdlc_fir_filter_size(pdlc_fir_filter_t* fir);
void                   pdlc_fir_filter_reset(pdlc_fir_filter_t* fir);
int                    pdlc_fir_filter_get_coef_at(const pdlc_fir_filter_t* fir, int index, float *value);
int                    pdlc_fir_filter_set_coef_at(pdlc_fir_filter_t* fir, int index, float  value);
float                  pdlc_fir_filter_filter_float(pdlc_fir_filter_t* fir, float sample, float *delayed);
pdlc_complex_t         pdlc_fir_filter_filter_complex(pdlc_fir_filter_t* fir, pdlc_complex_t sample, pdlc_complex_t *delayed);
pdlc_buffer_t*         pdlc_fir_filter_filter_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf, pdlc_buffer_t *delayed);
pdlc_complex_buffer_t* pdlc_fir_filter_filter_complex_buffer(pdlc_fir_filter_t* fir, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf, pdlc_complex_buffer_t *delayed);
pdlc_complex_buffer_t* pdlc_fir_filter_transform(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_complex_buffer_t *ocbuf);

int                    pdlc_fir_filter_get_factor(const pdlc_fir_filter_t* fir);
void                   pdlc_fir_filter_interpolator_initialize(pdlc_fir_filter_t* fir, int order, int factor);
pdlc_buffer_t*         pdlc_fir_filter_interpolate_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf);
pdlc_complex_buffer_t* pdlc_fir_filter_interpolate_complex_buffer(pdlc_fir_filter_t* fir, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf);
void                   pdlc_fir_filter_decimator_initialize(pdlc_fir_filter_t* fir, int order, int factor);
pdlc_buffer_t*         pdlc_fir_filter_decimate_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf);
pdlc_complex_buffer_t* pdlc_fir_filter_decimate_complex_buffer(pdlc_fir_filter_t* fir, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf);


typedef struct {
	unsigned int    delay;
	unsigned int    index_mask;
	unsigned int    index;
	pdlc_complex_t *data;
} pdlc_delay_t;


pdlc_delay_t*          pdlc_delay_new(int delay);
void                   pdlc_delay_initialize(pdlc_delay_t* del, int delay);
void                   pdlc_delay_free(pdlc_delay_t* del);
size_t                 pdlc_delay_size(const pdlc_delay_t* del);
void                   pdlc_delay_reset(pdlc_delay_t* del);
float                  pdlc_delay_delay_float(pdlc_delay_t* del, float value);
pdlc_complex_t         pdlc_delay_delay_complex(pdlc_delay_t* del, pdlc_complex_t value);
pdlc_buffer_t*         pdlc_delay_delay_float_buffer(pdlc_delay_t* del, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf);
pdlc_complex_buffer_t* pdlc_delay_delay_complex_buffer(pdlc_delay_t* del, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf);


/* Comparison */
pdlc_buffer_t* pdlc_fb_fb_cmpless(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_cmpless_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_cmpless(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_cmpless_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_cmplessequ(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_cmplessequ_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_cmplessequ(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_cmplessequ_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_cmpgrt(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_cmpgrt_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_cmpgrt(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_cmpgrt_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_cmpgrtequ(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_cmpgrtequ_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_cmpgrtequ(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_cmpgrtequ_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_equ(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_equ_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_equ(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_equ_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_cb_cb_equ(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_cb_cs_equ(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_cb_equ(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_cb_equ_inplace(pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_cs_equ(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_cs_equ_inplace(pdlc_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_buffer_t* pdlc_cb_fb_equ(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_cb_fs_equ(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_different(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_different_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_different(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_different_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_cb_cb_different(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_cb_cs_different(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_cb_different(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_cb_different_inplace(pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_cs_different(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_cs_different_inplace(pdlc_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_buffer_t* pdlc_cb_fb_different(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_cb_fs_different(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_buffer_t *res);

/* Rounding */
pdlc_buffer_t* pdlc_buffer_ceil(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_ceil_inplace(pdlc_buffer_t *fbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_ceil(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_ceil_inplace(pdlc_complex_buffer_t *cbuf);
pdlc_buffer_t* pdlc_buffer_floor(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_floor_inplace(pdlc_buffer_t *fbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_floor(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_floor_inplace(pdlc_complex_buffer_t *cbuf);
pdlc_buffer_t* pdlc_buffer_truncate(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_truncate_inplace(pdlc_buffer_t *fbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_truncate(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_truncate_inplace(pdlc_complex_buffer_t *cbuf);
pdlc_buffer_t* pdlc_buffer_round(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_round_inplace(pdlc_buffer_t *fbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_round(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_round_inplace(pdlc_complex_buffer_t *cbuf);
pdlc_buffer_t* pdlc_buffer_ceil_digits(const pdlc_buffer_t *fbuf, int digits, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_ceil_digits_inplace(pdlc_buffer_t *fbuf, int digits);
pdlc_complex_buffer_t* pdlc_complex_buffer_ceil_digits(const pdlc_complex_buffer_t *cbuf, int digits, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_ceil_digits_inplace(pdlc_complex_buffer_t *cbuf, int digits);
pdlc_buffer_t* pdlc_buffer_floor_digits(const pdlc_buffer_t *fbuf, int digits, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_floor_digits_inplace(pdlc_buffer_t *fbuf, int digits);
pdlc_complex_buffer_t* pdlc_complex_buffer_floor_digits(const pdlc_complex_buffer_t *cbuf, int digits, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_floor_digits_inplace(pdlc_complex_buffer_t *cbuf, int digits);
pdlc_buffer_t* pdlc_buffer_truncate_digits(const pdlc_buffer_t *fbuf, int digits, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_truncate_digits_inplace(pdlc_buffer_t *fbuf, int digits);
pdlc_complex_buffer_t* pdlc_complex_buffer_truncate_digits(const pdlc_complex_buffer_t *cbuf, int digits, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_truncate_digits_inplace(pdlc_complex_buffer_t *cbuf, int digits);
pdlc_buffer_t* pdlc_buffer_round_digits(const pdlc_buffer_t *fbuf, int digits, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_round_digits_inplace(pdlc_buffer_t *fbuf, int digits);
pdlc_complex_buffer_t* pdlc_complex_buffer_round_digits(const pdlc_complex_buffer_t *cbuf, int digits, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_round_digits_inplace(pdlc_complex_buffer_t *cbuf, int digits);

/* Complex */
pdlc_buffer_t* pdlc_buffer_abs(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_abs_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_complex_buffer_abs(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_abs2(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_abs2_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_complex_buffer_abs2(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_arg(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_arg_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_complex_buffer_arg(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_conjugate(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_conjugate_inplace(pdlc_buffer_t *fbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_conjugate(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_conjugate_inplace(pdlc_complex_buffer_t *cbuf);
pdlc_buffer_t* pdlc_buffer_imag(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_imag_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_complex_buffer_imag(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_real(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_real_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_complex_buffer_real(const pdlc_complex_buffer_t *cbuf, pdlc_buffer_t *ofbuf);
pdlc_complex_buffer_t* pdlc_buffer_swapIQ(const pdlc_buffer_t *fbuf, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_swapIQ(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_swapIQ_inplace(pdlc_complex_buffer_t *cbuf);

/* Arithmetic */
pdlc_buffer_t* pdlc_buffer_unaryminus(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_unaryminus_inplace(pdlc_buffer_t *fbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_unaryminus(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf);
pdlc_complex_buffer_t* pdlc_complex_buffer_unaryminus_inplace(pdlc_complex_buffer_t *cbuf);
pdlc_buffer_t* pdlc_fb_fb_sub(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_sub_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_sub(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_sub_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_sub(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_sub_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_sub(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_sub_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_sub(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_sub(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_sub(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_sub_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_sub(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_sub_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_esub(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_esub_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_esub(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_esub_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_esub(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_esub_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_esub(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_esub_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_esub(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_esub(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_esub(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_esub_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_esub(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_esub_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_add(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_add_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_add(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_add_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_add(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_add_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_add(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_add_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_add(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_add(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_add(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_add_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_add(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_add_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_eadd(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_eadd_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_eadd(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_eadd_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_eadd(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_eadd_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_eadd(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_eadd_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_eadd(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_eadd(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_eadd(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_eadd_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_eadd(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_eadd_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_mult(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_mult_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_mult(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_mult_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_mult(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_mult_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_mult(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_mult_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_mult(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_mult(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_mult(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_mult_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_mult(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_mult_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_emult(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_emult_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_emult(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_emult_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_emult(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_emult_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_emult(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_emult_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_emult(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_emult(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_emult(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_emult_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_emult(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_emult_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_div(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_div_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_div(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_div_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_div(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_div_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_div(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_div_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_div(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_div(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_div(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_div_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_div(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_div_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_ediv(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_ediv_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_ediv(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_ediv_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_ediv(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_ediv_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_ediv(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_ediv_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_ediv(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_ediv(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_ediv(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_ediv_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_ediv(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_ediv_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_pow(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_pow_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_pow(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_pow_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_fb_pow(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_pow_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_pow(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_pow_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_modulo(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_modulo_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_modulo(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_modulo_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_emod(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_emod_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_emod(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_emod_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_emod(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_emod_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_emod(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_emod_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_emod(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_emod(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_emod(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_emod_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_emod(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_emod_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_epow(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_epow_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_epow(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_epow_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_epow(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_epow_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_epow(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_epow_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_fb_cb_epow(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_fb_cs_epow(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_epow(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_epow_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_epow(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_epow_inplace(pdlc_complex_buffer_t *lhs, float rhs);

/* Math */
pdlc_buffer_t* pdlc_buffer_acos(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_acos_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_acosh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_acosh_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_asin(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_asin_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_asinh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_asinh_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_atan(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_atan_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_atanh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_atanh_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_cbrt(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_cbrt_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_cos(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_cos_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_cosh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_cosh_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_erf(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_erf_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_erfc(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_erfc_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_exp(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_exp_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_log(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_log_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_log10(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_log10_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_log2(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_log2_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_sin(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_sin_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_sinh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_sinh_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_sqrt(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_sqrt_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_tan(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_tan_inplace(pdlc_buffer_t *fbuf);
pdlc_buffer_t* pdlc_buffer_tanh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf);
pdlc_buffer_t* pdlc_buffer_tanh_inplace(pdlc_buffer_t *fbuf);

/* Min Max */
pdlc_buffer_t* pdlc_buffer_clipp(const pdlc_buffer_t *fbuf, float mi, float ma, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_buffer_clipp_inplace(pdlc_buffer_t *fbuf, float mi, float ma);
pdlc_complex_buffer_t* pdlc_complex_buffer_clipp(const pdlc_complex_buffer_t *cbuf, float mi, float ma, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_complex_buffer_clipp_inplace(pdlc_complex_buffer_t *cbuf, float mi, float ma);
float pdlc_buffer_max_of_self(const pdlc_buffer_t *fbuf);
float pdlc_buffer_min_of_self(const pdlc_buffer_t *fbuf);
void pdlc_buffer_minmax_of_self(const pdlc_buffer_t *fbuf, float *mi, float *ma);
pdlc_complex_t pdlc_complex_buffer_max_of_self(const pdlc_complex_buffer_t *cbuf);
pdlc_complex_t pdlc_complex_buffer_min_of_self(const pdlc_complex_buffer_t *cbuf);
void pdlc_complex_buffer_minmax_of_self(const pdlc_complex_buffer_t *cbuf, pdlc_complex_t *mi, pdlc_complex_t *ma);
pdlc_buffer_t* pdlc_fb_fb_min(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_min_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_min(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_min_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_min(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_min_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_min(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_min_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_cb_fb_min(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_min_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_min(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_min_inplace(pdlc_complex_buffer_t *lhs, float rhs);
pdlc_buffer_t* pdlc_fb_fb_max(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fb_max_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_buffer_t* pdlc_fb_fs_max(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res);
pdlc_buffer_t* pdlc_fb_fs_max_inplace(pdlc_buffer_t *lhs, float rhs);
pdlc_complex_buffer_t* pdlc_cb_cb_max(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cb_max_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_cs_max(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_cs_max_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs);
pdlc_complex_buffer_t* pdlc_cb_fb_max(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fb_max_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs);
pdlc_complex_buffer_t* pdlc_cb_fs_max(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res);
pdlc_complex_buffer_t* pdlc_cb_fs_max_inplace(pdlc_complex_buffer_t *lhs, float rhs);

float pdlc_buffer_sum(const pdlc_buffer_t *fbuf);
pdlc_complex_t pdlc_complex_buffer_sum(const pdlc_complex_buffer_t *cbuf);
float pdlc_buffer_mean(const pdlc_buffer_t *fbuf);
pdlc_complex_t pdlc_complex_buffer_mean(const pdlc_complex_buffer_t *cbuf);
float pdlc_buffer_variance(const pdlc_buffer_t *fbuf);
float pdlc_buffer_standard_deviation(const pdlc_buffer_t *fbuf);
float pdlc_buffer_prod(const pdlc_buffer_t *fbuf);
pdlc_complex_t pdlc_complex_buffer_prod(const pdlc_complex_buffer_t *cbuf);
pdlc_complex_t pdlc_complex_buffer_eprod(const pdlc_complex_buffer_t *cbuf);
float pdlc_buffer_fb_fs_sad(const pdlc_buffer_t *fbuf, float other);
float pdlc_buffer_fb_fb_sad(const pdlc_buffer_t *fbuf, const pdlc_buffer_t *other);
pdlc_complex_t pdlc_complex_buffer_cb_cs_sad(const pdlc_complex_buffer_t *cbuf, pdlc_complex_t other);
pdlc_complex_t pdlc_complex_buffer_cb_cb_sad(const pdlc_complex_buffer_t *cbuf, const pdlc_complex_buffer_t *other);
float pdlc_buffer_fb_fs_ssd(const pdlc_buffer_t *fbuf, float other);
float pdlc_buffer_fb_fb_ssd(const pdlc_buffer_t *fbuf, const pdlc_buffer_t *other);
pdlc_complex_t pdlc_complex_buffer_cb_cs_essd(const pdlc_complex_buffer_t *cbuf, pdlc_complex_t other);
pdlc_complex_t pdlc_complex_buffer_cb_cb_essd(const pdlc_complex_buffer_t *cbuf, const pdlc_complex_buffer_t *other);
pdlc_complex_t pdlc_complex_buffer_cb_cs_ssd(const pdlc_complex_buffer_t *cbuf, pdlc_complex_t other);
pdlc_complex_t pdlc_complex_buffer_cb_cb_ssd(const pdlc_complex_buffer_t *cbuf, const pdlc_complex_buffer_t *other);


#endif /* LIBPADDLEC_H */
