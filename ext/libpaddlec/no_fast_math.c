/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include "libpaddlec.h"

#ifndef NAN
#define NAN nanf("")
#endif

/* Min Max */

float pdlc_buffer_max_of_self(const pdlc_buffer_t *fbuf)
{
	size_t i;
	float m = NAN;

	for (i = 0; i < fbuf->length; i++)
		m = fmaxf(m, fbuf->data[i]);

	return m;
}


float pdlc_buffer_min_of_self(const pdlc_buffer_t *fbuf)
{
	size_t i;
	float m = NAN;

	for (i = 0; i < fbuf->length; i++)
		m = fminf(m, fbuf->data[i]);

	return m;
}


void pdlc_buffer_minmax_of_self(const pdlc_buffer_t *fbuf, float *mi, float *ma)
{
	size_t i;
	float lmi = NAN;
	float lma = NAN;

	for (i = 0; i < fbuf->length; i++) {
		lmi = fminf(lmi, fbuf->data[i]);
		lma = fmaxf(lma, fbuf->data[i]);
	}

	*mi = lmi;
	*ma = lma;
}


pdlc_complex_t pdlc_complex_buffer_max_of_self(const pdlc_complex_buffer_t *cbuf)
{
	size_t i;
	pdlc_complex_t m;
	m.real = NAN;
	m.imag = NAN;

	for (i = 0; i < cbuf->length; i++) {
		m.real = fmaxf(m.real, cbuf->data[i].real);
		m.imag = fmaxf(m.imag, cbuf->data[i].imag);
	}

	return m;
}


pdlc_complex_t pdlc_complex_buffer_min_of_self(const pdlc_complex_buffer_t *cbuf)
{
	size_t i;
	pdlc_complex_t m;
	m.real = NAN;
	m.imag = NAN;

	for (i = 0; i < cbuf->length; i++) {
		m.real = fminf(m.real, cbuf->data[i].real);
		m.imag = fminf(m.imag, cbuf->data[i].imag);
	}

	return m;
}


void pdlc_complex_buffer_minmax_of_self(const pdlc_complex_buffer_t *cbuf, pdlc_complex_t *mi, pdlc_complex_t *ma)
{
	size_t i;
	pdlc_complex_t lmi, lma;
	lmi.real = NAN;
	lmi.imag = NAN;
	lma.real = NAN;
	lma.imag = NAN;

	for (i = 0; i < cbuf->length; i++) {
		lmi.real = fminf(lmi.real, cbuf->data[i].real);
		lmi.imag = fminf(lmi.imag, cbuf->data[i].imag);
		lma.real = fmaxf(lma.real, cbuf->data[i].real);
		lma.imag = fmaxf(lma.imag, cbuf->data[i].imag);
	}

	*mi = lmi;
	*ma = lma;
}


pdlc_buffer_t* pdlc_fb_fb_min(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = fminf(lhs->data[i], rhs->data[i]);

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_min_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = fminf(lhs->data[i], rhs->data[i]);

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_min(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = fminf(lhs->data[i], rhs);

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_min_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = fminf(lhs->data[i], rhs);

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_min(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = fminf(lhs->data[i].real, rhs->data[i].real);
		res->data[i].imag = fminf(lhs->data[i].imag, rhs->data[i].imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_min_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = fminf(lhs->data[i].real, rhs->data[i].real);
		lhs->data[i].imag = fminf(lhs->data[i].imag, rhs->data[i].imag);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_min(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = fminf(lhs->data[i].real, rhs.real);
		res->data[i].imag = fminf(lhs->data[i].imag, rhs.imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_min_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = fminf(lhs->data[i].real, rhs.real);
		lhs->data[i].imag = fminf(lhs->data[i].imag, rhs.imag);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fb_min(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = fminf(lhs->data[i].real, rhs->data[i]);
		res->data[i].imag = fminf(lhs->data[i].imag, rhs->data[i]);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_min_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = fminf(lhs->data[i].real, rhs->data[i]);
		lhs->data[i].imag = fminf(lhs->data[i].imag, rhs->data[i]);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_min(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = fminf(lhs->data[i].real, rhs);
		res->data[i].imag = fminf(lhs->data[i].imag, rhs);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_min_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = fminf(lhs->data[i].real, rhs);
		lhs->data[i].imag = fminf(lhs->data[i].imag, rhs);
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_max(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = fmaxf(lhs->data[i], rhs->data[i]);

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_max_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = fmaxf(lhs->data[i], rhs->data[i]);

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_max(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = fmaxf(lhs->data[i], rhs);

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_max_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = fmaxf(lhs->data[i], rhs);

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_max(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = fmaxf(lhs->data[i].real, rhs->data[i].real);
		res->data[i].imag = fmaxf(lhs->data[i].imag, rhs->data[i].imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_max_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = fmaxf(lhs->data[i].real, rhs->data[i].real);
		lhs->data[i].imag = fmaxf(lhs->data[i].imag, rhs->data[i].imag);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_max(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = fmaxf(lhs->data[i].real, rhs.real);
		res->data[i].imag = fmaxf(lhs->data[i].imag, rhs.imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_max_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = fmaxf(lhs->data[i].real, rhs.real);
		lhs->data[i].imag = fmaxf(lhs->data[i].imag, rhs.imag);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fb_max(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = fmaxf(lhs->data[i].real, rhs->data[i]);
		res->data[i].imag = fmaxf(lhs->data[i].imag, rhs->data[i]);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_max_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = fmaxf(lhs->data[i].real, rhs->data[i]);
		lhs->data[i].imag = fmaxf(lhs->data[i].imag, rhs->data[i]);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_max(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = fmaxf(lhs->data[i].real, rhs);
		res->data[i].imag = fmaxf(lhs->data[i].imag, rhs);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_max_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = fmaxf(lhs->data[i].real, rhs);
		lhs->data[i].imag = fmaxf(lhs->data[i].imag, rhs);
	}

	return lhs;
}


pdlc_buffer_t* pdlc_buffer_clipp(const pdlc_buffer_t *fbuf, float mi, float ma, pdlc_buffer_t *res)
{
	const size_t len = fbuf->length;
	size_t i;
	float tmp;

	if (mi > ma) {
		tmp = mi;
		mi = ma;
		ma = tmp;
	}

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = fminf(ma, fmaxf(mi, fbuf->data[i]));

	return res;
}


pdlc_buffer_t* pdlc_buffer_clipp_inplace(pdlc_buffer_t *fbuf, float mi, float ma)
{
	const size_t len = fbuf->length;
	size_t i;
	float tmp;

	if (mi > ma) {
		tmp = mi;
		mi = ma;
		ma = tmp;
	}

	for (i = 0; i < len; i++)
		fbuf->data[i] = fminf(ma, fmaxf(mi, fbuf->data[i]));

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_clipp(const pdlc_complex_buffer_t *cbuf, float mi, float ma, pdlc_complex_buffer_t *res)
{
	const size_t len = cbuf->length;
	size_t i;
	float tmp;

	if (mi > ma) {
		tmp = mi;
		mi = ma;
		ma = tmp;
	}

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = fminf(ma, fmaxf(mi, cbuf->data[i].real));
		res->data[i].imag = fminf(ma, fmaxf(mi, cbuf->data[i].imag));
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_clipp_inplace(pdlc_complex_buffer_t *cbuf, float mi, float ma)
{
	const size_t len = cbuf->length;
	size_t i;
	float tmp;

	if (mi > ma) {
		tmp = mi;
		mi = ma;
		ma = tmp;
	}

	for (i = 0; i < len; i++) {
		cbuf->data[i].real = fminf(ma, fmaxf(mi, cbuf->data[i].real));
		cbuf->data[i].imag = fminf(ma, fmaxf(mi, cbuf->data[i].imag));
	}

	return cbuf;
}



/* Arithmetic modulo */

static inline float pdlc_fmodf(float x, float y)
{
	if (isnan(x) || isnan(y) || isinf(x) || y == 0.0f)
		return NAN;

	if (x == 0.0f || x == -0.0f)
		return x;

	return (x - floorf(x / y)*y);
}


pdlc_buffer_t* pdlc_fb_fb_modulo(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = pdlc_fmodf(lhs->data[i], rhs->data[i]);

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_modulo_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = pdlc_fmodf(lhs->data[i], rhs->data[i]);

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_modulo(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = pdlc_fmodf(lhs->data[i], rhs);

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_modulo_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = pdlc_fmodf(lhs->data[i], rhs);

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_emod(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = pdlc_fmodf(lhs->data[i], rhs->data[i]);

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_emod_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = pdlc_fmodf(lhs->data[i], rhs->data[i]);

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_emod(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = pdlc_fmodf(lhs->data[i], rhs);

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_emod_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = pdlc_fmodf(lhs->data[i], rhs);

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_emod(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = pdlc_fmodf(lhs->data[i].real, rhs->data[i].real);
		res->data[i].imag = pdlc_fmodf(lhs->data[i].imag, rhs->data[i].imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_emod_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = pdlc_fmodf(lhs->data[i].real, rhs->data[i].real);
		lhs->data[i].imag = pdlc_fmodf(lhs->data[i].imag, rhs->data[i].imag);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_emod(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = pdlc_fmodf(lhs->data[i].real, rhs.real);
		res->data[i].imag = pdlc_fmodf(lhs->data[i].imag, rhs.imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_emod_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = pdlc_fmodf(lhs->data[i].real, rhs.real);
		lhs->data[i].imag = pdlc_fmodf(lhs->data[i].imag, rhs.imag);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_emod(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = pdlc_fmodf(lhs->data[i], rhs->data[i].real);
		res->data[i].imag = pdlc_fmodf(lhs->data[i], rhs->data[i].imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_emod(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = pdlc_fmodf(lhs->data[i], rhs.real);
		res->data[i].imag = pdlc_fmodf(lhs->data[i], rhs.imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_emod(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = pdlc_fmodf(lhs->data[i].real, rhs->data[i]);
		res->data[i].imag = pdlc_fmodf(lhs->data[i].imag, rhs->data[i]);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_emod_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = pdlc_fmodf(lhs->data[i].real, rhs->data[i]);
		lhs->data[i].imag = pdlc_fmodf(lhs->data[i].imag, rhs->data[i]);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_emod(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = pdlc_fmodf(lhs->data[i].real, rhs);
		res->data[i].imag = pdlc_fmodf(lhs->data[i].imag, rhs);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_emod_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = pdlc_fmodf(lhs->data[i].real, rhs);
		lhs->data[i].imag = pdlc_fmodf(lhs->data[i].imag, rhs);
	}

	return lhs;
}


