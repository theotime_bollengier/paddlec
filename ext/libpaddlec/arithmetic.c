/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include "libpaddlec.h"


pdlc_buffer_t* pdlc_buffer_unaryminus(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = -fbuf->data[i];

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_unaryminus_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = -fbuf->data[i];

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_unaryminus(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *ocbuf)
{
	size_t i;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(cbuf->length);
	else
		pdlc_complex_buffer_resize(ocbuf, cbuf->length, 0);

	for (i = 0; i < cbuf->length; i++) {
		ocbuf->data[i].real = -cbuf->data[i].real;
		ocbuf->data[i].imag = -cbuf->data[i].imag;
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_unaryminus_inplace(pdlc_complex_buffer_t *cbuf)
{
	size_t i;

	for (i = 0; i < cbuf->length; i++) {
		cbuf->data[i].real = -cbuf->data[i].real;
		cbuf->data[i].imag = -cbuf->data[i].imag;
	}

	return cbuf;
}


pdlc_buffer_t* pdlc_fb_fb_sub(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] - rhs->data[i];

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_sub_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] - rhs->data[i];

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_sub(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] - rhs;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_sub_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] - rhs;

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_sub(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real - rhs->data[i].real;
		res->data[i].imag = lhs->data[i].imag - rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_sub_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real - rhs->data[i].real;
		lhs->data[i].imag = lhs->data[i].imag - rhs->data[i].imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_sub(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real - rhs.real;
		res->data[i].imag = lhs->data[i].imag - rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_sub_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real - rhs.real;
		lhs->data[i].imag = lhs->data[i].imag - rhs.imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_sub(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] - rhs->data[i].real;
		res->data[i].imag = -rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_sub(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] - rhs.real;
		res->data[i].imag = -rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_sub(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real - rhs->data[i];
		res->data[i].imag = lhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_sub_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real - rhs->data[i];
		lhs->data[i].imag = lhs->data[i].imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_sub(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real - rhs;
		res->data[i].imag = lhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_sub_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real - rhs;
		lhs->data[i].imag = lhs->data[i].imag;
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_esub(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] - rhs->data[i];

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_esub_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] - rhs->data[i];

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_esub(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] - rhs;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_esub_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] - rhs;

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_esub(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real - rhs->data[i].real;
		res->data[i].imag = lhs->data[i].imag - rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_esub_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real - rhs->data[i].real;
		lhs->data[i].imag = lhs->data[i].imag - rhs->data[i].imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_esub(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real - rhs.real;
		res->data[i].imag = lhs->data[i].imag - rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_esub_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real - rhs.real;
		lhs->data[i].imag = lhs->data[i].imag - rhs.imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_esub(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] - rhs->data[i].real;
		res->data[i].imag = lhs->data[i] - rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_esub(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] - rhs.real;
		res->data[i].imag = lhs->data[i] - rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_esub(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real - rhs->data[i];
		res->data[i].imag = lhs->data[i].imag - rhs->data[i];
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_esub_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real - rhs->data[i];
		lhs->data[i].imag = lhs->data[i].imag - rhs->data[i];
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_esub(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real - rhs;
		res->data[i].imag = lhs->data[i].imag - rhs;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_esub_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real - rhs;
		lhs->data[i].imag = lhs->data[i].imag - rhs;
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_add(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] + rhs->data[i];

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_add_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] + rhs->data[i];

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_add(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] + rhs;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_add_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] + rhs;

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_add(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real + rhs->data[i].real;
		res->data[i].imag = lhs->data[i].imag + rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_add_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real + rhs->data[i].real;
		lhs->data[i].imag = lhs->data[i].imag + rhs->data[i].imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_add(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real + rhs.real;
		res->data[i].imag = lhs->data[i].imag + rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_add_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real + rhs.real;
		lhs->data[i].imag = lhs->data[i].imag + rhs.imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_add(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] + rhs->data[i].real;
		res->data[i].imag = rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_add(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] + rhs.real;
		res->data[i].imag = rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_add(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real + rhs->data[i];
		res->data[i].imag = lhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_add_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real + rhs->data[i];
		lhs->data[i].imag = lhs->data[i].imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_add(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real + rhs;
		res->data[i].imag = lhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_add_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real + rhs;
		lhs->data[i].imag = lhs->data[i].imag;
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_eadd(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] + rhs->data[i];

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_eadd_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] + rhs->data[i];

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_eadd(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] + rhs;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_eadd_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] + rhs;

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_eadd(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real + rhs->data[i].real;
		res->data[i].imag = lhs->data[i].imag + rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_eadd_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real + rhs->data[i].real;
		lhs->data[i].imag = lhs->data[i].imag + rhs->data[i].imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_eadd(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real + rhs.real;
		res->data[i].imag = lhs->data[i].imag + rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_eadd_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real + rhs.real;
		lhs->data[i].imag = lhs->data[i].imag + rhs.imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_eadd(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] + rhs->data[i].real;
		res->data[i].imag = lhs->data[i] + rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_eadd(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] + rhs.real;
		res->data[i].imag = lhs->data[i] + rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_eadd(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real + rhs->data[i];
		res->data[i].imag = lhs->data[i].imag + rhs->data[i];
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_eadd_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real + rhs->data[i];
		lhs->data[i].imag = lhs->data[i].imag + rhs->data[i];
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_eadd(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real + rhs;
		res->data[i].imag = lhs->data[i].imag + rhs;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_eadd_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real + rhs;
		lhs->data[i].imag = lhs->data[i].imag + rhs;
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_mult(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] * rhs->data[i];

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_mult_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] * rhs->data[i];

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_mult(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] * rhs;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_mult_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] * rhs;

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_mult(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	pdlc_complex_t tmp;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		tmp.real = lhs->data[i].real * rhs->data[i].real - lhs->data[i].imag * rhs->data[i].imag;
		tmp.imag = lhs->data[i].real * rhs->data[i].imag + lhs->data[i].imag * rhs->data[i].real;
		res->data[i] = tmp;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_mult_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	pdlc_complex_t tmp;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		tmp.real = lhs->data[i].real * rhs->data[i].real - lhs->data[i].imag * rhs->data[i].imag;
		tmp.imag = lhs->data[i].real * rhs->data[i].imag + lhs->data[i].imag * rhs->data[i].real;
		lhs->data[i] = tmp;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_mult(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	pdlc_complex_t tmp;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		tmp.real = lhs->data[i].real * rhs.real - lhs->data[i].imag * rhs.imag;
		tmp.imag = lhs->data[i].real * rhs.imag + lhs->data[i].imag * rhs.real;
		res->data[i] = tmp;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_mult_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	pdlc_complex_t tmp;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		tmp.real = lhs->data[i].real * rhs.real - lhs->data[i].imag * rhs.imag;
		tmp.imag = lhs->data[i].real * rhs.imag + lhs->data[i].imag * rhs.real;
		lhs->data[i] = tmp;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_mult(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] * rhs->data[i].real;
		res->data[i].imag = lhs->data[i] * rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_mult(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] * rhs.real;
		res->data[i].imag = lhs->data[i] * rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_mult(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real * rhs->data[i];
		res->data[i].imag = lhs->data[i].imag * rhs->data[i];
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_mult_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real * rhs->data[i];
		lhs->data[i].imag = lhs->data[i].imag * rhs->data[i];
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_mult(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real * rhs;
		res->data[i].imag = lhs->data[i].imag * rhs;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_mult_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real * rhs;
		lhs->data[i].imag = lhs->data[i].imag * rhs;
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_emult(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] * rhs->data[i];

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_emult_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] * rhs->data[i];

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_emult(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] * rhs;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_emult_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] * rhs;

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_emult(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real * rhs->data[i].real;
		res->data[i].imag = lhs->data[i].imag * rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_emult_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real * rhs->data[i].real;
		lhs->data[i].imag = lhs->data[i].imag * rhs->data[i].imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_emult(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real * rhs.real;
		res->data[i].imag = lhs->data[i].imag * rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_emult_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real * rhs.real;
		lhs->data[i].imag = lhs->data[i].imag * rhs.imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_emult(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] * rhs->data[i].real;
		res->data[i].imag = lhs->data[i] * rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_emult(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] * rhs.real;
		res->data[i].imag = lhs->data[i] * rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_emult(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real * rhs->data[i];
		res->data[i].imag = lhs->data[i].imag * rhs->data[i];
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_emult_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real * rhs->data[i];
		lhs->data[i].imag = lhs->data[i].imag * rhs->data[i];
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_emult(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real * rhs;
		res->data[i].imag = lhs->data[i].imag * rhs;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_emult_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real * rhs;
		lhs->data[i].imag = lhs->data[i].imag * rhs;
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_div(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] / rhs->data[i];

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_div_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] / rhs->data[i];

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_div(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] / rhs;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_div_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] / rhs;

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_div(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	pdlc_complex_t tmp;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		tmp.real = (lhs->data[i].real*rhs->data[i].real + lhs->data[i].imag*rhs->data[i].imag) / (rhs->data[i].real*rhs->data[i].real + rhs->data[i].imag*rhs->data[i].imag);
		tmp.imag = (lhs->data[i].imag*rhs->data[i].real - lhs->data[i].real*rhs->data[i].imag) / (rhs->data[i].real*rhs->data[i].real + rhs->data[i].imag*rhs->data[i].imag);
		res->data[i] = tmp;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_div_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	pdlc_complex_t tmp;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		tmp.real = (lhs->data[i].real*rhs->data[i].real + lhs->data[i].imag*rhs->data[i].imag) / (rhs->data[i].real*rhs->data[i].real + rhs->data[i].imag*rhs->data[i].imag);
		tmp.imag = (lhs->data[i].imag*rhs->data[i].real - lhs->data[i].real*rhs->data[i].imag) / (rhs->data[i].real*rhs->data[i].real + rhs->data[i].imag*rhs->data[i].imag);
		lhs->data[i] = tmp;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_div(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	pdlc_complex_t tmp;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		tmp.real = (lhs->data[i].real*rhs.real + lhs->data[i].imag*rhs.imag) / (rhs.real*rhs.real + rhs.imag*rhs.imag);
		tmp.imag = (lhs->data[i].imag*rhs.real - lhs->data[i].real*rhs.imag) / (rhs.real*rhs.real + rhs.imag*rhs.imag);
		res->data[i] = tmp;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_div_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	pdlc_complex_t tmp;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		tmp.real = (lhs->data[i].real*rhs.real + lhs->data[i].imag*rhs.imag) / (rhs.real*rhs.real + rhs.imag*rhs.imag);
		tmp.imag = (lhs->data[i].imag*rhs.real - lhs->data[i].real*rhs.imag) / (rhs.real*rhs.real + rhs.imag*rhs.imag);
		lhs->data[i] = tmp;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_div(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = (lhs->data[i]*rhs->data[i].real) / (rhs->data[i].real*rhs->data[i].real + rhs->data[i].imag*rhs->data[i].imag);
		res->data[i].imag = -lhs->data[i]*rhs->data[i].imag / (rhs->data[i].real*rhs->data[i].real + rhs->data[i].imag*rhs->data[i].imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_div(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = (lhs->data[i]*rhs.real) / (rhs.real*rhs.real + rhs.imag*rhs.imag);
		res->data[i].imag = -lhs->data[i]*rhs.imag / (rhs.real*rhs.real + rhs.imag*rhs.imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_div(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = (lhs->data[i].real*rhs->data[i]) / (rhs->data[i]*rhs->data[i]);
		res->data[i].imag = (lhs->data[i].imag*rhs->data[i]) / (rhs->data[i]*rhs->data[i]);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_div_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = (lhs->data[i].real*rhs->data[i]) / (rhs->data[i]*rhs->data[i]);
		lhs->data[i].imag = (lhs->data[i].imag*rhs->data[i]) / (rhs->data[i]*rhs->data[i]);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_div(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = (lhs->data[i].real*rhs) / (rhs*rhs);
		res->data[i].imag = (lhs->data[i].imag*rhs) / (rhs*rhs);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_div_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = (lhs->data[i].real*rhs) / (rhs*rhs);
		lhs->data[i].imag = (lhs->data[i].imag*rhs) / (rhs*rhs);
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_ediv(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] / rhs->data[i];

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_ediv_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] / rhs->data[i];

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_ediv(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = lhs->data[i] / rhs;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_ediv_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = lhs->data[i] / rhs;

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_ediv(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real / rhs->data[i].real;
		res->data[i].imag = lhs->data[i].imag / rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_ediv_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real / rhs->data[i].real;
		lhs->data[i].imag = lhs->data[i].imag / rhs->data[i].imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_ediv(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real / rhs.real;
		res->data[i].imag = lhs->data[i].imag / rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_ediv_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real / rhs.real;
		lhs->data[i].imag = lhs->data[i].imag / rhs.imag;
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_ediv(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] / rhs->data[i].real;
		res->data[i].imag = lhs->data[i] / rhs->data[i].imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_ediv(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i] / rhs.real;
		res->data[i].imag = lhs->data[i] / rhs.imag;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_ediv(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real / rhs->data[i];
		res->data[i].imag = lhs->data[i].imag / rhs->data[i];
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_ediv_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real / rhs->data[i];
		lhs->data[i].imag = lhs->data[i].imag / rhs->data[i];
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_ediv(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = lhs->data[i].real / rhs;
		res->data[i].imag = lhs->data[i].imag / rhs;
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_ediv_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = lhs->data[i].real / rhs;
		lhs->data[i].imag = lhs->data[i].imag / rhs;
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_pow(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = powf(lhs->data[i], rhs->data[i]);

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_pow_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = powf(lhs->data[i], rhs->data[i]);

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_pow(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = powf(lhs->data[i], rhs);

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_pow_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = powf(lhs->data[i], rhs);

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fb_pow(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	pdlc_complex_t tmp;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		tmp.real = powf(hypotf(lhs->data[i].real, lhs->data[i].imag), rhs->data[i]);
		tmp.imag = atan2f(lhs->data[i].imag, lhs->data[i].real) * rhs->data[i];
		res->data[i].real = tmp.real * cosf(tmp.imag);
		res->data[i].imag = tmp.real * sinf(tmp.imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_pow_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	pdlc_complex_t tmp;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		tmp.real = powf(hypotf(lhs->data[i].real, lhs->data[i].imag), rhs->data[i]);
		tmp.imag = atan2f(lhs->data[i].imag, lhs->data[i].real) * rhs->data[i];
		lhs->data[i].real = tmp.real * cosf(tmp.imag);
		lhs->data[i].imag = tmp.real * sinf(tmp.imag);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_pow(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	pdlc_complex_t tmp;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		tmp.real = powf(hypotf(lhs->data[i].real, lhs->data[i].imag), rhs);
		tmp.imag = atan2f(lhs->data[i].imag, lhs->data[i].real) * rhs;
		res->data[i].real = tmp.real * cosf(tmp.imag);
		res->data[i].imag = tmp.real * sinf(tmp.imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_pow_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	pdlc_complex_t tmp;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		tmp.real = powf(hypotf(lhs->data[i].real, lhs->data[i].imag), rhs);
		tmp.imag = atan2f(lhs->data[i].imag, lhs->data[i].real) * rhs;
		lhs->data[i].real = tmp.real * cosf(tmp.imag);
		lhs->data[i].imag = tmp.real * sinf(tmp.imag);
	}

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_epow(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = powf(lhs->data[i], rhs->data[i]);

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_epow_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = powf(lhs->data[i], rhs->data[i]);

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_epow(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = powf(lhs->data[i], rhs);

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_epow_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = powf(lhs->data[i], rhs);

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cb_epow(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = powf(lhs->data[i].real, rhs->data[i].real);
		res->data[i].imag = powf(lhs->data[i].imag, rhs->data[i].imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cb_epow_inplace(pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = powf(lhs->data[i].real, rhs->data[i].real);
		lhs->data[i].imag = powf(lhs->data[i].imag, rhs->data[i].imag);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_cs_epow(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = powf(lhs->data[i].real, rhs.real);
		res->data[i].imag = powf(lhs->data[i].imag, rhs.imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_cs_epow_inplace(pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = powf(lhs->data[i].real, rhs.real);
		lhs->data[i].imag = powf(lhs->data[i].imag, rhs.imag);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_fb_cb_epow(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = powf(lhs->data[i], rhs->data[i].real);
		res->data[i].imag = powf(lhs->data[i], rhs->data[i].imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_fb_cs_epow(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = powf(lhs->data[i], rhs.real);
		res->data[i].imag = powf(lhs->data[i], rhs.imag);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_epow(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = powf(lhs->data[i].real, rhs->data[i]);
		res->data[i].imag = powf(lhs->data[i].imag, rhs->data[i]);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fb_epow_inplace(pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = powf(lhs->data[i].real, rhs->data[i]);
		lhs->data[i].imag = powf(lhs->data[i].imag, rhs->data[i]);
	}

	return lhs;
}


pdlc_complex_buffer_t* pdlc_cb_fs_epow(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_complex_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++) {
		res->data[i].real = powf(lhs->data[i].real, rhs);
		res->data[i].imag = powf(lhs->data[i].imag, rhs);
	}

	return res;
}


pdlc_complex_buffer_t* pdlc_cb_fs_epow_inplace(pdlc_complex_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++) {
		lhs->data[i].real = powf(lhs->data[i].real, rhs);
		lhs->data[i].imag = powf(lhs->data[i].imag, rhs);
	}

	return lhs;
}


