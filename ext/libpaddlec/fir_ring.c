/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

// +----+----+----+----+
// |1234|5678|----|---0|
// |2345|678-|----|--01|
// |3456|78--|----|-012|
// |4567|8---|----|0123|
// |5678|----|---0|1234|
// |678-|----|--01|2345|
// |78--|----|-012|3456|
// |8---|----|0123|4567|
// |----|---0|1234|5678|
// |----|--01|2345|678-|
// |----|-012|3456|78--|
// |----|0123|4567|8---|
// |---0|1234|5678|----|
// |--01|2345|678-|----|
// |-012|3456|78--|----|
// |0123|4567|8---|----|
// +----+----+----+----+
// |---0|1234|5678|----|
// |--01|2345|678-|----|
// |-012|3456|78--|----|
// |0123|4567|8---|----|
// +----+----+----+----+

// 012345678
// 012345678
// 012345678
// 012345678
// 012345678
// |----|---0|1234|5678|----|
// |----|0123|4567|8---|----|
// |-012|3456|78--|----|----|
// |2345|678-|----|----|--01|
// |5678|----|----|---0|1234|
// |8---|----|----|0123|4567|
// |----|----|-012|3456|78--|
// |----|--01|2345|678-|----|
// |---0|1234|5678|----|----|
// |0123|4567|8---|----|----|
// +----+----+----+----+----|
// |---0|1234|5678|----|----|
// |--01|2345|678-|----|----|
// |-012|3456|78--|----|----|
// |0123|4567|8---|----|----|
// +----+----+----+----+----|

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <xmmintrin.h>
#include <smmintrin.h>
#include <immintrin.h>
#include "libpaddlec.h"


pdlc_fir_filter_t* pdlc_fir_filter_new(int order)
{
	pdlc_fir_filter_t *fir;

	fir = malloc(sizeof(pdlc_fir_filter_t));

	fir->coefs[0] = NULL;
	fir->coefs[1] = NULL;
	fir->coefs[2] = NULL;
	fir->coefs[3] = NULL;
	fir->coefs[4] = NULL;
	fir->coefs[5] = NULL;
	fir->coefs[6] = NULL;
	fir->coefs[7] = NULL;
	fir->state = NULL;
	fir->nb_coefs = 0;
	fir->state_len = 0;
	fir->coef_len = 0;
	fir->index = 0;
	fir->index_mask = 0;

	if (order >= 0)
		pdlc_fir_filter_initialize(fir, order);

	return fir;
}


#if 0
void pdlc_fir_filter_initialize(pdlc_fir_filter_t* fir, int order)
{
	int i;

	for (i = 0; i < 4; i++) {
		if (fir->coefs[i]) {
			_mm_free(fir->coefs[i]);
			fir->coefs[i] = NULL;
		}
	}
	if (fir->state) {
		_mm_free(fir->state);
		fir->state = NULL;
	}
	fir->nb_coefs = 0;
	fir->state_len = 0;
	fir->coef_len = 0;
	fir->index = 0;
	if (order < 0)
		return;
	if (order > 67108863) {
		fprintf(stderr, "ERROR: libpaddlec: Filter order cannot be greater than 67108864\n");
		exit(EXIT_FAILURE);
	}

	fir->nb_coefs = (unsigned int)(order + 1);
	fir->state_len = (unsigned int)(pow(2.0, ceil(log2(((fir->nb_coefs + 4 + 3) >> 2) << 2))));
	fir->coef_len = ((fir->nb_coefs + 3 + 3) >> 2) << 2;
	fir->index = 0;
	fir->index_mask = fir->state_len - 1;

	fir->state    = _mm_malloc(fir->state_len * sizeof(float), sizeof(__m128));
	if (fir->state == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->state_len * sizeof(float));
		exit(EXIT_FAILURE);
	}

	fir->coefs[0] = _mm_malloc(fir->coef_len * sizeof(float), sizeof(__m128));
	if (fir->coefs[0] == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->coef_len * sizeof(float));
		exit(EXIT_FAILURE);
	}
	fir->coefs[1] = _mm_malloc(fir->coef_len * sizeof(float), sizeof(__m128));
	if (fir->coefs[1] == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->coef_len * sizeof(float));
		exit(EXIT_FAILURE);
	}
	fir->coefs[2] = _mm_malloc(fir->coef_len * sizeof(float), sizeof(__m128));
	if (fir->coefs[2] == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->coef_len * sizeof(float));
		exit(EXIT_FAILURE);
	}
	fir->coefs[3] = _mm_malloc(fir->coef_len * sizeof(float), sizeof(__m128));
	if (fir->coefs[3] == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->coef_len * sizeof(float));
		exit(EXIT_FAILURE);
	}

	memset(fir->state,    0, fir->state_len * sizeof(float));
	memset(fir->coefs[0], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[1], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[2], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[3], 0, fir->coef_len  * sizeof(float));
}
#else
void pdlc_fir_filter_initialize(pdlc_fir_filter_t* fir, int order)
{
	int i;

	for (i = 0; i < 8; i++) {
		if (fir->coefs[i]) {
			_mm_free(fir->coefs[i]);
			fir->coefs[i] = NULL;
		}
	}
	if (fir->state) {
		_mm_free(fir->state);
		fir->state = NULL;
	}
	fir->nb_coefs = 0;
	fir->state_len = 0;
	fir->coef_len = 0;
	fir->index = 0;
	if (order < 0)
		return;
	if (order > 67108863) {
		fprintf(stderr, "ERROR: libpaddlec: Filter order cannot be greater than 67108864\n");
		exit(EXIT_FAILURE);
	}

	fir->nb_coefs = (unsigned int)(order + 1);
	fir->state_len = (unsigned int)(pow(2.0, ceil(log2(((fir->nb_coefs + 8 + 7) >> 3) << 3))));
	fir->coef_len = ((fir->nb_coefs + 7 + 7) >> 3) << 3;
	fir->index = 0;
	fir->index_mask = fir->state_len - 1;

	fir->state    = _mm_malloc(fir->state_len * sizeof(float), sizeof(__m256));
	if (fir->state == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->state_len * sizeof(float));
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < 8; i++) {
		fir->coefs[i] = _mm_malloc(fir->coef_len * sizeof(float), sizeof(__m256));
		if (fir->coefs[i] == NULL) {
			fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->coef_len * sizeof(float));
			exit(EXIT_FAILURE);
		}
	}

	memset(fir->state,    0, fir->state_len * sizeof(float));
	memset(fir->coefs[0], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[1], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[2], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[3], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[4], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[5], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[6], 0, fir->coef_len  * sizeof(float));
	memset(fir->coefs[7], 0, fir->coef_len  * sizeof(float));
}
#endif

void pdlc_fir_filter_free(pdlc_fir_filter_t* fir)
{
	int i;

	if (!fir)
		return;

	for (i = 0; i < 8; i++)
		if (fir->coefs[i])
			_mm_free(fir->coefs[i]);

	if (fir->state)
		_mm_free(fir->state);

	free(fir);
}


int pdlc_fir_filter_set_coef_at(pdlc_fir_filter_t* fir, int index, float  value)
{
	int i;

	if (index < 0 || index >= (int)fir->nb_coefs)
		return -1;

	for (i = 0; i < 8; i++)
		fir->coefs[i][(fir->nb_coefs - 1 - index + i) % fir->coef_len] = value;

	return 0;
}


int pdlc_fir_filter_get_coef_at(const pdlc_fir_filter_t* fir, int index, float *value)
{
	if (index < 0 || index >= (int)fir->nb_coefs)
		return -1;

	if (value)
		*value = fir->coefs[0][fir->nb_coefs - 1 - index];

	return 0;
}


void pdlc_fir_filter_reset(pdlc_fir_filter_t* fir)
{
	memset(fir->state, 0, fir->coef_len * sizeof(float));
	fir->index = 0;
}


float pdlc_fir_filter_filter_float(pdlc_fir_filter_t* fir, float sample)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	const float *coefs = fir->coefs[0];
	float acc = 0.0f;
	unsigned int i, j;

	if (flt_len < 1)
		return sample;

	fir->state[fir->index] = sample;

	for (i = 0, j = start_index; i < nb_coefs; i++, j = (j+1) & mask)
		acc += coefs[i] * fir->state[j];

	fir->index = (fir->index + 1) & mask;

	return acc;
}


float pdlc_fir_filter_filter_float_sse(pdlc_fir_filter_t* fir, float sample)
{
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int start_index = (flt_len + fir->index + 1 - fir->nb_coefs) & mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int startsimd = start_index >> 2;
	const unsigned int masksimd = mask >> 2;
	unsigned int i, j;
	register __m128 acc = _mm_setzero_ps();
	register __m128 prod;
	__m128 *coefs = (__m128*)fir->coefs[start_index & 3];
	__m128 *state = (__m128*)fir->state;

	if (flt_len < 1)
		return sample;

	fir->state[fir->index] = sample;
	fir->index = (fir->index + 1) & mask;

	for (i = 0, j = startsimd; i < lensimd; i++, j = (j+1) & masksimd) {
		prod = _mm_mul_ps(coefs[i], state[j]);
		acc  = _mm_add_ps(acc, prod);
	}

	return acc[0] + acc[1] + acc[2] + acc[3];
}


pdlc_buffer_t* pdlc_fir_filter_filter_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const float *coefs = fir->coefs[0];
	const unsigned int mask = fir->index_mask;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	float acc;
	unsigned int i, j;
	size_t k;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(ifbuf->length);
	else
		pdlc_buffer_resize(ofbuf, ifbuf->length);

	if (flt_len < 1) {
		for (k = 0; k < ifbuf->length; k++)
			ofbuf->data[k] = ifbuf->data[k];
		return ofbuf;
	}

	for (k = 0; k < ifbuf->length; k++) {
		fir->state[fir->index] = ifbuf->data[k];
		fir->index = (fir->index + 1) & mask;
		acc = 0.0f;
		for (i = 0, j = start_index; i < nb_coefs; i++, j = (j+1) & mask)
			acc += coefs[i] * fir->state[j];
		ofbuf->data[k] = acc;
		start_index = (start_index + 1) & mask;
	}

	return ofbuf;
}


pdlc_buffer_t* pdlc_fir_filter_filter_float_buffer_sse(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int masksimd = mask >> 2;
	unsigned int start_index = (flt_len + fir->index + 1 - fir->nb_coefs) & mask;
	unsigned int startsimd = start_index >> 2;
	unsigned int i, j;
	size_t k;
	register __m128 acc;
	register __m128 prod;
	__m128 *coefs;
	__m128 *state = (__m128*)fir->state;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(ifbuf->length);
	else
		pdlc_buffer_resize(ofbuf, ifbuf->length);

	if (flt_len < 1) {
		for (k = 0; k < ifbuf->length; k++)
			ofbuf->data[k] = ifbuf->data[k];
		return ofbuf;
	}

	for (k = 0; k < ifbuf->length; k++) {
		fir->state[fir->index] = ifbuf->data[k];
		fir->index = (fir->index + 1) & mask;
		coefs = (__m128*)fir->coefs[start_index & 3];
		acc = _mm_setzero_ps();
		for (i = 0, j = startsimd; i < lensimd; i++, j = (j+1) & masksimd) {
			prod = _mm_mul_ps(coefs[i], state[j]);
			acc  = _mm_add_ps(acc, prod);
		}
		ofbuf->data[k] = acc[0] + acc[1] + acc[2] + acc[3];
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 2;
	}

	return ofbuf;
}


pdlc_buffer_t* pdlc_fir_filter_filter_float_buffer_psse(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 2;
	const unsigned int masksimd = mask >> 2;
	unsigned int start_index = (flt_len + fir->index + 1 - fir->nb_coefs) & mask;
	unsigned int startsimd = start_index >> 2;
	unsigned int i, j;
	size_t ibuflen, k;
	register __m128 acc0, acc1, acc2, acc3;
	register __m128 prod0, prod1, prod2, prod3;
	register __m128 stater;
	const __m128 *coefs0, *coefs1, *coefs2, *coefs3;
	__m128 *state = (__m128*)fir->state;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(ifbuf->length);
	else
		pdlc_buffer_resize(ofbuf, ifbuf->length);

	if (flt_len < 1) {
		for (k = 0; k < ifbuf->length; k++)
			ofbuf->data[k] = ifbuf->data[k];
		return ofbuf;
	}

	ibuflen = ifbuf->length;

	k = 0;
	while ((start_index & 3) && k < ibuflen) {
		fir->state[fir->index] = ifbuf->data[k];
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m128*)fir->coefs[start_index & 3];
		acc0 = _mm_setzero_ps();
		for (i = 0, j = startsimd; i < lensimd; i++, j = (j+1) & masksimd) {
			prod0 = _mm_mul_ps(coefs0[i], state[j]);
			acc0  = _mm_add_ps(acc0, prod0);
		}
		ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 2;
		k++;
	}
	while (k + 4 <= ibuflen) {
		fir->state[fir->index] = ifbuf->data[k + 0]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 1]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 2]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 3]; fir->index = (fir->index + 1) & mask;
		coefs0 = (__m128*)fir->coefs[(start_index + 0) & 3];
		coefs1 = (__m128*)fir->coefs[(start_index + 1) & 3];
		coefs2 = (__m128*)fir->coefs[(start_index + 2) & 3];
		coefs3 = (__m128*)fir->coefs[(start_index + 3) & 3];
		acc0 = _mm_setzero_ps();
		acc1 = _mm_setzero_ps();
		acc2 = _mm_setzero_ps();
		acc3 = _mm_setzero_ps();
		for (i = 0, j = startsimd; i < lensimd; i++, j = (j+1) & masksimd) {
			stater = state[j];
			prod0 = _mm_mul_ps(coefs0[i], stater);
			acc0  = _mm_add_ps(acc0, prod0);
			prod1 = _mm_mul_ps(coefs1[i], stater);
			acc1  = _mm_add_ps(acc1, prod1);
			prod2 = _mm_mul_ps(coefs2[i], stater);
			acc2  = _mm_add_ps(acc2, prod2);
			prod3 = _mm_mul_ps(coefs3[i], stater);
			acc3  = _mm_add_ps(acc3, prod3);
		}
		ofbuf->data[k+0] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
		ofbuf->data[k+1] = acc1[0] + acc1[1] + acc1[2] + acc1[3];
		ofbuf->data[k+2] = acc2[0] + acc2[1] + acc2[2] + acc2[3];
		ofbuf->data[k+3] = acc3[0] + acc3[1] + acc3[2] + acc3[3];
		start_index = (start_index + 4) & mask;
		startsimd = start_index >> 2;
		k += 4;
	}
	for (; k < ibuflen; k++) {
		fir->state[fir->index] = ifbuf->data[k];
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m128*)fir->coefs[start_index & 3];
		acc0 = _mm_setzero_ps();
		for (i = 0, j = startsimd; i < lensimd; i++, j = (j+1) & masksimd) {
			prod0 = _mm_mul_ps(coefs0[i], state[j]);
			acc0  = _mm_add_ps(acc0, prod0);
		}
		ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3];
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 2;
	}

	return ofbuf;
}


pdlc_buffer_t* pdlc_fir_filter_filter_float_buffer_transp2(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const float *coefs = fir->coefs[0];
	unsigned int i;
	size_t k;
	float in;
	float *state = fir->state;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(ifbuf->length);
	else
		pdlc_buffer_resize(ofbuf, ifbuf->length);

	if (flt_len < 1) {
		for (k = 0; k < ifbuf->length; k++)
			ofbuf->data[k] = ifbuf->data[k];
		return ofbuf;
	}

	for (k = 0; k < ifbuf->length; k++) {
		in = ifbuf->data[k];
		state[0] = in*coefs[0] + state[0];
		ofbuf->data[k] = state[0];
		for (i = 1; i < nb_coefs; i++) {
			state[i-1] = in*coefs[i] + state[i];
		}
	}

	return ofbuf;
}


pdlc_buffer_t* pdlc_fir_filter_filter_float_buffer_transp2_sse(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int lensimd  = (fir->nb_coefs + 3) >> 2;
	unsigned int i;
	size_t k;
	const __m128 *coefs = (__m128*)fir->coefs[0];
	__m128 *state = (__m128*)fir->state;
	register __m128 in, prod, shuffle;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(ifbuf->length);
	else
		pdlc_buffer_resize(ofbuf, ifbuf->length);

	if (fir->state_len < 1) {
		for (k = 0; k < ifbuf->length; k++)
			ofbuf->data[k] = ifbuf->data[k];
		return ofbuf;
	}

	for (k = 0; k < ifbuf->length; k++) {
		//in = _mm_load1_ps(ifbuf->data + k);
		in = _mm_set_ps1(ifbuf->data[k]);
		prod = _mm_mul_ps(in, coefs[0]);
		state[0] = _mm_add_ps(state[0], prod);
		ofbuf->data[k] = state[0][0];
		for (i = 1; i < lensimd; i++) {
			prod = _mm_mul_ps(in, coefs[i]);
			state[i] = _mm_add_ps(state[i], prod);
			shuffle = _mm_shuffle_ps(state[i-1], state[i-1], 0x39);
			state[i-1] = _mm_insert_ps(shuffle, state[i], 0x30);
		}
		shuffle = _mm_shuffle_ps(state[i-1], state[i-1], 0x39);
		state[i-1] = _mm_insert_ps(shuffle, shuffle, 0x38);
	}

	return ofbuf;
}


pdlc_buffer_t* pdlc_fir_filter_filter_float_buffer_pavx(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
#ifdef __AVX__
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 3;
	const unsigned int masksimd = mask >> 3;
	unsigned int start_index = (flt_len + fir->index + 1 - fir->nb_coefs) & mask;
	unsigned int startsimd = start_index >> 3;
	unsigned int i, j;
	size_t ibuflen, k;
	register __m256 acc0, acc1, acc2, acc3, acc4, acc5, acc6, acc7;
	register __m256 prod0, prod1, prod2, prod3, prod4, prod5, prod6, prod7;
	register __m256 stater;
	const __m256 *coefs0, *coefs1, *coefs2, *coefs3, *coefs4, *coefs5, *coefs6, *coefs7;
	__m256 *state = (__m256*)fir->state;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(ifbuf->length);
	else
		pdlc_buffer_resize(ofbuf, ifbuf->length);

	if (flt_len < 1) {
		for (k = 0; k < ifbuf->length; k++)
			ofbuf->data[k] = ifbuf->data[k];
		return ofbuf;
	}

	ibuflen = ifbuf->length;

	k = 0;
	while ((start_index & 7) && k < ibuflen) {
		fir->state[fir->index] = ifbuf->data[k];
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m256*)fir->coefs[start_index & 7];
		acc0 = _mm256_setzero_ps();
		for (i = 0, j = startsimd; i < lensimd; i++, j = (j+1) & masksimd) {
			prod0 = _mm256_mul_ps(coefs0[i], state[j]);
			acc0  = _mm256_add_ps(acc0, prod0);
		}
		ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 3;
		k++;
	}
	while (k + 8 <= ibuflen) {
		fir->state[fir->index] = ifbuf->data[k + 0]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 1]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 2]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 3]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 4]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 5]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 6]; fir->index = (fir->index + 1) & mask;
		fir->state[fir->index] = ifbuf->data[k + 7]; fir->index = (fir->index + 1) & mask;
		coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
		coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
		coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
		coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
		coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
		coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
		coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
		coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
		acc0 = _mm256_setzero_ps();
		acc1 = _mm256_setzero_ps();
		acc2 = _mm256_setzero_ps();
		acc3 = _mm256_setzero_ps();
		acc4 = _mm256_setzero_ps();
		acc5 = _mm256_setzero_ps();
		acc6 = _mm256_setzero_ps();
		acc7 = _mm256_setzero_ps();
		for (i = 0, j = startsimd; i < lensimd; i++, j = (j+1) & masksimd) {
			stater = state[j];
			prod0 = _mm256_mul_ps(coefs0[i], stater);
			acc0  = _mm256_add_ps(acc0, prod0);
			prod1 = _mm256_mul_ps(coefs1[i], stater);
			acc1  = _mm256_add_ps(acc1, prod1);
			prod2 = _mm256_mul_ps(coefs2[i], stater);
			acc2  = _mm256_add_ps(acc2, prod2);
			prod3 = _mm256_mul_ps(coefs3[i], stater);
			acc3  = _mm256_add_ps(acc3, prod3);
			prod4 = _mm256_mul_ps(coefs4[i], stater);
			acc4  = _mm256_add_ps(acc4, prod4);
			prod5 = _mm256_mul_ps(coefs5[i], stater);
			acc5  = _mm256_add_ps(acc5, prod5);
			prod6 = _mm256_mul_ps(coefs6[i], stater);
			acc6  = _mm256_add_ps(acc6, prod6);
			prod7 = _mm256_mul_ps(coefs7[i], stater);
			acc7  = _mm256_add_ps(acc7, prod7);
		}
#if 0 // no noticeable impact
		ofbuf->data[k+0] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
		ofbuf->data[k+1] = acc1[0] + acc1[1] + acc1[2] + acc1[3] + acc1[4] + acc1[5] + acc1[6] + acc1[7];
		ofbuf->data[k+2] = acc2[0] + acc2[1] + acc2[2] + acc2[3] + acc2[4] + acc2[5] + acc2[6] + acc2[7];
		ofbuf->data[k+3] = acc3[0] + acc3[1] + acc3[2] + acc3[3] + acc3[4] + acc3[5] + acc3[6] + acc3[7];
		ofbuf->data[k+4] = acc4[0] + acc4[1] + acc4[2] + acc4[3] + acc4[4] + acc4[5] + acc4[6] + acc4[7];
		ofbuf->data[k+5] = acc5[0] + acc5[1] + acc5[2] + acc5[3] + acc5[4] + acc5[5] + acc5[6] + acc5[7];
		ofbuf->data[k+6] = acc6[0] + acc6[1] + acc6[2] + acc6[3] + acc6[4] + acc6[5] + acc6[6] + acc6[7];
		ofbuf->data[k+7] = acc7[0] + acc7[1] + acc7[2] + acc7[3] + acc7[4] + acc7[5] + acc7[6] + acc7[7];
#else
		acc0 = _mm256_hadd_ps(acc0, acc1);
		acc1 = _mm256_hadd_ps(acc2, acc3);
		acc2 = _mm256_hadd_ps(acc4, acc5);
		acc3 = _mm256_hadd_ps(acc6, acc7);
		acc4 = _mm256_hadd_ps(acc0, acc1);
		acc5 = _mm256_hadd_ps(acc2, acc3);
		ofbuf->data[k+0] = acc4[0] + acc4[4];
		ofbuf->data[k+1] = acc4[1] + acc4[5];
		ofbuf->data[k+2] = acc4[2] + acc4[6];
		ofbuf->data[k+3] = acc4[3] + acc4[7];
		ofbuf->data[k+4] = acc5[0] + acc5[4];
		ofbuf->data[k+5] = acc5[1] + acc5[5];
		ofbuf->data[k+6] = acc5[2] + acc5[6];
		ofbuf->data[k+7] = acc5[3] + acc5[7];
#endif
		start_index = (start_index + 8) & mask;
		startsimd = start_index >> 3;
		k += 8;
	}
	for (; k < ibuflen; k++) {
		fir->state[fir->index] = ifbuf->data[k];
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m256*)fir->coefs[start_index & 7];
		acc0 = _mm256_setzero_ps();
		for (i = 0, j = startsimd; i < lensimd; i++, j = (j+1) & masksimd) {
			prod0 = _mm256_mul_ps(coefs0[i], state[j]);
			acc0  = _mm256_add_ps(acc0, prod0);
		}
		ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 3;
	}

	return ofbuf;
#else
	return pdlc_fir_filter_filter_float_buffer_psse(fir, ifbuf, ofbuf);
#endif
}


