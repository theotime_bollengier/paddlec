/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <immintrin.h>

#if !(defined __FMA__) && defined __FMA4__
#include <x86intrin.h>
#endif


void pdlc_fir_filter_inspect(pdlc_fir_filter_t* fir)
{
	size_t i, j;

	printf("nb_coefs: %u, state_len: %u, coef_len: %u, index_mask: 0x%x, index: %u\n",
			fir->nb_coefs, fir->state_len, fir->coef_len, fir->index_mask, fir->index);
	printf("state:    [%.7g", fir->stater[0]);
	for (i = 1; i < fir->state_len; i++)
		printf(", %.7g", fir->stater[i]);
	printf("]\n");
	for (j = 0; j < 8; j++) {
		printf("coefs: {%lu}[%.7g", j, fir->coefs[j][0]);
		for (i = 1; i < fir->coef_len; i++)
			printf(", %.7g", fir->coefs[j][i]);
		printf("]\n");
	}
}


void pdlc_fir_filter_initialize(pdlc_fir_filter_t* fir, int order)
{
	int i;

	if (fir->coefs) {
		for (i = 0; i < 8; i++)
			if (fir->coefs[i])
				_mm_free(fir->coefs[i]);
		free(fir->coefs);
		fir->coefs = NULL;
	}

	if (fir->stater)
		_mm_free(fir->stater);
	fir->stater = NULL;

	if (fir->statei)
		_mm_free(fir->statei);
	fir->statei = NULL;

	fir->nb_coefs = 0;
	fir->state_len = 0;
	fir->coef_len = 0;
	fir->index = 0;
	fir->index_mask = 0;
	fir->counter = 0;
	fir->max_counter = 1;

	if (order < 0)
		return;

	if (order > 67108863) {
		fprintf(stderr, "ERROR: libpaddlec: Filter order cannot be greater than 67108864\n");
		exit(EXIT_FAILURE);
	}

	fir->nb_coefs = (unsigned int)(order + 1);
	fir->coef_len = ((fir->nb_coefs + 7 + 7) >> 3) << 3;
	fir->state_len = (unsigned int)(pow(2.0, ceil(log2(fir->coef_len))));
	fir->index = 0;
	fir->index_mask = fir->state_len - 1;

	fir->coefs = malloc(8*sizeof(float*));
	if (fir->coefs == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", 8 * sizeof(float*));
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < 8; i++) {
		fir->coefs[i] = _mm_malloc(fir->coef_len * sizeof(float), sizeof(__m256));
		if (fir->coefs[i] == NULL) {
			fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->coef_len * sizeof(float));
			exit(EXIT_FAILURE);
		}
	}

	fir->stater = _mm_malloc(fir->state_len * sizeof(float), sizeof(__m256));
	if (fir->stater == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->state_len * sizeof(float));
		exit(EXIT_FAILURE);
	}

	fir->statei = _mm_malloc(fir->state_len * sizeof(float), sizeof(__m256));
	if (fir->statei == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes for FIR!\n", fir->state_len * sizeof(float));
		exit(EXIT_FAILURE);
	}

	memset(fir->stater,   0, fir->state_len * sizeof(float));
	memset(fir->statei,   0, fir->state_len * sizeof(float));
	for (i = 0; i < 8; i++)
		memset(fir->coefs[i], 0, fir->coef_len  * sizeof(float));
}


void pdlc_fir_filter_free(pdlc_fir_filter_t* fir)
{
	int i;

	if (!fir)
		return;

	if (fir->coefs) {
		for (i = 0; i < 8; i++)
			if (fir->coefs[i])
				_mm_free(fir->coefs[i]);
		free(fir->coefs);
	}

	if (fir->stater)
		_mm_free(fir->stater);

	if (fir->statei)
		_mm_free(fir->statei);

	free(fir);
}


size_t pdlc_fir_filter_size(pdlc_fir_filter_t* fir)
{
	size_t res;

	res  = sizeof(pdlc_fir_filter_t);
	res += sizeof(float*)* 8;
	res += sizeof(float) * fir->state_len * 2;
	res += sizeof(float) * fir->coef_len * 8;

	return res;
}


int pdlc_fir_filter_set_coef_at(pdlc_fir_filter_t* fir, int index, float  value)
{
	int i;

	if (index < 0 || index >= (int)fir->nb_coefs)
		return -1;

	for (i = 0; i < 8; i++)
		fir->coefs[i][(fir->nb_coefs - 1 - index + i) % fir->coef_len] = value;

	return 0;
}


float pdlc_fir_filter_filter_float(pdlc_fir_filter_t* fir, float sample, float *delayed)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	const unsigned int middle_index = (start_index + nb_coefs / 2) & mask;
	const unsigned int lensimd = fir->coef_len >> 3;
	const unsigned int startsimd = start_index >> 3;
	const unsigned int masksimd = mask >> 3;
	unsigned int i, j;
	register __m256 acc;
#if !(defined __FMA__ || defined __FMA4__)
	register __m256 prod;
#endif
	const __m256 *coefs = (__m256*)fir->coefs[start_index & 7];
	__m256 *stater = (__m256*)fir->stater;

	fir->stater[fir->index] = sample;
	fir->index = (fir->index + 1) & mask;

	if (delayed) {
		if (nb_coefs & 1)
			*delayed = fir->stater[middle_index];
		else
			*delayed = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
	}

	acc  = _mm256_setzero_ps();
	j = startsimd;
	for (i = 0; i < lensimd; i++) {
#if defined __FMA__
		acc = _mm256_fmadd_ps(coefs[i], stater[j], acc);
#elif defined __FMA4__
		acc = _mm256_macc_ps(coefs[i], stater[j], acc);
#else
		prod = _mm256_mul_ps(coefs[i], stater[j]);
		acc  = _mm256_add_ps(acc, prod);
#endif
		j = (j+1) & masksimd;
	}

	return acc[0] + acc[1] + acc[2] + acc[3] + acc[4] + acc[5] + acc[6] + acc[7];
}


pdlc_complex_t pdlc_fir_filter_filter_complex(pdlc_fir_filter_t* fir, pdlc_complex_t sample, pdlc_complex_t *delayed)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	const unsigned int middle_index = (start_index + nb_coefs / 2) & mask;
	const unsigned int lensimd = fir->coef_len >> 3;
	const unsigned int startsimd = start_index >> 3;
	const unsigned int masksimd = mask >> 3;
	unsigned int i, j;
	pdlc_complex_t res = {0.0f, 0.0f};
	register __m256 accr, acci;
#if !(defined __FMA__ || defined __FMA4__)
	register __m256 prodr, prodi;
#endif
	const __m256 *coefs = (__m256*)fir->coefs[start_index & 7];
	__m256 *stater = (__m256*)fir->stater;
	__m256 *statei = (__m256*)fir->statei;

	fir->stater[fir->index] = sample.real;
	fir->statei[fir->index] = sample.imag;
	fir->index = (fir->index + 1) & mask;

	accr = _mm256_setzero_ps();
	acci = _mm256_setzero_ps();
	j = startsimd;
	for (i = 0; i < lensimd; i++) {
#if defined __FMA__
		accr = _mm256_fmadd_ps(coefs[i], stater[j], accr);
		acci = _mm256_fmadd_ps(coefs[i], statei[j], acci);
#elif defined __FMA4__
		accr = _mm256_macc_ps(coefs[i], stater[j], accr);
		acci = _mm256_macc_ps(coefs[i], statei[j], acci);
#else
		prodr = _mm256_mul_ps(coefs[i], stater[j]);
		prodi = _mm256_mul_ps(coefs[i], statei[j]);
		accr  = _mm256_add_ps(accr, prodr);
		acci  = _mm256_add_ps(acci, prodi);
#endif
		j = (j+1) & masksimd;
	}
	res.real = accr[0] + accr[1] + accr[2] + accr[3] + accr[4] + accr[5] + accr[6] + accr[7];
	res.imag = acci[0] + acci[1] + acci[2] + acci[3] + acci[4] + acci[5] + acci[6] + acci[7];

	if (delayed) {
		if (nb_coefs & 1) {
			delayed->real = fir->stater[middle_index];
			delayed->imag = fir->statei[middle_index];
		}
		else {
			delayed->real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			delayed->imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
		}
	}

	return res;
}


pdlc_buffer_t* pdlc_fir_filter_filter_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf, pdlc_buffer_t *delayed)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 3;
	const unsigned int masksimd = mask >> 3;
	const size_t ibuflen = ifbuf->length;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 3;
	unsigned int middle_index;
	unsigned int i, j;
	size_t k;
	register __m256 acc0, acc1, acc2, acc3, acc4, acc5, acc6, acc7;
#if !(defined __FMA__ || defined __FMA4__)
	register __m256 prod0, prod1, prod2, prod3, prod4, prod5, prod6, prod7;
#endif
	register __m256 statereal;
	const __m256 *coefs0, *coefs1, *coefs2, *coefs3, *coefs4, *coefs5, *coefs6, *coefs7;
	__m256 *stater = (__m256*)fir->stater;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(ibuflen);
	else if (ofbuf->length != ibuflen)
		pdlc_buffer_resize(ofbuf, ibuflen, 0);

	if (delayed) {
		if (delayed->length != ibuflen)
			pdlc_buffer_resize(delayed, ibuflen, 0);
		middle_index = (start_index + nb_coefs / 2) & mask;
		if (nb_coefs & 1) {
			k = 0;
			while ((start_index & 7) && k < ibuflen) {
				fir->stater[fir->index] = ifbuf->data[k];
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m256*)fir->coefs[start_index & 7];
				acc0 = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
#if defined __FMA__
					acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
					acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
					prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
					acc0  = _mm256_add_ps(acc0, prod0);
#endif
					j = (j+1) & masksimd;
				}
				ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 3;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			while (k + 8 <= ibuflen) {
				fir->stater[(fir->index + 0) & mask] = ifbuf->data[k + 0];
				fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
				fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
				fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
				fir->stater[(fir->index + 4) & mask] = ifbuf->data[k + 4];
				fir->stater[(fir->index + 5) & mask] = ifbuf->data[k + 5];
				fir->stater[(fir->index + 6) & mask] = ifbuf->data[k + 6];
				fir->stater[(fir->index + 7) & mask] = ifbuf->data[k + 7];
				fir->index = (fir->index + 8) & mask;
				coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
				coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
				coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
				coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
				coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
				coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
				coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
				coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
				acc0 = _mm256_setzero_ps();
				acc1 = _mm256_setzero_ps();
				acc2 = _mm256_setzero_ps();
				acc3 = _mm256_setzero_ps();
				acc4 = _mm256_setzero_ps();
				acc5 = _mm256_setzero_ps();
				acc6 = _mm256_setzero_ps();
				acc7 = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					statereal = stater[j];
#if defined __FMA__
					acc0 = _mm256_fmadd_ps(coefs0[i], statereal, acc0);
					acc1 = _mm256_fmadd_ps(coefs1[i], statereal, acc1);
					acc2 = _mm256_fmadd_ps(coefs2[i], statereal, acc2);
					acc3 = _mm256_fmadd_ps(coefs3[i], statereal, acc3);
					acc4 = _mm256_fmadd_ps(coefs4[i], statereal, acc4);
					acc5 = _mm256_fmadd_ps(coefs5[i], statereal, acc5);
					acc6 = _mm256_fmadd_ps(coefs6[i], statereal, acc6);
					acc7 = _mm256_fmadd_ps(coefs7[i], statereal, acc7);
#elif defined __FMA4__
					acc0 = _mm256_macc_ps(coefs0[i], statereal, acc0);
					acc1 = _mm256_macc_ps(coefs1[i], statereal, acc1);
					acc2 = _mm256_macc_ps(coefs2[i], statereal, acc2);
					acc3 = _mm256_macc_ps(coefs3[i], statereal, acc3);
					acc4 = _mm256_macc_ps(coefs4[i], statereal, acc4);
					acc5 = _mm256_macc_ps(coefs5[i], statereal, acc5);
					acc6 = _mm256_macc_ps(coefs6[i], statereal, acc6);
					acc7 = _mm256_macc_ps(coefs7[i], statereal, acc7);
#else
					prod0 = _mm256_mul_ps(coefs0[i], statereal);
					acc0  = _mm256_add_ps(acc0, prod0);
					prod1 = _mm256_mul_ps(coefs1[i], statereal);
					acc1  = _mm256_add_ps(acc1, prod1);
					prod2 = _mm256_mul_ps(coefs2[i], statereal);
					acc2  = _mm256_add_ps(acc2, prod2);
					prod3 = _mm256_mul_ps(coefs3[i], statereal);
					acc3  = _mm256_add_ps(acc3, prod3);
					prod4 = _mm256_mul_ps(coefs4[i], statereal);
					acc4  = _mm256_add_ps(acc4, prod4);
					prod5 = _mm256_mul_ps(coefs5[i], statereal);
					acc5  = _mm256_add_ps(acc5, prod5);
					prod6 = _mm256_mul_ps(coefs6[i], statereal);
					acc6  = _mm256_add_ps(acc6, prod6);
					prod7 = _mm256_mul_ps(coefs7[i], statereal);
					acc7  = _mm256_add_ps(acc7, prod7);
#endif
					j = (j+1) & masksimd;
				}
				register __m256 h00 = _mm256_permute2f128_ps(acc0, acc4, 0x20);
				register __m256 h01 = _mm256_permute2f128_ps(acc0, acc4, 0x31);
				register __m256 h02 = _mm256_permute2f128_ps(acc1, acc5, 0x20);
				register __m256 h03 = _mm256_permute2f128_ps(acc1, acc5, 0x31);
				register __m256 h04 = _mm256_permute2f128_ps(acc2, acc6, 0x20);
				register __m256 h05 = _mm256_permute2f128_ps(acc2, acc6, 0x31);
				register __m256 h06 = _mm256_permute2f128_ps(acc3, acc7, 0x20);
				register __m256 h07 = _mm256_permute2f128_ps(acc3, acc7, 0x31);
				register __m256 h10 = _mm256_hadd_ps(h00, h01);
				register __m256 h11 = _mm256_hadd_ps(h02, h03);
				register __m256 h12 = _mm256_hadd_ps(h04, h05);
				register __m256 h13 = _mm256_hadd_ps(h06, h07);
				register __m256 h20 = _mm256_hadd_ps(h10, h11);
				register __m256 h21 = _mm256_hadd_ps(h12, h13);
				register __m256 h30 = _mm256_hadd_ps(h20, h21);
				_mm256_storeu_ps(ofbuf->data + k, h30);
				start_index = (start_index + 8) & mask;
				startsimd = start_index >> 3;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			for (; k < ibuflen; k++) {
				fir->stater[fir->index] = ifbuf->data[k];
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m256*)fir->coefs[start_index & 7];
				acc0 = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
#if defined __FMA__
					acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
					acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
					prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
					acc0  = _mm256_add_ps(acc0, prod0);
#endif
					j = (j+1) & masksimd;
				}
				ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 3;
				delayed->data[k] = fir->stater[middle_index];
				middle_index = (middle_index + 1) & mask;
			}
		}
		else {
			k = 0;
			while ((start_index & 7) && k < ibuflen) {
				fir->stater[fir->index] = ifbuf->data[k];
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m256*)fir->coefs[start_index & 7];
				acc0 = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
#if defined __FMA__
					acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
					acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
					prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
					acc0  = _mm256_add_ps(acc0, prod0);
#endif
					j = (j+1) & masksimd;
				}
				ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 3;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			while (k + 8 <= ibuflen) {
				fir->stater[(fir->index + 0) & mask] = ifbuf->data[k + 0];
				fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
				fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
				fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
				fir->stater[(fir->index + 4) & mask] = ifbuf->data[k + 4];
				fir->stater[(fir->index + 5) & mask] = ifbuf->data[k + 5];
				fir->stater[(fir->index + 6) & mask] = ifbuf->data[k + 6];
				fir->stater[(fir->index + 7) & mask] = ifbuf->data[k + 7];
				fir->index = (fir->index + 8) & mask;
				coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
				coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
				coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
				coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
				coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
				coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
				coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
				coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
				acc0 = _mm256_setzero_ps();
				acc1 = _mm256_setzero_ps();
				acc2 = _mm256_setzero_ps();
				acc3 = _mm256_setzero_ps();
				acc4 = _mm256_setzero_ps();
				acc5 = _mm256_setzero_ps();
				acc6 = _mm256_setzero_ps();
				acc7 = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					statereal = stater[j];
#if defined __FMA__
					acc0 = _mm256_fmadd_ps(coefs0[i], statereal, acc0);
					acc1 = _mm256_fmadd_ps(coefs1[i], statereal, acc1);
					acc2 = _mm256_fmadd_ps(coefs2[i], statereal, acc2);
					acc3 = _mm256_fmadd_ps(coefs3[i], statereal, acc3);
					acc4 = _mm256_fmadd_ps(coefs4[i], statereal, acc4);
					acc5 = _mm256_fmadd_ps(coefs5[i], statereal, acc5);
					acc6 = _mm256_fmadd_ps(coefs6[i], statereal, acc6);
					acc7 = _mm256_fmadd_ps(coefs7[i], statereal, acc7);
#elif defined __FMA4__
					acc0 = _mm256_macc_ps(coefs0[i], statereal, acc0);
					acc1 = _mm256_macc_ps(coefs1[i], statereal, acc1);
					acc2 = _mm256_macc_ps(coefs2[i], statereal, acc2);
					acc3 = _mm256_macc_ps(coefs3[i], statereal, acc3);
					acc4 = _mm256_macc_ps(coefs4[i], statereal, acc4);
					acc5 = _mm256_macc_ps(coefs5[i], statereal, acc5);
					acc6 = _mm256_macc_ps(coefs6[i], statereal, acc6);
					acc7 = _mm256_macc_ps(coefs7[i], statereal, acc7);
#else
					prod0 = _mm256_mul_ps(coefs0[i], statereal);
					acc0  = _mm256_add_ps(acc0, prod0);
					prod1 = _mm256_mul_ps(coefs1[i], statereal);
					acc1  = _mm256_add_ps(acc1, prod1);
					prod2 = _mm256_mul_ps(coefs2[i], statereal);
					acc2  = _mm256_add_ps(acc2, prod2);
					prod3 = _mm256_mul_ps(coefs3[i], statereal);
					acc3  = _mm256_add_ps(acc3, prod3);
					prod4 = _mm256_mul_ps(coefs4[i], statereal);
					acc4  = _mm256_add_ps(acc4, prod4);
					prod5 = _mm256_mul_ps(coefs5[i], statereal);
					acc5  = _mm256_add_ps(acc5, prod5);
					prod6 = _mm256_mul_ps(coefs6[i], statereal);
					acc6  = _mm256_add_ps(acc6, prod6);
					prod7 = _mm256_mul_ps(coefs7[i], statereal);
					acc7  = _mm256_add_ps(acc7, prod7);
#endif
					j = (j+1) & masksimd;
				}
				register __m256 h00 = _mm256_permute2f128_ps(acc0, acc4, 0x20);
				register __m256 h01 = _mm256_permute2f128_ps(acc0, acc4, 0x31);
				register __m256 h02 = _mm256_permute2f128_ps(acc1, acc5, 0x20);
				register __m256 h03 = _mm256_permute2f128_ps(acc1, acc5, 0x31);
				register __m256 h04 = _mm256_permute2f128_ps(acc2, acc6, 0x20);
				register __m256 h05 = _mm256_permute2f128_ps(acc2, acc6, 0x31);
				register __m256 h06 = _mm256_permute2f128_ps(acc3, acc7, 0x20);
				register __m256 h07 = _mm256_permute2f128_ps(acc3, acc7, 0x31);
				register __m256 h10 = _mm256_hadd_ps(h00, h01);
				register __m256 h11 = _mm256_hadd_ps(h02, h03);
				register __m256 h12 = _mm256_hadd_ps(h04, h05);
				register __m256 h13 = _mm256_hadd_ps(h06, h07);
				register __m256 h20 = _mm256_hadd_ps(h10, h11);
				register __m256 h21 = _mm256_hadd_ps(h12, h13);
				register __m256 h30 = _mm256_hadd_ps(h20, h21);
				_mm256_storeu_ps(ofbuf->data + k, h30);
				start_index = (start_index + 8) & mask;
				startsimd = start_index >> 3;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			for (; k < ibuflen; k++) {
				fir->stater[fir->index] = ifbuf->data[k];
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m256*)fir->coefs[start_index & 7];
				acc0 = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
#if defined __FMA__
					acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
					acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
					prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
					acc0  = _mm256_add_ps(acc0, prod0);
#endif
					j = (j+1) & masksimd;
				}
				ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 3;
				delayed->data[k] = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
			}
		}
	}
	else {
		k = 0;
		while ((start_index & 7) && k < ibuflen) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
				prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm256_add_ps(acc0, prod0);
#endif
				j = (j+1) & masksimd;
			}
			ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
			k++;
		}
		while (k + 8 <= ibuflen) {
			fir->stater[(fir->index + 0) & mask] = ifbuf->data[k + 0];
			fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
			fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
			fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
			fir->stater[(fir->index + 4) & mask] = ifbuf->data[k + 4];
			fir->stater[(fir->index + 5) & mask] = ifbuf->data[k + 5];
			fir->stater[(fir->index + 6) & mask] = ifbuf->data[k + 6];
			fir->stater[(fir->index + 7) & mask] = ifbuf->data[k + 7];
			fir->index = (fir->index + 8) & mask;
			coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
			coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
			coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
			coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
			coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
			coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
			coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
			coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
			acc0 = _mm256_setzero_ps();
			acc1 = _mm256_setzero_ps();
			acc2 = _mm256_setzero_ps();
			acc3 = _mm256_setzero_ps();
			acc4 = _mm256_setzero_ps();
			acc5 = _mm256_setzero_ps();
			acc6 = _mm256_setzero_ps();
			acc7 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				statereal = stater[j];
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], statereal, acc0);
				acc1 = _mm256_fmadd_ps(coefs1[i], statereal, acc1);
				acc2 = _mm256_fmadd_ps(coefs2[i], statereal, acc2);
				acc3 = _mm256_fmadd_ps(coefs3[i], statereal, acc3);
				acc4 = _mm256_fmadd_ps(coefs4[i], statereal, acc4);
				acc5 = _mm256_fmadd_ps(coefs5[i], statereal, acc5);
				acc6 = _mm256_fmadd_ps(coefs6[i], statereal, acc6);
				acc7 = _mm256_fmadd_ps(coefs7[i], statereal, acc7);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], statereal, acc0);
				acc1 = _mm256_macc_ps(coefs1[i], statereal, acc1);
				acc2 = _mm256_macc_ps(coefs2[i], statereal, acc2);
				acc3 = _mm256_macc_ps(coefs3[i], statereal, acc3);
				acc4 = _mm256_macc_ps(coefs4[i], statereal, acc4);
				acc5 = _mm256_macc_ps(coefs5[i], statereal, acc5);
				acc6 = _mm256_macc_ps(coefs6[i], statereal, acc6);
				acc7 = _mm256_macc_ps(coefs7[i], statereal, acc7);
#else
				prod0 = _mm256_mul_ps(coefs0[i], statereal);
				acc0  = _mm256_add_ps(acc0, prod0);
				prod1 = _mm256_mul_ps(coefs1[i], statereal);
				acc1  = _mm256_add_ps(acc1, prod1);
				prod2 = _mm256_mul_ps(coefs2[i], statereal);
				acc2  = _mm256_add_ps(acc2, prod2);
				prod3 = _mm256_mul_ps(coefs3[i], statereal);
				acc3  = _mm256_add_ps(acc3, prod3);
				prod4 = _mm256_mul_ps(coefs4[i], statereal);
				acc4  = _mm256_add_ps(acc4, prod4);
				prod5 = _mm256_mul_ps(coefs5[i], statereal);
				acc5  = _mm256_add_ps(acc5, prod5);
				prod6 = _mm256_mul_ps(coefs6[i], statereal);
				acc6  = _mm256_add_ps(acc6, prod6);
				prod7 = _mm256_mul_ps(coefs7[i], statereal);
				acc7  = _mm256_add_ps(acc7, prod7);
#endif
				j = (j+1) & masksimd;
			}
			register __m256 h00 = _mm256_permute2f128_ps(acc0, acc4, 0x20);
			register __m256 h01 = _mm256_permute2f128_ps(acc0, acc4, 0x31);
			register __m256 h02 = _mm256_permute2f128_ps(acc1, acc5, 0x20);
			register __m256 h03 = _mm256_permute2f128_ps(acc1, acc5, 0x31);
			register __m256 h04 = _mm256_permute2f128_ps(acc2, acc6, 0x20);
			register __m256 h05 = _mm256_permute2f128_ps(acc2, acc6, 0x31);
			register __m256 h06 = _mm256_permute2f128_ps(acc3, acc7, 0x20);
			register __m256 h07 = _mm256_permute2f128_ps(acc3, acc7, 0x31);
			register __m256 h10 = _mm256_hadd_ps(h00, h01);
			register __m256 h11 = _mm256_hadd_ps(h02, h03);
			register __m256 h12 = _mm256_hadd_ps(h04, h05);
			register __m256 h13 = _mm256_hadd_ps(h06, h07);
			register __m256 h20 = _mm256_hadd_ps(h10, h11);
			register __m256 h21 = _mm256_hadd_ps(h12, h13);
			register __m256 h30 = _mm256_hadd_ps(h20, h21);
			_mm256_storeu_ps(ofbuf->data + k, h30);
			start_index = (start_index + 8) & mask;
			startsimd = start_index >> 3;
			k += 8;
		}
		for (; k < ibuflen; k++) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
				prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm256_add_ps(acc0, prod0);
#endif
				j = (j+1) & masksimd;
			}
			ofbuf->data[k] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
		}
	}

	return ofbuf;
}


pdlc_complex_buffer_t* pdlc_fir_filter_filter_complex_buffer(pdlc_fir_filter_t* fir, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf, pdlc_complex_buffer_t *delayed)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 3;
	const unsigned int masksimd = mask >> 3;
	const size_t ibuflen = icbuf->length;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 3;
	unsigned int middle_index;
	unsigned int i, j;
	size_t k;
	register __m256 acc0r, acc1r, acc2r, acc3r, acc4r, acc5r, acc6r, acc7r;
	register __m256 acc0i, acc1i, acc2i, acc3i, acc4i, acc5i, acc6i, acc7i;
#if !(defined __FMA__ || defined __FMA4__)
	register __m256 prod0r, prod1r, prod2r, prod3r, prod4r, prod5r, prod6r, prod7r;
	register __m256 prod0i, prod1i, prod2i, prod3i, prod4i, prod5i, prod6i, prod7i;
#endif
	register __m256 statereal, stateimag;
	const __m256 *coefs0, *coefs1, *coefs2, *coefs3, *coefs4, *coefs5, *coefs6, *coefs7;
	__m256 *stater = (__m256*)fir->stater;
	__m256 *statei = (__m256*)fir->statei;

	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(ibuflen);
	else if (ocbuf->length != ibuflen)
		pdlc_complex_buffer_resize(ocbuf, ibuflen, 0);

	if (delayed) {
		if (delayed->length != ibuflen)
			pdlc_complex_buffer_resize(delayed, ibuflen, 0);
		middle_index = (start_index + nb_coefs / 2) & mask;
		if (nb_coefs & 1) {
			k = 0;
			while ((start_index & 7) && k < ibuflen) {
				fir->stater[fir->index] = icbuf->data[k].real;
				fir->statei[fir->index] = icbuf->data[k].imag;
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m256*)fir->coefs[start_index & 7];
				acc0r = _mm256_setzero_ps();
				acc0i = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
#if defined __FMA__
					acc0r = _mm256_fmadd_ps(coefs0[i], stater[j], acc0r);
					acc0i = _mm256_fmadd_ps(coefs0[i], statei[j], acc0i);
#elif defined __FMA4__
					acc0r = _mm256_macc_ps(coefs0[i], stater[j], acc0r);
					acc0i = _mm256_macc_ps(coefs0[i], statei[j], acc0i);
#else
					prod0r = _mm256_mul_ps(coefs0[i], stater[j]);
					prod0i = _mm256_mul_ps(coefs0[i], statei[j]);
					acc0r  = _mm256_add_ps(acc0r, prod0r);
					acc0i  = _mm256_add_ps(acc0i, prod0i);
#endif
					j = (j+1) & masksimd;
				}
				ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3] + acc0r[4] + acc0r[5] + acc0r[6] + acc0r[7];
				ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3] + acc0i[4] + acc0i[5] + acc0i[6] + acc0i[7];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 3;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			while (k + 8 <= ibuflen) {
				fir->stater[ fir->index            ] = icbuf->data[k + 0].real;
				fir->statei[ fir->index            ] = icbuf->data[k + 0].imag;
				fir->stater[(fir->index + 1) & mask] = icbuf->data[k + 1].real;
				fir->statei[(fir->index + 1) & mask] = icbuf->data[k + 1].imag;
				fir->stater[(fir->index + 2) & mask] = icbuf->data[k + 2].real;
				fir->statei[(fir->index + 2) & mask] = icbuf->data[k + 2].imag;
				fir->stater[(fir->index + 3) & mask] = icbuf->data[k + 3].real;
				fir->statei[(fir->index + 3) & mask] = icbuf->data[k + 3].imag;
				fir->stater[(fir->index + 4) & mask] = icbuf->data[k + 4].real;
				fir->statei[(fir->index + 4) & mask] = icbuf->data[k + 4].imag;
				fir->stater[(fir->index + 5) & mask] = icbuf->data[k + 5].real;
				fir->statei[(fir->index + 5) & mask] = icbuf->data[k + 5].imag;
				fir->stater[(fir->index + 6) & mask] = icbuf->data[k + 6].real;
				fir->statei[(fir->index + 6) & mask] = icbuf->data[k + 6].imag;
				fir->stater[(fir->index + 7) & mask] = icbuf->data[k + 7].real;
				fir->statei[(fir->index + 7) & mask] = icbuf->data[k + 7].imag;
				fir->index = (fir->index + 8) & mask;
				coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
				coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
				coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
				coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
				coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
				coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
				coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
				coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
				acc0r = _mm256_setzero_ps();
				acc0i = _mm256_setzero_ps();
				acc1r = _mm256_setzero_ps();
				acc1i = _mm256_setzero_ps();
				acc2r = _mm256_setzero_ps();
				acc2i = _mm256_setzero_ps();
				acc3r = _mm256_setzero_ps();
				acc3i = _mm256_setzero_ps();
				acc4r = _mm256_setzero_ps();
				acc4i = _mm256_setzero_ps();
				acc5r = _mm256_setzero_ps();
				acc5i = _mm256_setzero_ps();
				acc6r = _mm256_setzero_ps();
				acc6i = _mm256_setzero_ps();
				acc7r = _mm256_setzero_ps();
				acc7i = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					statereal = stater[j];
					stateimag = statei[j];
#if defined __FMA__
					acc0r = _mm256_fmadd_ps(coefs0[i], statereal, acc0r);
					acc1r = _mm256_fmadd_ps(coefs1[i], statereal, acc1r);
					acc2r = _mm256_fmadd_ps(coefs2[i], statereal, acc2r);
					acc3r = _mm256_fmadd_ps(coefs3[i], statereal, acc3r);
					acc4r = _mm256_fmadd_ps(coefs4[i], statereal, acc4r);
					acc5r = _mm256_fmadd_ps(coefs5[i], statereal, acc5r);
					acc6r = _mm256_fmadd_ps(coefs6[i], statereal, acc6r);
					acc7r = _mm256_fmadd_ps(coefs7[i], statereal, acc7r);
					acc0i = _mm256_fmadd_ps(coefs0[i], stateimag, acc0i);
					acc1i = _mm256_fmadd_ps(coefs1[i], stateimag, acc1i);
					acc2i = _mm256_fmadd_ps(coefs2[i], stateimag, acc2i);
					acc3i = _mm256_fmadd_ps(coefs3[i], stateimag, acc3i);
					acc4i = _mm256_fmadd_ps(coefs4[i], stateimag, acc4i);
					acc5i = _mm256_fmadd_ps(coefs5[i], stateimag, acc5i);
					acc6i = _mm256_fmadd_ps(coefs6[i], stateimag, acc6i);
					acc7i = _mm256_fmadd_ps(coefs7[i], stateimag, acc7i);
#elif defined __FMA4__
					acc0r = _mm256_macc_ps(coefs0[i], statereal, acc0r);
					acc1r = _mm256_macc_ps(coefs1[i], statereal, acc1r);
					acc2r = _mm256_macc_ps(coefs2[i], statereal, acc2r);
					acc3r = _mm256_macc_ps(coefs3[i], statereal, acc3r);
					acc4r = _mm256_macc_ps(coefs4[i], statereal, acc4r);
					acc5r = _mm256_macc_ps(coefs5[i], statereal, acc5r);
					acc6r = _mm256_macc_ps(coefs6[i], statereal, acc6r);
					acc7r = _mm256_macc_ps(coefs7[i], statereal, acc7r);
					acc0i = _mm256_macc_ps(coefs0[i], stateimag, acc0i);
					acc1i = _mm256_macc_ps(coefs1[i], stateimag, acc1i);
					acc2i = _mm256_macc_ps(coefs2[i], stateimag, acc2i);
					acc3i = _mm256_macc_ps(coefs3[i], stateimag, acc3i);
					acc4i = _mm256_macc_ps(coefs4[i], stateimag, acc4i);
					acc5i = _mm256_macc_ps(coefs5[i], stateimag, acc5i);
					acc6i = _mm256_macc_ps(coefs6[i], stateimag, acc6i);
					acc7i = _mm256_macc_ps(coefs7[i], stateimag, acc7i);
#else
					prod0r = _mm256_mul_ps(coefs0[i], statereal);
					acc0r  = _mm256_add_ps(acc0r, prod0r);
					prod1r = _mm256_mul_ps(coefs1[i], statereal);
					acc1r  = _mm256_add_ps(acc1r, prod1r);
					prod2r = _mm256_mul_ps(coefs2[i], statereal);
					acc2r  = _mm256_add_ps(acc2r, prod2r);
					prod3r = _mm256_mul_ps(coefs3[i], statereal);
					acc3r  = _mm256_add_ps(acc3r, prod3r);
					prod4r = _mm256_mul_ps(coefs4[i], statereal);
					acc4r  = _mm256_add_ps(acc4r, prod4r);
					prod5r = _mm256_mul_ps(coefs5[i], statereal);
					acc5r  = _mm256_add_ps(acc5r, prod5r);
					prod6r = _mm256_mul_ps(coefs6[i], statereal);
					acc6r  = _mm256_add_ps(acc6r, prod6r);
					prod7r = _mm256_mul_ps(coefs7[i], statereal);
					acc7r  = _mm256_add_ps(acc7r, prod7r);
					prod0i = _mm256_mul_ps(coefs0[i], stateimag);
					acc0i  = _mm256_add_ps(acc0i, prod0i);
					prod1i = _mm256_mul_ps(coefs1[i], stateimag);
					acc1i  = _mm256_add_ps(acc1i, prod1i);
					prod2i = _mm256_mul_ps(coefs2[i], stateimag);
					acc2i  = _mm256_add_ps(acc2i, prod2i);
					prod3i = _mm256_mul_ps(coefs3[i], stateimag);
					acc3i  = _mm256_add_ps(acc3i, prod3i);
					prod4i = _mm256_mul_ps(coefs4[i], stateimag);
					acc4i  = _mm256_add_ps(acc4i, prod4i);
					prod5i = _mm256_mul_ps(coefs5[i], stateimag);
					acc5i  = _mm256_add_ps(acc5i, prod5i);
					prod6i = _mm256_mul_ps(coefs6[i], stateimag);
					acc6i  = _mm256_add_ps(acc6i, prod6i);
					prod7i = _mm256_mul_ps(coefs7[i], stateimag);
					acc7i  = _mm256_add_ps(acc7i, prod7i);
#endif
					j = (j+1) & masksimd;
				}
				register __m256 h00r = _mm256_permute2f128_ps(acc0r, acc4r, 0x20);
				register __m256 h01r = _mm256_permute2f128_ps(acc0r, acc4r, 0x31);
				register __m256 h02r = _mm256_permute2f128_ps(acc1r, acc5r, 0x20);
				register __m256 h03r = _mm256_permute2f128_ps(acc1r, acc5r, 0x31);
				register __m256 h04r = _mm256_permute2f128_ps(acc2r, acc6r, 0x20);
				register __m256 h05r = _mm256_permute2f128_ps(acc2r, acc6r, 0x31);
				register __m256 h06r = _mm256_permute2f128_ps(acc3r, acc7r, 0x20);
				register __m256 h07r = _mm256_permute2f128_ps(acc3r, acc7r, 0x31);
				register __m256 h10r = _mm256_hadd_ps(h00r, h01r);
				register __m256 h11r = _mm256_hadd_ps(h02r, h03r);
				register __m256 h12r = _mm256_hadd_ps(h04r, h05r);
				register __m256 h13r = _mm256_hadd_ps(h06r, h07r);
				register __m256 h20r = _mm256_hadd_ps(h10r, h11r);
				register __m256 h21r = _mm256_hadd_ps(h12r, h13r);
				register __m256 h30r = _mm256_hadd_ps(h20r, h21r);
				register __m256 h00i = _mm256_permute2f128_ps(acc0i, acc4i, 0x20);
				register __m256 h01i = _mm256_permute2f128_ps(acc0i, acc4i, 0x31);
				register __m256 h02i = _mm256_permute2f128_ps(acc1i, acc5i, 0x20);
				register __m256 h03i = _mm256_permute2f128_ps(acc1i, acc5i, 0x31);
				register __m256 h04i = _mm256_permute2f128_ps(acc2i, acc6i, 0x20);
				register __m256 h05i = _mm256_permute2f128_ps(acc2i, acc6i, 0x31);
				register __m256 h06i = _mm256_permute2f128_ps(acc3i, acc7i, 0x20);
				register __m256 h07i = _mm256_permute2f128_ps(acc3i, acc7i, 0x31);
				register __m256 h10i = _mm256_hadd_ps(h00i, h01i);
				register __m256 h11i = _mm256_hadd_ps(h02i, h03i);
				register __m256 h12i = _mm256_hadd_ps(h04i, h05i);
				register __m256 h13i = _mm256_hadd_ps(h06i, h07i);
				register __m256 h20i = _mm256_hadd_ps(h10i, h11i);
				register __m256 h21i = _mm256_hadd_ps(h12i, h13i);
				register __m256 h30i = _mm256_hadd_ps(h20i, h21i);
				ocbuf->data[k+0].real = h30r[0];
				ocbuf->data[k+0].imag = h30i[0];
				ocbuf->data[k+1].real = h30r[1];
				ocbuf->data[k+1].imag = h30i[1];
				ocbuf->data[k+2].real = h30r[2];
				ocbuf->data[k+2].imag = h30i[2];
				ocbuf->data[k+3].real = h30r[3];
				ocbuf->data[k+3].imag = h30i[3];
				ocbuf->data[k+4].real = h30r[4];
				ocbuf->data[k+4].imag = h30i[4];
				ocbuf->data[k+5].real = h30r[5];
				ocbuf->data[k+5].imag = h30i[5];
				ocbuf->data[k+6].real = h30r[6];
				ocbuf->data[k+6].imag = h30i[6];
				ocbuf->data[k+7].real = h30r[7];
				ocbuf->data[k+7].imag = h30i[7];
				start_index = (start_index + 8) & mask;
				startsimd = start_index >> 3;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			for (; k < ibuflen; k++) {
				fir->stater[fir->index] = icbuf->data[k].real;
				fir->statei[fir->index] = icbuf->data[k].imag;
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m256*)fir->coefs[start_index & 7];
				acc0r = _mm256_setzero_ps();
				acc0i = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
#if defined __FMA__
					acc0r = _mm256_fmadd_ps(coefs0[i], stater[j], acc0r);
					acc0i = _mm256_fmadd_ps(coefs0[i], statei[j], acc0i);
#elif defined __FMA4__
					acc0r = _mm256_macc_ps(coefs0[i], stater[j], acc0r);
					acc0i = _mm256_macc_ps(coefs0[i], statei[j], acc0i);
#else
					prod0r = _mm256_mul_ps(coefs0[i], stater[j]);
					prod0i = _mm256_mul_ps(coefs0[i], statei[j]);
					acc0r  = _mm256_add_ps(acc0r, prod0r);
					acc0i  = _mm256_add_ps(acc0i, prod0i);
#endif
					j = (j+1) & masksimd;
				}
				ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3] + acc0r[4] + acc0r[5] + acc0r[6] + acc0r[7];
				ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3] + acc0i[4] + acc0i[5] + acc0i[6] + acc0i[7];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 3;
				delayed->data[k].real = fir->stater[middle_index];
				delayed->data[k].imag = fir->statei[middle_index];
				middle_index = (middle_index + 1) & mask;
			}
		}
		else {
			k = 0;
			while ((start_index & 7) && k < ibuflen) {
				fir->stater[fir->index] = icbuf->data[k].real;
				fir->statei[fir->index] = icbuf->data[k].imag;
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m256*)fir->coefs[start_index & 7];
				acc0r = _mm256_setzero_ps();
				acc0i = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
#if defined __FMA__
					acc0r = _mm256_fmadd_ps(coefs0[i], stater[j], acc0r);
					acc0i = _mm256_fmadd_ps(coefs0[i], statei[j], acc0i);
#elif defined __FMA4__
					acc0r = _mm256_macc_ps(coefs0[i], stater[j], acc0r);
					acc0i = _mm256_macc_ps(coefs0[i], statei[j], acc0i);
#else
					prod0r = _mm256_mul_ps(coefs0[i], stater[j]);
					prod0i = _mm256_mul_ps(coefs0[i], statei[j]);
					acc0r  = _mm256_add_ps(acc0r, prod0r);
					acc0i  = _mm256_add_ps(acc0i, prod0i);
#endif
					j = (j+1) & masksimd;
				}
				ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3] + acc0r[4] + acc0r[5] + acc0r[6] + acc0r[7];
				ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3] + acc0i[4] + acc0i[5] + acc0i[6] + acc0i[7];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 3;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			while (k + 4 <= ibuflen) {
				fir->stater[ fir->index            ] = icbuf->data[k + 0].real;
				fir->statei[ fir->index            ] = icbuf->data[k + 0].imag;
				fir->stater[(fir->index + 1) & mask] = icbuf->data[k + 1].real;
				fir->statei[(fir->index + 1) & mask] = icbuf->data[k + 1].imag;
				fir->stater[(fir->index + 2) & mask] = icbuf->data[k + 2].real;
				fir->statei[(fir->index + 2) & mask] = icbuf->data[k + 2].imag;
				fir->stater[(fir->index + 3) & mask] = icbuf->data[k + 3].real;
				fir->statei[(fir->index + 3) & mask] = icbuf->data[k + 3].imag;
				fir->stater[(fir->index + 4) & mask] = icbuf->data[k + 4].real;
				fir->statei[(fir->index + 4) & mask] = icbuf->data[k + 4].imag;
				fir->stater[(fir->index + 5) & mask] = icbuf->data[k + 5].real;
				fir->statei[(fir->index + 5) & mask] = icbuf->data[k + 5].imag;
				fir->stater[(fir->index + 6) & mask] = icbuf->data[k + 6].real;
				fir->statei[(fir->index + 6) & mask] = icbuf->data[k + 6].imag;
				fir->stater[(fir->index + 7) & mask] = icbuf->data[k + 7].real;
				fir->statei[(fir->index + 7) & mask] = icbuf->data[k + 7].imag;
				fir->index = (fir->index + 8) & mask;
				coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
				coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
				coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
				coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
				coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
				coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
				coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
				coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
				acc0r = _mm256_setzero_ps();
				acc0i = _mm256_setzero_ps();
				acc1r = _mm256_setzero_ps();
				acc1i = _mm256_setzero_ps();
				acc2r = _mm256_setzero_ps();
				acc2i = _mm256_setzero_ps();
				acc3r = _mm256_setzero_ps();
				acc3i = _mm256_setzero_ps();
				acc4r = _mm256_setzero_ps();
				acc4i = _mm256_setzero_ps();
				acc5r = _mm256_setzero_ps();
				acc5i = _mm256_setzero_ps();
				acc6r = _mm256_setzero_ps();
				acc6i = _mm256_setzero_ps();
				acc7r = _mm256_setzero_ps();
				acc7i = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
					statereal = stater[j];
					stateimag = statei[j];
#if defined __FMA__
					acc0r = _mm256_fmadd_ps(coefs0[i], statereal, acc0r);
					acc1r = _mm256_fmadd_ps(coefs1[i], statereal, acc1r);
					acc2r = _mm256_fmadd_ps(coefs2[i], statereal, acc2r);
					acc3r = _mm256_fmadd_ps(coefs3[i], statereal, acc3r);
					acc4r = _mm256_fmadd_ps(coefs4[i], statereal, acc4r);
					acc5r = _mm256_fmadd_ps(coefs5[i], statereal, acc5r);
					acc6r = _mm256_fmadd_ps(coefs6[i], statereal, acc6r);
					acc7r = _mm256_fmadd_ps(coefs7[i], statereal, acc7r);
					acc0i = _mm256_fmadd_ps(coefs0[i], stateimag, acc0i);
					acc1i = _mm256_fmadd_ps(coefs1[i], stateimag, acc1i);
					acc2i = _mm256_fmadd_ps(coefs2[i], stateimag, acc2i);
					acc3i = _mm256_fmadd_ps(coefs3[i], stateimag, acc3i);
					acc4i = _mm256_fmadd_ps(coefs4[i], stateimag, acc4i);
					acc5i = _mm256_fmadd_ps(coefs5[i], stateimag, acc5i);
					acc6i = _mm256_fmadd_ps(coefs6[i], stateimag, acc6i);
					acc7i = _mm256_fmadd_ps(coefs7[i], stateimag, acc7i);
#elif defined __FMA4__
					acc0r = _mm256_macc_ps(coefs0[i], statereal, acc0r);
					acc1r = _mm256_macc_ps(coefs1[i], statereal, acc1r);
					acc2r = _mm256_macc_ps(coefs2[i], statereal, acc2r);
					acc3r = _mm256_macc_ps(coefs3[i], statereal, acc3r);
					acc4r = _mm256_macc_ps(coefs4[i], statereal, acc4r);
					acc5r = _mm256_macc_ps(coefs5[i], statereal, acc5r);
					acc6r = _mm256_macc_ps(coefs6[i], statereal, acc6r);
					acc7r = _mm256_macc_ps(coefs7[i], statereal, acc7r);
					acc0i = _mm256_macc_ps(coefs0[i], stateimag, acc0i);
					acc1i = _mm256_macc_ps(coefs1[i], stateimag, acc1i);
					acc2i = _mm256_macc_ps(coefs2[i], stateimag, acc2i);
					acc3i = _mm256_macc_ps(coefs3[i], stateimag, acc3i);
					acc4i = _mm256_macc_ps(coefs4[i], stateimag, acc4i);
					acc5i = _mm256_macc_ps(coefs5[i], stateimag, acc5i);
					acc6i = _mm256_macc_ps(coefs6[i], stateimag, acc6i);
					acc7i = _mm256_macc_ps(coefs7[i], stateimag, acc7i);
#else
					prod0r = _mm256_mul_ps(coefs0[i], statereal);
					acc0r  = _mm256_add_ps(acc0r, prod0r);
					prod1r = _mm256_mul_ps(coefs1[i], statereal);
					acc1r  = _mm256_add_ps(acc1r, prod1r);
					prod2r = _mm256_mul_ps(coefs2[i], statereal);
					acc2r  = _mm256_add_ps(acc2r, prod2r);
					prod3r = _mm256_mul_ps(coefs3[i], statereal);
					acc3r  = _mm256_add_ps(acc3r, prod3r);
					prod4r = _mm256_mul_ps(coefs4[i], statereal);
					acc4r  = _mm256_add_ps(acc4r, prod4r);
					prod5r = _mm256_mul_ps(coefs5[i], statereal);
					acc5r  = _mm256_add_ps(acc5r, prod5r);
					prod6r = _mm256_mul_ps(coefs6[i], statereal);
					acc6r  = _mm256_add_ps(acc6r, prod6r);
					prod7r = _mm256_mul_ps(coefs7[i], statereal);
					acc7r  = _mm256_add_ps(acc7r, prod7r);
					prod0i = _mm256_mul_ps(coefs0[i], stateimag);
					acc0i  = _mm256_add_ps(acc0i, prod0i);
					prod1i = _mm256_mul_ps(coefs1[i], stateimag);
					acc1i  = _mm256_add_ps(acc1i, prod1i);
					prod2i = _mm256_mul_ps(coefs2[i], stateimag);
					acc2i  = _mm256_add_ps(acc2i, prod2i);
					prod3i = _mm256_mul_ps(coefs3[i], stateimag);
					acc3i  = _mm256_add_ps(acc3i, prod3i);
					prod4i = _mm256_mul_ps(coefs4[i], stateimag);
					acc4i  = _mm256_add_ps(acc4i, prod4i);
					prod5i = _mm256_mul_ps(coefs5[i], stateimag);
					acc5i  = _mm256_add_ps(acc5i, prod5i);
					prod6i = _mm256_mul_ps(coefs6[i], stateimag);
					acc6i  = _mm256_add_ps(acc6i, prod6i);
					prod7i = _mm256_mul_ps(coefs7[i], stateimag);
					acc7i  = _mm256_add_ps(acc7i, prod7i);
#endif
					j = (j+1) & masksimd;
				}
				register __m256 h00r = _mm256_permute2f128_ps(acc0r, acc4r, 0x20);
				register __m256 h01r = _mm256_permute2f128_ps(acc0r, acc4r, 0x31);
				register __m256 h02r = _mm256_permute2f128_ps(acc1r, acc5r, 0x20);
				register __m256 h03r = _mm256_permute2f128_ps(acc1r, acc5r, 0x31);
				register __m256 h04r = _mm256_permute2f128_ps(acc2r, acc6r, 0x20);
				register __m256 h05r = _mm256_permute2f128_ps(acc2r, acc6r, 0x31);
				register __m256 h06r = _mm256_permute2f128_ps(acc3r, acc7r, 0x20);
				register __m256 h07r = _mm256_permute2f128_ps(acc3r, acc7r, 0x31);
				register __m256 h10r = _mm256_hadd_ps(h00r, h01r);
				register __m256 h11r = _mm256_hadd_ps(h02r, h03r);
				register __m256 h12r = _mm256_hadd_ps(h04r, h05r);
				register __m256 h13r = _mm256_hadd_ps(h06r, h07r);
				register __m256 h20r = _mm256_hadd_ps(h10r, h11r);
				register __m256 h21r = _mm256_hadd_ps(h12r, h13r);
				register __m256 h30r = _mm256_hadd_ps(h20r, h21r);
				register __m256 h00i = _mm256_permute2f128_ps(acc0i, acc4i, 0x20);
				register __m256 h01i = _mm256_permute2f128_ps(acc0i, acc4i, 0x31);
				register __m256 h02i = _mm256_permute2f128_ps(acc1i, acc5i, 0x20);
				register __m256 h03i = _mm256_permute2f128_ps(acc1i, acc5i, 0x31);
				register __m256 h04i = _mm256_permute2f128_ps(acc2i, acc6i, 0x20);
				register __m256 h05i = _mm256_permute2f128_ps(acc2i, acc6i, 0x31);
				register __m256 h06i = _mm256_permute2f128_ps(acc3i, acc7i, 0x20);
				register __m256 h07i = _mm256_permute2f128_ps(acc3i, acc7i, 0x31);
				register __m256 h10i = _mm256_hadd_ps(h00i, h01i);
				register __m256 h11i = _mm256_hadd_ps(h02i, h03i);
				register __m256 h12i = _mm256_hadd_ps(h04i, h05i);
				register __m256 h13i = _mm256_hadd_ps(h06i, h07i);
				register __m256 h20i = _mm256_hadd_ps(h10i, h11i);
				register __m256 h21i = _mm256_hadd_ps(h12i, h13i);
				register __m256 h30i = _mm256_hadd_ps(h20i, h21i);
				ocbuf->data[k+0].real = h30r[0];
				ocbuf->data[k+0].imag = h30i[0];
				ocbuf->data[k+1].real = h30r[1];
				ocbuf->data[k+1].imag = h30i[1];
				ocbuf->data[k+2].real = h30r[2];
				ocbuf->data[k+2].imag = h30i[2];
				ocbuf->data[k+3].real = h30r[3];
				ocbuf->data[k+3].imag = h30i[3];
				ocbuf->data[k+4].real = h30r[4];
				ocbuf->data[k+4].imag = h30i[4];
				ocbuf->data[k+5].real = h30r[5];
				ocbuf->data[k+5].imag = h30i[5];
				ocbuf->data[k+6].real = h30r[6];
				ocbuf->data[k+6].imag = h30i[6];
				ocbuf->data[k+7].real = h30r[7];
				ocbuf->data[k+7].imag = h30i[7];
				start_index = (start_index + 8) & mask;
				startsimd = start_index >> 3;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
				k++;
			}
			for (; k < ibuflen; k++) {
				fir->stater[fir->index] = icbuf->data[k].real;
				fir->statei[fir->index] = icbuf->data[k].imag;
				fir->index = (fir->index + 1) & mask;
				coefs0 = (__m256*)fir->coefs[start_index & 7];
				acc0r = _mm256_setzero_ps();
				acc0i = _mm256_setzero_ps();
				j = startsimd;
				for (i = 0; i < lensimd; i++) {
#if defined __FMA__
					acc0r = _mm256_fmadd_ps(coefs0[i], stater[j], acc0r);
					acc0i = _mm256_fmadd_ps(coefs0[i], statei[j], acc0i);
#elif defined __FMA4__
					acc0r = _mm256_macc_ps(coefs0[i], stater[j], acc0r);
					acc0i = _mm256_macc_ps(coefs0[i], statei[j], acc0i);
#else
					prod0r = _mm256_mul_ps(coefs0[i], stater[j]);
					prod0i = _mm256_mul_ps(coefs0[i], statei[j]);
					acc0r  = _mm256_add_ps(acc0r, prod0r);
					acc0i  = _mm256_add_ps(acc0i, prod0i);
#endif
					j = (j+1) & masksimd;
				}
				ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3] + acc0r[4] + acc0r[5] + acc0r[6] + acc0r[7];
				ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3] + acc0i[4] + acc0i[5] + acc0i[6] + acc0i[7];
				start_index = (start_index + 1) & mask;
				startsimd = start_index >> 3;
				delayed->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
				delayed->data[k].imag = (fir->statei[middle_index] + fir->statei[(middle_index - 1) & mask]) / 2.0f;
				middle_index = (middle_index + 1) & mask;
			}
		}
	}
	else {
		k = 0;
		while ((start_index & 7) && k < ibuflen) {
			fir->stater[fir->index] = icbuf->data[k].real;
			fir->statei[fir->index] = icbuf->data[k].imag;
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0r = _mm256_setzero_ps();
			acc0i = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0r = _mm256_fmadd_ps(coefs0[i], stater[j], acc0r);
				acc0i = _mm256_fmadd_ps(coefs0[i], statei[j], acc0i);
#elif defined __FMA4__
				acc0r = _mm256_macc_ps(coefs0[i], stater[j], acc0r);
				acc0i = _mm256_macc_ps(coefs0[i], statei[j], acc0i);
#else
				prod0r = _mm256_mul_ps(coefs0[i], stater[j]);
				prod0i = _mm256_mul_ps(coefs0[i], statei[j]);
				acc0r  = _mm256_add_ps(acc0r, prod0r);
				acc0i  = _mm256_add_ps(acc0i, prod0i);
#endif
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3] + acc0r[4] + acc0r[5] + acc0r[6] + acc0r[7];
			ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3] + acc0i[4] + acc0i[5] + acc0i[6] + acc0i[7];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
			k++;
		}
		while (k + 8 <= ibuflen) {
			fir->stater[ fir->index            ] = icbuf->data[k + 0].real;
			fir->statei[ fir->index            ] = icbuf->data[k + 0].imag;
			fir->stater[(fir->index + 1) & mask] = icbuf->data[k + 1].real;
			fir->statei[(fir->index + 1) & mask] = icbuf->data[k + 1].imag;
			fir->stater[(fir->index + 2) & mask] = icbuf->data[k + 2].real;
			fir->statei[(fir->index + 2) & mask] = icbuf->data[k + 2].imag;
			fir->stater[(fir->index + 3) & mask] = icbuf->data[k + 3].real;
			fir->statei[(fir->index + 3) & mask] = icbuf->data[k + 3].imag;
			fir->stater[(fir->index + 4) & mask] = icbuf->data[k + 4].real;
			fir->statei[(fir->index + 4) & mask] = icbuf->data[k + 4].imag;
			fir->stater[(fir->index + 5) & mask] = icbuf->data[k + 5].real;
			fir->statei[(fir->index + 5) & mask] = icbuf->data[k + 5].imag;
			fir->stater[(fir->index + 6) & mask] = icbuf->data[k + 6].real;
			fir->statei[(fir->index + 6) & mask] = icbuf->data[k + 6].imag;
			fir->stater[(fir->index + 7) & mask] = icbuf->data[k + 7].real;
			fir->statei[(fir->index + 7) & mask] = icbuf->data[k + 7].imag;
			fir->index = (fir->index + 8) & mask;
			coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
			coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
			coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
			coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
			coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
			coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
			coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
			coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
			acc0r = _mm256_setzero_ps();
			acc0i = _mm256_setzero_ps();
			acc1r = _mm256_setzero_ps();
			acc1i = _mm256_setzero_ps();
			acc2r = _mm256_setzero_ps();
			acc2i = _mm256_setzero_ps();
			acc3r = _mm256_setzero_ps();
			acc3i = _mm256_setzero_ps();
			acc4r = _mm256_setzero_ps();
			acc4i = _mm256_setzero_ps();
			acc5r = _mm256_setzero_ps();
			acc5i = _mm256_setzero_ps();
			acc6r = _mm256_setzero_ps();
			acc6i = _mm256_setzero_ps();
			acc7r = _mm256_setzero_ps();
			acc7i = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				statereal = stater[j];
				stateimag = statei[j];
#if defined __FMA__
				acc0r = _mm256_fmadd_ps(coefs0[i], statereal, acc0r);
				acc1r = _mm256_fmadd_ps(coefs1[i], statereal, acc1r);
				acc2r = _mm256_fmadd_ps(coefs2[i], statereal, acc2r);
				acc3r = _mm256_fmadd_ps(coefs3[i], statereal, acc3r);
				acc4r = _mm256_fmadd_ps(coefs4[i], statereal, acc4r);
				acc5r = _mm256_fmadd_ps(coefs5[i], statereal, acc5r);
				acc6r = _mm256_fmadd_ps(coefs6[i], statereal, acc6r);
				acc7r = _mm256_fmadd_ps(coefs7[i], statereal, acc7r);
				acc0i = _mm256_fmadd_ps(coefs0[i], stateimag, acc0i);
				acc1i = _mm256_fmadd_ps(coefs1[i], stateimag, acc1i);
				acc2i = _mm256_fmadd_ps(coefs2[i], stateimag, acc2i);
				acc3i = _mm256_fmadd_ps(coefs3[i], stateimag, acc3i);
				acc4i = _mm256_fmadd_ps(coefs4[i], stateimag, acc4i);
				acc5i = _mm256_fmadd_ps(coefs5[i], stateimag, acc5i);
				acc6i = _mm256_fmadd_ps(coefs6[i], stateimag, acc6i);
				acc7i = _mm256_fmadd_ps(coefs7[i], stateimag, acc7i);
#elif defined __FMA4__
				acc0r = _mm256_macc_ps(coefs0[i], statereal, acc0r);
				acc1r = _mm256_macc_ps(coefs1[i], statereal, acc1r);
				acc2r = _mm256_macc_ps(coefs2[i], statereal, acc2r);
				acc3r = _mm256_macc_ps(coefs3[i], statereal, acc3r);
				acc4r = _mm256_macc_ps(coefs4[i], statereal, acc4r);
				acc5r = _mm256_macc_ps(coefs5[i], statereal, acc5r);
				acc6r = _mm256_macc_ps(coefs6[i], statereal, acc6r);
				acc7r = _mm256_macc_ps(coefs7[i], statereal, acc7r);
				acc0i = _mm256_macc_ps(coefs0[i], stateimag, acc0i);
				acc1i = _mm256_macc_ps(coefs1[i], stateimag, acc1i);
				acc2i = _mm256_macc_ps(coefs2[i], stateimag, acc2i);
				acc3i = _mm256_macc_ps(coefs3[i], stateimag, acc3i);
				acc4i = _mm256_macc_ps(coefs4[i], stateimag, acc4i);
				acc5i = _mm256_macc_ps(coefs5[i], stateimag, acc5i);
				acc6i = _mm256_macc_ps(coefs6[i], stateimag, acc6i);
				acc7i = _mm256_macc_ps(coefs7[i], stateimag, acc7i);
#else
				prod0r = _mm256_mul_ps(coefs0[i], statereal);
				acc0r  = _mm256_add_ps(acc0r, prod0r);
				prod1r = _mm256_mul_ps(coefs1[i], statereal);
				acc1r  = _mm256_add_ps(acc1r, prod1r);
				prod2r = _mm256_mul_ps(coefs2[i], statereal);
				acc2r  = _mm256_add_ps(acc2r, prod2r);
				prod3r = _mm256_mul_ps(coefs3[i], statereal);
				acc3r  = _mm256_add_ps(acc3r, prod3r);
				prod4r = _mm256_mul_ps(coefs4[i], statereal);
				acc4r  = _mm256_add_ps(acc4r, prod4r);
				prod5r = _mm256_mul_ps(coefs5[i], statereal);
				acc5r  = _mm256_add_ps(acc5r, prod5r);
				prod6r = _mm256_mul_ps(coefs6[i], statereal);
				acc6r  = _mm256_add_ps(acc6r, prod6r);
				prod7r = _mm256_mul_ps(coefs7[i], statereal);
				acc7r  = _mm256_add_ps(acc7r, prod7r);
				prod0i = _mm256_mul_ps(coefs0[i], stateimag);
				acc0i  = _mm256_add_ps(acc0i, prod0i);
				prod1i = _mm256_mul_ps(coefs1[i], stateimag);
				acc1i  = _mm256_add_ps(acc1i, prod1i);
				prod2i = _mm256_mul_ps(coefs2[i], stateimag);
				acc2i  = _mm256_add_ps(acc2i, prod2i);
				prod3i = _mm256_mul_ps(coefs3[i], stateimag);
				acc3i  = _mm256_add_ps(acc3i, prod3i);
				prod4i = _mm256_mul_ps(coefs4[i], stateimag);
				acc4i  = _mm256_add_ps(acc4i, prod4i);
				prod5i = _mm256_mul_ps(coefs5[i], stateimag);
				acc5i  = _mm256_add_ps(acc5i, prod5i);
				prod6i = _mm256_mul_ps(coefs6[i], stateimag);
				acc6i  = _mm256_add_ps(acc6i, prod6i);
				prod7i = _mm256_mul_ps(coefs7[i], stateimag);
				acc7i  = _mm256_add_ps(acc7i, prod7i);
#endif
				j = (j+1) & masksimd;
			}
			register __m256 h00r = _mm256_permute2f128_ps(acc0r, acc4r, 0x20);
			register __m256 h01r = _mm256_permute2f128_ps(acc0r, acc4r, 0x31);
			register __m256 h02r = _mm256_permute2f128_ps(acc1r, acc5r, 0x20);
			register __m256 h03r = _mm256_permute2f128_ps(acc1r, acc5r, 0x31);
			register __m256 h04r = _mm256_permute2f128_ps(acc2r, acc6r, 0x20);
			register __m256 h05r = _mm256_permute2f128_ps(acc2r, acc6r, 0x31);
			register __m256 h06r = _mm256_permute2f128_ps(acc3r, acc7r, 0x20);
			register __m256 h07r = _mm256_permute2f128_ps(acc3r, acc7r, 0x31);
			register __m256 h10r = _mm256_hadd_ps(h00r, h01r);
			register __m256 h11r = _mm256_hadd_ps(h02r, h03r);
			register __m256 h12r = _mm256_hadd_ps(h04r, h05r);
			register __m256 h13r = _mm256_hadd_ps(h06r, h07r);
			register __m256 h20r = _mm256_hadd_ps(h10r, h11r);
			register __m256 h21r = _mm256_hadd_ps(h12r, h13r);
			register __m256 h30r = _mm256_hadd_ps(h20r, h21r);
			register __m256 h00i = _mm256_permute2f128_ps(acc0i, acc4i, 0x20);
			register __m256 h01i = _mm256_permute2f128_ps(acc0i, acc4i, 0x31);
			register __m256 h02i = _mm256_permute2f128_ps(acc1i, acc5i, 0x20);
			register __m256 h03i = _mm256_permute2f128_ps(acc1i, acc5i, 0x31);
			register __m256 h04i = _mm256_permute2f128_ps(acc2i, acc6i, 0x20);
			register __m256 h05i = _mm256_permute2f128_ps(acc2i, acc6i, 0x31);
			register __m256 h06i = _mm256_permute2f128_ps(acc3i, acc7i, 0x20);
			register __m256 h07i = _mm256_permute2f128_ps(acc3i, acc7i, 0x31);
			register __m256 h10i = _mm256_hadd_ps(h00i, h01i);
			register __m256 h11i = _mm256_hadd_ps(h02i, h03i);
			register __m256 h12i = _mm256_hadd_ps(h04i, h05i);
			register __m256 h13i = _mm256_hadd_ps(h06i, h07i);
			register __m256 h20i = _mm256_hadd_ps(h10i, h11i);
			register __m256 h21i = _mm256_hadd_ps(h12i, h13i);
			register __m256 h30i = _mm256_hadd_ps(h20i, h21i);
			ocbuf->data[k+0].real = h30r[0];
			ocbuf->data[k+0].imag = h30i[0];
			ocbuf->data[k+1].real = h30r[1];
			ocbuf->data[k+1].imag = h30i[1];
			ocbuf->data[k+2].real = h30r[2];
			ocbuf->data[k+2].imag = h30i[2];
			ocbuf->data[k+3].real = h30r[3];
			ocbuf->data[k+3].imag = h30i[3];
			ocbuf->data[k+4].real = h30r[4];
			ocbuf->data[k+4].imag = h30i[4];
			ocbuf->data[k+5].real = h30r[5];
			ocbuf->data[k+5].imag = h30i[5];
			ocbuf->data[k+6].real = h30r[6];
			ocbuf->data[k+6].imag = h30i[6];
			ocbuf->data[k+7].real = h30r[7];
			ocbuf->data[k+7].imag = h30i[7];
			start_index = (start_index + 8) & mask;
			startsimd = start_index >> 3;
			k += 8;
		}
		for (; k < ibuflen; k++) {
			fir->stater[fir->index] = icbuf->data[k].real;
			fir->statei[fir->index] = icbuf->data[k].imag;
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0r = _mm256_setzero_ps();
			acc0i = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0r = _mm256_fmadd_ps(coefs0[i], stater[j], acc0r);
				acc0i = _mm256_fmadd_ps(coefs0[i], statei[j], acc0i);
#elif defined __FMA4__
				acc0r = _mm256_macc_ps(coefs0[i], stater[j], acc0r);
				acc0i = _mm256_macc_ps(coefs0[i], statei[j], acc0i);
#else
				prod0r = _mm256_mul_ps(coefs0[i], stater[j]);
				prod0i = _mm256_mul_ps(coefs0[i], statei[j]);
				acc0r  = _mm256_add_ps(acc0r, prod0r);
				acc0i  = _mm256_add_ps(acc0i, prod0i);
#endif
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3] + acc0r[4] + acc0r[5] + acc0r[6] + acc0r[7];
			ocbuf->data[k].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3] + acc0i[4] + acc0i[5] + acc0i[6] + acc0i[7];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
		}
	}

	return ocbuf;
}


pdlc_buffer_t* pdlc_fir_filter_interpolate_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 3;
	const unsigned int masksimd = mask >> 3;
	const size_t ibuflen = ifbuf->length;
	const size_t obuflen = ibuflen*fir->max_counter;
	const float ffactor = (float)(fir->max_counter);
	const size_t mcounter = fir->max_counter;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 3;
	unsigned int i, j;
	size_t k = 0, l = 0;
	register __m256 acc0, acc1, acc2, acc3, acc4, acc5, acc6, acc7;
#if !(defined __FMA__ || defined __FMA4__)
	register __m256 prod0, prod1, prod2, prod3, prod4, prod5, prod6, prod7;
#endif
	register __m256 statereal;
	const __m256 *coefs0, *coefs1, *coefs2, *coefs3, *coefs4, *coefs5, *coefs6, *coefs7;
	__m256 *stater = (__m256*)fir->stater;


	if (!ofbuf)
		ofbuf = pdlc_buffer_new(obuflen);
	else if (ofbuf->length != obuflen)
		pdlc_buffer_resize(ofbuf, obuflen, 0);


	while ((start_index & 7) && k < obuflen) {
		if ((k % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m256*)fir->coefs[start_index & 7];
		acc0 = _mm256_setzero_ps();
		j = startsimd;
		for (i = 0; i < lensimd; i++) {
#if defined __FMA__
			acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
			acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
			prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
			acc0  = _mm256_add_ps(acc0, prod0);
#endif
			j = (j+1) & masksimd;
		}
		ofbuf->data[k] = (acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7]) * ffactor;
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 3;
		k++;
	}
	while (k + 8 <= obuflen) {
		if (((k+0) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+1) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+2) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+3) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+4) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+5) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+6) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		if (((k+7) % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
		coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
		coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
		coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
		coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
		coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
		coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
		coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
		acc0 = _mm256_setzero_ps();
		acc1 = _mm256_setzero_ps();
		acc2 = _mm256_setzero_ps();
		acc3 = _mm256_setzero_ps();
		acc4 = _mm256_setzero_ps();
		acc5 = _mm256_setzero_ps();
		acc6 = _mm256_setzero_ps();
		acc7 = _mm256_setzero_ps();
		j = startsimd;
		for (i = 0; i < lensimd; i++) {
			statereal = stater[j];
#if defined __FMA__
			acc0 = _mm256_fmadd_ps(coefs0[i], statereal, acc0);
			acc1 = _mm256_fmadd_ps(coefs1[i], statereal, acc1);
			acc2 = _mm256_fmadd_ps(coefs2[i], statereal, acc2);
			acc3 = _mm256_fmadd_ps(coefs3[i], statereal, acc3);
			acc4 = _mm256_fmadd_ps(coefs4[i], statereal, acc4);
			acc5 = _mm256_fmadd_ps(coefs5[i], statereal, acc5);
			acc6 = _mm256_fmadd_ps(coefs6[i], statereal, acc6);
			acc7 = _mm256_fmadd_ps(coefs7[i], statereal, acc7);
#elif defined __FMA4__
			acc0 = _mm256_macc_ps(coefs0[i], statereal, acc0);
			acc1 = _mm256_macc_ps(coefs1[i], statereal, acc1);
			acc2 = _mm256_macc_ps(coefs2[i], statereal, acc2);
			acc3 = _mm256_macc_ps(coefs3[i], statereal, acc3);
			acc4 = _mm256_macc_ps(coefs4[i], statereal, acc4);
			acc5 = _mm256_macc_ps(coefs5[i], statereal, acc5);
			acc6 = _mm256_macc_ps(coefs6[i], statereal, acc6);
			acc7 = _mm256_macc_ps(coefs7[i], statereal, acc7);
#else
			prod0 = _mm256_mul_ps(coefs0[i], statereal);
			acc0  = _mm256_add_ps(acc0, prod0);
			prod1 = _mm256_mul_ps(coefs1[i], statereal);
			acc1  = _mm256_add_ps(acc1, prod1);
			prod2 = _mm256_mul_ps(coefs2[i], statereal);
			acc2  = _mm256_add_ps(acc2, prod2);
			prod3 = _mm256_mul_ps(coefs3[i], statereal);
			acc3  = _mm256_add_ps(acc3, prod3);
			prod4 = _mm256_mul_ps(coefs4[i], statereal);
			acc4  = _mm256_add_ps(acc4, prod4);
			prod5 = _mm256_mul_ps(coefs5[i], statereal);
			acc5  = _mm256_add_ps(acc5, prod5);
			prod6 = _mm256_mul_ps(coefs6[i], statereal);
			acc6  = _mm256_add_ps(acc6, prod6);
			prod7 = _mm256_mul_ps(coefs7[i], statereal);
			acc7  = _mm256_add_ps(acc7, prod7);
#endif
			j = (j+1) & masksimd;
		}
		register __m256 h00 = _mm256_permute2f128_ps(acc0, acc4, 0x20);
		register __m256 h01 = _mm256_permute2f128_ps(acc0, acc4, 0x31);
		register __m256 h02 = _mm256_permute2f128_ps(acc1, acc5, 0x20);
		register __m256 h03 = _mm256_permute2f128_ps(acc1, acc5, 0x31);
		register __m256 h04 = _mm256_permute2f128_ps(acc2, acc6, 0x20);
		register __m256 h05 = _mm256_permute2f128_ps(acc2, acc6, 0x31);
		register __m256 h06 = _mm256_permute2f128_ps(acc3, acc7, 0x20);
		register __m256 h07 = _mm256_permute2f128_ps(acc3, acc7, 0x31);
		register __m256 h10 = _mm256_hadd_ps(h00, h01);
		register __m256 h11 = _mm256_hadd_ps(h02, h03);
		register __m256 h12 = _mm256_hadd_ps(h04, h05);
		register __m256 h13 = _mm256_hadd_ps(h06, h07);
		register __m256 h20 = _mm256_hadd_ps(h10, h11);
		register __m256 h21 = _mm256_hadd_ps(h12, h13);
		register __m256 h30 = _mm256_hadd_ps(h20, h21);
		h30 = _mm256_mul_ps(h30, _mm256_set1_ps(ffactor));
		_mm256_storeu_ps(ofbuf->data + k, h30);
		start_index = (start_index + 8) & mask;
		startsimd = start_index >> 3;
		k += 8;
	}
	for (; k < obuflen; k++) {
		if ((k % mcounter) == 0)
			fir->stater[fir->index] = ifbuf->data[l++];
		else
			fir->stater[fir->index] = 0.0f;
		fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
				prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm256_add_ps(acc0, prod0);
#endif
				j = (j+1) & masksimd;
			}
			ofbuf->data[k] = (acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7]) * ffactor;
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
	}

	return ofbuf;
}


pdlc_complex_buffer_t* pdlc_fir_filter_interpolate_complex_buffer(pdlc_fir_filter_t* fir, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 3;
	const unsigned int masksimd = mask >> 3;
	const size_t ibuflen = icbuf->length;
	const size_t obuflen = ibuflen*fir->max_counter;
	const float ffactor = (float)(fir->max_counter);
	const size_t mcounter = fir->max_counter;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 3;
	unsigned int i, j;
	size_t k = 0, l = 0;
	register __m256 acc0r, acc1r, acc2r, acc3r, acc4r, acc5r, acc6r, acc7r;
	register __m256 acc0i, acc1i, acc2i, acc3i, acc4i, acc5i, acc6i, acc7i;
#if !(defined __FMA__ || defined __FMA4__)
	register __m256 prod0r, prod1r, prod2r, prod3r, prod4r, prod5r, prod6r, prod7r;
	register __m256 prod0i, prod1i, prod2i, prod3i, prod4i, prod5i, prod6i, prod7i;
#endif
	register __m256 statereal, stateimag;
	const __m256 *coefs0, *coefs1, *coefs2, *coefs3, *coefs4, *coefs5, *coefs6, *coefs7;
	__m256 *stater = (__m256*)fir->stater;
	__m256 *statei = (__m256*)fir->statei;


	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(obuflen);
	else if (ocbuf->length != obuflen)
		pdlc_complex_buffer_resize(ocbuf, obuflen, 0);


	while ((start_index & 7) && k < obuflen) {
		if ((k % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		coefs0 = (__m256*)fir->coefs[start_index & 7];
		acc0r = _mm256_setzero_ps();
		acc0i = _mm256_setzero_ps();
		j = startsimd;
		for (i = 0; i < lensimd; i++) {
#if defined __FMA__
			acc0r = _mm256_fmadd_ps(coefs0[i], stater[j], acc0r);
			acc0i = _mm256_fmadd_ps(coefs0[i], statei[j], acc0i);
#elif defined __FMA4__
			acc0r = _mm256_macc_ps(coefs0[i], stater[j], acc0r);
			acc0i = _mm256_macc_ps(coefs0[i], statei[j], acc0i);
#else
			prod0r = _mm256_mul_ps(coefs0[i], stater[j]);
			prod0i = _mm256_mul_ps(coefs0[i], statei[j]);
			acc0r  = _mm256_add_ps(acc0r, prod0r);
			acc0i  = _mm256_add_ps(acc0i, prod0i);
#endif
			j = (j+1) & masksimd;
		}
		ocbuf->data[k].real = (acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3] + acc0r[4] + acc0r[5] + acc0r[6] + acc0r[7]) * ffactor;
		ocbuf->data[k].imag = (acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3] + acc0i[4] + acc0i[5] + acc0i[6] + acc0i[7]) * ffactor;
		start_index = (start_index + 1) & mask;
		startsimd = start_index >> 3;
		k++;
	}
	while (k + 8 <= obuflen) {
		if (((k+0) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+1) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+2) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+3) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+4) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+5) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+6) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
		if (((k+7) % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
			coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
			coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
			coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
			coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
			coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
			coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
			coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
			acc0r = _mm256_setzero_ps();
			acc0i = _mm256_setzero_ps();
			acc1r = _mm256_setzero_ps();
			acc1i = _mm256_setzero_ps();
			acc2r = _mm256_setzero_ps();
			acc2i = _mm256_setzero_ps();
			acc3r = _mm256_setzero_ps();
			acc3i = _mm256_setzero_ps();
			acc4r = _mm256_setzero_ps();
			acc4i = _mm256_setzero_ps();
			acc5r = _mm256_setzero_ps();
			acc5i = _mm256_setzero_ps();
			acc6r = _mm256_setzero_ps();
			acc6i = _mm256_setzero_ps();
			acc7r = _mm256_setzero_ps();
			acc7i = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				statereal = stater[j];
				stateimag = statei[j];
#if defined __FMA__
				acc0r = _mm256_fmadd_ps(coefs0[i], statereal, acc0r);
				acc1r = _mm256_fmadd_ps(coefs1[i], statereal, acc1r);
				acc2r = _mm256_fmadd_ps(coefs2[i], statereal, acc2r);
				acc3r = _mm256_fmadd_ps(coefs3[i], statereal, acc3r);
				acc4r = _mm256_fmadd_ps(coefs4[i], statereal, acc4r);
				acc5r = _mm256_fmadd_ps(coefs5[i], statereal, acc5r);
				acc6r = _mm256_fmadd_ps(coefs6[i], statereal, acc6r);
				acc7r = _mm256_fmadd_ps(coefs7[i], statereal, acc7r);
				acc0i = _mm256_fmadd_ps(coefs0[i], stateimag, acc0i);
				acc1i = _mm256_fmadd_ps(coefs1[i], stateimag, acc1i);
				acc2i = _mm256_fmadd_ps(coefs2[i], stateimag, acc2i);
				acc3i = _mm256_fmadd_ps(coefs3[i], stateimag, acc3i);
				acc4i = _mm256_fmadd_ps(coefs4[i], stateimag, acc4i);
				acc5i = _mm256_fmadd_ps(coefs5[i], stateimag, acc5i);
				acc6i = _mm256_fmadd_ps(coefs6[i], stateimag, acc6i);
				acc7i = _mm256_fmadd_ps(coefs7[i], stateimag, acc7i);
#elif defined __FMA4__
				acc0r = _mm256_macc_ps(coefs0[i], statereal, acc0r);
				acc1r = _mm256_macc_ps(coefs1[i], statereal, acc1r);
				acc2r = _mm256_macc_ps(coefs2[i], statereal, acc2r);
				acc3r = _mm256_macc_ps(coefs3[i], statereal, acc3r);
				acc4r = _mm256_macc_ps(coefs4[i], statereal, acc4r);
				acc5r = _mm256_macc_ps(coefs5[i], statereal, acc5r);
				acc6r = _mm256_macc_ps(coefs6[i], statereal, acc6r);
				acc7r = _mm256_macc_ps(coefs7[i], statereal, acc7r);
				acc0i = _mm256_macc_ps(coefs0[i], stateimag, acc0i);
				acc1i = _mm256_macc_ps(coefs1[i], stateimag, acc1i);
				acc2i = _mm256_macc_ps(coefs2[i], stateimag, acc2i);
				acc3i = _mm256_macc_ps(coefs3[i], stateimag, acc3i);
				acc4i = _mm256_macc_ps(coefs4[i], stateimag, acc4i);
				acc5i = _mm256_macc_ps(coefs5[i], stateimag, acc5i);
				acc6i = _mm256_macc_ps(coefs6[i], stateimag, acc6i);
				acc7i = _mm256_macc_ps(coefs7[i], stateimag, acc7i);
#else
				prod0r = _mm256_mul_ps(coefs0[i], statereal);
				acc0r  = _mm256_add_ps(acc0r, prod0r);
				prod1r = _mm256_mul_ps(coefs1[i], statereal);
				acc1r  = _mm256_add_ps(acc1r, prod1r);
				prod2r = _mm256_mul_ps(coefs2[i], statereal);
				acc2r  = _mm256_add_ps(acc2r, prod2r);
				prod3r = _mm256_mul_ps(coefs3[i], statereal);
				acc3r  = _mm256_add_ps(acc3r, prod3r);
				prod4r = _mm256_mul_ps(coefs4[i], statereal);
				acc4r  = _mm256_add_ps(acc4r, prod4r);
				prod5r = _mm256_mul_ps(coefs5[i], statereal);
				acc5r  = _mm256_add_ps(acc5r, prod5r);
				prod6r = _mm256_mul_ps(coefs6[i], statereal);
				acc6r  = _mm256_add_ps(acc6r, prod6r);
				prod7r = _mm256_mul_ps(coefs7[i], statereal);
				acc7r  = _mm256_add_ps(acc7r, prod7r);
				prod0i = _mm256_mul_ps(coefs0[i], stateimag);
				acc0i  = _mm256_add_ps(acc0i, prod0i);
				prod1i = _mm256_mul_ps(coefs1[i], stateimag);
				acc1i  = _mm256_add_ps(acc1i, prod1i);
				prod2i = _mm256_mul_ps(coefs2[i], stateimag);
				acc2i  = _mm256_add_ps(acc2i, prod2i);
				prod3i = _mm256_mul_ps(coefs3[i], stateimag);
				acc3i  = _mm256_add_ps(acc3i, prod3i);
				prod4i = _mm256_mul_ps(coefs4[i], stateimag);
				acc4i  = _mm256_add_ps(acc4i, prod4i);
				prod5i = _mm256_mul_ps(coefs5[i], stateimag);
				acc5i  = _mm256_add_ps(acc5i, prod5i);
				prod6i = _mm256_mul_ps(coefs6[i], stateimag);
				acc6i  = _mm256_add_ps(acc6i, prod6i);
				prod7i = _mm256_mul_ps(coefs7[i], stateimag);
				acc7i  = _mm256_add_ps(acc7i, prod7i);
#endif
				j = (j+1) & masksimd;
			}
			register __m256 h00r = _mm256_permute2f128_ps(acc0r, acc4r, 0x20);
			register __m256 h01r = _mm256_permute2f128_ps(acc0r, acc4r, 0x31);
			register __m256 h02r = _mm256_permute2f128_ps(acc1r, acc5r, 0x20);
			register __m256 h03r = _mm256_permute2f128_ps(acc1r, acc5r, 0x31);
			register __m256 h04r = _mm256_permute2f128_ps(acc2r, acc6r, 0x20);
			register __m256 h05r = _mm256_permute2f128_ps(acc2r, acc6r, 0x31);
			register __m256 h06r = _mm256_permute2f128_ps(acc3r, acc7r, 0x20);
			register __m256 h07r = _mm256_permute2f128_ps(acc3r, acc7r, 0x31);
			register __m256 h10r = _mm256_hadd_ps(h00r, h01r);
			register __m256 h11r = _mm256_hadd_ps(h02r, h03r);
			register __m256 h12r = _mm256_hadd_ps(h04r, h05r);
			register __m256 h13r = _mm256_hadd_ps(h06r, h07r);
			register __m256 h20r = _mm256_hadd_ps(h10r, h11r);
			register __m256 h21r = _mm256_hadd_ps(h12r, h13r);
			register __m256 h30r = _mm256_hadd_ps(h20r, h21r);
			h30r = _mm256_mul_ps(h30r, _mm256_set1_ps(ffactor));
			register __m256 h00i = _mm256_permute2f128_ps(acc0i, acc4i, 0x20);
			register __m256 h01i = _mm256_permute2f128_ps(acc0i, acc4i, 0x31);
			register __m256 h02i = _mm256_permute2f128_ps(acc1i, acc5i, 0x20);
			register __m256 h03i = _mm256_permute2f128_ps(acc1i, acc5i, 0x31);
			register __m256 h04i = _mm256_permute2f128_ps(acc2i, acc6i, 0x20);
			register __m256 h05i = _mm256_permute2f128_ps(acc2i, acc6i, 0x31);
			register __m256 h06i = _mm256_permute2f128_ps(acc3i, acc7i, 0x20);
			register __m256 h07i = _mm256_permute2f128_ps(acc3i, acc7i, 0x31);
			register __m256 h10i = _mm256_hadd_ps(h00i, h01i);
			register __m256 h11i = _mm256_hadd_ps(h02i, h03i);
			register __m256 h12i = _mm256_hadd_ps(h04i, h05i);
			register __m256 h13i = _mm256_hadd_ps(h06i, h07i);
			register __m256 h20i = _mm256_hadd_ps(h10i, h11i);
			register __m256 h21i = _mm256_hadd_ps(h12i, h13i);
			register __m256 h30i = _mm256_hadd_ps(h20i, h21i);
			h30i = _mm256_mul_ps(h30i, _mm256_set1_ps(ffactor));
			ocbuf->data[k+0].real = h30r[0];
			ocbuf->data[k+0].imag = h30i[0];
			ocbuf->data[k+1].real = h30r[1];
			ocbuf->data[k+1].imag = h30i[1];
			ocbuf->data[k+2].real = h30r[2];
			ocbuf->data[k+2].imag = h30i[2];
			ocbuf->data[k+3].real = h30r[3];
			ocbuf->data[k+3].imag = h30i[3];
			ocbuf->data[k+4].real = h30r[4];
			ocbuf->data[k+4].imag = h30i[4];
			ocbuf->data[k+5].real = h30r[5];
			ocbuf->data[k+5].imag = h30i[5];
			ocbuf->data[k+6].real = h30r[6];
			ocbuf->data[k+6].imag = h30i[6];
			ocbuf->data[k+7].real = h30r[7];
			ocbuf->data[k+7].imag = h30i[7];
			start_index = (start_index + 8) & mask;
			startsimd = start_index >> 3;
			k += 8;
	}
	for (; k < obuflen; k++) {
		if ((k % mcounter) == 0) {
			fir->stater[fir->index] = icbuf->data[l].real;
			fir->statei[fir->index] = icbuf->data[l].imag;
			l++;
		}
		else {
			fir->stater[fir->index] = 0.0f;
			fir->statei[fir->index] = 0.0f;
		}
		fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0r = _mm256_setzero_ps();
			acc0i = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0r = _mm256_fmadd_ps(coefs0[i], stater[j], acc0r);
				acc0i = _mm256_fmadd_ps(coefs0[i], statei[j], acc0i);
#elif defined __FMA4__
				acc0r = _mm256_macc_ps(coefs0[i], stater[j], acc0r);
				acc0i = _mm256_macc_ps(coefs0[i], statei[j], acc0i);
#else
				prod0r = _mm256_mul_ps(coefs0[i], stater[j]);
				prod0i = _mm256_mul_ps(coefs0[i], statei[j]);
				acc0r  = _mm256_add_ps(acc0r, prod0r);
				acc0i  = _mm256_add_ps(acc0i, prod0i);
#endif
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].real = (acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3] + acc0r[4] + acc0r[5] + acc0r[6] + acc0r[7]) * ffactor;
			ocbuf->data[k].imag = (acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3] + acc0i[4] + acc0i[5] + acc0i[6] + acc0i[7]) * ffactor;
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
	}

	return ocbuf;
}


pdlc_buffer_t* pdlc_fir_filter_decimate_float_buffer(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_buffer_t *ofbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 3;
	const unsigned int masksimd = mask >> 3;
	const int mcounter = fir->max_counter;
	const size_t ibuflen = ifbuf->length;
	const size_t obuflen = (size_t)ceil(((double)ibuflen - (double)((mcounter - fir->counter) % mcounter)) / (double)mcounter);
	unsigned int start_index = (flt_len + fir->index + fir->counter + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 3;
	unsigned int i0, i1, j0, j1;
	size_t k, l;
	register __m256 acc0, acc1;
#if !(defined __FMA__ || defined __FMA4__)
	register __m256 prod0, prod1;
#endif
	const __m256 *coefs;
	__m256 *stater = (__m256*)fir->stater;


	if (!ofbuf)
		ofbuf = pdlc_buffer_new(obuflen);
	else if (ofbuf->length != obuflen)
		pdlc_buffer_resize(ofbuf, obuflen, 0);


	for (k = 0, l = 0; k < ibuflen; k++) {
		fir->stater[fir->index] = ifbuf->data[k];
		fir->index = (fir->index + 1) & mask;
		if (fir->counter == 0) {
			coefs = (__m256*)fir->coefs[start_index & 7];
			acc0 = _mm256_setzero_ps();
			acc1 = _mm256_setzero_ps();
			j0 = startsimd;
			j1 = (startsimd+1) & masksimd;
			i0 = 0;
			i1 = 1;
			while (i1 < lensimd) {
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs[i0], stater[j0], acc0);
				acc1 = _mm256_fmadd_ps(coefs[i1], stater[j1], acc1);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs[i0], stater[j0], acc0);
				acc1 = _mm256_macc_ps(coefs[i1], stater[j1], acc1);
#else
				prod0 = _mm256_mul_ps(coefs[i0], stater[j0]);
				acc0  = _mm256_add_ps(acc0, prod0);
				prod1 = _mm256_mul_ps(coefs[i1], stater[j1]);
				acc1  = _mm256_add_ps(acc1, prod1);
#endif
				i0 += 2;
				i1 += 2;
				j0 = (j0+2) & masksimd;
				j1 = (j1+2) & masksimd;
			}
			while (i0 < lensimd) {
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs[i0], stater[j0], acc0);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs[i0], stater[j0], acc0);
#else
				prod0 = _mm256_mul_ps(coefs[i0], stater[j0]);
				acc0  = _mm256_add_ps(acc0, prod0);
#endif
				i0 += 2;
				j0 = (j0+2) & masksimd;
			}
			acc0 = _mm256_add_ps(acc0, acc1);
			ofbuf->data[l++] = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
			start_index = (start_index + mcounter) & mask;
			startsimd = start_index >> 3;
		}
		fir->counter = (fir->counter + 1) % mcounter;
	}

	return ofbuf;
}


pdlc_complex_buffer_t* pdlc_fir_filter_decimate_complex_buffer(pdlc_fir_filter_t* fir, const pdlc_complex_buffer_t *icbuf, pdlc_complex_buffer_t *ocbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 3;
	const unsigned int masksimd = mask >> 3;
	const int mcounter = fir->max_counter;
	const size_t ibuflen = icbuf->length;
	const size_t obuflen = (size_t)ceil(((double)ibuflen - (double)((mcounter - fir->counter) % mcounter)) / (double)mcounter);
	unsigned int start_index = (flt_len + fir->index + fir->counter + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 3;
	unsigned int i0, j0, i1, j1;
	size_t k, l;
	register __m256 acc0r, acc0i, acc1r, acc1i;
#if !(defined __FMA__ || defined __FMA4__)
	register __m256 prod0r, prod0i, prod1r, prod1i;
#endif
	const __m256 *coefs;
	__m256 *stater = (__m256*)fir->stater;
	__m256 *statei = (__m256*)fir->statei;


	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(obuflen);
	else if (ocbuf->length != obuflen)
		pdlc_complex_buffer_resize(ocbuf, obuflen, 0);


	for (k = 0, l = 0; k < ibuflen; k++) {
		fir->stater[fir->index] = icbuf->data[k].real;
		fir->statei[fir->index] = icbuf->data[k].imag;
		fir->index = (fir->index + 1) & mask;
		if (fir->counter == 0) {
			coefs = (__m256*)fir->coefs[start_index & 7];
			acc0r = _mm256_setzero_ps();
			acc0i = _mm256_setzero_ps();
			acc1r = _mm256_setzero_ps();
			acc1i = _mm256_setzero_ps();
			j0 = startsimd;
			j1 = (startsimd+1) & masksimd;
			i0 = 0;
			i1 = 1;
			while (i1 < lensimd) {
#if defined __FMA__
				acc0r = _mm256_fmadd_ps(coefs[i0], stater[j0], acc0r);
				acc0i = _mm256_fmadd_ps(coefs[i0], statei[j0], acc0i);
				acc1r = _mm256_fmadd_ps(coefs[i1], stater[j1], acc1r);
				acc1i = _mm256_fmadd_ps(coefs[i1], statei[j1], acc1i);
#elif defined __FMA4__
				acc0r = _mm256_macc_ps(coefs[i0], stater[j0], acc0r);
				acc0i = _mm256_macc_ps(coefs[i0], statei[j0], acc0i);
				acc1r = _mm256_macc_ps(coefs[i1], stater[j1], acc1r);
				acc1i = _mm256_macc_ps(coefs[i1], statei[j1], acc1i);
#else
				prod0r = _mm256_mul_ps(coefs[i0], stater[j0]);
				acc0r  = _mm256_add_ps(acc0r, prod0r);
				prod0i = _mm256_mul_ps(coefs[i0], statei[j0]);
				acc0i  = _mm256_add_ps(acc0i, prod0i);
				prod1r = _mm256_mul_ps(coefs[i1], stater[j1]);
				acc1r  = _mm256_add_ps(acc1r, prod1r);
				prod1i = _mm256_mul_ps(coefs[i1], statei[j1]);
				acc1i  = _mm256_add_ps(acc1i, prod1i);
#endif
				i0 += 2;
				i1 += 2;
				j0 = (j0+2) & masksimd;
				j1 = (j1+2) & masksimd;
			}
			while (i0 < lensimd) {
#if defined __FMA__
				acc0r = _mm256_fmadd_ps(coefs[i0], stater[j0], acc0r);
				acc0i = _mm256_fmadd_ps(coefs[i0], statei[j0], acc0i);
#elif defined __FMA4__
				acc0r = _mm256_macc_ps(coefs[i0], stater[j0], acc0r);
				acc0i = _mm256_macc_ps(coefs[i0], statei[j0], acc0i);
#else
				prod0r = _mm256_mul_ps(coefs[i0], stater[j0]);
				acc0r  = _mm256_add_ps(acc0r, prod0r);
				prod0i = _mm256_mul_ps(coefs[i0], statei[j0]);
				acc0i  = _mm256_add_ps(acc0i, prod0i);
#endif
				i0 += 2;
				j0 = (j0+2) & masksimd;
			}
			acc0r = _mm256_add_ps(acc0r, acc1r);
			acc0i = _mm256_add_ps(acc0i, acc1i);
			ocbuf->data[l].real = acc0r[0] + acc0r[1] + acc0r[2] + acc0r[3] + acc0r[4] + acc0r[5] + acc0r[6] + acc0r[7];
			ocbuf->data[l].imag = acc0i[0] + acc0i[1] + acc0i[2] + acc0i[3] + acc0i[4] + acc0i[5] + acc0i[6] + acc0i[7];
			l++;
			start_index = (start_index + mcounter) & mask;
			startsimd = start_index >> 3;
		}
		fir->counter = (fir->counter + 1) % mcounter;
	}

	return ocbuf;
}


pdlc_complex_buffer_t* pdlc_fir_filter_transform(pdlc_fir_filter_t* fir, const pdlc_buffer_t *ifbuf, pdlc_complex_buffer_t *ocbuf)
{
	const unsigned int nb_coefs = fir->nb_coefs;
	const unsigned int flt_len  = fir->state_len;
	const unsigned int mask = fir->index_mask;
	const unsigned int lensimd  = fir->coef_len >> 3;
	const unsigned int masksimd = mask >> 3;
	const size_t ibuflen = ifbuf->length;
	unsigned int start_index = (flt_len + fir->index + 1 - nb_coefs) & mask;
	unsigned int startsimd = start_index >> 3;
	unsigned int middle_index = (start_index + nb_coefs / 2) & mask;
	unsigned int i, j;
	size_t k = 0;
	register __m256 acc0, acc1, acc2, acc3, acc4, acc5, acc6, acc7;
#if !(defined __FMA__ || defined __FMA4__)
	register __m256 prod0, prod1, prod2, prod3, prod4, prod5, prod6, prod7;
#endif
	register __m256 statereal;
	const __m256 *coefs0, *coefs1, *coefs2, *coefs3, *coefs4, *coefs5, *coefs6, *coefs7;
	__m256 *stater = (__m256*)fir->stater;


	if (!ocbuf)
		ocbuf = pdlc_complex_buffer_new(ibuflen);
	else if (ocbuf->length != ibuflen)
		pdlc_complex_buffer_resize(ocbuf, ibuflen, 0);


	if (nb_coefs & 1) {
		while ((start_index & 7) && k < ibuflen) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
				prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm256_add_ps(acc0, prod0);
#endif
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
		}
		while (k + 8 <= ibuflen) {
			fir->stater[(fir->index + 0) & mask] = ifbuf->data[k + 0];
			fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
			fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
			fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
			fir->stater[(fir->index + 4) & mask] = ifbuf->data[k + 4];
			fir->stater[(fir->index + 5) & mask] = ifbuf->data[k + 5];
			fir->stater[(fir->index + 6) & mask] = ifbuf->data[k + 6];
			fir->stater[(fir->index + 7) & mask] = ifbuf->data[k + 7];
			fir->index = (fir->index + 8) & mask;
			coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
			coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
			coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
			coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
			coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
			coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
			coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
			coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
			acc0 = _mm256_setzero_ps();
			acc1 = _mm256_setzero_ps();
			acc2 = _mm256_setzero_ps();
			acc3 = _mm256_setzero_ps();
			acc4 = _mm256_setzero_ps();
			acc5 = _mm256_setzero_ps();
			acc6 = _mm256_setzero_ps();
			acc7 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				statereal = stater[j];
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], statereal, acc0);
				acc1 = _mm256_fmadd_ps(coefs1[i], statereal, acc1);
				acc2 = _mm256_fmadd_ps(coefs2[i], statereal, acc2);
				acc3 = _mm256_fmadd_ps(coefs3[i], statereal, acc3);
				acc4 = _mm256_fmadd_ps(coefs4[i], statereal, acc4);
				acc5 = _mm256_fmadd_ps(coefs5[i], statereal, acc5);
				acc6 = _mm256_fmadd_ps(coefs6[i], statereal, acc6);
				acc7 = _mm256_fmadd_ps(coefs7[i], statereal, acc7);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], statereal, acc0);
				acc1 = _mm256_macc_ps(coefs1[i], statereal, acc1);
				acc2 = _mm256_macc_ps(coefs2[i], statereal, acc2);
				acc3 = _mm256_macc_ps(coefs3[i], statereal, acc3);
				acc4 = _mm256_macc_ps(coefs4[i], statereal, acc4);
				acc5 = _mm256_macc_ps(coefs5[i], statereal, acc5);
				acc6 = _mm256_macc_ps(coefs6[i], statereal, acc6);
				acc7 = _mm256_macc_ps(coefs7[i], statereal, acc7);
#else
				prod0 = _mm256_mul_ps(coefs0[i], statereal);
				acc0  = _mm256_add_ps(acc0, prod0);
				prod1 = _mm256_mul_ps(coefs1[i], statereal);
				acc1  = _mm256_add_ps(acc1, prod1);
				prod2 = _mm256_mul_ps(coefs2[i], statereal);
				acc2  = _mm256_add_ps(acc2, prod2);
				prod3 = _mm256_mul_ps(coefs3[i], statereal);
				acc3  = _mm256_add_ps(acc3, prod3);
				prod4 = _mm256_mul_ps(coefs4[i], statereal);
				acc4  = _mm256_add_ps(acc4, prod4);
				prod5 = _mm256_mul_ps(coefs5[i], statereal);
				acc5  = _mm256_add_ps(acc5, prod5);
				prod6 = _mm256_mul_ps(coefs6[i], statereal);
				acc6  = _mm256_add_ps(acc6, prod6);
				prod7 = _mm256_mul_ps(coefs7[i], statereal);
				acc7  = _mm256_add_ps(acc7, prod7);
#endif
				j = (j+1) & masksimd;
			}
			register __m256 h00 = _mm256_permute2f128_ps(acc0, acc4, 0x20);
			register __m256 h01 = _mm256_permute2f128_ps(acc0, acc4, 0x31);
			register __m256 h02 = _mm256_permute2f128_ps(acc1, acc5, 0x20);
			register __m256 h03 = _mm256_permute2f128_ps(acc1, acc5, 0x31);
			register __m256 h04 = _mm256_permute2f128_ps(acc2, acc6, 0x20);
			register __m256 h05 = _mm256_permute2f128_ps(acc2, acc6, 0x31);
			register __m256 h06 = _mm256_permute2f128_ps(acc3, acc7, 0x20);
			register __m256 h07 = _mm256_permute2f128_ps(acc3, acc7, 0x31);
			register __m256 h10 = _mm256_hadd_ps(h00, h01);
			register __m256 h11 = _mm256_hadd_ps(h02, h03);
			register __m256 h12 = _mm256_hadd_ps(h04, h05);
			register __m256 h13 = _mm256_hadd_ps(h06, h07);
			register __m256 h20 = _mm256_hadd_ps(h10, h11);
			register __m256 h21 = _mm256_hadd_ps(h12, h13);
			register __m256 h30 = _mm256_hadd_ps(h20, h21);
			ocbuf->data[k+0].imag = h30[0];
			ocbuf->data[k+1].imag = h30[1];
			ocbuf->data[k+2].imag = h30[2];
			ocbuf->data[k+3].imag = h30[3];
			ocbuf->data[k+4].imag = h30[4];
			ocbuf->data[k+5].imag = h30[5];
			ocbuf->data[k+6].imag = h30[6];
			ocbuf->data[k+7].imag = h30[7];
			start_index = (start_index + 8) & mask;
			startsimd = start_index >> 3;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
			k++;
		}
		for (; k < ibuflen; k++) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
				prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm256_add_ps(acc0, prod0);
#endif
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
			ocbuf->data[k].real = fir->stater[middle_index];
			middle_index = (middle_index + 1) & mask;
		}
	}
	else {
		while ((start_index & 7) && k < ibuflen) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
				prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm256_add_ps(acc0, prod0);
#endif
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
		}
		while (k + 8 <= ibuflen) {
			fir->stater[(fir->index + 0) & mask] = ifbuf->data[k + 0];
			fir->stater[(fir->index + 1) & mask] = ifbuf->data[k + 1];
			fir->stater[(fir->index + 2) & mask] = ifbuf->data[k + 2];
			fir->stater[(fir->index + 3) & mask] = ifbuf->data[k + 3];
			fir->stater[(fir->index + 4) & mask] = ifbuf->data[k + 4];
			fir->stater[(fir->index + 5) & mask] = ifbuf->data[k + 5];
			fir->stater[(fir->index + 6) & mask] = ifbuf->data[k + 6];
			fir->stater[(fir->index + 7) & mask] = ifbuf->data[k + 7];
			fir->index = (fir->index + 8) & mask;
			coefs0 = (__m256*)fir->coefs[(start_index + 0) & 7];
			coefs1 = (__m256*)fir->coefs[(start_index + 1) & 7];
			coefs2 = (__m256*)fir->coefs[(start_index + 2) & 7];
			coefs3 = (__m256*)fir->coefs[(start_index + 3) & 7];
			coefs4 = (__m256*)fir->coefs[(start_index + 4) & 7];
			coefs5 = (__m256*)fir->coefs[(start_index + 5) & 7];
			coefs6 = (__m256*)fir->coefs[(start_index + 6) & 7];
			coefs7 = (__m256*)fir->coefs[(start_index + 7) & 7];
			acc0 = _mm256_setzero_ps();
			acc1 = _mm256_setzero_ps();
			acc2 = _mm256_setzero_ps();
			acc3 = _mm256_setzero_ps();
			acc4 = _mm256_setzero_ps();
			acc5 = _mm256_setzero_ps();
			acc6 = _mm256_setzero_ps();
			acc7 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
				statereal = stater[j];
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], statereal, acc0);
				acc1 = _mm256_fmadd_ps(coefs1[i], statereal, acc1);
				acc2 = _mm256_fmadd_ps(coefs2[i], statereal, acc2);
				acc3 = _mm256_fmadd_ps(coefs3[i], statereal, acc3);
				acc4 = _mm256_fmadd_ps(coefs4[i], statereal, acc4);
				acc5 = _mm256_fmadd_ps(coefs5[i], statereal, acc5);
				acc6 = _mm256_fmadd_ps(coefs6[i], statereal, acc6);
				acc7 = _mm256_fmadd_ps(coefs7[i], statereal, acc7);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], statereal, acc0);
				acc1 = _mm256_macc_ps(coefs1[i], statereal, acc1);
				acc2 = _mm256_macc_ps(coefs2[i], statereal, acc2);
				acc3 = _mm256_macc_ps(coefs3[i], statereal, acc3);
				acc4 = _mm256_macc_ps(coefs4[i], statereal, acc4);
				acc5 = _mm256_macc_ps(coefs5[i], statereal, acc5);
				acc6 = _mm256_macc_ps(coefs6[i], statereal, acc6);
				acc7 = _mm256_macc_ps(coefs7[i], statereal, acc7);
#else
				prod0 = _mm256_mul_ps(coefs0[i], statereal);
				acc0  = _mm256_add_ps(acc0, prod0);
				prod1 = _mm256_mul_ps(coefs1[i], statereal);
				acc1  = _mm256_add_ps(acc1, prod1);
				prod2 = _mm256_mul_ps(coefs2[i], statereal);
				acc2  = _mm256_add_ps(acc2, prod2);
				prod3 = _mm256_mul_ps(coefs3[i], statereal);
				acc3  = _mm256_add_ps(acc3, prod3);
				prod4 = _mm256_mul_ps(coefs4[i], statereal);
				acc4  = _mm256_add_ps(acc4, prod4);
				prod5 = _mm256_mul_ps(coefs5[i], statereal);
				acc5  = _mm256_add_ps(acc5, prod5);
				prod6 = _mm256_mul_ps(coefs6[i], statereal);
				acc6  = _mm256_add_ps(acc6, prod6);
				prod7 = _mm256_mul_ps(coefs7[i], statereal);
				acc7  = _mm256_add_ps(acc7, prod7);
#endif
				j = (j+1) & masksimd;
			}
			register __m256 h00 = _mm256_permute2f128_ps(acc0, acc4, 0x20);
			register __m256 h01 = _mm256_permute2f128_ps(acc0, acc4, 0x31);
			register __m256 h02 = _mm256_permute2f128_ps(acc1, acc5, 0x20);
			register __m256 h03 = _mm256_permute2f128_ps(acc1, acc5, 0x31);
			register __m256 h04 = _mm256_permute2f128_ps(acc2, acc6, 0x20);
			register __m256 h05 = _mm256_permute2f128_ps(acc2, acc6, 0x31);
			register __m256 h06 = _mm256_permute2f128_ps(acc3, acc7, 0x20);
			register __m256 h07 = _mm256_permute2f128_ps(acc3, acc7, 0x31);
			register __m256 h10 = _mm256_hadd_ps(h00, h01);
			register __m256 h11 = _mm256_hadd_ps(h02, h03);
			register __m256 h12 = _mm256_hadd_ps(h04, h05);
			register __m256 h13 = _mm256_hadd_ps(h06, h07);
			register __m256 h20 = _mm256_hadd_ps(h10, h11);
			register __m256 h21 = _mm256_hadd_ps(h12, h13);
			register __m256 h30 = _mm256_hadd_ps(h20, h21);
			ocbuf->data[k+0].imag = h30[0];
			ocbuf->data[k+1].imag = h30[1];
			ocbuf->data[k+2].imag = h30[2];
			ocbuf->data[k+3].imag = h30[3];
			ocbuf->data[k+4].imag = h30[4];
			ocbuf->data[k+5].imag = h30[5];
			ocbuf->data[k+6].imag = h30[6];
			ocbuf->data[k+7].imag = h30[7];
			start_index = (start_index + 8) & mask;
			startsimd = start_index >> 3;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
			k++;
		}
		for (; k < ibuflen; k++) {
			fir->stater[fir->index] = ifbuf->data[k];
			fir->index = (fir->index + 1) & mask;
			coefs0 = (__m256*)fir->coefs[start_index & 7];
			acc0 = _mm256_setzero_ps();
			j = startsimd;
			for (i = 0; i < lensimd; i++) {
#if defined __FMA__
				acc0 = _mm256_fmadd_ps(coefs0[i], stater[j], acc0);
#elif defined __FMA4__
				acc0 = _mm256_macc_ps(coefs0[i], stater[j], acc0);
#else
				prod0 = _mm256_mul_ps(coefs0[i], stater[j]);
				acc0  = _mm256_add_ps(acc0, prod0);
#endif
				j = (j+1) & masksimd;
			}
			ocbuf->data[k].imag = acc0[0] + acc0[1] + acc0[2] + acc0[3] + acc0[4] + acc0[5] + acc0[6] + acc0[7];
			start_index = (start_index + 1) & mask;
			startsimd = start_index >> 3;
			ocbuf->data[k].real = (fir->stater[middle_index] + fir->stater[(middle_index - 1) & mask]) / 2.0f;
			middle_index = (middle_index + 1) & mask;
		}
	}

	return ocbuf;
}


