/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "libpaddlec.h"


const char* pdlc_accelerator()
{
#if defined __AVX__
#if !(defined __FMA__ || defined __FMA4__)
	return "AVX";
#else
	return "AVX-FMA";
#endif
#elif defined __SSE__
	return "SSE";
#elif (defined __ARM_NEON) && ((__ARM_NEON_FP & 4) == 4)
#ifndef __FP_FAST_FMA
	return "NEON";
#else
	return "NEON-VFPv4";
#endif
#else
	return "";
#endif
}


pdlc_buffer_t* pdlc_buffer_new(size_t length)
{
	pdlc_buffer_t *res;

	res = malloc(sizeof(pdlc_buffer_t));
	res->capacity = 0;
	res->length = 0;
	res->data = NULL;

	pdlc_buffer_resize(res, length, 1);

	return res;
}


void pdlc_buffer_free(pdlc_buffer_t *buf)
{
	if (buf == NULL)
		return; 
	if (buf->data)
		free(buf->data);
	free(buf);
}


pdlc_buffer_t* pdlc_buffer_copy(const pdlc_buffer_t *orig)
{
	pdlc_buffer_t *res = pdlc_buffer_new(orig->length);
	memcpy(res->data, orig->data, orig->length*sizeof(float));
	return res;
}


void pdlc_buffer_resize(pdlc_buffer_t *buf, size_t new_length, int copy_old_and_zero_excess)
{
	float *new_data;
	size_t new_capacity;

	if (new_length <= buf->capacity) {
		if (copy_old_and_zero_excess != 0 && new_length > buf->length)
			memset(buf->data + buf->length, 0, (new_length - buf->length)*sizeof(float));
		buf->length = new_length;
		return;
	}

	new_capacity = new_length + (new_length >> 2);
	new_data = malloc(new_capacity*sizeof(float));
	if (new_data == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes of buffer!\n", new_capacity*sizeof(float));
		exit(EXIT_FAILURE);
	}
	if (buf->data != NULL) {
		if (copy_old_and_zero_excess != 0 && buf->length > 0)
			memcpy(new_data, buf->data, buf->length*sizeof(float));
		free(buf->data);
	}
	if (copy_old_and_zero_excess != 0 && new_length > buf->length)
		memset(new_data + buf->length, 0, (new_length - buf->length)*sizeof(float));
	buf->data = new_data;
	buf->capacity = new_capacity;
	buf->length = new_length;
}


void pdlc_buffer_set(pdlc_buffer_t *buf, float val)
{
	size_t i;

	for (i = 0; i < buf->length; i++)
		buf->data[i] = val;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_new(size_t length)
{
	pdlc_complex_buffer_t *res;

	res = malloc(sizeof(pdlc_complex_buffer_t));
	res->capacity = 0;
	res->length = 0;
	res->data = NULL;

	pdlc_complex_buffer_resize(res, length, 1);

	return res;
}


void pdlc_complex_buffer_free(pdlc_complex_buffer_t *buf)
{
	if (buf == NULL)
		return; 
	if (buf->data)
		free(buf->data);
	free(buf);
}


pdlc_complex_buffer_t* pdlc_complex_buffer_copy(const pdlc_complex_buffer_t *orig)
{
	pdlc_complex_buffer_t *res = pdlc_complex_buffer_new(orig->length);
	memcpy(res->data, orig->data, orig->length*sizeof(pdlc_complex_t));
	return res;
}


void pdlc_complex_buffer_resize(pdlc_complex_buffer_t *buf, size_t new_length, int copy_old_and_zero_excess)
{
	pdlc_complex_t *new_data;
	size_t new_capacity;

	if (new_length <= buf->capacity) {
		if (copy_old_and_zero_excess != 0 && new_length > buf->length)
			memset(buf->data + buf->length, 0, (new_length - buf->length)*sizeof(pdlc_complex_t));
		buf->length = new_length;
		return;
	}

	new_capacity = new_length + (new_length >> 2);
	new_data = malloc(new_capacity*sizeof(pdlc_complex_t));
	if (new_data == NULL) {
		fprintf(stderr, "ERROR: libpaddlec: Cannot allocate %lu bytes of buffer!\n", new_capacity*sizeof(pdlc_complex_t));
		exit(EXIT_FAILURE);
	}
	if (buf->data != NULL) {
		if (copy_old_and_zero_excess != 0 && buf->length > 0)
			memcpy(new_data, buf->data, buf->length*sizeof(pdlc_complex_t));
		free(buf->data);
	}
	if (copy_old_and_zero_excess != 0 && new_length > buf->length)
		memset(new_data + buf->length, 0, (new_length - buf->length)*sizeof(pdlc_complex_t));
	buf->data = new_data;
	buf->capacity = new_capacity;
	buf->length = new_length;
}


void pdlc_complex_buffer_set(pdlc_complex_buffer_t *buf, pdlc_complex_t val)
{
	size_t i;

	for (i = 0; i < buf->length; i++)
		buf->data[i] = val;
}


pdlc_buffer_t* pdlc_buffer_reverse(const pdlc_buffer_t *fbuf, pdlc_buffer_t *res)
{
	const size_t len = fbuf->length;
	size_t i;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = fbuf->data[len - 1 - i];

	return res;
}


pdlc_buffer_t* pdlc_buffer_reverse_inplace(pdlc_buffer_t *fbuf)
{
	const size_t lenmone = fbuf->length - 1;
	const size_t half_len = fbuf->length / 2;
	size_t i;
	float tmp;

	for (i = 0; i < half_len; i++) {
		tmp = fbuf->data[lenmone - i];
		fbuf->data[lenmone - i] = fbuf->data[i];
		fbuf->data[i] = tmp;
	}

	return fbuf;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_reverse(const pdlc_complex_buffer_t *cbuf, pdlc_complex_buffer_t *res)
{
	const size_t len = cbuf->length;
	size_t i;

	if (!res)
		res = pdlc_complex_buffer_new(len);
	else
		pdlc_complex_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = cbuf->data[len - 1 - i];

	return res;
}


pdlc_complex_buffer_t* pdlc_complex_buffer_reverse_inplace(pdlc_complex_buffer_t *cbuf)
{
	const size_t lenmone = cbuf->length - 1;
	const size_t half_len = cbuf->length / 2;
	size_t i;
	pdlc_complex_t tmp;

	for (i = 0; i < half_len; i++) {
		tmp = cbuf->data[lenmone - i];
		cbuf->data[lenmone - i] = cbuf->data[i];
		cbuf->data[i] = tmp;
	}

	return cbuf;
}


float pdlc_buffer_sum(const pdlc_buffer_t *fbuf)
{
	const size_t len = fbuf->length;
	size_t alen = len;
	size_t i, hlen;
	float *copy;
	float res = 0.0f;

	if (len < 1)
		return 0.0f;

	copy = malloc(len*sizeof(float));
	memcpy(copy, fbuf->data, len*sizeof(float));

	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}

	res = copy[0];

	free(copy);

	return res;
}


pdlc_complex_t pdlc_complex_buffer_sum(const pdlc_complex_buffer_t *cbuf)
{
	const size_t len = cbuf->length;
	size_t alen, hlen, i;
	float *copy;
	pdlc_complex_t res = {0.0f, 0.0f};

	if (len < 1)
		return res;

	copy = malloc(len*sizeof(float));

	alen = len;
	for (i = 0; i < len; i++)
		copy[i] = cbuf->data[i].real;
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.real = copy[0];

	alen = len;
	for (i = 0; i < len; i++)
		copy[i] = cbuf->data[i].imag;
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.imag = copy[0];

	free(copy);

	return res;
}


float pdlc_buffer_mean(const pdlc_buffer_t *fbuf)
{
	const float sum = pdlc_buffer_sum(fbuf);
	return sum / (float)fbuf->length;
}


pdlc_complex_t pdlc_complex_buffer_mean(const pdlc_complex_buffer_t *cbuf)
{
	pdlc_complex_t res = pdlc_complex_buffer_sum(cbuf);
	res.real = res.real / (float)cbuf->length;
	res.imag = res.imag / (float)cbuf->length;
	return res;
}


float pdlc_buffer_variance(const pdlc_buffer_t *fbuf)
{
	const size_t len = fbuf->length;
	size_t i, hlen, alen;
	float *copy;
	float res = 0.0f;

	if (len < 1)
		return 0.0f;

	copy = malloc(len*sizeof(float));

	/* Compute mean */
	memcpy(copy, fbuf->data, len*sizeof(float));
	alen = len;
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res = copy[0] / (float)len;

	/* Compute variance */
	for (i = 0; i < len; i++)
		copy[i] = fbuf->data[i] * fbuf->data[i];
	alen = len;
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res = copy[0] / (float)len - res*res;

	free(copy);

	return res;
}


float pdlc_buffer_standard_deviation(const pdlc_buffer_t *fbuf)
{
	return sqrtf(pdlc_buffer_variance(fbuf));
}


float pdlc_buffer_prod(const pdlc_buffer_t *fbuf)
{
	const size_t len = fbuf->length;
	size_t i;
	float res = 1.0f;

	for (i = 0; i < len; i++)
		res *= fbuf->data[i];

	return res;
}


pdlc_complex_t pdlc_complex_buffer_prod(const pdlc_complex_buffer_t *cbuf)
{
	const size_t len = cbuf->length;
	size_t i;
	pdlc_complex_t tmp;
	pdlc_complex_t res = {1.0f, 0.0f};

	for (i = 0; i < len; i++) {
		tmp.real = res.real * cbuf->data[i].real - res.imag * cbuf->data[i].imag;
		tmp.imag = res.real * cbuf->data[i].imag + res.imag * cbuf->data[i].real;
		res = tmp;
	}

	return res;
}


pdlc_complex_t pdlc_complex_buffer_eprod(const pdlc_complex_buffer_t *cbuf)
{
	const size_t len = cbuf->length;
	size_t i;
	pdlc_complex_t res = {1.0f, 1.0f};

	for (i = 0; i < len; i++) {
		res.real *= cbuf->data[i].real;
		res.imag *= cbuf->data[i].imag;
	}

	return res;
}


float pdlc_buffer_fb_fs_sad(const pdlc_buffer_t *fbuf, float other)
{
	const size_t len = fbuf->length;
	size_t alen = len;
	size_t i, hlen;
	float *copy;
	float res = 0.0f;

	if (len < 1)
		return 0.0f;

	copy = malloc(len*sizeof(float));
	for (i = 0; i < len; i++)
		copy[i] = fabsf(fbuf->data[i] - other);

	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}

	res = copy[0];

	free(copy);

	return res;
}


float pdlc_buffer_fb_fb_sad(const pdlc_buffer_t *fbuf, const pdlc_buffer_t *other)
{
	size_t len = fbuf->length;
	size_t alen = len;
	size_t i, hlen;
	float *copy;
	float res = 0.0f;

	if (len > other->length)
		len = other->length;

	if (len < 1)
		return 0.0f;

	copy = malloc(len*sizeof(float));
	for (i = 0; i < len; i++)
		copy[i] = fabsf(fbuf->data[i] - other->data[i]);

	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}

	res = copy[0];

	free(copy);

	return res;
}


pdlc_complex_t pdlc_complex_buffer_cb_cs_sad(const pdlc_complex_buffer_t *cbuf, pdlc_complex_t other)
{
	const size_t len = cbuf->length;
	size_t alen, hlen, i;
	float *copy;
	pdlc_complex_t res = {0.0f, 0.0f};

	if (len < 1)
		return res;

	copy = malloc(len*sizeof(float));

	alen = len;
	for (i = 0; i < len; i++) 
		copy[i] = fabsf(cbuf->data[i].real - other.real);
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.real = copy[0];

	alen = len;
	for (i = 0; i < len; i++)
		copy[i] = fabsf(cbuf->data[i].imag - other.imag);
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.imag = copy[0];

	free(copy);

	return res;
}


pdlc_complex_t pdlc_complex_buffer_cb_cb_sad(const pdlc_complex_buffer_t *cbuf, const pdlc_complex_buffer_t *other)
{
	size_t len = cbuf->length;
	size_t alen, hlen, i;
	float *copy;
	pdlc_complex_t res = {0.0f, 0.0f};

	if (len > other->length)
		len = other->length;

	if (len < 1)
		return res;

	copy = malloc(len*sizeof(float));

	alen = len;
	for (i = 0; i < len; i++) 
		copy[i] = fabsf(cbuf->data[i].real - other->data[i].real);
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.real = copy[0];

	alen = len;
	for (i = 0; i < len; i++)
		copy[i] = fabsf(cbuf->data[i].imag - other->data[i].imag);
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.imag = copy[0];

	free(copy);

	return res;
}


float pdlc_buffer_fb_fs_ssd(const pdlc_buffer_t *fbuf, float other)
{
	const size_t len = fbuf->length;
	size_t alen = len;
	size_t i, hlen;
	float *copy;
	float res = 0.0f;

	if (len < 1)
		return 0.0f;

	copy = malloc(len*sizeof(float));
	for (i = 0; i < len; i++)
		copy[i] = (fbuf->data[i] - other)*(fbuf->data[i] - other);

	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}

	res = copy[0];

	free(copy);

	return res;
}


float pdlc_buffer_fb_fb_ssd(const pdlc_buffer_t *fbuf, const pdlc_buffer_t *other)
{
	size_t len = fbuf->length;
	size_t alen = len;
	size_t i, hlen;
	float *copy;
	float res = 0.0f;

	if (len > other->length)
		len = other->length;

	if (len < 1)
		return 0.0f;

	copy = malloc(len*sizeof(float));
	for (i = 0; i < len; i++)
		copy[i] = (fbuf->data[i] - other->data[i])*(fbuf->data[i] - other->data[i]);

	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}

	res = copy[0];

	free(copy);

	return res;
}


pdlc_complex_t pdlc_complex_buffer_cb_cs_essd(const pdlc_complex_buffer_t *cbuf, pdlc_complex_t other)
{
	const size_t len = cbuf->length;
	size_t alen, hlen, i;
	float *copy;
	pdlc_complex_t res = {0.0f, 0.0f};

	if (len < 1)
		return res;

	copy = malloc(len*sizeof(float));

	alen = len;
	for (i = 0; i < len; i++) 
		copy[i] = (cbuf->data[i].real - other.real)*(cbuf->data[i].real - other.real);
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.real = copy[0];

	alen = len;
	for (i = 0; i < len; i++)
		copy[i] = (cbuf->data[i].imag - other.imag)*(cbuf->data[i].imag - other.imag);
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.imag = copy[0];

	free(copy);

	return res;
}


pdlc_complex_t pdlc_complex_buffer_cb_cb_essd(const pdlc_complex_buffer_t *cbuf, const pdlc_complex_buffer_t *other)
{
	size_t len = cbuf->length;
	size_t alen, hlen, i;
	float *copy;
	pdlc_complex_t res = {0.0f, 0.0f};

	if (len > other->length)
		len = other->length;

	if (len < 1)
		return res;

	copy = malloc(len*sizeof(float));

	alen = len;
	for (i = 0; i < len; i++) 
		copy[i] = (cbuf->data[i].real - other->data[i].real)*(cbuf->data[i].real - other->data[i].real);
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.real = copy[0];

	alen = len;
	for (i = 0; i < len; i++)
		copy[i] = (cbuf->data[i].imag - other->data[i].imag)*(cbuf->data[i].imag - other->data[i].imag);
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.imag = copy[0];

	free(copy);

	return res;
}


pdlc_complex_t pdlc_complex_buffer_cb_cs_ssd(const pdlc_complex_buffer_t *cbuf, pdlc_complex_t other)
{
	const size_t len = cbuf->length;
	size_t alen, hlen, i;
	float *copy;
	pdlc_complex_t tmp;
	pdlc_complex_t res = {0.0f, 0.0f};

	if (len < 1)
		return res;

	copy = malloc(len*sizeof(float));

	alen = len;
	for (i = 0; i < len; i++) {
		tmp.real = cbuf->data[i].real - other.real;
		tmp.imag = cbuf->data[i].imag - other.imag;
		copy[i] = tmp.real * tmp.real - tmp.imag * tmp.imag;
	}
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.real = copy[0];

	alen = len;
	for (i = 0; i < len; i++) {
		tmp.real = cbuf->data[i].real - other.real;
		tmp.imag = cbuf->data[i].imag - other.imag;
		copy[i] = 2.0f * tmp.real * tmp.imag;
	}
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.imag = copy[0];

	free(copy);

	return res;
}


pdlc_complex_t pdlc_complex_buffer_cb_cb_ssd(const pdlc_complex_buffer_t *cbuf, const pdlc_complex_buffer_t *other)
{
	size_t len = cbuf->length;
	size_t alen, hlen, i;
	float *copy;
	pdlc_complex_t tmp;
	pdlc_complex_t res = {0.0f, 0.0f};

	if (len > other->length)
		len = other->length;

	if (len < 1)
		return res;

	copy = malloc(len*sizeof(float));

	alen = len;
	for (i = 0; i < len; i++) {
		tmp.real = cbuf->data[i].real - other->data[i].real;
		tmp.imag = cbuf->data[i].imag - other->data[i].imag;
		copy[i] = tmp.real * tmp.real - tmp.imag * tmp.imag;
	}
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.real = copy[0];

	alen = len;
	for (i = 0; i < len; i++)
	for (i = 0; i < len; i++) {
		tmp.real = cbuf->data[i].real - other->data[i].real;
		tmp.imag = cbuf->data[i].imag - other->data[i].imag;
		copy[i] = 2.0f * tmp.real * tmp.imag;
	}
	while (alen > 1) {
		hlen = alen / 2;
		for (i = 0; i < hlen; i++)
			copy[i] = copy[2*i] + copy[2*i+1];
		if (alen % 2) {
			copy[i] = copy[2*i];
			alen = hlen + 1;
		}
		else
			alen = hlen;
	}
	res.imag = copy[0];

	free(copy);

	return res;
}


