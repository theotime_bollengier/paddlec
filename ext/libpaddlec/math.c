/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include "libpaddlec.h"


pdlc_buffer_t* pdlc_buffer_acos(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = acosf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_acos_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = acosf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_acosh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = acoshf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_acosh_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = acoshf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_asin(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = asinf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_asin_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = asinf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_asinh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = asinhf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_asinh_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = asinhf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_atan(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = atanf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_atan_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = atanf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_atanh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = atanhf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_atanh_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = atanhf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_cbrt(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = cbrtf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_cbrt_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = cbrtf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_cos(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = cosf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_cos_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = cosf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_cosh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = coshf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_cosh_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = coshf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_erf(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = erff(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_erf_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = erff(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_erfc(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = erfcf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_erfc_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = erfcf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_exp(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = expf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_exp_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = expf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_log(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = logf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_log_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = logf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_log10(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = log10f(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_log10_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = log10f(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_log2(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = log2f(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_log2_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = log2f(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_sin(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = sinf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_sin_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = sinf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_sinh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = sinhf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_sinh_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = sinhf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_sqrt(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = sqrtf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_sqrt_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = sqrtf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_tan(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = tanf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_tan_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = tanf(fbuf->data[i]);

	return fbuf;
}


pdlc_buffer_t* pdlc_buffer_tanh(const pdlc_buffer_t *fbuf, pdlc_buffer_t *ofbuf)
{
	size_t i;

	if (!ofbuf)
		ofbuf = pdlc_buffer_new(fbuf->length);
	else
		pdlc_buffer_resize(ofbuf, fbuf->length, 0);

	for (i = 0; i < fbuf->length; i++)
		ofbuf->data[i] = tanhf(fbuf->data[i]);

	return ofbuf;
}


pdlc_buffer_t* pdlc_buffer_tanh_inplace(pdlc_buffer_t *fbuf)
{
	size_t i;

	for (i = 0; i < fbuf->length; i++)
		fbuf->data[i] = tanhf(fbuf->data[i]);

	return fbuf;
}


