/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of PaddleC
 * 
 * PaddleC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaddleC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaddleC. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include "libpaddlec.h"


pdlc_buffer_t* pdlc_fb_fb_cmpless(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] < rhs->data[i]) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_cmpless_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] < rhs->data[i]) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_cmpless(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] < rhs) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_cmpless_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] < rhs) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_cmplessequ(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] <= rhs->data[i]) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_cmplessequ_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] <= rhs->data[i]) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_cmplessequ(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] <= rhs) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_cmplessequ_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] <= rhs) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_cmpgrt(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] > rhs->data[i]) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_cmpgrt_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] > rhs->data[i]) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_cmpgrt(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] > rhs) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_cmpgrt_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] > rhs) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_cmpgrtequ(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] >= rhs->data[i]) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_cmpgrtequ_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] >= rhs->data[i]) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_cmpgrtequ(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] >= rhs) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_cmpgrtequ_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] >= rhs) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fb_equ(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] == rhs->data[i]) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_equ_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] == rhs->data[i]) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_equ(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] == rhs) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_equ_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] == rhs) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_cb_cb_equ(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i].real == rhs->data[i].real) && (lhs->data[i].imag == rhs->data[i].imag)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_cb_cs_equ(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i].real == rhs.real) && (lhs->data[i].imag == rhs.imag)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_cb_equ(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i] == rhs->data[i].real) && (rhs->data[i].imag == 0.0f)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_cb_equ_inplace(pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = ((lhs->data[i] == rhs->data[i].real) && (rhs->data[i].imag == 0.0f)) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_cs_equ(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i] == rhs.real) && (rhs.imag == 0.0f)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_cs_equ_inplace(pdlc_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = ((lhs->data[i] == rhs.real) && (rhs.imag == 0.0f)) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_cb_fb_equ(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i].real == rhs->data[i]) && (lhs->data[i].imag == 0.0f)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_cb_fs_equ(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i].real == rhs) && (lhs->data[i].imag == 0.0f)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_different(const pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] != rhs->data[i]) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fb_different_inplace(pdlc_buffer_t *lhs, const pdlc_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] != rhs->data[i]) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_fs_different(const pdlc_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = (lhs->data[i] != rhs) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_fs_different_inplace(pdlc_buffer_t *lhs, float rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = (lhs->data[i] != rhs) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_cb_cb_different(const pdlc_complex_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i].real != rhs->data[i].real) || (lhs->data[i].imag != rhs->data[i].imag)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_cb_cs_different(const pdlc_complex_buffer_t *lhs, pdlc_complex_t rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i].real != rhs.real) || (lhs->data[i].imag != rhs.imag)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_cb_different(const pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i] != rhs->data[i].real) || (rhs->data[i].imag != 0.0f)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_cb_different_inplace(pdlc_buffer_t *lhs, const pdlc_complex_buffer_t *rhs)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = ((lhs->data[i] != rhs->data[i].real) || (rhs->data[i].imag != 0.0f)) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_fb_cs_different(const pdlc_buffer_t *lhs, pdlc_complex_t rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i] != rhs.real) || (rhs.imag != 0.0f)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_fb_cs_different_inplace(pdlc_buffer_t *lhs, pdlc_complex_t rhs)
{
	size_t i;
	const size_t len = lhs->length;

	for (i = 0; i < len; i++)
		lhs->data[i] = ((lhs->data[i] != rhs.real) || (rhs.imag != 0.0f)) ? 1.0f : 0.0f;

	return lhs;
}


pdlc_buffer_t* pdlc_cb_fb_different(const pdlc_complex_buffer_t *lhs, const pdlc_buffer_t *rhs, pdlc_buffer_t *res)
{
	size_t i;
	size_t len = lhs->length;

	if (len > rhs->length)
		len = rhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i].real != rhs->data[i]) && (lhs->data[i].imag != 0.0f)) ? 1.0f : 0.0f;

	return res;
}


pdlc_buffer_t* pdlc_cb_fs_different(const pdlc_complex_buffer_t *lhs, float rhs, pdlc_buffer_t *res)
{
	size_t i;
	const size_t len = lhs->length;

	if (!res)
		res = pdlc_buffer_new(len);
	else
		pdlc_buffer_resize(res, len, 0);

	for (i = 0; i < len; i++)
		res->data[i] = ((lhs->data[i].real != rhs) && (lhs->data[i].imag != 0.0f)) ? 1.0f : 0.0f;

	return res;
}


